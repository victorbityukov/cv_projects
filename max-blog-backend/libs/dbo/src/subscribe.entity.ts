import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { TableNameEnum } from '@app/dbo/enum';
import { BasicEntity, UserEntity } from '@app/dbo';

@Entity({ name: TableNameEnum.SUBSCRIBES })
export class SubscribeEntity extends BasicEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.id)
  @JoinColumn({ name: 'user' })
  @Column({
    name: 'user',
  })
  user: UserEntity;

  @Column({
    name: 'type',
    type: 'varchar',
  })
  type: string;

  @Column({
    name: 'sum',
    type: 'bigint',
  })
  sum: number;
}
