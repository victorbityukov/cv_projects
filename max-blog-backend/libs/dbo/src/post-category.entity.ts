import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { TableNameEnum } from '@app/dbo/enum';
import { PostEntity, BasicEntity } from '@app/dbo';
import { CategoryEntity } from '@app/dbo';

@Entity({ name: TableNameEnum.POST_CATEGORY })
export class PostCategoryEntity extends BasicEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => PostEntity, (post: PostEntity) => post.categories)
  @JoinColumn({ name: 'post' })
  @Column({
    name: 'post',
  })
  post: PostEntity;

  @ManyToOne(() => CategoryEntity, (category: CategoryEntity) => category.id)
  @JoinColumn({ name: 'category' })
  @Column({
    name: 'category',
  })
  category: CategoryEntity;
}
