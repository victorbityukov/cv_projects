import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PostStatusEnum, TableNameEnum } from '@app/dbo/enum';
import { PostCategoryEntity, BasicEntity } from '@app/dbo';

@Entity({ name: TableNameEnum.POSTS })
export class PostEntity extends BasicEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'title',
    type: 'varchar',
  })
  title: string;

  @Column({
    name: 'subtitle',
    type: 'varchar',
  })
  subtitle: string;

  @Column({
    name: 'teaser',
    type: 'varchar',
  })
  teaser: string;

  @Column({
    name: 'content',
    type: 'varchar',
  })
  content: string;

  @Column({
    name: 'readTime',
    type: 'int',
    default: 0,
  })
  readTime: number;

  @Column({
    name: 'subscribe',
    type: 'boolean',
    default: false,
  })
  subscribe: boolean;

  @Column({
    name: 'status',
    type: 'enum',
    default: PostStatusEnum.DRAFT,
    enum: PostStatusEnum,
    nullable: false,
  })
  status: PostStatusEnum;

  @Column({
    name: 'imgLogo',
    type: 'varchar',
    nullable: true,
  })
  imgLogo: string;

  @Column({
    name: 'imgLogoRect',
    type: 'varchar',
    nullable: true,
  })
  imgLogoRect: string;

  @Column({
    name: 'imgHead',
    type: 'varchar',
    nullable: true,
  })
  imgHead: string;

  @OneToMany(
    () => PostCategoryEntity,
    (postCategory: PostCategoryEntity) => postCategory.post,
  )
  categories: PostCategoryEntity[];
}
