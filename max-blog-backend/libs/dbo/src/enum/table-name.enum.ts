export enum TableNameEnum {
  USERS = 'users',
  SUBSCRIBES = 'subscribes',
  POSTS = 'posts',
  POST_CATEGORY = 'post_category',
  CATEGORIES = 'categories',
}
