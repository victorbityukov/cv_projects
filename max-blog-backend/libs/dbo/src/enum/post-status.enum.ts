export enum PostStatusEnum {
  APPROVE = 'approve',
  DRAFT = 'draft',
}
