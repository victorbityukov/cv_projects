export enum UserRoleEnum {
  ADMIN = 'admin',
  MEMBER = 'member',
}
