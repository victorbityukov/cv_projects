import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { TableNameEnum } from '@app/dbo/enum';
import { PostCategoryEntity, BasicEntity } from '@app/dbo';

@Entity({ name: TableNameEnum.CATEGORIES })
export class CategoryEntity extends BasicEntity {
  @OneToMany(
    () => PostCategoryEntity,
    (postCategory: PostCategoryEntity) => postCategory.category,
  )
  @JoinColumn({ name: 'category' })
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'title',
    type: 'varchar',
    unique: true,
  })
  title: string;
}
