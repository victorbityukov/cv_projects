import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { hash } from 'bcrypt';
import { TableNameEnum, UserRoleEnum } from '@app/dbo/enum';
import { BasicEntity } from '@app/dbo';

@Unique(['email'])
@Entity({ name: TableNameEnum.USERS })
export class UserEntity extends BasicEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'email',
    type: 'varchar',
    unique: true,
  })
  email: string;

  @Column({
    name: 'password',
    type: 'varchar',
    select: false,
  })
  password: string;

  @Column({
    name: 'subscriptionRange',
    type: 'timestamp',
    default: new Date(),
  })
  subscriptionRange: string;

  @Column({
    name: 'refreshToken',
    type: 'varchar',
    nullable: true,
  })
  refreshToken: string;

  @Column({
    name: 'role',
    type: 'enum',
    default: UserRoleEnum.MEMBER,
    enum: UserRoleEnum,
    nullable: false,
  })
  role: UserRoleEnum;

  @BeforeUpdate()
  @BeforeInsert()
  async hashPassword() {
    this.password = await hash(this.password, 10);
  }
}
