import { get } from 'env-var';

export class ReadmanServiceConfig {
  public static readonly READMAN_SERVICE_CONTAINER_NAME: string = get(
    'READMAN_SERVICE_CONTAINER_NAME',
  )
    .required()
    .asString();

  public static readonly READMAN_SERVICE_BASE_PORT: number = get(
    'READMAN_SERVICE_BASE_PORT',
  )
    .required()
    .asPortNumber();
}
