import { get } from 'env-var';

export class JwtConfig {
  public static readonly JWT_SECRET: string = get('JWT_SECRET')
    .required()
    .asString();

  public static readonly JWT_EXPIRATION_TIME: string = get(
    'JWT_EXPIRATION_TIME',
  )
    .required()
    .asString();

  public static readonly JWT_REFRESH_SECRET: string = get('JWT_REFRESH_SECRET')
    .required()
    .asString();

  public static readonly JWT_REFRESH_EXPIRATION_TIME: string = get(
    'JWT_REFRESH_EXPIRATION_TIME',
  )
    .required()
    .asString();
}
