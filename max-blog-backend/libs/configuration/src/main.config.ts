import { get } from 'env-var';

export class MainConfig {
  public static readonly URL_CLIENT: string = get('URL_CLIENT')
    .required()
    .asString();
}
