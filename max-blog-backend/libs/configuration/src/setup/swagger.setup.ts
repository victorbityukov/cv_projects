import { DocumentBuilder } from '@nestjs/swagger';

export function setupSwagger(
  title = '',
  description = '',
  version = '1.0.0',
  tag = 'api',
): any {
  return new DocumentBuilder()
    .setTitle(title)
    .setDescription(description)
    .addBearerAuth()
    .setVersion(version)
    .addTag(tag)
    .build();
}
