import { get } from 'env-var';

export class MailConfig {
  public static readonly MAIL_HOST: string = get('MAIL_HOST')
    .required()
    .asString();

  public static readonly MAIL_USER: string = get('MAIL_USER')
    .required()
    .asString();

  public static readonly MAIL_PASSWORD: string = get('MAIL_PASSWORD')
    .required()
    .asString();

  public static readonly MAIL_FROM: string = get('MAIL_FROM')
    .required()
    .asString();

  public static readonly MAIL_TRANSPORT: string = get('MAIL_TRANSPORT')
    .required()
    .asString();
}
