import { readFileSync } from 'fs';
import config from 'config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import {
  PostCategoryEntity,
  PostEntity,
  CategoryEntity,
  SubscribeEntity,
  UserEntity,
} from '@app/dbo';
import { DbConfigInterface } from '@app/configuration/interfaces';

const coreDbConfig = config.get<DbConfigInterface>('db');

export const DB_ENTITIES = [
  UserEntity,
  SubscribeEntity,
  PostEntity,
  CategoryEntity,
  PostCategoryEntity,
];

export const coreDbConfiguration: TypeOrmModuleOptions = {
  host: coreDbConfig.host,
  port: coreDbConfig.port,
  username: coreDbConfig.username,
  password: coreDbConfig.password,
  database: coreDbConfig.database,
  logging: false,
  entities: DB_ENTITIES,
  synchronize: coreDbConfig.synchronize,
  keepConnectionAlive: true,
  ssl: coreDbConfig.sslOn
    ? {
        ca: readFileSync(coreDbConfig.ssl?.ca, 'utf-8'),
        key: coreDbConfig.ssl?.key
          ? readFileSync(coreDbConfig.ssl?.key, 'utf-8')
          : null,
        cert: coreDbConfig.ssl?.cert
          ? readFileSync(coreDbConfig.ssl?.cert, 'utf-8')
          : null,
      }
    : null,
};

Object.assign(coreDbConfiguration, { type: coreDbConfig.type });

export const initialDbConfiguration: TypeOrmModuleOptions = {
  host: coreDbConfig.host,
  port: coreDbConfig.port,
  username: coreDbConfig.username,
  password: coreDbConfig.password,
  database: coreDbConfig.database,
  logging: false,
  entities: DB_ENTITIES,
  synchronize: true,
  keepConnectionAlive: true,
  ssl: coreDbConfig.sslOn
    ? {
        ca: readFileSync(coreDbConfig.ssl?.ca, 'utf-8'),
        key: coreDbConfig.ssl?.key
          ? readFileSync(coreDbConfig.ssl?.key, 'utf-8')
          : null,
        cert: coreDbConfig.ssl?.cert
          ? readFileSync(coreDbConfig.ssl?.cert, 'utf-8')
          : null,
      }
    : null,
};

Object.assign(initialDbConfiguration, { type: coreDbConfig.type });
