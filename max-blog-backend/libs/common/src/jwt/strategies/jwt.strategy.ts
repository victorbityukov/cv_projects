import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { JwtConfig } from '@app/configuration';
import { JWTValidationInterface } from '../interfaces';
import { AuthService } from '../../../../../apps/readman-service/src/auth';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    @Inject(forwardRef(() => AuthService))
    private readonly authAppService: AuthService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: JwtConfig.JWT_SECRET,
    });
  }

  async validate(payload: JWTValidationInterface) {
    return this.authAppService.validate(payload);
  }
}
