import { UserRoleEnum } from '@app/dbo/enum';

export interface JwtPayloadDto {
  email: string;
  role: UserRoleEnum;
}
