import { JWTSignInterface } from './index';

export interface JWTValidationInterface extends JWTSignInterface {
  readonly iat: number;
  readonly exp: number;
}
