import { ApiPropertyOptions } from '@nestjs/swagger';

export const SWAGGER_USER_ID: ApiPropertyOptions = {
  name: 'id',
  description: 'User ID',
  type: Number,
  required: true,
  example: 1,
};

export const SWAGGER_USER_EMAIL: ApiPropertyOptions = {
  name: 'email',
  description: 'User email',
  type: String,
  required: true,
  example: 'test@mail.ru',
};

export const SWAGGER_USER_PASSWORD: ApiPropertyOptions = {
  name: 'password',
  description: 'User password',
  type: String,
  required: true,
  example: 'k*rton!',
};

export const SWAGGER_USER_SUBSCRIPTION_RANGE: ApiPropertyOptions = {
  name: 'subscriptionRange',
  description: 'User subscription range',
  type: String,
  required: true,
  example: '2022-03-06 10:07:24.000',
};

export const SWAGGER_USER_ACCESS_TOKEN: ApiPropertyOptions = {
  name: 'accessToken',
  description: 'User ACCESS TOKEN',
  type: String,
};

export const SWAGGER_USER_REFRESH_TOKEN: ApiPropertyOptions = {
  name: 'refreshToken',
  description: 'User REFRESH TOKEN',
  type: String,
};

export const SWAGGER_USER_ROLE: ApiPropertyOptions = {
  name: 'role',
  description: 'Role User',
  type: String,
  required: true,
  example: 'member',
};
