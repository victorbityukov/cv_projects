import { ApiPropertyOptions } from '@nestjs/swagger';
import { CategoryDto } from '@app/common/dto';

export const SWAGGER_POST_ID: ApiPropertyOptions = {
  name: 'id',
  description: 'Post ID',
  type: Number,
  required: true,
  example: 1,
};

export const SWAGGER_POST_CREATED_AT: ApiPropertyOptions = {
  name: 'createdAt',
  description: 'Post Created At',
  type: Date,
  required: false,
  example: '2022-03-29 09:59:55.357',
};

export const SWAGGER_POST_TITLE: ApiPropertyOptions = {
  name: 'title',
  description: 'Post Title',
  type: String,
  required: true,
  example: 'Default Title',
};

export const SWAGGER_POST_SUBTITLE: ApiPropertyOptions = {
  name: 'subtitle',
  description: 'Post Subtitle',
  type: String,
  required: true,
  example: 'Default Subtitle',
};

export const SWAGGER_POST_TEASER: ApiPropertyOptions = {
  name: 'teaser',
  description: 'Post Teaser',
  type: String,
  required: true,
  example:
    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem eum, fugiat obcaecati reprehenderit sapiente velit.',
};

export const SWAGGER_POST_CONTENT: ApiPropertyOptions = {
  name: 'content',
  description: 'Post Content',
  type: String,
  required: true,
  example:
    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad aliquid asperiores assumenda at blanditiis commodi corporis cupiditate dicta doloribus eos et eum, ex explicabo facilis in iure laudantium magni maiores nam natus nobis porro quisquam quod quos recusandae saepe sapiente sequi sit suscipit totam ullam veritatis vero voluptatem voluptatibus!',
};

export const SWAGGER_POST_SUBSCRIBE: ApiPropertyOptions = {
  name: 'subscribe',
  description: 'Subscribe Post or free',
  type: Boolean,
  required: false,
  default: false,
};

export const SWAGGER_POST_CATEGORIES: ApiPropertyOptions = {
  name: 'categories',
  description: 'Post Categories',
  type: [Number],
  required: false,
  default: [],
};

export const SWAGGER_POST_CATEGORIES_FULL: ApiPropertyOptions = {
  name: 'categories',
  description: 'Post Categories',
  type: [CategoryDto],
  required: false,
  default: [],
  isArray: true,
};

export const SWAGGER_POST_READ_TIME: ApiPropertyOptions = {
  name: 'readTime',
  description: 'Post Read Time (in minutes)',
  type: Number,
  required: true,
  example: 15,
};

export const SWAGGER_POST_IMAGE_LOGO: ApiPropertyOptions = {
  name: 'imgLogo',
  description: 'Post Logo Url',
  type: String,
  required: true,
  example: 'https://via.placeholder.com/200',
};

export const SWAGGER_POST_IMAGE_LOGO_RECT: ApiPropertyOptions = {
  name: 'imgLogoRect',
  description: 'Post Logo Url',
  type: String,
  required: true,
  example: 'https://via.placeholder.com/350x200',
};

export const SWAGGER_POST_IMAGE_HEAD: ApiPropertyOptions = {
  name: 'imgHead',
  description: 'Post Head Url',
  type: String,
  required: true,
  example: 'https://via.placeholder.com/400',
};

export const SWAGGER_POST_STATUS: ApiPropertyOptions = {
  name: 'status',
  description: 'Post Status Publication',
  type: String,
  required: true,
  example: 'draft',
};
