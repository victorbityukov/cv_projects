import { ApiPropertyOptions } from '@nestjs/swagger';

export const SWAGGER_CATEGORY_ID: ApiPropertyOptions = {
  name: 'id',
  description: 'Category ID',
  type: Number,
  required: true,
  example: 1,
};

export const SWAGGER_CATEGORY_TITLE: ApiPropertyOptions = {
  name: 'title',
  description: 'Category Title',
  type: String,
  required: true,
  example: 'Society',
};
