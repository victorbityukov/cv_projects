export const calcReadTimeByText = (content: string): number => {
  const wordsPerMinute = 150;
  const countSpaces = content.split(' ').length;
  return Math.ceil(countSpaces / wordsPerMinute);
};

export const setLimitWords = (text: string, amount = 280): string => {
  const arrWords = text.split(' ');
  arrWords.length = amount;
  return arrWords.join(' ');
};
