import { join } from 'path';
import { Global, Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { MailService } from '@app/common/mail/mail.service';
import { MailConfig } from '@app/configuration';

@Global()
@Module({
  imports: [
    MailerModule.forRootAsync({
      useFactory: async () => ({
        transport: {
          host: MailConfig.MAIL_HOST,
          secure: false,
          port: 587,
          auth: {
            user: MailConfig.MAIL_USER,
            pass: MailConfig.MAIL_PASSWORD,
          },
        },
        defaults: {
          from: `"No Reply" <${MailConfig.MAIL_FROM}>`,
        },
        template: {
          dir: join(__dirname, 'templates'),
          adapter: new HandlebarsAdapter(),
          options: {
            strict: true,
          },
        },
      }),
    }),
  ],
  providers: [MailService],
  exports: [MailService],
})
export class MailModule {}
