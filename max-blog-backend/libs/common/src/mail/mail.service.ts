import { join } from 'path';
import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { MainConfig } from '@app/configuration';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendUserSignUp(email: string, pass: string, days: number) {
    await this.mailerService.sendMail({
      to: email,
      // from: '"Support Team" <support@example.com>', // override default from
      subject: 'Это Ример. Данные для входа на сайт лежат в этом письме 😏',
      template: join(__dirname, 'sign-up'),
      context: {
        urlLogin: `${MainConfig.URL_CLIENT}/login`,
        days,
        pass,
      },
    });
  }

  async sendUserExtend(email: string, pass: string, days: number) {
    await this.mailerService.sendMail({
      to: email,
      // from: '"Support Team" <support@example.com>', // override default from
      subject: `Это Ример. Подписка успешно продлена на ${days} дней. Актуальные данные внутри.`,
      template: join(__dirname, 'extend'),
      context: {
        email,
        days,
        pass,
      },
    });
  }
}
