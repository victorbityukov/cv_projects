import { IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_CATEGORY_ID, SWAGGER_CATEGORY_TITLE } from '@app/common';

export class GetCategoriesItemResponseDto {
  @ApiProperty(SWAGGER_CATEGORY_ID)
  @IsNumber()
  readonly id: number;

  @ApiProperty(SWAGGER_CATEGORY_TITLE)
  @IsString()
  readonly title: string;
}
