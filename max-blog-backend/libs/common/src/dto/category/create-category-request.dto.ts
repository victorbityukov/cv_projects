import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_CATEGORY_TITLE } from '@app/common/swagger';

export class CreateCategoryRequestDto {
  @ApiProperty(SWAGGER_CATEGORY_TITLE)
  @IsString()
  readonly title: string;
}
