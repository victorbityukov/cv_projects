import { IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_USER_EMAIL, SWAGGER_USER_PASSWORD } from '@app/common';

export class AuthUserLoginRequestDto {
  @ApiProperty(SWAGGER_USER_EMAIL)
  @IsEmail()
  readonly email: string;

  @ApiProperty(SWAGGER_USER_PASSWORD)
  @IsString()
  readonly password: string;
}
