import { IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_USER_EMAIL, SWAGGER_USER_REFRESH_TOKEN } from '@app/common';

export class AuthRefreshRequestDto {
  @ApiProperty(SWAGGER_USER_EMAIL)
  @IsEmail()
  readonly email: string;

  @ApiProperty(SWAGGER_USER_REFRESH_TOKEN)
  @IsString()
  readonly refreshToken: string;
}
