import { IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_USER_ACCESS_TOKEN,
  SWAGGER_USER_EMAIL,
  SWAGGER_USER_REFRESH_TOKEN,
  SWAGGER_USER_ROLE,
  SWAGGER_USER_SUBSCRIPTION_RANGE,
} from '@app/common/swagger';

export class AuthUserLoginResponseDto {
  @ApiProperty(SWAGGER_USER_EMAIL)
  @IsEmail()
  readonly email: string;

  @ApiProperty(SWAGGER_USER_SUBSCRIPTION_RANGE)
  @IsString()
  readonly subscriptionRange: string;

  @ApiProperty(SWAGGER_USER_ACCESS_TOKEN)
  @IsString()
  readonly accessToken: string;

  @ApiProperty(SWAGGER_USER_REFRESH_TOKEN)
  @IsString()
  readonly refreshToken: string;

  @ApiProperty(SWAGGER_USER_ROLE)
  @IsString()
  readonly role: string;
}
