import { ApiProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';
import { SWAGGER_USER_EMAIL } from '@app/common/swagger';

export class AuthUserCreateRequest {
  @ApiProperty(SWAGGER_USER_EMAIL)
  @IsEmail()
  readonly email: string;
}
