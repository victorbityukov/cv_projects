import { IsBoolean, IsDate, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_CATEGORY_ID,
  SWAGGER_CATEGORY_TITLE,
  SWAGGER_POST_CATEGORIES_FULL,
  SWAGGER_POST_CONTENT,
  SWAGGER_POST_CREATED_AT,
  SWAGGER_POST_ID,
  SWAGGER_POST_SUBSCRIBE,
  SWAGGER_POST_TEASER,
  SWAGGER_POST_TITLE,
} from '@app/common';

export class GetPostResponseDto {
  @ApiProperty(SWAGGER_POST_ID)
  @IsNumber()
  readonly id: number;

  @ApiProperty(SWAGGER_POST_TITLE)
  @IsString()
  readonly title: string;

  @ApiProperty(SWAGGER_POST_TEASER)
  @IsString()
  readonly teaser: string;

  @ApiProperty(SWAGGER_POST_CONTENT)
  @IsString()
  readonly content: string;

  @ApiProperty(SWAGGER_POST_SUBSCRIBE)
  @IsBoolean()
  readonly subscribe: boolean;

  @ApiProperty(SWAGGER_POST_CATEGORIES_FULL)
  readonly categories: CategoryDto[];

  @ApiProperty(SWAGGER_POST_CREATED_AT)
  @IsDate()
  readonly createdAt?: Date;
}

export class CategoryDto {
  @ApiProperty(SWAGGER_CATEGORY_ID)
  @IsNumber()
  readonly id: number;

  @ApiProperty(SWAGGER_CATEGORY_TITLE)
  @IsString()
  readonly title: string;
}
