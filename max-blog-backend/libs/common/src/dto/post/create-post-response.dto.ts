import { IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_POST_ID } from '@app/common/swagger';

export class CreatePostResponseDto {
  @ApiProperty(SWAGGER_POST_ID)
  @IsNumber()
  readonly id: number;
}
