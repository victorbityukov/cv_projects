import { IsArray, IsBoolean, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_POST_CATEGORIES,
  SWAGGER_POST_CONTENT,
  SWAGGER_POST_CREATED_AT,
  SWAGGER_POST_IMAGE_HEAD,
  SWAGGER_POST_IMAGE_LOGO,
  SWAGGER_POST_IMAGE_LOGO_RECT,
  SWAGGER_POST_READ_TIME,
  SWAGGER_POST_STATUS,
  SWAGGER_POST_SUBSCRIBE,
  SWAGGER_POST_SUBTITLE,
  SWAGGER_POST_TEASER,
  SWAGGER_POST_TITLE,
} from '@app/common/swagger';
import { PostStatusEnum } from '@app/dbo/enum';

export class CreatePostRequestDto {
  @ApiProperty(SWAGGER_POST_TITLE)
  @IsString()
  readonly title: string;

  @ApiProperty(SWAGGER_POST_SUBTITLE)
  @IsString()
  readonly subtitle: string;

  @ApiProperty(SWAGGER_POST_TEASER)
  @IsString()
  readonly teaser: string;

  @ApiProperty(SWAGGER_POST_CONTENT)
  @IsString()
  readonly content: string;

  @ApiProperty(SWAGGER_POST_SUBSCRIBE)
  @IsBoolean()
  readonly subscribe: boolean;

  @ApiProperty(SWAGGER_POST_CATEGORIES)
  @IsArray()
  readonly categories: number[];

  @ApiProperty(SWAGGER_POST_READ_TIME)
  @IsNumber()
  readonly readTime: number;

  @ApiProperty(SWAGGER_POST_IMAGE_LOGO)
  @IsString()
  readonly imgLogo: string;

  @ApiProperty(SWAGGER_POST_IMAGE_LOGO_RECT)
  @IsString()
  readonly imgLogoRect: string;

  @ApiProperty(SWAGGER_POST_IMAGE_HEAD)
  @IsString()
  readonly imgHead: string;

  @ApiProperty(SWAGGER_POST_CREATED_AT)
  @IsString()
  readonly createdAt: string;

  @ApiProperty(SWAGGER_POST_STATUS)
  @IsString()
  readonly status: PostStatusEnum;
}
