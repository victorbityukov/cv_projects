import {
  IsBoolean,
  IsDate,
  IsEnum,
  IsNumber,
  IsString,
  IsUrl,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  CategoryDto,
  SWAGGER_POST_CATEGORIES_FULL,
  SWAGGER_POST_CREATED_AT,
  SWAGGER_POST_ID,
  SWAGGER_POST_IMAGE_HEAD,
  SWAGGER_POST_IMAGE_LOGO,
  SWAGGER_POST_IMAGE_LOGO_RECT,
  SWAGGER_POST_READ_TIME,
  SWAGGER_POST_STATUS,
  SWAGGER_POST_SUBSCRIBE,
  SWAGGER_POST_TEASER,
  SWAGGER_POST_TITLE,
} from '@app/common';
import { PostStatusEnum } from '@app/dbo/enum';

export class GetPostsItemResponseDto {
  @ApiProperty(SWAGGER_POST_ID)
  @IsNumber()
  readonly id: number;

  @ApiProperty(SWAGGER_POST_TITLE)
  @IsString()
  readonly title: string;

  @ApiProperty(SWAGGER_POST_TEASER)
  @IsString()
  readonly teaser: string;

  @ApiProperty(SWAGGER_POST_SUBSCRIBE)
  @IsBoolean()
  readonly subscribe: boolean;

  @ApiProperty(SWAGGER_POST_READ_TIME)
  @IsBoolean()
  readonly readTime: number;

  @ApiProperty(SWAGGER_POST_STATUS)
  @IsEnum(PostStatusEnum)
  readonly status: string;

  @ApiProperty(SWAGGER_POST_CREATED_AT)
  @IsDate()
  readonly createdAt: Date;

  @ApiProperty(SWAGGER_POST_IMAGE_LOGO)
  @IsUrl()
  readonly imgLogo: string;

  @ApiProperty(SWAGGER_POST_IMAGE_LOGO_RECT)
  @IsUrl()
  readonly imgLogoRect: string;

  @ApiProperty(SWAGGER_POST_IMAGE_HEAD)
  @IsUrl()
  readonly imgHead: string;

  @ApiProperty(SWAGGER_POST_CATEGORIES_FULL)
  readonly categories: CategoryDto[];
}
