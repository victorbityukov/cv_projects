import fs from 'fs';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { nanoid } from 'nanoid';
import moment from 'moment';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  CreatePostRequestDto,
  CreatePostResponseDto,
  GetPostResponseDto,
  GetPostsItemResponseDto,
  UpdatePostRequestDto,
} from '@app/common';
import { PostsService } from './posts.service';

const storage = diskStorage({
  destination: (
    req: Express.Request,
    file: Express.Multer.File,
    cb: (error: Error | null, destination: string) => void,
  ) => {
    const date = moment(new Date()).format('DD-MM-YYYY');
    const path = `./files/upload/${date}`;
    fs.mkdirSync(path, { recursive: true });
    cb(null, path);
  },
  filename: (
    req: Express.Request,
    file: Express.Multer.File,
    cb: (error: Error | null, filename: string) => void,
  ) => {
    cb(null, `${nanoid(16)}${extname(file.originalname)}`);
  },
});

@ApiTags('Posts')
@Controller('posts')
export class PostsController {
  constructor(private readonly appService: PostsService) {}

  @ApiOperation({ description: 'Create post' })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiBearerAuth()
  @ApiSecurity('jwt', ['token'])
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.CREATED)
  @Post()
  async createPost(
    @Req() req,
    @Body() data: CreatePostRequestDto,
  ): Promise<CreatePostResponseDto> {
    const { user } = req;
    return this.appService.createPost(data, user.role);
  }

  @ApiOperation({ description: 'Get post by id' })
  @ApiOkResponse({
    description: 'Return post',
    type: GetPostResponseDto,
  })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Post not found' })
  @HttpCode(HttpStatus.OK)
  @Get(':id')
  async getPost(@Param('id') id: number): Promise<GetPostResponseDto> {
    return this.appService.getPost(id);
  }

  @ApiOperation({ description: 'Get post by id with subscribe' })
  @ApiOkResponse({
    description: 'Return post',
    type: GetPostResponseDto,
  })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Post not found' })
  @ApiBearerAuth()
  @ApiSecurity('jwt', ['token'])
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  @Get('private/:id')
  async getPostWithSubscribe(
    @Req() req,
    @Param('id') id: number,
  ): Promise<GetPostResponseDto> {
    const { user } = req;
    return this.appService.getPostWithSubscribe(
      id,
      user.role,
      user.subscriptionRange,
    );
  }

  @ApiOperation({ description: 'Delete post' })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiBearerAuth()
  @ApiSecurity('jwt', ['token'])
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  @Delete(':id')
  async deletePost(@Req() req, @Param('id') id: number): Promise<void> {
    const { user } = req;
    return this.appService.deletePost(id, user.role);
  }

  @ApiOperation({ description: 'Update post' })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiBearerAuth()
  @ApiSecurity('jwt', ['token'])
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  @Put()
  async updatePost(
    @Req() req,
    @Body() data: UpdatePostRequestDto,
  ): Promise<void> {
    const { user } = req;
    return this.appService.updatePost(data, user.role);
  }

  @ApiOperation({ description: 'Get list of posts' })
  @ApiOkResponse({
    description: 'Return posts',
    type: GetPostsItemResponseDto,
    isArray: true,
  })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @HttpCode(HttpStatus.OK)
  @Get()
  async getPosts(): Promise<GetPostsItemResponseDto[]> {
    return this.appService.getPosts();
  }

  @ApiBearerAuth()
  @ApiSecurity('jwt', ['token'])
  @UseGuards(AuthGuard('jwt'))
  @Post('upload-file')
  @UseInterceptors(
    FileInterceptor('file', {
      fileFilter: (
        req: Express.Request,
        file: Express.Multer.File,
        cb: (error: Error | null, acceptFile: boolean) => void,
      ) => {
        if (file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) {
          cb(null, true);
        } else {
          cb(
            new HttpException(
              `Unsupported file type ${extname(file.originalname)}`,
              HttpStatus.BAD_REQUEST,
            ),
            false,
          );
        }
      },
      storage,
    }),
  )
  uploadFile(@UploadedFile() file: Express.Multer.File): Express.Multer.File {
    return file;
  }
}
