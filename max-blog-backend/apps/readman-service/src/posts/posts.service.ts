import { Repository } from 'typeorm';
import moment from 'moment';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  calcReadTimeByText,
  CreatePostRequestDto,
  CreatePostResponseDto,
  GetPostResponseDto,
  GetPostsItemResponseDto,
  setLimitWords,
  UpdatePostRequestDto,
} from '@app/common';
import { CategoryEntity, PostCategoryEntity, PostEntity } from '@app/dbo';
import { PostStatusEnum, UserRoleEnum } from '@app/dbo/enum';
import { UnauthorizedException } from '@nestjs/common/exceptions/unauthorized.exception';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(PostEntity)
    private readonly postRepository: Repository<PostEntity>,

    @InjectRepository(PostCategoryEntity)
    private readonly postCategoryRepository: Repository<PostCategoryEntity>,

    @InjectRepository(CategoryEntity)
    private readonly categoryRepository: Repository<CategoryEntity>,
  ) {}

  async createPost(
    data: CreatePostRequestDto,
    role: string,
  ): Promise<CreatePostResponseDto> {
    if (role !== UserRoleEnum.ADMIN) {
      throw new UnauthorizedException();
    }
    const { categories, ...postData } = data;
    if (!postData.readTime) {
      postData.readTime = calcReadTimeByText(data.content);
    }

    const postResult = this.postRepository.create(postData);
    await this.postRepository.save(postResult);

    if (categories.length > 0) {
      const categoriesEntities = await this.categoryRepository
        .createQueryBuilder('categories')
        .where('id IN (:...categories)', { categories })
        .getMany();

      const movieCategories = categoriesEntities.map((category) => ({
        post: postResult,
        category,
      }));
      await this.postCategoryRepository.insert(movieCategories);
    }
    return { id: postResult.id };
  }

  async getPost(id: number): Promise<GetPostResponseDto> {
    const post = await this.getPostById(id, false);
    if (!post) throw new NotFoundException(`Post by id ${id} not found`);

    const categories = post.categories.map((item) => {
      return item.category;
    });

    if (post.subscribe) {
      post.content = setLimitWords(post.content, 200);
    }

    return {
      ...post,
      categories,
    };
  }

  async getPostWithSubscribe(
    id: number,
    userRole: UserRoleEnum,
    subscriptionRange: string,
  ): Promise<GetPostResponseDto> {
    let post: PostEntity;
    if (userRole === UserRoleEnum.ADMIN) {
      post = await this.getPostById(id, true);
    } else {
      post = await this.getPostById(id, false);
    }
    if (!post) throw new NotFoundException(`Post by id ${id} not found`);
    const categories = post.categories.map((item) => {
      return item.category;
    });

    if (moment(subscriptionRange).isBefore()) {
      if (post.subscribe) {
        post.content = setLimitWords(post.content, 200);
      }
    }

    return {
      ...post,
      categories,
    };
  }

  async getPosts(): Promise<GetPostsItemResponseDto[]> {
    const posts = await this.postRepository
      .createQueryBuilder('posts')
      .select([
        'posts.id',
        'posts.title',
        'posts.teaser',
        'posts.readTime',
        'posts.subscribe',
        'posts.status',
        'posts.imgLogo',
        'posts.imgLogoRect',
        'posts.createdAt',
      ])
      .orderBy('posts.createdAt', 'DESC')
      .leftJoinAndSelect('posts.categories', 'categories')
      .leftJoinAndSelect('categories.category', 'category')
      .getMany();

    return posts.map((post) => {
      const { createdAt, ...postData } = post;
      return {
        ...postData,
        createdAt,
        categories: post.categories.map((item) => {
          return item.category;
        }),
      };
    });
  }

  async updatePost(data: UpdatePostRequestDto, role: string): Promise<void> {
    if (role !== UserRoleEnum.ADMIN) {
      throw new UnauthorizedException();
    }
    const { categories, ...postData } = data;

    if (!postData.readTime) {
      postData.readTime = calcReadTimeByText(data.content);
    }

    const postResult = await this.postRepository.save(postData);

    await this.postCategoryRepository
      .createQueryBuilder()
      .delete()
      .where('post = :id', { id: postData.id })
      .execute();

    if (categories.length > 0) {
      const categoriesEntities = await this.categoryRepository
        .createQueryBuilder('categories')
        .where('id IN (:...categories)', { categories })
        .getMany();

      const movieCategories = categoriesEntities.map((category) => ({
        post: postResult,
        category,
      }));
      await this.postCategoryRepository.insert(movieCategories);
    }
  }

  async deletePost(id: number, role: string): Promise<void> {
    if (role !== UserRoleEnum.ADMIN) {
      throw new UnauthorizedException();
    }
    await this.postCategoryRepository
      .createQueryBuilder()
      .delete()
      .where('post = :id', { id })
      .execute();

    await this.postRepository
      .createQueryBuilder()
      .delete()
      .where('id = :id', { id })
      .execute();
  }

  private async getPostById(id: number, draft: boolean) {
    const queryPost = this.postRepository
      .createQueryBuilder('posts')
      .addSelect('posts.createdAt')
      .leftJoinAndSelect('posts.categories', 'categories')
      .leftJoinAndSelect('categories.category', 'category')
      .where('posts.id=:id', { id });
    if (!draft) {
      queryPost.andWhere('posts.status<>:status', {
        status: PostStatusEnum.DRAFT,
      });
    }
    return queryPost.getOne();
  }
}
