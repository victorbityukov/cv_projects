import { Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '@app/dbo';
import { UserGetResponse } from '@app/common';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async getUser(email: string): Promise<UserGetResponse> {
    const user = await this.getUserByEmail(email);

    if (!user) {
      throw new NotFoundException('User with similar email already exists');
    }

    const { refreshToken, subscriptionRange, role } = user;

    return {
      refreshToken,
      email,
      subscriptionRange,
      role,
    };
  }

  async getUserByEmail(email: string): Promise<UserEntity> {
    return this.userRepository.findOne({ email });
  }
}
