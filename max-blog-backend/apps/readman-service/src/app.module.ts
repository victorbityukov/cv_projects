import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import {
  coreDbConfiguration,
  DB_ENTITIES,
  JwtConfig,
} from '@app/configuration';
import { JwtStrategy, MailModule } from '@app/common';
import { CategoriesController, CategoriesService } from './categories';
import { PostsController, PostsService } from './posts';
import { AuthController, AuthService } from './auth';
import { UserController, UserService } from './user';

@Module({
  imports: [
    TypeOrmModule.forRoot(coreDbConfiguration),
    TypeOrmModule.forFeature(DB_ENTITIES),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async () => ({
        secret: JwtConfig.JWT_SECRET,
        signOptions: {
          expiresIn: JwtConfig.JWT_EXPIRATION_TIME,
        },
      }),
    }),
    MailModule,
  ],
  controllers: [
    AuthController,
    CategoriesController,
    PostsController,
    UserController,
  ],
  providers: [
    AuthService,
    CategoriesService,
    PostsService,
    UserService,
    JwtStrategy,
  ],
})
export class AppModule {}
