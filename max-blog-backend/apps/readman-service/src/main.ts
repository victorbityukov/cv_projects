import cors from 'cors';
import { NestFactory } from '@nestjs/core';
import { Logger, ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { setupDotEnv, ReadmanServiceConfig } from '@app/configuration';
import { AppModule } from './app.module';

async function bootstrap() {
  setupDotEnv();
  const PORT = ReadmanServiceConfig.READMAN_SERVICE_BASE_PORT;
  const NAME_SERVICE = ReadmanServiceConfig.READMAN_SERVICE_CONTAINER_NAME;
  const app = await NestFactory.create(AppModule);
  app.use(cors());
  app.setGlobalPrefix('api/v1');
  app.useGlobalPipes(new ValidationPipe());

  app.enableCors({
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    allowedHeaders:
      'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Observe',
    credentials: true,
  });

  const options = new DocumentBuilder()
    .setTitle('Readman Service')
    .setDescription('Provides REST API')
    .setVersion('1.0.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/v1/docs', app, document);

  await app.listen(PORT);
  Logger.log(`${NAME_SERVICE} service running PORT: ${PORT}`);
}
bootstrap();
