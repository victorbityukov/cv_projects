import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UnauthorizedException } from '@nestjs/common/exceptions/unauthorized.exception';
import { CategoryEntity, PostCategoryEntity } from '@app/dbo';
import { GetCategoriesItemResponseDto } from '@app/common';
import { UserRoleEnum } from '@app/dbo/enum';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(CategoryEntity)
    private readonly categoryRepository: Repository<CategoryEntity>,
    @InjectRepository(PostCategoryEntity)
    private readonly postCategoryRepository: Repository<PostCategoryEntity>,
  ) {}

  async getCategories(): Promise<GetCategoriesItemResponseDto[]> {
    return this.categoryRepository.find();
  }

  async createCategory(
    title: string,
    role: string,
  ): Promise<GetCategoriesItemResponseDto[]> {
    if (role !== UserRoleEnum.ADMIN) {
      throw new UnauthorizedException();
    }
    const category = await this.categoryRepository.create({ title });
    await this.categoryRepository.save(category);

    return this.categoryRepository.find();
  }

  async removeCategory(
    id: number,
    role: string,
  ): Promise<GetCategoriesItemResponseDto[]> {
    if (role !== UserRoleEnum.ADMIN) {
      throw new UnauthorizedException();
    }
    await this.postCategoryRepository
      .createQueryBuilder()
      .delete()
      .where('category = :id', { id })
      .execute();

    await this.categoryRepository
      .createQueryBuilder()
      .delete()
      .where('id = :id', { id })
      .execute();

    return this.categoryRepository.find();
  }
}
