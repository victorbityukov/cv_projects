import { AuthGuard } from '@nestjs/passport';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import {
  CreateCategoryRequestDto,
  GetCategoriesItemResponseDto,
} from '@app/common';
import { CategoriesService } from './categories.service';

@ApiTags('Categories')
@Controller('categories')
export class CategoriesController {
  constructor(private readonly appService: CategoriesService) {}

  @ApiOperation({ description: 'Get list of categories' })
  @ApiOkResponse({
    description: 'Return categories',
    type: GetCategoriesItemResponseDto,
    isArray: true,
  })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @HttpCode(HttpStatus.OK)
  @Get()
  async getCategories(): Promise<GetCategoriesItemResponseDto[]> {
    return this.appService.getCategories();
  }

  @ApiOperation({ description: 'Create category' })
  @ApiOkResponse({
    description: 'Return categories',
    type: GetCategoriesItemResponseDto,
    isArray: true,
  })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiBearerAuth()
  @ApiSecurity('jwt', ['token'])
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  @Post()
  async createCategory(
    @Req() req,
    @Body() { title }: CreateCategoryRequestDto,
  ): Promise<GetCategoriesItemResponseDto[]> {
    const { user } = req;
    return this.appService.createCategory(title, user.role);
  }

  @ApiOperation({ description: 'Remove category by id' })
  @ApiOkResponse({
    description: 'Return categories',
    type: GetCategoriesItemResponseDto,
    isArray: true,
  })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiBearerAuth()
  @ApiSecurity('jwt', ['token'])
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  @Delete(':id')
  async removeCategory(
    @Req() req,
    @Param('id') id: number,
  ): Promise<GetCategoriesItemResponseDto[]> {
    const { user } = req;
    return this.appService.removeCategory(id, user.role);
  }
}
