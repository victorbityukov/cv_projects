import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiServiceUnavailableResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  AuthRefreshRequestDto,
  AuthUserLoginRequestDto,
  AuthUserLoginResponseDto,
  AuthUserCreateRequest,
} from '@app/common';
import { AuthService } from './auth.service';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authServiceService: AuthService) {}

  @ApiOperation({ description: 'Create/Update user' })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @HttpCode(HttpStatus.OK)
  @Post('sign-up')
  async signUp(@Body() body: AuthUserCreateRequest): Promise<void> {
    return this.authServiceService.signUp(body);
  }

  @ApiOperation({
    description: 'Authorization user by login and password',
  })
  @ApiOkResponse({
    description: 'Successful authorization',
    type: () => AuthUserLoginResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Invalid request body',
  })
  @ApiServiceUnavailableResponse({
    description: 'Resource is unavailable',
  })
  @ApiInternalServerErrorResponse({
    description: 'Something gone wrong, please check log for errors',
  })
  @HttpCode(HttpStatus.OK)
  @Post('login')
  async login(
    @Body() data: AuthUserLoginRequestDto,
  ): Promise<AuthUserLoginResponseDto> {
    return this.authServiceService.login(data);
  }

  @ApiOperation({
    description: 'Use refresh token for get new access token',
  })
  @ApiOkResponse({
    description: 'Successful authorization',
    type: () => AuthUserLoginResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Invalid request body',
  })
  @ApiServiceUnavailableResponse({
    description: 'Resource is unavailable',
  })
  @ApiInternalServerErrorResponse({
    description: 'Something gone wrong, please check log for errors',
  })
  @HttpCode(HttpStatus.OK)
  @Post('refresh')
  async refresh(
    @Body() data: AuthRefreshRequestDto,
  ): Promise<AuthUserLoginResponseDto> {
    const { email, refreshToken } = data;
    return this.authServiceService.refresh(email, refreshToken);
  }
}
