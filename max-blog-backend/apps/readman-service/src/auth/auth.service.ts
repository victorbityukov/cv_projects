import bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { nanoid } from 'nanoid';
import moment from 'moment';
import { InjectRepository } from '@nestjs/typeorm';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import {
  AuthUserLoginRequestDto,
  AuthUserLoginResponseDto,
  AuthUserCreateRequest,
  JwtPayloadDto,
  JWTValidationInterface,
} from '@app/common';
import { MailService } from '@app/common/mail/mail.service';
import { SubscribeEntity, UserEntity } from '@app/dbo';
import { JwtConfig } from '@app/configuration';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(SubscribeEntity)
    private readonly subscribeRepository: Repository<SubscribeEntity>,
    private jwtService: JwtService,
    private mailService: MailService,
  ) {}

  async signUp(data: AuthUserCreateRequest): Promise<void> {
    // TODO: ответ платежки будет прилетать сюда (в зависимости от типа услуги плюсовать разное время)
    const { email } = data;
    const userExist = await this.getUserByEmail(email);
    const password = nanoid(8);
    //TODO: subscribes 30/90/180/365 days
    const subscriptionMonth = 30;
    // const subscriptionYear = 365;
    if (userExist) {
      const user = new UserEntity();
      let subscriptionRange = moment().add(subscriptionMonth, 'day').format();
      const isAfter = moment(moment()).isAfter(userExist.subscriptionRange);
      if (!isAfter) {
        subscriptionRange = moment(userExist.subscriptionRange)
          .add(subscriptionMonth, 'day')
          .format();
      }
      user.subscriptionRange = subscriptionRange;
      await this.userRepository.update({ email }, { subscriptionRange });
      await this.mailService.sendUserExtend(email, password, subscriptionMonth);
    } else {
      const monthSubs = moment().add(subscriptionMonth, 'day').format();
      const newUser = {
        email,
        password,
        subscription_range: monthSubs,
      };
      const userSave = this.userRepository.create(newUser);
      await this.userRepository.save(userSave);
      await this.mailService.sendUserSignUp(email, password, subscriptionMonth);
    }

    const userPay = await this.userRepository.findOne({ email });
    // TODO: доделать когда будет ясно какие данные прилетают с оплаты
    await this.subscribeRepository.insert({
      user: userPay,
      type: 'month',
      sum: 300,
    });
  }

  async login(
    data: AuthUserLoginRequestDto,
  ): Promise<AuthUserLoginResponseDto> {
    const { email, password } = data;
    const user = await this.userRepository.findOne(
      { email },
      { select: ['email', 'password', 'subscriptionRange', 'role'] },
    );
    if (!user) {
      throw new NotFoundException('User with similar login already exists');
    }

    const isCompared: boolean = await bcrypt.compare(password, user.password);

    if (!isCompared) {
      throw new UnauthorizedException('Invalid credentials');
    }

    const accessToken = await this.getAccessToken({
      email: user.email,
      role: user.role,
    });
    const refreshToken = await this.getRefreshToken();

    await this.userRepository.update({ email }, { refreshToken });
    const subscriptionRange = user.subscriptionRange;
    return {
      accessToken,
      refreshToken,
      subscriptionRange,
      email,
      role: user.role,
    };
  }

  async validate(input: JWTValidationInterface): Promise<UserEntity> {
    const { email } = input;
    try {
      return this.userRepository.findOne({ email });
    } catch (e) {
      throw new BadRequestException(e);
    }
  }

  async refresh(email: string, refreshToken: string) {
    const user = await this.getUserByEmail(email);

    if (!user) {
      throw new NotFoundException('User with similar login already exists');
    }
    if (refreshToken !== user.refreshToken) {
      throw new BadRequestException('Refresh token not valid');
    }

    const accessToken = await this.getAccessToken({
      email,
      role: user.role,
    });
    const newRefreshToken = await this.getRefreshToken();

    await this.userRepository.update(
      { email },
      { refreshToken: newRefreshToken },
    );
    const subscriptionRange = user.subscriptionRange;
    return {
      accessToken,
      refreshToken,
      email,
      subscriptionRange,
      role: user.role,
    };
  }

  async getUserByEmail(email: string): Promise<UserEntity> {
    return this.userRepository.findOne({ email });
  }

  async getAccessToken(payload: JwtPayloadDto) {
    return this.jwtService.sign(payload, {
      secret: JwtConfig.JWT_SECRET,
      expiresIn: JwtConfig.JWT_EXPIRATION_TIME,
    });
  }

  async getRefreshToken() {
    return this.jwtService.sign(
      { data: nanoid(16) },
      {
        secret: JwtConfig.JWT_REFRESH_SECRET,
        expiresIn: JwtConfig.JWT_REFRESH_EXPIRATION_TIME,
      },
    );
  }
}
