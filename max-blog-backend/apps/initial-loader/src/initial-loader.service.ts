import { Repository } from 'typeorm';
import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryEntity } from '@app/dbo';

@Injectable()
export class InitialLoaderService implements OnApplicationBootstrap {
  private readonly logger = new Logger(InitialLoaderService.name, {
    timestamp: true,
  });

  constructor(
    @InjectRepository(CategoryEntity)
    private readonly categoryRepository: Repository<CategoryEntity>,
  ) {}

  async onApplicationBootstrap() {
    this.logger.log('InitialLoaderService started');
    await this.loadInitialCategories();
    this.logger.log('InitialLoaderService started');
  }

  private async loadInitialCategories(): Promise<void> {
    this.logger.log('InitialCategories');
    const categories = [
      { title: 'Философия' },
      { title: 'Психология' },
      { title: 'Отношения' },
      { title: 'Продуктивность' },
      { title: 'Общество' },
    ];

    try {
      await this.categoryRepository
        .createQueryBuilder()
        .insert()
        .values(categories)
        .orIgnore(`("id") DO NOTHING`)
        .execute();
    } catch (errorOrException) {
      this.logger.error(errorOrException);
    }
  }
}
