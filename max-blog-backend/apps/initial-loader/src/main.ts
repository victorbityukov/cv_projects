import { NestFactory } from '@nestjs/core';
import { InitialLoaderModule } from './initial-loader.module';
import { setupDotEnv } from '@app/configuration';

async function bootstrap() {
  setupDotEnv();
  const app = await NestFactory.create(InitialLoaderModule);
  await app.init();
  await process.exit();
}
bootstrap();
