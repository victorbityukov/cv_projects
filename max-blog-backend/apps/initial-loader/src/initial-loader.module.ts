import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DB_ENTITIES, initialDbConfiguration } from '@app/configuration';
import { InitialLoaderService } from './initial-loader.service';

@Module({
  imports: [
    TypeOrmModule.forRoot(initialDbConfiguration),
    TypeOrmModule.forFeature(DB_ENTITIES),
  ],
  controllers: [],
  providers: [InitialLoaderService],
})
export class InitialLoaderModule {}
