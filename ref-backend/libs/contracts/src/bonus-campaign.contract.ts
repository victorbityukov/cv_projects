import { Injectable } from '@nestjs/common';
import BigNumber from 'bignumber.js';
import utils from 'web3-utils';

import {
  Web3Contract,
  ContractBaseMethod,
  Web3ProviderService,
  Web3TransactionInterface,
} from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';
import { BaseContractInterface } from '@app/contracts';

interface BonusCampaignMethods {
  rewardsDuration: () => ContractBaseMethod<string>;
  bonusEmission: () => ContractBaseMethod<string>;
  totalSupply: () => ContractBaseMethod<string>;
  startMintTime: () => ContractBaseMethod<string>;
  periodFinish: () => ContractBaseMethod<string>;
  stopRegisterTime: () => ContractBaseMethod<string>;
  startMint: () => Web3TransactionInterface;
}

@Injectable()
export class BonusCampaignContract implements BaseContractInterface<BonusCampaignMethods> {
  contract: Web3Contract<BonusCampaignMethods>;

  constructor(readonly web3Provider: Web3ProviderService) {
    this.init();
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.bonusCampaign;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }

  /**
   * @return BigNumber
   */
  async totalSupply(): Promise<BigNumber> {
    const e = await this.contract.methods.totalSupply().call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return BigNumber
   */
  async bonusEmission(): Promise<BigNumber> {
    const e = await this.contract.methods.bonusEmission().call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return number
   */
  async startMintTime(): Promise<number> {
    const e = await this.contract.methods.startMintTime().call();
    return Number(e);
  }

  /**
   * @return number
   */
  async periodFinish(): Promise<number> {
    const e = await this.contract.methods.periodFinish().call();
    return Number(e);
  }

  /**
   * @return number
   */
  async stopRegisterTime(): Promise<number> {
    const e = await this.contract.methods.stopRegisterTime().call();
    return Number(e);
  }

  /**
   * @return number
   */
  async rewardsDuration(): Promise<number> {
    const e = await this.contract.methods.rewardsDuration().call();
    return Number(e);
  }

  async startMint(): Promise<void> {
    const transaction = this.contract.methods.startMint();
    await this.web3Provider.sendTransactions(transaction, this.contract.options.address);
  }
}
