import { Injectable } from '@nestjs/common';

import {
  ContractBaseMethod,
  Web3Contract,
  Web3ProviderService,
} from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';
import { BaseContractInterface } from '@app/contracts';
import BigNumber from 'bignumber.js';
import utils from 'web3-utils';
import EventEmitter from 'events';

interface VeXbeMethods {
  supply: () => ContractBaseMethod<string>;
  totalSupply: () => ContractBaseMethod<string>;
  lockedSupply: () => ContractBaseMethod<string>;
  lockedAmount: (address: string) => ContractBaseMethod<string>;
}

interface VeXbeEvents {
  ApplyOwnership: () => EventEmitter,
  CommitOwn: () => EventEmitter,
  Deposit: () => EventEmitter,
  Supply: () => EventEmitter,
  Withdraw: () => EventEmitter,
  allEvents: () => EventEmitter,
}

@Injectable()
export class VeXbeContract implements BaseContractInterface<VeXbeMethods> {
  contract: Web3Contract<VeXbeMethods, VeXbeEvents>;

  constructor(readonly web3Provider: Web3ProviderService) {
    this.init();
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.veXbe;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }

  /**
   * @return BigNumber
   */
  async supply(): Promise<BigNumber> {
    const e = await this.contract.methods.supply().call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return BigNumber
   */
  async totalSupply(): Promise<BigNumber> {
    const e = await this.contract.methods.totalSupply().call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return BigNumber
   */
  async lockedSupply(): Promise<BigNumber> {
    const e = await this.contract.methods.lockedSupply().call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return BigNumber
   */
  async lockedAmount(address: string): Promise<BigNumber> {
    const e = await this.contract.methods.lockedAmount(address).call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return EventEmitter
   */
  deposit(): EventEmitter {
    return this.contract.events.Deposit();
  }

  /**
   * @return EventEmitter
   */
  withdraw(): EventEmitter {
    return this.contract.events.Withdraw();
  }
}
