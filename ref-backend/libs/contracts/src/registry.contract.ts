import { Injectable } from '@nestjs/common';

import {
  ContractBaseMethod,
  Web3Contract,
  Web3ProviderService,
} from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';
import { BaseContractInterface } from '@app/contracts';

interface RegistryMethods {
  getVaultsInfo: () => ContractBaseMethod<{ strategyArray: string[] }>;
}

@Injectable()
export class RegistryContract implements BaseContractInterface<RegistryMethods> {
  contract: Web3Contract<RegistryMethods>;

  constructor(readonly web3Provider: Web3ProviderService) {
    this.init();
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.registry;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }

  async getVaultsInfo(): Promise<{ strategyArray: string[] }> {
    return this.contract.methods
      .getVaultsInfo()
      .call();
  }
}
