import {
  ContractBaseMethod,
  Web3Contract,
  Web3ProviderService,
} from '@app/web3-provider';
import { Injectable } from '@nestjs/common';

export interface TokenMethods {
  symbol: () => ContractBaseMethod<string>;
  balanceOf: (address: string) => ContractBaseMethod<string>;
}

@Injectable()
export class BaseTokenContractFactory {
  contracts: [string, Web3Contract<TokenMethods>][] = [];

  constructor(readonly web3Provider: Web3ProviderService) {}

  async init(tokenAddress: string): Promise<Web3Contract<TokenMethods>> {
    let contract = this.getContract(tokenAddress);

    if (contract) {
      return contract;
    }

    contract = await this.web3Provider.getBaseTokenContract(tokenAddress);
    this.contracts[this.contracts.length] = [tokenAddress, contract];
    return contract;
  }

  getContract(tokenAddress: string): Web3Contract<TokenMethods> | null {
    const tokenContractEntity = this.contracts.find(
      (entity) => tokenAddress === entity[0],
    );
    if (tokenContractEntity) {
      return tokenContractEntity[1];
    }
    return null;
  }
}
