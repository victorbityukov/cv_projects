import { Injectable, Logger } from '@nestjs/common';
import { BaseVaultContract } from '@app/contracts';
import { Web3ProviderService } from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';
import { PoolNameEnum } from '@app/constants';

@Injectable()
export class SushiVaultContract
  extends BaseVaultContract {
  constructor(
    readonly web3Provider: Web3ProviderService,
  ) {
    super();

    this.poolName = PoolNameEnum.SUSHI_VAULT;

    this.init().catch((err) => {
      if (err) {
        Logger.error(err);
      }
    });
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.sushiVault;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }
}
