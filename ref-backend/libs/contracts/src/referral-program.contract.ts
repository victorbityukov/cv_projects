import { Injectable } from '@nestjs/common';

import {
  Web3ProviderService,
  ContractBaseMethod,
  Web3Contract,
  Web3TransactionInterface,
} from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';
import { BaseContractInterface } from '@app/contracts/interfaces';
import EventEmitter from 'events';

interface ReferralProgramMethods {
  getDistributionList: () => ContractBaseMethod<number[]>;
  rootAddress: () => ContractBaseMethod<string>;
  registerUser: (addressParent: string, addressUser: string) => Web3TransactionInterface;
  claimRewardsForRoot: () => Web3TransactionInterface;
}

interface ReferralProgramEvents {
  RegisterUser: () => EventEmitter,
  RewardReceived: () => EventEmitter,
  RewardsClaimed: () => EventEmitter,
  allEvents: () => EventEmitter,
}

@Injectable()
export class ReferralProgramContract implements BaseContractInterface<ReferralProgramMethods> {
  contract: Web3Contract<ReferralProgramMethods, ReferralProgramEvents>;

  constructor(readonly web3Provider: Web3ProviderService) {
    this.init();
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.referralProgram;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }

  async createUser(addressParent: string, addressUser: string): Promise<void> {
    const transaction = this.contract.methods.registerUser(
      addressParent,
      addressUser,
    );
    await this.web3Provider.sendTransactions(transaction, this.contract.options.address);
  }

  async claimRewardsForRoot(): Promise<void> {
    const transaction = this.contract.methods.claimRewardsForRoot();
    await this.web3Provider.sendTransactions(transaction, this.contract.options.address);
  }

  async getDistributionList(): Promise<number[]> {
    return this.contract.methods
      .getDistributionList()
      .call();
  }

  /**
   * @return EventEmitter
   */
  registerUser(): EventEmitter {
    return this.contract.events.RegisterUser();
  }

  /**
   * @return EventEmitter
   */
  rewardReceived(): EventEmitter {
    return this.contract.events.RewardReceived();
  }

  /**
   * @return EventEmitter
   */
  rewardsClaimed(): EventEmitter {
    return this.contract.events.RewardsClaimed();
  }

  async rootAddress(): Promise<string> {
    return this.contract.methods
      .rootAddress()
      .call();
  }
}
