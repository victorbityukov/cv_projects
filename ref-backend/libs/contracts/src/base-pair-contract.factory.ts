import {
  ContractBaseMethod,
  Web3Contract,
  Web3ProviderService,
} from '@app/web3-provider';
import { Injectable } from '@nestjs/common';

export interface TokenMethods {
  balanceOf: (address: string) => ContractBaseMethod<string>;
  token0: () => ContractBaseMethod<string>;
  token1: () => ContractBaseMethod<string>;
  totalSupply: () => ContractBaseMethod<string>;
}

@Injectable()
export class BasePairContractFactory {
  contracts: [string, Web3Contract<TokenMethods>][] = [];

  constructor(readonly web3Provider: Web3ProviderService) {}

  async init(pairAddress: string): Promise<Web3Contract<TokenMethods>> {
    let contract = this.getContract(pairAddress);

    if (contract) {
      return contract;
    }

    contract = await this.web3Provider.getBasePairContract(pairAddress);
    this.contracts[this.contracts.length] = [pairAddress, contract];
    return contract;
  }

  getContract(pairAddress: string): Web3Contract<TokenMethods> | null {
    const tokenContractEntity = this.contracts.find(
      (entity) => pairAddress === entity[0],
    );
    if (tokenContractEntity) {
      return tokenContractEntity[1];
    }
    return null;
  }
}
