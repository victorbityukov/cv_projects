import { Injectable } from '@nestjs/common';

import {
  Web3Contract,
  Web3ProviderService, Web3TransactionInterface,
} from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';
import { BaseContractInterface } from '@app/contracts';

interface TreasureMethods {
  toVoters: () => Web3TransactionInterface;
}

@Injectable()
export class TreasureContract implements BaseContractInterface<TreasureMethods> {
  contract: Web3Contract<TreasureMethods>;

  constructor(readonly web3Provider: Web3ProviderService) {
    this.init();
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.treasury;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }

  async toVoters(): Promise<void> {
    const transaction = this.contract.methods.toVoters();
    await this.web3Provider.sendTransactions(transaction, this.contract.options.address);
  }
}
