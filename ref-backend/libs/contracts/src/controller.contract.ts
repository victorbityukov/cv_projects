import { Injectable } from '@nestjs/common';

import {
  Web3Contract,
  Web3ProviderService, Web3TransactionInterface,
} from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';
import { BaseContractInterface } from '@app/contracts';

interface ControllerMethods {
  getRewardStrategy: (strategy: string) => Web3TransactionInterface;
}

@Injectable()
export class ControllerContract implements BaseContractInterface<ControllerMethods> {
  contract: Web3Contract<ControllerMethods>;

  constructor(readonly web3Provider: Web3ProviderService) {
    this.init();
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.controller;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }

  async getRewardStrategy(strategy: string): Promise<void> {
    const transaction = this.contract.methods.getRewardStrategy(strategy);
    await this.web3Provider.sendTransactions(transaction, this.contract.options.address);
  }
}
