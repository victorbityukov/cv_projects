import { Injectable } from '@nestjs/common';

import {
  ContractBaseMethod,
  Web3Contract,
  Web3ProviderService,
  Web3TransactionInterface,
} from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';
import { BaseContractInterface } from '@app/contracts';
import BigNumber from 'bignumber.js';
import utils from 'web3-utils';

interface XbeInflationMethods {
  totalMinted: () => ContractBaseMethod<string>;
  mintForContracts: () => Web3TransactionInterface;
}

@Injectable()
export class XbeInflationContract implements BaseContractInterface<XbeInflationMethods> {
  contract: Web3Contract<XbeInflationMethods>;

  constructor(readonly web3Provider: Web3ProviderService) {
    this.init();
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.xbeInflation;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }

  /**
   * @return BigNumber
   */
  async totalMinted(): Promise<BigNumber> {
    const e = await this.contract.methods.totalMinted().call();
    return new BigNumber(utils.fromWei(e));
  }

  async mintForContracts(): Promise<void> {
    const transaction = this.contract.methods.mintForContracts();
    await this.web3Provider.sendTransactions(transaction, this.contract.options.address);
  }
}
