import { Injectable } from '@nestjs/common';
import utils from 'web3-utils';
import BigNumber from 'bignumber.js';
import { ContractBaseMethod, Web3Contract, Web3ProviderService } from '@app/web3-provider';
import { BaseContractInterface } from '@app/contracts/interfaces';
import { contractsConfig } from '@app/configuration';
import { PoolNameEnum } from '@app/constants';

interface VotingStakingRewardsMethods {
  earned: (address: string) => ContractBaseMethod<string>;

  rewardsToken: () => ContractBaseMethod<string>;

  calculateBoostLevel: (address: string) => ContractBaseMethod<string>;

  stakingToken: () => ContractBaseMethod<string>;

  rewardRate: () => ContractBaseMethod<string>;

  totalSupply: () => ContractBaseMethod<string>;

  potentialXbeReturns: (
    duration: number,
    address: string,
  ) => ContractBaseMethod<string>;

  bondedRewardLocks: (
    address: string,
  ) => ContractBaseMethod<[string, string, boolean]>;

  balanceOf: (address: string) => ContractBaseMethod<string>;
}

@Injectable()
export class VotingStakingRewardsContract
implements BaseContractInterface<VotingStakingRewardsMethods> {
  contract: Web3Contract<VotingStakingRewardsMethods>;

  poolName = PoolNameEnum.VOTING_STAKING_REWARDS;

  constructor(readonly web3Provider: Web3ProviderService) {
    this.init();
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.votingStakingRewards;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }

  /**
   * @param address
   * @return uint256
   */
  async calculateBoostLevel(address: string): Promise<string> {
    return this.contract.methods.calculateBoostLevel(address).call();
  }

  /**
   * @param address
   * @return [amount: BigNumber, unlockTime: number, requested: boolean]
   */
  async bondedRewardLocks(address: string): Promise<[BigNumber, number, boolean]> {
    const e = await this.contract.methods.bondedRewardLocks(address).call();

    return [
      new BigNumber(utils.fromWei(e[0])),
      Number(e[1]),
      e[2],
    ];
  }

  /**
   * @param address
   * @return BigNumber
   */
  async earned(address: string): Promise<BigNumber> {
    const e = await this.contract.methods.earned(address).call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return address
   */
  async stakingToken(): Promise<string> {
    return this.contract.methods.stakingToken().call();
  }

  /**
   * @return address
   */
  async rewardsToken(): Promise<string> {
    return this.contract.methods.rewardsToken().call();
  }

  /**
   * @return BigNumber
   */
  async totalSupply(): Promise<BigNumber> {
    const e = await this.contract.methods.totalSupply().call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return BigNumber
   */
  async rewardRate(): Promise<BigNumber> {
    const e = await this.contract.methods.rewardRate().call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return BigNumber
   */
  async potentialXbeReturns(
    duration: number,
    address: string,
  ): Promise<BigNumber> {
    const e = await this.contract.methods.potentialXbeReturns(duration, address).call();
    return new BigNumber(utils.fromWei(e[0]));
  }

  /**
   * @return BigNumber
   */
  async balanceOf(address: string): Promise<BigNumber> {
    const e = await this.contract.methods.balanceOf(address).call();
    return new BigNumber(utils.fromWei(e));
  }
}
