import { Web3Contract, Web3ProviderService } from '@app/web3-provider';

export interface BaseContractInterface<T> {
  readonly contract: Web3Contract<T>;
  readonly web3Provider: Web3ProviderService;
}
