import {
  ContractBaseMethod,
  Web3Contract,
  Web3ProviderService,
  Web3TransactionInterface,
} from '@app/web3-provider';
import { BaseContractInterface } from '@app/contracts';
import BigNumber from 'bignumber.js';
import utils from 'web3-utils';
import { PoolNameEnum } from '@app/constants';

export interface VaultMethods {
  earned: (rewardToken: string, address: string) => ContractBaseMethod<string>;

  rewardRates: (rewardToken: string) => ContractBaseMethod<string>;

  stakingToken: () => ContractBaseMethod<string>;

  totalSupply: () => ContractBaseMethod<string>;

  getRewardTokensCount: () => ContractBaseMethod<string>;

  getRewardToken: (index: number) => ContractBaseMethod<string>;

  potentialRewardReturns: (
    rewardsToken: string,
    duration: number,
    account: string,
  ) => ContractBaseMethod<string>;

  getPoolRewardForDuration: (
    address: string,
    duration: number,
  ) => ContractBaseMethod<string>;

  balance: () => ContractBaseMethod<string>;

  balanceOf: (address: string) => ContractBaseMethod<string>;

  earn: () => Web3TransactionInterface;
}

export type EarnedProps = { address: string; rewardToken: string };

export class BaseVaultContract implements BaseContractInterface<VaultMethods> {
  contract: Web3Contract<VaultMethods>;

  poolName: PoolNameEnum;

  web3Provider: Web3ProviderService;

  async getEarned(props: { userAddress: string; tokenAddress: string; }) {
    const { tokenAddress, userAddress } = props;

    const amountUint256 = await this.earned({
      rewardToken: tokenAddress,
      address: userAddress,
    });

    const amountDecimal = utils.fromWei(amountUint256);

    return new BigNumber(amountDecimal);
  }

  async getRewardRates(props: { rewardToken: string; }) {
    const amountUint256 = await this.rewardRates(props);

    const amountDecimal = utils.fromWei(amountUint256);

    return new BigNumber(amountDecimal);
  }

  async getRewardTokensAddresses() {
    const rewardTokensCount = await this.getRewardTokensCount();
    const rewardTokens: string[] = [];

    for (let i = 0; i < rewardTokensCount; i += 1) {
      const rewardToken = await this.getRewardToken(i);
      rewardTokens.push(rewardToken);
    }

    return rewardTokens;
  }

  /**
   * @return address
   */
  async stakingToken(): Promise<string> {
    return this.contract.methods.stakingToken().call();
  }

  /**
   * @param rewardToken
   * @param address
   * @return uint256
   */
  async earned({ rewardToken, address }: EarnedProps): Promise<string> {
    return this.contract.methods.earned(rewardToken, address).call();
  }

  /**
   * @param rewardToken
   * @param address
   * @return uint256
   */
  async rewardRates({ rewardToken }: { rewardToken: string }): Promise<string> {
    return this.contract.methods.rewardRates(rewardToken).call();
  }

  /**
   * @return BigNumber
   */
  async balance(): Promise<BigNumber> {
    const e = await this.contract.methods.balance().call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return BigNumber
   */
  async totalSupply(): Promise<BigNumber> {
    const e = await this.contract.methods.totalSupply().call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @param address
   * @return BigNumber
   */
  async balanceOf(address: string): Promise<BigNumber> {
    const e = await this.contract.methods.balanceOf(address).call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return uint256
   */
  async getRewardTokensCount(): Promise<number> {
    return Number(await this.contract.methods.getRewardTokensCount().call());
  }

  /**
   * @param index
   * @return address
   */
  async getRewardToken(index: number): Promise<string> {
    return this.contract.methods.getRewardToken(index).call();
  }

  /**
   * @param address
   * @param duration
   * @return uint256
   */
  async getPoolRewardForDuration(
    address: string,
    duration: number,
  ): Promise<string> {
    return this.contract.methods
      .getPoolRewardForDuration(address, duration)
      .call();
  }

  /**
   * @return BigNumber
   */
  async potentialRewardReturns({
    rewardToken,
    duration,
    address,
  }: EarnedProps & { duration: number }): Promise<BigNumber> {
    try {
      const amountUint256 = await this.contract.methods.potentialRewardReturns(
        rewardToken,
        duration,
        address,
      ).call();

      const amountDecimal = utils.fromWei(amountUint256);

      return new BigNumber(amountDecimal);
    } catch (e) {
      return new BigNumber(0);
    }
  }

  async earn(): Promise<void> {
    const transaction: Web3TransactionInterface = this.contract.methods.earn();
    await this.web3Provider.sendTransactions(transaction, this.contract.options.address);
  }
}
