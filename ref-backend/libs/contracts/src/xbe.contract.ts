import { Injectable } from '@nestjs/common';

import { ContractBaseMethod, Web3Contract, Web3ProviderService } from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';
import { BaseContractInterface } from '@app/contracts';
import BigNumber from 'bignumber.js';
import utils from 'web3-utils';

interface XbeMethods {
  balanceOf: (address: string) => ContractBaseMethod<string>;
  totalSupply: () => ContractBaseMethod<string>;
}

@Injectable()
export class XbeContract implements BaseContractInterface<XbeMethods> {
  contract: Web3Contract<XbeMethods>;

  constructor(readonly web3Provider: Web3ProviderService) {
    this.init();
  }

  async init(): Promise<void> {
    const xbeAddress = contractsConfig.list.mockXbe.address || contractsConfig.list.xbe.address;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      xbeAddress,
    );
  }

  /**
   * @return BigNumber
   */
  async balanceOf(address: string): Promise<BigNumber> {
    const e = await this.contract.methods.balanceOf(address).call();
    return new BigNumber(utils.fromWei(e));
  }

  /**
   * @return BigNumber
   */
  async totalSupply(): Promise<BigNumber> {
    const e = await this.contract.methods.totalSupply().call();
    return new BigNumber(utils.fromWei(e));
  }
}
