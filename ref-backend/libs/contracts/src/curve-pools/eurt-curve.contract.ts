import { Injectable, Logger } from '@nestjs/common';
import { BaseContractInterface } from '@app/contracts';
import { ContractBaseMethod, Web3Contract, Web3ProviderService } from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';

@Injectable()
export class EurtCurveContract implements BaseContractInterface<EurtCurveMethods> {
  contract: Web3Contract<EurtCurveMethods>;

  constructor(
    readonly web3Provider: Web3ProviderService,
  ) {
    this.init().catch((err) => {
      if (err) {
        Logger.error(err);
      }
    });
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.eurtCurve;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }
}

export interface EurtCurveMethods {
  get_virtual_price: () => ContractBaseMethod<string>;

  symbol: () => ContractBaseMethod<string>;
}
