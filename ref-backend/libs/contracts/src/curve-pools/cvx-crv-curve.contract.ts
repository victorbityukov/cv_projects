import { Injectable, Logger } from '@nestjs/common';
import { BaseContractInterface } from '@app/contracts';
import { ContractBaseMethod, Web3Contract, Web3ProviderService } from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';
import { PoolNameEnum } from '@app/constants';

@Injectable()
export class CvxCrvCurveContract implements BaseContractInterface<CvxCrvCurveMethods> {
  contract: Web3Contract<CvxCrvCurveMethods>;

  poolName: PoolNameEnum;

  constructor(
    readonly web3Provider: Web3ProviderService,
  ) {
    this.poolName = PoolNameEnum.CVX_CRV_VAULT;

    this.init().catch((err) => {
      if (err) {
        Logger.error(err);
      }
    });
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.cvxCrvCurve;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }
}

export interface CvxCrvCurveMethods {
  get_virtual_price: () => ContractBaseMethod<string>;

  symbol: () => ContractBaseMethod<string>;
}
