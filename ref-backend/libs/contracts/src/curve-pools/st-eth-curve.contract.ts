import { Injectable, Logger } from '@nestjs/common';
import { BaseContractInterface } from '@app/contracts';
import { ContractBaseMethod, Web3Contract, Web3ProviderService } from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';

@Injectable()
export class StEthCurveContract implements BaseContractInterface<StEthCurveMethods> {
  contract: Web3Contract<StEthCurveMethods>;

  constructor(
    readonly web3Provider: Web3ProviderService,
  ) {
    this.init().catch((err) => {
      if (err) {
        Logger.error(err);
      }
    });
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.stEthCurve;
    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }
}

export interface StEthCurveMethods {
  get_virtual_price: () => ContractBaseMethod<string>;

  lp_token: () => ContractBaseMethod<string>;
}
