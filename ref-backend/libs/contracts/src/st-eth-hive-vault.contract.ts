import { Injectable, Logger } from '@nestjs/common';
import { BaseVaultContract } from '@app/contracts';
import { Web3ProviderService } from '@app/web3-provider';
import { contractsConfig } from '@app/configuration';
import { PoolNameEnum } from '@app/constants';

@Injectable()
export class StEthHiveVaultContract
  extends BaseVaultContract {
  constructor(
    readonly web3Provider: Web3ProviderService,
  ) {
    super();

    this.poolName = PoolNameEnum.ST_ETH_VAULT;

    this.init().catch((err) => {
      if (err) {
        Logger.error(err);
      }
    });
  }

  async init(): Promise<void> {
    const { address } = contractsConfig.list.stEthHiveVault;

    this.contract = await this.web3Provider.getContractInstanceByAddress(
      address,
    );
  }
}
