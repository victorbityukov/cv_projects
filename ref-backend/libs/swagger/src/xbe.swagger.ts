import { ApiPropertyOptions } from '@nestjs/swagger';

export const SWAGGER_XBE_APY: ApiPropertyOptions = {
  name: 'apy',
  type: Number,
};

export const SWAGGER_XBE_DAILY_APY: ApiPropertyOptions = {
  name: 'daily_apy',
  type: Number,
};

export const SWAGGER_XBE_CIRCULATING_SUPPLY: ApiPropertyOptions = {
  name: 'circulating_supply',
  type: Number,
};

export const SWAGGER_XBE_TOTAL_CIRCULATING_SUPPLY: ApiPropertyOptions = {
  name: 'total_circulating_supply',
  type: Number,
};

export const SWAGGER_XBE_TOTAL_LOCKED: ApiPropertyOptions = {
  name: 'total_locked_xbe',
  type: Number,
};

export const SWAGGER_XBE_TOTAL_LOCKED_SHARE: ApiPropertyOptions = {
  name: 'total_locked_xbe_share',
  type: Number,
};

export const SWAGGER_XBE_TOTAL_VOTES_LOCKED: ApiPropertyOptions = {
  name: 'total_votes_locked',
  type: Number,
};

export const SWAGGER_XBE_TOTAL_VOTES_SHARE: ApiPropertyOptions = {
  name: 'total_locked_xbe_share',
  type: Number,
};

export const SWAGGER_XBE_VE_TOTAL: ApiPropertyOptions = {
  name: 'total_locked_xbe_ve_total',
  type: Number,
};

export const SWAGGER_XBE_AVG_LOCK_TIME: ApiPropertyOptions = {
  name: 'total_locked_xbe_avg_lock_time',
  type: Number,
};
