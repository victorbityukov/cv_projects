import { ApiPropertyOptions } from '@nestjs/swagger';
import { CurrencyTypeEnum, NetworkEnum, VenueEnum } from '@app/constants';

export const SWAGGER_TOKEN_SYMBOL: ApiPropertyOptions = {
  name: 'token',
  description: 'This field shows the currency ticker SYMBOL',
  type: String,
  required: true,
  enum: CurrencyTypeEnum,
  default: CurrencyTypeEnum.XBE,
  example: CurrencyTypeEnum.XBE,
};

export const SWAGGER_TOKEN_NETWORK: ApiPropertyOptions = {
  name: 'network',
  description: 'Represents value for the selected blockchain network',
  type: String,
  required: false,
  enum: NetworkEnum,
  default: NetworkEnum.ETH_M,
  example: NetworkEnum.ETH_M,
};

export const SWAGGER_TOKEN_CURRENCY: ApiPropertyOptions = {
  name: 'currency',
  description:
    'Represents currency for value field, in which quote for the selected token will nominated in',
  type: String,
  required: true,
  enum: CurrencyTypeEnum,
  default: CurrencyTypeEnum.USDT,
  example: CurrencyTypeEnum.USDT,
};

export const SWAGGER_TOKEN_VENUE: ApiPropertyOptions = {
  name: 'venue',
  description: 'This field shows the venue of the quote\'s source',
  required: false,
  type: String,
  enum: VenueEnum,
  default: VenueEnum.SUSHI,
  example: VenueEnum.SUSHI,
};

export const SWAGGER_TOKEN_QUOTE: ApiPropertyOptions = {
  name: 'quote',
  description:
    'This field represents the price for the selected token, nominated in currency. For example, for XBE/USDT you need to pick XBE as token and USDT as currency',
  type: String,
  example: '2.127235819951091',
};

export const SWAGGER_TOKEN_QUANTITY: ApiPropertyOptions = {
  name: 'quantity',
  description:
    'If this fields presents in response, it shows the liquidity available',
  type: String,
  example: '73.91917636697741',
};

export const SWAGGER_TOKEN_TIMESTAMP: ApiPropertyOptions = {
  name: 'timestamp',
  description: 'Last updated date as ISO string for the selected quote',
  type: Date,
  example: new Date().toString(),
};
