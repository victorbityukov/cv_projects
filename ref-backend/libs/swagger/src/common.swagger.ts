import { ApiPropertyOptions } from '@nestjs/swagger';
import { ResponseReferralStatisticsMembersDto } from '@app/dto';
import { ResponseReferralStatisticsDepositsDto } from '@app/dto/response-referral-statistics.dto';

export const SWAGGER_COMMON_SUM_CLAIMABLE_REWARDS: ApiPropertyOptions = {
  name: 'claimable',
  description:
    'This field shows the sum claimable rewards for all referral levels',
  type: Number,
  required: true,
  default: 0,
  example: 0,
};

export const SWAGGER_COMMON_REFERRAL_STATISTICS_REFERRAL_HITS: ApiPropertyOptions = {
  name: 'referralHots',
  description: 'Number of visits by this referral code',
  type: String,
  default: 0,
  example: 0,
};

export const SWAGGER_COMMON_REFERRAL_STATISTICS_MEMBERS: ApiPropertyOptions = {
  name: 'members',
  description: 'This field shows amount referral members by tier and amount',
  type: () => ResponseReferralStatisticsMembersDto,
  required: true,
};

export const SWAGGER_COMMON_REFERRAL_STATISTICS_DEPOSITS: ApiPropertyOptions = {
  name: 'deposits',
  description: 'This field shows sum deposits referral members by tier and total sum',
  type: () => ResponseReferralStatisticsDepositsDto,
  required: true,
};

export const SWAGGER_COMMON_REFERRAL_STATISTICS_MEMBERS_LIST_BY_TIER: ApiPropertyOptions = {
  name: 'list',
  description: 'This field shows amount referral members by tier',
  type: [Number],
  required: true,
  default: [0, 0, 0],
  example: [1, 3, 5],
};

export const SWAGGER_COMMON_REFERRAL_STATISTICS_DEPOSITS_LIST_BY_TIER: ApiPropertyOptions = {
  name: 'list',
  description: 'This field shows amount referral deposits by tier',
  type: [Number],
  required: true,
  default: [0, 0, 0],
  example: [150, 200, 57],
};

export const SWAGGER_COMMON_REFERRAL_STATISTICS_MEMBERS_AMOUNT: ApiPropertyOptions = {
  name: 'totalAmount',
  description: 'This field shows total amount referral members',
  type: Number,
  required: false,
  default: 0,
  example: 0,
};

export const SWAGGER_COMMON_REFERRAL_STATISTICS_DEPOSITS_SUM: ApiPropertyOptions = {
  name: 'totalSum',
  description: 'This field shows total sum referral deposits',
  type: Number,
  required: false,
  default: 0,
  example: 0,
};

export const SWAGGER_COMMON_REFERRAL_FROM: ApiPropertyOptions = {
  name: 'from',
  description: 'This field shows from who pay tithe',
  type: String,
  required: true,
  default: '0x0000000000000000000000000000000000000000',
  example: '0x0000000000000000000000000000000000000000',
};

export const SWAGGER_COMMON_REFERRAL_TO: ApiPropertyOptions = {
  name: 'to',
  description: 'This field shows who is being paid tithing',
  type: String,
  required: true,
  default: '0x0000000000000000000000000000000000000000',
  example: '0x0000000000000000000000000000000000000000',
};

export const SWAGGER_COMMON_REFERRAL_TITHE: ApiPropertyOptions = {
  name: 'tithe',
  description: 'This field shows how much tithe in percent',
  type: Number,
  required: true,
};

export const SWAGGER_COMMON_REFERRAL_CURRENCY: ApiPropertyOptions = {
  name: 'currency',
  description: 'This field shows in what currency to pay',
  type: String,
  required: true,
  example: 'ETH',
  default: 'ETH',
};

export const SWAGGER_COMMON_REFERRAL_VALUE: ApiPropertyOptions = {
  name: 'value',
  description: 'This field shows how much was paid',
  type: String,
  required: true,
};

export const SWAGGER_COMMON_REFERRAL_CUSTOM: ApiPropertyOptions = {
  name: 'custom',
  description: 'This custom field',
  type: String,
  required: true,
};
