import { ApiPropertyOptions } from '@nestjs/swagger';

export const SWAGGER_SUSHI_EARNED_USD: ApiPropertyOptions = {
  name: 'earned_usd',
  type: Number,
};

export const SWAGGER_SUSHI_AVERAGE_APR: ApiPropertyOptions = {
  name: 'average_apr',
  type: Number,
};
