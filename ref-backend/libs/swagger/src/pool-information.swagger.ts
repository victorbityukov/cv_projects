import { ApiPropertyOptions } from '@nestjs/swagger';
import { PoolNameEnum } from '@app/constants';

export const SWAGGER_POOL_INFORMATION_POOL_NAME: ApiPropertyOptions = {
  name: 'poolName',
  description: 'This field shows the pool name',
  type: String,
  required: false,
  enum: PoolNameEnum,
  default: PoolNameEnum.VOTING_STAKING_REWARDS,
  example: PoolNameEnum.VOTING_STAKING_REWARDS,
};

export const SWAGGER_POOL_INFORMATION_EARNED: ApiPropertyOptions = {
  name: 'earned',
  description: 'Amount of your claimed coins earned, nominated in USD',
  type: Number,
  example: 12,
};

export const SWAGGER_POOL_INFORMATION_APR: ApiPropertyOptions = {
  name: 'apr',
  description: 'Average Annual Percentage Rate, nominated in %',
  type: Number,
  example: 0.12,
};

export const SWAGGER_POOL_INFORMATION_TVL: ApiPropertyOptions = {
  name: 'tvl',
  description: 'TVL',
  type: Number,
  example: 1,
};

export const SWAGGER_POOL_INFORMATION_DEPOSITS: ApiPropertyOptions = {
  name: 'deposits',
  description: 'Amount of tokens that has been claim',
  type: Number,
  example: 100,
};
