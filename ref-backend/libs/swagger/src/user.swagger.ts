import { ApiPropertyOptions } from '@nestjs/swagger';

export const SWAGGER_USER_ID: ApiPropertyOptions = {
  name: 'address',
  description: 'User address',
  type: String,
  required: true,
  example: '0x0000000000000000000000000000000000000000',
};

export const SWAGGER_USER_PARENT_ID: ApiPropertyOptions = {
  name: 'parentId',
  description: 'User parent id',
  type: String,
  required: false,
  example: '0x0000000000000000000000000000000000000000',
};

export const SWAGGER_USER_REFERRAL_CODE: ApiPropertyOptions = {
  name: 'referralCode',
  description: 'User referral code',
  type: String,
  required: true,
  example: 'XBE-DEFAULT',
};

export const SWAGGER_USER_DATE_REGISTERED: ApiPropertyOptions = {
  name: 'dateRegistered',
  description: 'User date registered',
  type: String,
  required: false,
  example: '2021-08-04T07:17:08.742Z',
};

export const SWAGGER_USER_LIQUIDITY: ApiPropertyOptions = {
  name: 'liquidity',
  description: 'The cost of all deposits of the referral',
  type: Number,
  required: true,
  example: 200.15,
};
