import { ApiPropertyOptions } from '@nestjs/swagger';

export const SWAGGER_TOTAL_CLAIMABLE : ApiPropertyOptions = {
  name: 'totalClaimable',
  description: 'This field shows the total claimable in USD',
  type: Number,
  example: '3.00',
};

export const SWAGGER_TOTAL_DEPOSITS : ApiPropertyOptions = {
  name: 'totalDeposits',
  description: 'This field shows the total deposits in USD',
  type: Number,
  example: '56.00',
};

export const SWAGGER_TOTAL_COMBINED_APY : ApiPropertyOptions = {
  name: 'totalCombinedApy',
  description: 'This field shows the total combined apy in USD',
  type: Number,
  example: '15.00',
};

export const SWAGGER_TOTAL_CRV_EARNED : ApiPropertyOptions = {
  name: 'totalCrvEarned',
  description: 'This field shows the total crv earned in USD',
  type: Number,
  example: '15.00',
};

export const SWAGGER_TOTAL_CVX_EARNED : ApiPropertyOptions = {
  name: 'totalCvxEarned',
  description: 'This field shows the total cvx earned in USD',
  type: Number,
  example: '10.00',
};

export const SWAGGER_TOTAL_CVX_CRV_EARNED : ApiPropertyOptions = {
  name: 'totalCvxCrvEarned',
  description: 'This field shows the total cvx crv earned in USD',
  type: Number,
  example: '10.00',
};

export const SWAGGER_TOTAL_XBE_EARNED : ApiPropertyOptions = {
  name: 'totalXbeEarned',
  description: 'This field shows the total xbe earned in USD',
  type: Number,
  example: '15.00',
};

export const SWAGGER_STAKED_ASSETS_YIELD : ApiPropertyOptions = {
  name: 'stakedAssetsYield',
  description: 'USD cost of all CVX, CRV, cvxCRV and XBE tokens that users can claim',
  type: Number,
  example: '123.00',
};

export const SWAGGER_DAILY_YIELD : ApiPropertyOptions = {
  name: 'dailyYield',
  description: 'Cost of all rewards that users will receive every day',
  type: Number,
  example: '10.00',
};

export const SWAGGER_CURVE_APYS_IB : ApiPropertyOptions = {
  name: 'ib',
  description: 'Returns info apy',
};
export const SWAGGER_CURVE_APYS_USDN : ApiPropertyOptions = {
  name: 'usdn',
  description: 'Returns info apy',
};
export const SWAGGER_CURVE_APYS_STETH : ApiPropertyOptions = {
  name: 'steth',
  description: 'Returns info apy',
};
export const SWAGGER_CURVE_APYS_EURS : ApiPropertyOptions = {
  name: 'eurs',
  description: 'Returns info apy',
};
export const SWAGGER_CURVE_APYS_CVXCRV : ApiPropertyOptions = {
  name: 'cvxcrv',
  description: 'Returns info apy',
};
export const SWAGGER_CURVE_APYS_EURT : ApiPropertyOptions = {
  name: 'eurt',
  description: 'Returns info apy',
};
