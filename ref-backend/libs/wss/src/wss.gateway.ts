import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { Injectable, Logger } from '@nestjs/common';
import { WsEventEnum } from '@app/wss/interfaces';

@Injectable()
@WebSocketGateway(80)
export class WssGateway
implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  private readonly logger = new Logger(WssGateway.name, { timestamp: true });

  @WebSocketServer()
  server: Server;

  wsClients: Socket[] = [];

  afterInit(server: Server): Server {
    this.logger.log('Init');
    return server;
  }

  handleDisconnect(client: Socket): void {
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  handleConnection(client: Socket): void {
    this.wsClients.push(client);
    this.logger.log(`Client connected: ${client.id}`);
  }

  @SubscribeMessage(WsEventEnum.text)
  handleText(client: Server, message: string): void {
    this.server.emit(WsEventEnum.message, message);
  }
}
