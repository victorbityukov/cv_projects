import * as WebSocket from 'ws';
import { WebSocketAdapter, Logger } from '@nestjs/common';
import { MessageMappingProperties } from '@nestjs/websockets';
import { Observable, EMPTY, fromEvent } from 'rxjs';
import { Server, ServerOptions } from 'ws';
import { mergeMap } from 'rxjs/operators';
import { WsEventEnum } from '@app/wss/interfaces';

export class WssAdapter implements WebSocketAdapter {
  private readonly logger = new Logger(WssAdapter.name);

  create(port: number, options: ServerOptions = {}): Server {
    this.logger.debug(`${WssAdapter.name}:${port} server created`);
    return new Server({ port, ...options });
  }

  bindClientConnect(
    server: { on: (arg0: string, arg1: Function) => void },
    callback: Function,
  ) {
    server.on('connection', callback);
  }

  bindMessageHandlers(
    client: WebSocket,
    handlers: MessageMappingProperties[],
    process: (data: string) => Observable<string>,
  ) {
    fromEvent(client, WsEventEnum.message)
      .pipe(
        mergeMap((data: unknown) => this.bindMessageHandler(data as string, handlers, process)),
      )
      .subscribe((response) => client.send(JSON.stringify(response)));
  }

  bindMessageHandler(
    buffer: string,
    handlers: MessageMappingProperties[],
    process: (data: string) => Observable<string>,
  ): Observable<string> {
    const message = JSON.parse(buffer);
    const messageHandler = handlers.find(
      (handler) => handler.message === message.event,
    );
    if (!messageHandler) {
      return EMPTY;
    }
    return process(messageHandler.callback(message.data));
  }

  close(server: Server) {
    this.logger.debug(`${WssAdapter.name}: server closed`);
    server.close();
  }
}
