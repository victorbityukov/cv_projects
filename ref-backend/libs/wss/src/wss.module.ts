import { Module } from '@nestjs/common';
import { WssAdapter } from './wss.adapter';

@Module({
  providers: [WssAdapter],
  exports: [WssAdapter],
})
export class WssModule {}
