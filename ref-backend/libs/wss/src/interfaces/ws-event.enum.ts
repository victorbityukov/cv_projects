export enum WsEventEnum {
  on_register = 'on-register',
  message = 'message',
  text = 'text',
}
