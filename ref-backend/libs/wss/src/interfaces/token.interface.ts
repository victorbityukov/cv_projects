export interface TokenInterface {
  readonly symbol: string;
  readonly id: number;
  readonly decimals: number;
  readonly address: string;
}
