/** Global constants for redis
 */
export const REDIS_VALUE_PLACEHOLDER = '!';

/** Official redis docs to SET()
 * EX Set the specified expire time, in seconds.
 * PX Set the specified expire time, in milliseconds.
 * see more on https://redis.io/commands/set
 */
export const REDIS_DEFAULT_EXPIRES_MODE = 'PX';
