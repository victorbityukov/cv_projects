export const VE_XBE_SUPPLY = 've_xbe_supply';
export const VE_XBE_LOCKED_SUPPLY = 've_xbe_locked_supply';
export const VE_XBE_TOTAL_SUPPLY = 've_xbe_total_supply';

export const TOTAL_LOCKED_XBE_AVG_LOCK_TIME = 'total_locked_xbe_avg_lock_time';
