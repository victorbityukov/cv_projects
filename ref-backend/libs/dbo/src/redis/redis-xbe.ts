export const XBE_BALANCE_OF = 'xbe_balance_of';
export const XBE_BALANCE_OF_XBE = `${XBE_BALANCE_OF}_xbe`;
export const XBE_BALANCE_OF_CLIENT = `${XBE_BALANCE_OF}_client`;
export const XBE_BALANCE_OF_TREASURY = `${XBE_BALANCE_OF}_treasury`;
export const XBE_TOTAL_SUPPLY = 'xbe_total_supply';
