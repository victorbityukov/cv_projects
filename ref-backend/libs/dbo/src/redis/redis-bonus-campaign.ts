export const BONUS_CAMPAIGN_BONUS_EMISSION = 'bonus_campaign_bonus_emission';
export const BONUS_CAMPAIGN_TOTAL_SUPPLY = 'bonus_campaign_total_supply';
export const BONUS_CAMPAIGN_REWARDS_DURATION = 'bonus_campaign_rewards_duration';
export const BONUS_CAMPAIGN_START_MINT_TIME = 'bonus_campaign_start_mint_time';
export const BONUS_CAMPAIGN_PERIOD_FINISH = 'bonus_campaign_period_finish';
export const BONUS_CAMPAIGN_STOP_REGISTER_TIME = 'bonus_campaign_stop_register_time';
