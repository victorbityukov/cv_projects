export const sushiXCCYBoard = 'sushi:xccy:board';

export function generateSushiXCCYBoard(xccy_pair: string): string {
  return `sushi:xccy:board:${xccy_pair}`;
}

export function extractSushiKeyPair(xccy_key: string): string[] {
  return xccy_key.split(':');
}
