export const VOTING_STAKING_REWARDS_TOTAL_SUPPLY = 'voting_staking_rewards_total_supply';
export const VOTING_STAKING_REWARDS_REWARD_RATE = 'voting_staking_rewards_reward_rate';

export const VOTING_STAKING_REWARDS_DAILY_APY = 'voting_staking_rewards_daily_apy';
export const VOTING_STAKING_REWARDS_YEARLY_APY = 'voting_staking_rewards_yearly_apy';
