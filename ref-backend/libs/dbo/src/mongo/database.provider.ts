import { MONGO_CONFIG, MONGO_OPTIONS } from '@app/configuration';
import * as mongoose from 'mongoose';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: (): Promise<typeof mongoose> => mongoose.connect(
      MONGO_CONFIG.connection_string, MONGO_OPTIONS,
    ),
  },
];
