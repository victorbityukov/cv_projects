import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { NetworkEnum, VenueEnum } from '@app/constants';

@Schema({ timestamps: true })
export class TokenRates extends Document {
  @Prop({ type: String, required: true })
  token: string;

  @Prop({ type: String })
  token_id: string;

  @Prop({
    type: String,
    required: true,
    default: NetworkEnum.ETH_M,
    enum: NetworkEnum,
  })
  network: string;

  @Prop({ type: String, required: true, enum: VenueEnum })
  venue: string;

  @Prop({ type: MongooseSchema.Types.Decimal128, required: true })
  quote: number;

  @Prop({ type: MongooseSchema.Types.Decimal128 })
  quantity: number;

  @Prop({ type: String, required: true })
  currency: string;

  @Prop({ type: String })
  currency_id: string;

  @Prop()
  updatedAt: Date;

  @Prop()
  createdAt: Date;
}

export const TokenRatesSchema = SchemaFactory.createForClass(TokenRates);
TokenRatesSchema.index(
  {
    createdAt: -1,
  },
  { name: 'TTL', expireAfterSeconds: 86400 },
);
