import { CurrencyTypeEnum } from '@app/constants/enum';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Types } from 'mongoose';

@Schema({ timestamps: true })
export class ReferralRewardPayment extends Document {
  @Prop({ type: Types.ObjectId, ref: 'ReferralRewardNode' })
  relationId: ObjectId;

  @Prop({ type: String, ref: 'User' })
  from: string;

  @Prop({ type: String, ref: 'User' })
  to: string;

  @Prop({ max: 1, min: 0 })
  tithe: number;

  @Prop({ type: String, enum: CurrencyTypeEnum })
  currency: CurrencyTypeEnum;

  @Prop()
  value: string;

  @Prop()
  custom: string;
}

export const ReferralRewardPaymentSchema = SchemaFactory.createForClass(
  ReferralRewardPayment,
);
ReferralRewardPaymentSchema.index(
  {
    relationId: 1,
    from: 1,
    to: 1,
  },
);
