import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { NetworkEnum, VenueEnum } from '@app/constants';

@Schema()
class SushiPair extends Document {
  @Prop({ type: String, lowercase: true })
  _id: string;

  @Prop()
  tokenA: string;

  @Prop()
  tokenB: string;
}

export const SushiPairSchema = SchemaFactory.createForClass(SushiPair);
SushiPairSchema.index(
  {
    tokenA: 1,
    tokenB: 1,
  },
);

@Schema({ timestamps: true })
export class Token extends Document {
  @Prop({ type: String, lowercase: true })
  _id: string;

  @Prop({ type: String })
  name: string;

  @Prop({ type: Number })
  decimals: number;

  @Prop({ type: Boolean, default: false })
  is_stablecoin: boolean;

  @Prop({ type: String })
  symbol: string;

  @Prop({
    type: String,
    required: true,
    enum: NetworkEnum,
    default: NetworkEnum.ETH_M,
  })
  network: string;

  @Prop({ type: String, enum: VenueEnum })
  provider: string;

  @Prop({ type: [SushiPairSchema] })
  sushi_pairs: Types.Array<SushiPair>;

  @Prop()
  updatedAt?: Date;

  @Prop()
  createdAt?: Date;
}

export const TokenSchema = SchemaFactory.createForClass(Token);
TokenSchema.index(
  {
    createdAt: -1,
    name: 1,
    symbol: 1,
  },
);
