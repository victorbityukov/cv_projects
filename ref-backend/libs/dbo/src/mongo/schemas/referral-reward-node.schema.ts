import { CurrencyTypeEnum } from '@app/constants/enum';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class ReferralRewardNode extends Document {
  @Prop({ type: String, ref: 'User' })
  from: string;

  @Prop({ type: String, ref: 'User' })
  to: string;

  @Prop({ max: 1, min: 0 })
  tithe: number;

  @Prop({ type: String, enum: CurrencyTypeEnum, default: CurrencyTypeEnum.XBE })
  currency: CurrencyTypeEnum;
}

export const ReferralRewardNodeSchema = SchemaFactory.createForClass(
  ReferralRewardNode,
);
ReferralRewardNodeSchema.index(
  {
    from: 1,
    to: 1,
  },
);
