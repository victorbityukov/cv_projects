import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class User extends Document {
  @Prop({ lowercase: true })
  _id: string; // address

  @Prop({ type: String, ref: 'User', immutable: true })
  parentId: string;

  @Prop({ type: String })
  referralCode: string;

  @Prop({ type: Number, default: 0 })
  referralHit: number;
}

export const UserSchema = SchemaFactory.createForClass(User);
UserSchema.index(
  {
    parentId: 1,
    referralCode: 1,
  },
);
