import { Document, Schema as MongooseSchema } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { AbiItem } from 'web3-utils';

@Schema({ timestamps: true })
export class ContractCollection extends Document {
  @Prop({ type: String, lowercase: true })
  _id: string;

  @Prop({ type: String, immutable: true })
  name: string;

  @Prop({ type: [String] })
  tags: string[];

  @Prop({ default: false, type: MongooseSchema.Types.Mixed })
  abi: AbiItem | AbiItem[];
}

export const ContractCollectionSchema = SchemaFactory.createForClass(
  ContractCollection,
);
ContractCollectionSchema.index(
  {
    createdAt: -1,
  },
);
