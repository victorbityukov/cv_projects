import { DepositTypeEnum } from '@app/constants/enum';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

@Schema({ timestamps: true })
export class XBELockup extends Document {
  @Prop({ index: true })
  userAddress: string;

  @Prop({ type: Number, immutable: true })
  lockStart: number;

  @Prop({ type: Number })
  lockEnd: number;

  @Prop({ type: MongooseSchema.Types.Decimal128 })
  lockAmountDecimal: number;

  @Prop({ type: String })
  lockAmountBigNumber: string;

  @Prop({ default: false, type: Boolean })
  withdrawn: boolean;

  @Prop({ type: Number, enum: DepositTypeEnum })
  depositType: DepositTypeEnum;
}

export const XBELockupSchema = SchemaFactory.createForClass(XBELockup);
