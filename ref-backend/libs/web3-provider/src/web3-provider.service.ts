import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import Web3 from 'web3';
import { Contract } from 'web3-eth-contract';
import { contractsConfig, tokenBaseId, WALLET_CONFIG } from '@app/configuration';
import BigNumber from 'bignumber.js';
import { appConfig } from '@app/configuration/app.config';
import path from 'path';
import { optionsWs } from '@app/web3-provider/config';
import { InjectModel } from '@nestjs/mongoose';
import { ContractCollection } from '@app/dbo';
import { Model } from 'mongoose';
import { pairBaseId } from '@app/configuration/contracts.config';
import { Web3TransactionInterface } from './interfaces';

@Injectable()
export class Web3ProviderService {
  private readonly logger = new Logger(Web3ProviderService.name, { timestamp: true });

  readonly web3: Web3;

  constructor(
    @InjectModel(ContractCollection.name)
    private readonly contractCollectionModel: Model<ContractCollection>,
  ) {
    const web3Provider = new Web3.providers.WebsocketProvider(
      Web3.givenProvider || contractsConfig.infuraUrl,
      optionsWs,
    );
    this.web3 = new Web3(web3Provider);
  }

  private getAccount() {
    return this.web3.eth.accounts.privateKeyToAccount(WALLET_CONFIG.privateKey);
  }

  async getContractInstanceByAddress(address: string): Promise<Contract> {
    const addressLower = address.toLowerCase();
    const contractConfig = await this.contractCollectionModel
      .findOne({ _id: addressLower });
    if (!contractConfig) {
      throw new NotFoundException(`Contract by address ${addressLower} not found`);
    }
    return new this.web3.eth.Contract(contractConfig.abi, addressLower);
  }

  async getBaseTokenContract(address: string): Promise<Contract> {
    const addressLower = address.toLowerCase();
    const contractConfig = await this.contractCollectionModel
      .findOne({ _id: tokenBaseId });
    if (!contractConfig) {
      throw new NotFoundException(`Base token contract by address ${addressLower} not found`);
    }
    return new this.web3.eth.Contract(contractConfig.abi, addressLower);
  }

  async getBasePairContract(address: string): Promise<Contract> {
    const addressLower = address.toLowerCase();
    const contractConfig = await this.contractCollectionModel
      .findOne({ _id: pairBaseId });
    if (!contractConfig) {
      throw new NotFoundException(`Base pair contract by address ${addressLower} not found`);
    }
    return new this.web3.eth.Contract(contractConfig.abi, addressLower);
  }

  getAbiPath(abiFile: string) {
    return path.join(appConfig.rootDir, 'config', 'abi', abiFile);
  }

  public async sendTransactions(
    transaction: Web3TransactionInterface,
    addressContract: string,
  ): Promise<void> {
    try {
      const account = this.getAccount();
      const options = {
        to: addressContract,
        data: transaction.encodeABI(),
        gas: await transaction.estimateGas({ from: account.address }),
        gasPrice: await this.web3.eth.getGasPrice(),
      };
      const signed = await this.web3.eth.accounts.signTransaction(
        options,
        WALLET_CONFIG.privateKey,
      );

      if (signed?.rawTransaction) {
        this.logger.log('sendSignedTransaction');
        await this.web3.eth.sendSignedTransaction(signed.rawTransaction);
      }
    } catch (err) {
      this.logger.error(err);
    }
  }

  fromWei(value: string): string {
    return this.web3.utils.fromWei(value);
  }

  convertFromWei(value: number | string): number {
    return new BigNumber(this.fromWei(`${value}`).toString(), 10).toNumber();
  }
}
