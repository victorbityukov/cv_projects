import { Contract } from 'web3-eth-contract';

export interface ContractBaseMethod<T> {
  call: () => Promise<T>;
}

export interface Web3Contract<M, E = {}> extends Contract {
  methods: M;
  events: E;
}
