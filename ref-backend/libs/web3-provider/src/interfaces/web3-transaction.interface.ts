export interface Web3TransactionInterface {
  readonly encodeABI: () => string;
  readonly estimateGas: (object: { from: string }) => Promise<number>;
}
