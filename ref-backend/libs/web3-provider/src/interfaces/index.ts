export { Web3TransactionInterface } from './web3-transaction.interface';
export { Web3Contract, ContractBaseMethod } from './web3-contract.interface';
