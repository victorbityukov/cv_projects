import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MONGO_CONFIG, MONGO_OPTIONS } from '@app/configuration';
import {
  ContractCollection,
  ContractCollectionSchema,
} from '@app/dbo';
import { Web3ProviderService } from './web3-provider.service';

@Module({
  imports: [
    MongooseModule.forRoot(MONGO_CONFIG.connection_string, MONGO_OPTIONS),
    MongooseModule.forFeature([
      { name: ContractCollection.name, schema: ContractCollectionSchema },
    ]),
  ],
  providers: [Web3ProviderService],
  exports: [Web3ProviderService],
})
export class Web3ProviderModule {}
