export const isStableCoin = (name: string, symbol: string): boolean => {
  if (name.includes('stable')) {
    return true;
  }
  if (name.includes('USD')) {
    return true;
  }
  return symbol === 'USDT';
};

export const toSlug = (s: string): string => s.replace(/\s+/g, '_').replace(/'+/g, '').replace(
  /\$+/g,
  '',
);

export const camelCaseToDash = (s: string): string => s.replace(/([a-z])([A-Z])/g, '$1-$2')
  .toLowerCase();

export const sleep = async (delay: number): Promise<void> => new Promise((resolve) => {
  setTimeout(resolve, delay);
});

export const fixSymbol = (tokenSymbol: string) => {
  if (tokenSymbol === 'mXBE') {
    return 'XBE';
  }
  return tokenSymbol;
};
