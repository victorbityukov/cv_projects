import { handleServerErrors } from '.';
import { HandleTryCatchOptionsInterface, SuppressionEnum } from './interfaces';

/** Boilerplate remover for try-catch blocks with forced await.
 * It is a not one-fits-all decision, just for predictable cases.
 * You should write your own handlers for advanced logic.
 * @returns Promise<T>
 * @param options
 */
export async function handleTryCatch<T>(
  options: HandleTryCatchOptionsInterface,
): Promise<T | null | void> {
  const {
    func,
    logger,
    customCode,
    customMessage,
    handlerFunc,
    suppress,
  } = options;
  const methodName = func.name;
  try {
    return func();
  } catch (errorOrException) {
    if (handlerFunc) {
      return handlerFunc(errorOrException);
    }
    if (suppress === SuppressionEnum.SILENT) {
      return null; /** void */
    }
    if (suppress === SuppressionEnum.JUST_LOG) {
      logger.error(`Error in ${func.name}`);
      logger.error(errorOrException);
      return null; /** void */
    }
    if (suppress === SuppressionEnum.LOG_AND_RETURN_NULL) {
      logger.error(`Error in ${func.name}`);
      logger.error(errorOrException);
      return null;
    }
    if (suppress === SuppressionEnum.GET_ERROR) {
      return errorOrException;
    }
    if (suppress === SuppressionEnum.LOG_AND_GET_ERROR) {
      logger.error(`Error in ${func.name}`);
      logger.error(errorOrException);
      return errorOrException;
    }
    throw handleServerErrors({
      errorOrException,
      logger,
      methodName,
      customCode,
      customMessage,
    });
  }
}
