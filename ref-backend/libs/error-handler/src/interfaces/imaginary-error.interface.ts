/** This interfaces should be used only to provide access
 *  to various fields of undeclared types
 *  for resource-access libs errors (like TypeORM or nodemailer).
 *  @devs please mark all fields you add here as optional.
 *  @name ImaginaryErrorInterface means that we try to guess
 *  what kind of error or exception we can get here.
 *  It is more accurate description than
 *  ExpectedErrorInterface or GuessedErrorInterface.
 */
export interface ImaginaryErrorInterface {
  readonly code?: string | number;
}
