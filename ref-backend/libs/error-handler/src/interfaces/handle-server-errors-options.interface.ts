import { HttpException, HttpStatus, Logger } from '@nestjs/common';
import { ImaginaryErrorInterface } from '.';

export interface HandleServerErrorsOptionsInterface {
  readonly logger: Logger;
  readonly errorOrException: Error | HttpException | ImaginaryErrorInterface;
  readonly methodName: string;
  readonly customCode?: HttpStatus;
  readonly customMessage?: string;
}
