/* eslint-disable @typescript-eslint/ban-types */

import { HttpStatus, Logger } from '@nestjs/common';

/** Use if you definitely avoiding of throwing errors.
 * For example, if you use websockets instead of request-response.
 * Best practice here is to return something
 * (null or Error) instead of <Void> */
export enum SuppressionEnum {
  SILENT = 0,
  JUST_LOG = 1,
  LOG_AND_RETURN_NULL = 2,
  GET_ERROR = 3,
  LOG_AND_GET_ERROR = 4,
}

export interface HandleTryCatchOptionsInterface {
  readonly func: Function;
  readonly logger: Logger;
  readonly handlerFunc?: Function;
  readonly customCode?: HttpStatus;
  readonly customMessage?: string;
  readonly suppress?: SuppressionEnum;
}
