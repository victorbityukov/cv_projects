## Error-handler
This simple lib is for usage common situation:
```ts
try { ..code } catch(e) { ..handling}
```
The default handling process works this way:
- throws your `ApiException` (except `InternalServerErrorException`) exceptions without logging (this scenario assumes it is your own `NotFoundException` or `ConflictException` as a part of business logic)
- logs errors, then throws `InternalServerErrorException`. This is main scenario for any unexpected errors.
- if you pass here your custom `code` and `message`, it throws custom `ApiException`

> Note: This module aims to replace your boilerplate code in `catch` block. You still need to throw business logic exceptions or handle errors before calling `throw handleServerErrors(..params)`

> It also should be very usable to cover most 'popular' errors for TypeORM, Redis.io, Websocket.io in `utils/error-handler.constants.ts`, but I'm running out of time

## Handle-try-catch
Simple try-catch wrapper for async functions. Usable for avoiding try-catch boilerplate code. Optional error suppression included (usable for websocket cases). Use it this way:
```ts
handleTryCatch({
  func: () => functionToWrap({yourParams}), // for example, calls to resources, external to node process (db, redis, web3, axios, etc)
  logger: this.logger, // to log errors via `throw ErrorHandler`
  handlerFunc: () => functionToHandle(error), // optional, 
  suppress: SuppressionEnum.value // optional, if you want to suppress, log or get error  
})
```
