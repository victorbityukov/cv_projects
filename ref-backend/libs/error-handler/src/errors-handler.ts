import {
  ConflictException,
  HttpException,
  InternalServerErrorException,
  ServiceUnavailableException,
} from '@nestjs/common';
import {
  HandleServerErrorsOptionsInterface,
  ImaginaryErrorInterface,
} from './interfaces';
import { EHConstants } from './utils';

/** Simple error handler: logging errors and throws exceptions.
 * @name handleServerErrors
 * It is necessary to provide logger & method name for better debugging.
 * If you provide custom code & message, it just throws HttpException with no other checks.
 * @param options handleServerErrorsOptionsInterface
 * @throws any httpException you need
 */
export function handleServerErrors(
  options: HandleServerErrorsOptionsInterface,
): Error {
  const {
    logger,
    errorOrException,
    methodName,
    customCode,
    customMessage,
  } = options;
  if (customCode && customMessage) {
    // Custom exception support for non-conditional situations.
    throw new HttpException(customMessage, customCode);
  } else if (
    errorOrException instanceof HttpException
    && !(errorOrException instanceof InternalServerErrorException)
  ) {
    // Allowed business-logic exceptions.
    throw errorOrException as HttpException;
  } else if (
    (errorOrException as ImaginaryErrorInterface).code
    === EHConstants.fieldValues.EMAIL_CODE_EAUTH
  ) {
    // Example of handling resource-produced errors
    throw new ServiceUnavailableException(
      EHConstants.messages.UNAVAILABLE_EMAIL,
    );
  } else if (
    (errorOrException as ImaginaryErrorInterface).code
    === EHConstants.fieldValues.DB_ALREADY_EXISTS
  ) {
    // Example of handling resource-produced errors
    throw new ConflictException(EHConstants.messages.DB_ALREADY_EXISTS);
  } else {
    // Error is unhandled or not expected.
    logger.error(`Place: ${methodName}, details:\n ${errorOrException} `);
    throw new InternalServerErrorException();
  }
}
