export const EHConstants = {
  fieldValues: {
    EMAIL_CODE_EAUTH: 'EAUTH',
    DB_ALREADY_EXISTS: 23505,
  },
  messages: {
    UNAVAILABLE_EMAIL: 'Cannot send email: sending auth error',
    DB_ALREADY_EXISTS:
      'Cannot add this to database, because it is already exists',
  },
};
