export enum TithesEnum {
  FATHER = 0.07,
  GRANDFATHER = 0.02,
  GREAT_GRANDFATHER = 0.01,
}
