export enum NetworkEnum {
  ETH_M = 'mainnet',
  ETH_R = 'rinkeby',
  POLY = 'polygon',
}
