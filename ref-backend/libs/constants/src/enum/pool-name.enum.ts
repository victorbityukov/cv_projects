export enum PoolNameEnum {
  HIVE_VAULT = 'hiveVault',
  SUSHI_VAULT = 'sushiVault',
  CVX_VAULT = 'cvxVault',
  // CRV_VAULT = 'crvVault',
  CVX_CRV_VAULT = 'cvxCrvVault',
  VOTING_STAKING_REWARDS = 'votingStakingRewards',
  //
  IRONBANK_VAULT = 'ironbankVault',
  ST_ETH_VAULT = 'stEthVault',
  USDN_VAULT = 'usdnVault',
  //
  // EUR_XB_VAULT = 'eurXbVault',
  // EURS_VAULT = 'eursVault',
  EURT_VAULT = 'eurtVault',
}
