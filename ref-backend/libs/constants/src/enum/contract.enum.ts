export enum ContractEnum {
  HIVE_VAULT = 'hiveVault',
  REFERRAL_PROGRAM = 'referralProgram',
  SUSHI_VAULT = 'sushiVault',
  VOTING_STAKING_REWARDS = 'votingStakingRewards',
  VE_XBE = 'veXbe',
}
