export enum WsEventEnum {
  message = 'message',
  text = 'text',
  registration = 'registration',
  message_registration = 'message_registration',
}
