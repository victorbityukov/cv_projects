export enum ContractListenerEventEnum {
  REGISTER_USER = 'register-user',
  REWARDS_RECEIVED = 'rewards-received',
  REWARDS_CLAIMED = 'rewards-claimed',
  VE_XBE_DEPOSIT = 've-xbe-deposit',
  VE_XBE_WITHDRAW = 've-xbe-withdraw',
}
