export enum VenueEnum {
  SUSHI = 'sushiswap',
  UNI = 'uniswap',
  CMC = 'coinmarketcap',
  MOCK = 'mock',
}
