export enum CurrencyTypeEnum {
  USD = 'USD',
  USDT = 'USDT',
  XBE = 'XBE',
  LP = 'LP',
  ETH = 'ETH',
  CVX = 'CVX',
  CRV = 'CRV',
  CVX_CRV = 'CVX_CRV',
}
