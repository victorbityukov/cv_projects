export enum TableNameEnum {
  REF = 'ref',
  REF_TREE = 'ref_tree',
  WALLET = 'wallet',
}
