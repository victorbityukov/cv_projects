## Configuration library
This lib reads your config variables from `/config` folder and place it into objects via node.js `config` lib.
You can freely define objects for your needs (like `TypeOrmModuleOptions` instead of `db-config-interface` example) or use it 'as is', like `redis-config`
Best practices here utilizes single responsibility principle:
- do not build mega-all-included configs, it is hard to manage
- use a separate config for each app, big module or external service