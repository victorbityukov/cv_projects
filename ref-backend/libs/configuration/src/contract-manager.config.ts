import config from 'config';
import { ContractManagerInterface } from './interfaces';

export const contractManager = config.get<ContractManagerInterface>('contract-manager');

export const CONTRACT_MANAGER = {
  startMint: contractManager.startMint,
  earnPools: contractManager.earnPools,
  mintForContracts: contractManager.mintForContracts,
  claimRewardsForRoot: contractManager.claimRewardsForRoot,
  toVoters: contractManager.toVoters,
  getRewardStrategy: contractManager.getRewardStrategy,
};
