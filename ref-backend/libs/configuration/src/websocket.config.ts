import config from 'config';
import { WebsocketInterface } from './interfaces';

export const WEBSOCKET_CONFIG = config.get<WebsocketInterface>('websocket');
