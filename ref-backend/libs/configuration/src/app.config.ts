import { AppInterface } from '@app/configuration/interfaces/app.interface';
import path from 'path';

export const appConfig: AppInterface = {
  rootDir: path.resolve('./'),
};
