import config from 'config';
import { MongoInterface } from './interfaces';

export const MONGO_CONFIG = config.get<MongoInterface>('mongo').config;
