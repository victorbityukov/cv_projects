import { MongoConfigInterface } from './mongo-config.interface';
import { MongoOptionsInterface } from './mongo-options.interface';

export interface MongoInterface {
  readonly config: MongoConfigInterface;
  readonly options: MongoOptionsInterface;
}
