export interface RedisConfigInterface {
  readonly host: string;
  readonly port: number;
  flushAll: boolean;
}
