export interface CoinMarketCapInterface {
  readonly key: string;
  readonly header: string;
}
