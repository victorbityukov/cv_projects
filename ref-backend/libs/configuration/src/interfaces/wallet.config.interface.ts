export interface WalletConfigInterface {
  readonly address: string;
  readonly privateKey: string;
  readonly passPhrase: string;
}
