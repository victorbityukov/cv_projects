import { ContractsListInterface } from './contracts-config.interface';

export type ContractListName = keyof ContractsListInterface;

export interface ContractListenerConfigInterface {
  readonly contract: ContractListName | null;
}
