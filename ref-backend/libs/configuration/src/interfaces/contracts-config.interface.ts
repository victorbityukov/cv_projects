import { ContractConfigInterface } from './contract-config.interface';

export interface ContractsConfigInterface {
  readonly initial: boolean;
  readonly infuraUrl: string;
  readonly clientAddress: string;
  readonly list: ContractsListInterface;
}

export interface ContractsListInterface {
  readonly xbe: ContractConfigInterface;
  readonly mockXbe: ContractConfigInterface;
  readonly treasury: ContractConfigInterface;
  readonly controller: ContractConfigInterface;
  readonly xbeInflation: ContractConfigInterface;
  readonly referralProgram: ContractConfigInterface;
  readonly veXbe: ContractConfigInterface;
  readonly registry: ContractConfigInterface;
  readonly bonusCampaign: ContractConfigInterface;
  readonly mockLpSushi: ContractConfigInterface;
  readonly sushiVault: ContractConfigInterface;
  readonly sushiStrategy: ContractConfigInterface;
  readonly hiveVault: ContractConfigInterface;
  readonly hiveStrategy: ContractConfigInterface;
  readonly voting: ContractConfigInterface;
  readonly votingStakingRewards: ContractConfigInterface;
  readonly cvxCrvVault: ContractConfigInterface;
  readonly cvxCrvStrategy: ContractConfigInterface;
  readonly cvxStrategy: ContractConfigInterface;
  readonly cvxVault: ContractConfigInterface;
  readonly ironBankHiveVault: ContractConfigInterface;
  readonly ironBankHiveStrategy: ContractConfigInterface;
  readonly eurtHiveVault: ContractConfigInterface;
  readonly eurtHiveStrategy: ContractConfigInterface;
  readonly usdnHiveVault: ContractConfigInterface;
  readonly usdnHiveStrategy: ContractConfigInterface;
  readonly stEthHiveVault: ContractConfigInterface;
  readonly stEthHiveStrategy: ContractConfigInterface;

  readonly cvxCrvCurve: ContractConfigInterface;
  readonly ib3CrvCurve: ContractConfigInterface;
  readonly usdn3CrvCurve: ContractConfigInterface;
  readonly stEthCurve: ContractConfigInterface;
  readonly eurtCurve: ContractConfigInterface;
}
