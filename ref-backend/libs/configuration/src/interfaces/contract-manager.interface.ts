export interface ContractManagerInterface {
  readonly startMint: boolean;
  readonly earnPools: boolean;
  readonly mintForContracts: boolean;
  readonly claimRewardsForRoot: boolean;
  readonly toVoters: boolean;
  readonly getRewardStrategy: boolean;
}
