export interface MongoOptionsInterface {
  readonly useCreateIndex: boolean;
  readonly useNewUrlParser: boolean;
}
