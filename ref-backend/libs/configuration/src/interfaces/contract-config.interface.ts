export interface ContractConfigInterface {
  readonly address: string;
  readonly abiFile: string;
  readonly tags: string[];
}
