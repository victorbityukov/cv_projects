import config from 'config';
import { CoinMarketCapInterface } from '@app/configuration/interfaces';

export const CMC_CONFIG = config.get<CoinMarketCapInterface>('coinmarketcap');
