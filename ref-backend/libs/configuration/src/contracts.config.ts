import config from 'config';
import { ContractsConfigInterface } from './interfaces';

export const tokenBaseId = '0x0000000000000000000000000000000tokenbase';
export const pairBaseId = '0x00000000000000000000000000000000pairbase';

export const contractsConfig = config.get<ContractsConfigInterface>(
  'contracts',
);
