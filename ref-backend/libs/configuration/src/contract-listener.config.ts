import {
  ContractListName,
  ContractListenerConfigInterface,
} from './interfaces';

export const CONTRACT_LISTENER_CONFIG: ContractListenerConfigInterface = {
  contract: <ContractListName>process.env.CONTRACT || null,
};
