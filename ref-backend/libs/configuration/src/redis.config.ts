import config from 'config';
import { RedisConfigInterface } from './interfaces';

const redis = config.get<RedisConfigInterface>('redis');

export const REDIS_CONFIG = {
  host: redis.host,
  port: redis.port,
  flushAll: redis.flushAll,
  url: `redis://${redis.host}:${redis.port}`,
};
