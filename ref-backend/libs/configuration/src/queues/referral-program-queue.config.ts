import { JobsOptions } from 'bullmq';
import { QueueInterface } from '../interfaces/queue.interface';

const queueOptions: JobsOptions = {};

export const referralProgramQueueConfig: QueueInterface = {
  name: 'REFERRAL_PROGRAM_QUEUE',
  workerOptions: {
    concurrency: 1,
  },
  options: {
    defaultJobOptions: queueOptions,
  },
};
