import { QueueInterface } from '@app/configuration/interfaces';
import { JobsOptions } from 'bullmq';

const queueOptions: JobsOptions = {};

export const veXbeDepositQueueConfig: QueueInterface = {
  name: 'VE_XBE_DEPOSIT_QUEUE',
  workerOptions: {
    concurrency: 1,
  },
  options: {
    defaultJobOptions: queueOptions,
  },
};
