import { JobsOptions } from 'bullmq';
import { QueueInterface } from '../interfaces/queue.interface';

const queueOptions: JobsOptions = {};

export const registerUserQueueConfig: QueueInterface = {
  name: 'REGISTER_USER_QUEUE',
  workerOptions: {
    concurrency: 1,
  },
  options: {
    defaultJobOptions: queueOptions,
  },
};
