import { JobsOptions } from 'bullmq';
import { QueueInterface } from '@app/configuration/interfaces';

const queueOptions: JobsOptions = {};

export const veXbeWithdrawQueueConfig: QueueInterface = {
  name: 'VE_XBE_WITHDRAW_QUEUE',
  workerOptions: {
    concurrency: 1,
  },
  options: {
    defaultJobOptions: queueOptions,
  },
};
