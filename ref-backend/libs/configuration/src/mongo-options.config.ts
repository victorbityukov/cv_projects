import config from 'config';
import { MongoInterface } from './interfaces';

export const MONGO_OPTIONS = config.get<MongoInterface>('mongo').options;
