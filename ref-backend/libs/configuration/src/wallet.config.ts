import config from 'config';
import { WalletConfigInterface } from './interfaces';

export const WALLET_CONFIG: WalletConfigInterface = config.get<
WalletConfigInterface
>('wallet');
