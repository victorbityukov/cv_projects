import config from 'config';
import { NetworkInterface } from '@app/configuration/interfaces/network.interface';

export const networkConfig = config.get<NetworkInterface>('network');
