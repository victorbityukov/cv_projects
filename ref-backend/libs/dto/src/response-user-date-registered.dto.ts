import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_USER_DATE_REGISTERED } from '@app/swagger';

export class ResponseUserDateRegisteredDto {
  @ApiProperty(SWAGGER_USER_DATE_REGISTERED)
  readonly date: string | null;
}
