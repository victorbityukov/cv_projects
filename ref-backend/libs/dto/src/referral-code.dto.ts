import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { SWAGGER_USER_REFERRAL_CODE } from '@app/swagger';

export class ReferralCodeDto {
  @ApiProperty(SWAGGER_USER_REFERRAL_CODE)
  @IsString()
  readonly referralCode: string;
}
