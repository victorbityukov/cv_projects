import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_POOL_INFORMATION_TVL,
  SWAGGER_POOL_INFORMATION_APR,
  SWAGGER_POOL_INFORMATION_EARNED,
  SWAGGER_POOL_INFORMATION_DEPOSITS,
  SWAGGER_POOL_INFORMATION_POOL_NAME,
} from '@app/swagger';
import { PoolNameEnum } from '@app/constants';

export class ResponsePoolInformationDto {
  @ApiProperty(SWAGGER_POOL_INFORMATION_POOL_NAME)
  poolName?: PoolNameEnum;

  @ApiProperty(SWAGGER_POOL_INFORMATION_EARNED)
  earned: number;

  @ApiProperty(SWAGGER_POOL_INFORMATION_APR)
  apr: number;

  @ApiProperty(SWAGGER_POOL_INFORMATION_DEPOSITS)
  deposits: number;

  @ApiProperty(SWAGGER_POOL_INFORMATION_TVL)
  tvl?: number;

  potentialEarned?: number;
}
