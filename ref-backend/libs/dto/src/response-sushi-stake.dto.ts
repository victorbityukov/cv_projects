import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_SUSHI_AVERAGE_APR,
  SWAGGER_SUSHI_EARNED_USD,
} from '@app/swagger';

export class ResponseSushiStakeDto {
  @ApiProperty(SWAGGER_SUSHI_EARNED_USD)
  readonly earned_usd: number;

  @ApiProperty(SWAGGER_SUSHI_AVERAGE_APR)
  readonly average_apr: number;
}
