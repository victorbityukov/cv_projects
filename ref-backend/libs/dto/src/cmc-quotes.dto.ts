export class CmcQuotesDto {
  readonly status: {
    readonly timestamp: string;
    readonly error_code: number;
    readonly error_message: string | null;
    readonly elapsed: number;
    readonly credit_count: number;
    readonly notice: string | null;
  };

  readonly data: {
    [key: string]: {
      readonly id: number;
      readonly name: string;
      readonly symbol: string;
      readonly slug: string;
      readonly num_market_pairs: number;
      readonly date_added: string;
      readonly tags: string[];
      readonly max_supply: number | null;
      readonly circulating_supply: number | null;
      readonly total_supply: number;
      readonly is_active: number | null;
      readonly platform: null;
      readonly cmc_rank: number | null;
      readonly is_fiat: number;
      readonly last_updated: string;
      readonly quote: {
        [key: string]: {
          readonly price: number;
          readonly volume_24h: number;
          readonly percent_change_1h: number;
          readonly percent_change_24h: number;
          readonly percent_change_7d: number;
          readonly percent_change_30d: number;
          readonly percent_change_60d: number;
          readonly percent_change_90d: number;
          readonly market_cap: number;
          readonly last_updated: string;
        };
      };
    };
  };
}
