import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_COMMON_REFERRAL_CURRENCY,
  SWAGGER_COMMON_REFERRAL_FROM,
  SWAGGER_COMMON_REFERRAL_TITHE,
  SWAGGER_COMMON_REFERRAL_TO,
} from '@app/swagger';

export class CreateNodeDto {
  @ApiProperty(SWAGGER_COMMON_REFERRAL_FROM)
  readonly from: string;

  @ApiProperty(SWAGGER_COMMON_REFERRAL_TO)
  readonly to: string;

  @ApiProperty(SWAGGER_COMMON_REFERRAL_TITHE)
  readonly tithe: number;

  @ApiProperty(SWAGGER_COMMON_REFERRAL_CURRENCY)
  readonly currency: string;
}
