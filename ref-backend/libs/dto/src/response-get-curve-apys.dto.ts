import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_CURVE_APYS_CVXCRV,
  SWAGGER_CURVE_APYS_EURS,
  SWAGGER_CURVE_APYS_EURT,
  SWAGGER_CURVE_APYS_IB,
  SWAGGER_CURVE_APYS_STETH,
  SWAGGER_CURVE_APYS_USDN,
} from '@app/swagger';

export interface ResponseGetCurveApysInterface {
  apys: { [key: string]: Apy };
}

export interface Apy {
  baseApy: string;
  crvApy: number;
  crvBoost: number;
  additionalRewards: AdditionalReward[];
  crvPrice: number;
}

export interface AdditionalReward {
  name: string;
  apy: number;
}

export class ResponseGetCurveApysDto {
  @ApiProperty(SWAGGER_CURVE_APYS_IB)
  ib: Apy;

  @ApiProperty(SWAGGER_CURVE_APYS_USDN)
  usdn: Apy;

  @ApiProperty(SWAGGER_CURVE_APYS_STETH)
  steth: Apy;

  @ApiProperty(SWAGGER_CURVE_APYS_EURS)
  eurs: Apy;

  @ApiProperty(SWAGGER_CURVE_APYS_CVXCRV)
  cvxcrv: Apy;

  @ApiProperty(SWAGGER_CURVE_APYS_EURT)
  eurt: Apy;
}
