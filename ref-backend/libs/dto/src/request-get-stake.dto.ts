import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { SWAGGER_TOKEN_SYMBOL } from '@app/swagger';
import { CurrencyTypeEnum } from '@app/constants';
import { AddressDto } from '@app/dto';

export class RequestGetStakeDto extends AddressDto {
  @ApiProperty(SWAGGER_TOKEN_SYMBOL)
  @IsOptional()
  @IsString()
  @IsEnum(CurrencyTypeEnum)
  readonly token?: CurrencyTypeEnum;
}
