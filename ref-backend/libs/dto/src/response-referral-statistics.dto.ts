import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_COMMON_REFERRAL_STATISTICS_DEPOSITS,
  SWAGGER_COMMON_REFERRAL_STATISTICS_DEPOSITS_LIST_BY_TIER,
  SWAGGER_COMMON_REFERRAL_STATISTICS_DEPOSITS_SUM,
  SWAGGER_COMMON_REFERRAL_STATISTICS_MEMBERS,
  SWAGGER_COMMON_REFERRAL_STATISTICS_MEMBERS_AMOUNT,
  SWAGGER_COMMON_REFERRAL_STATISTICS_MEMBERS_LIST_BY_TIER,
  SWAGGER_COMMON_REFERRAL_STATISTICS_REFERRAL_HITS,
  SWAGGER_COMMON_SUM_CLAIMABLE_REWARDS,
} from '@app/swagger';

export class ResponseReferralStatisticsMembersDto {
  @ApiProperty(SWAGGER_COMMON_REFERRAL_STATISTICS_MEMBERS_LIST_BY_TIER)
  readonly list: number[];

  @ApiProperty(SWAGGER_COMMON_REFERRAL_STATISTICS_MEMBERS_AMOUNT)
  readonly totalAmount: number;
}

export class ResponseReferralStatisticsDepositsDto {
  @ApiProperty(SWAGGER_COMMON_REFERRAL_STATISTICS_DEPOSITS_LIST_BY_TIER)
  readonly list: number[];

  @ApiProperty(SWAGGER_COMMON_REFERRAL_STATISTICS_DEPOSITS_SUM)
  readonly totalSum: number;
}

export class ResponseReferralStatisticsDto {
  @ApiProperty(SWAGGER_COMMON_REFERRAL_STATISTICS_MEMBERS)
  readonly members: ResponseReferralStatisticsMembersDto;

  @ApiProperty(SWAGGER_COMMON_REFERRAL_STATISTICS_DEPOSITS)
  readonly deposits: ResponseReferralStatisticsDepositsDto;

  @ApiProperty(SWAGGER_COMMON_SUM_CLAIMABLE_REWARDS)
  readonly claimable: number;

  @ApiProperty(SWAGGER_COMMON_REFERRAL_STATISTICS_REFERRAL_HITS)
  readonly referralHits: number;
}
