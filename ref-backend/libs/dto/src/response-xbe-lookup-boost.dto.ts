import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_XBE_AVG_LOCK_TIME,
  SWAGGER_XBE_TOTAL_VOTES_LOCKED,
  SWAGGER_XBE_TOTAL_VOTES_SHARE,
  SWAGGER_XBE_VE_TOTAL,
} from '@app/swagger';

export class ResponseXbeLookupBoostDto {
  @ApiProperty(SWAGGER_XBE_TOTAL_VOTES_LOCKED)
  readonly total_votes_locked: number;

  @ApiProperty(SWAGGER_XBE_TOTAL_VOTES_SHARE)
  readonly total_locked_xbe_share: number;

  @ApiProperty(SWAGGER_XBE_VE_TOTAL)
  readonly total_locked_xbe_ve_total: number;

  @ApiProperty(SWAGGER_XBE_AVG_LOCK_TIME)
  readonly total_locked_xbe_avg_lock_time: number;
}
