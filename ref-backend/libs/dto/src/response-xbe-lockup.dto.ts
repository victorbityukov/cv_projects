import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_XBE_APY,
  SWAGGER_XBE_CIRCULATING_SUPPLY,
  SWAGGER_XBE_DAILY_APY,
  SWAGGER_XBE_TOTAL_CIRCULATING_SUPPLY,
  SWAGGER_XBE_TOTAL_LOCKED,
  SWAGGER_XBE_TOTAL_LOCKED_SHARE,
} from '@app/swagger';

export class ResponseXbeLockupDto {
  @ApiProperty(SWAGGER_XBE_APY)
  readonly apy: number;

  @ApiProperty(SWAGGER_XBE_DAILY_APY)
  readonly daily_apy: number;

  @ApiProperty(SWAGGER_XBE_CIRCULATING_SUPPLY)
  readonly circulating_supply: number;

  @ApiProperty(SWAGGER_XBE_TOTAL_CIRCULATING_SUPPLY)
  readonly total_circulating_supply: number;

  @ApiProperty(SWAGGER_XBE_TOTAL_LOCKED)
  readonly total_locked_xbe: number;

  @ApiProperty(SWAGGER_XBE_TOTAL_LOCKED_SHARE)
  readonly total_locked_xbe_share: number;
}
