import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_TOKEN_CURRENCY,
  SWAGGER_TOKEN_NETWORK,
  SWAGGER_TOKEN_QUANTITY,
  SWAGGER_TOKEN_QUOTE,
  SWAGGER_TOKEN_SYMBOL,
  SWAGGER_TOKEN_TIMESTAMP,
  SWAGGER_TOKEN_VENUE,
} from '@app/swagger';

export class ResponseRatesDto {
  @ApiProperty(SWAGGER_TOKEN_SYMBOL)
  readonly token: string;

  @ApiProperty(SWAGGER_TOKEN_NETWORK)
  readonly network: string;

  @ApiProperty(SWAGGER_TOKEN_VENUE)
  readonly venue: string;

  @ApiProperty(SWAGGER_TOKEN_QUOTE)
  readonly quote: string;

  @ApiProperty(SWAGGER_TOKEN_QUANTITY)
  readonly quantity: string;

  @ApiProperty(SWAGGER_TOKEN_CURRENCY)
  readonly currency: string;

  @ApiProperty(SWAGGER_TOKEN_TIMESTAMP)
  readonly timestamp: Date;
}
