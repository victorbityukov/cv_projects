export class SushiApiAction {
  readonly action: string;

  readonly chain: string;

  readonly number_of_results: number;
}

export class SushiPair {
  readonly Pair_ID: string;

  readonly Token_1_contract: string;

  readonly Token_1_symbol: string;

  readonly Token_1_name: string;

  readonly Token_1_decimals: number;

  readonly Token_1_price: number;

  readonly Token_1_reserve: number;

  readonly Token_1_derivedETH: number;

  readonly Token_2_contract: string;

  readonly Token_2_symbol: string;

  readonly Token_2_name: string;

  readonly Token_2_decimals: number;

  readonly Token_2_price: number;

  readonly Token_2_reserve: number;

  readonly Token_2_derivedETH: number;
}
