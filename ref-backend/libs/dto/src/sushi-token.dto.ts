import { NetworkEnum, VenueEnum } from '@app/constants';

export class SushiTokenDto {
  readonly _id: string;

  readonly symbol: string;

  readonly name: string;

  readonly network: NetworkEnum;

  readonly decimals: number;

  readonly provider: VenueEnum;

  readonly sushi_pairs: {
    readonly _id: string;

    readonly tokenA: string;

    readonly tokenB: string;
  };
}
