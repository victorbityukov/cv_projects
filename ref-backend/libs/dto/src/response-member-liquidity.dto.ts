import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_USER_DATE_REGISTERED, SWAGGER_USER_LIQUIDITY, SWAGGER_USER_REFERRAL_CODE } from '@app/swagger';

export class ResponseMemberLiquidityDto {
  @ApiProperty(SWAGGER_USER_DATE_REGISTERED)
  readonly date: string;

  @ApiProperty(SWAGGER_USER_REFERRAL_CODE)
  readonly referralCode: string;

  @ApiProperty(SWAGGER_USER_LIQUIDITY)
  readonly liquidity: number;
}
