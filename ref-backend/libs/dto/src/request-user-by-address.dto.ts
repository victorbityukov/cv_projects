import { SWAGGER_USER_ID } from '@app/swagger';
import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class RequestUserByAddressDto {
  @ApiProperty(SWAGGER_USER_ID)
  @IsString()
  readonly address: string;
}
