import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_DAILY_YIELD,
  SWAGGER_STAKED_ASSETS_YIELD,
  SWAGGER_TOTAL_CLAIMABLE,
  SWAGGER_TOTAL_COMBINED_APY,
  SWAGGER_TOTAL_CRV_EARNED,
  SWAGGER_TOTAL_CVX_CRV_EARNED,
  SWAGGER_TOTAL_CVX_EARNED,
  SWAGGER_TOTAL_DEPOSITS,
  SWAGGER_TOTAL_XBE_EARNED,
} from '@app/swagger';

export class ResponseGetStatisticsDto {
  @ApiProperty(SWAGGER_TOTAL_CLAIMABLE)
  totalClaimable: number;

  @ApiProperty(SWAGGER_TOTAL_DEPOSITS)
  totalDeposits: number;

  @ApiProperty(SWAGGER_TOTAL_COMBINED_APY)
  totalCombinedApy: number;

  @ApiProperty(SWAGGER_TOTAL_CRV_EARNED)
  totalCrvEarned: number;

  @ApiProperty(SWAGGER_TOTAL_CVX_EARNED)
  totalCvxEarned: number;

  @ApiProperty(SWAGGER_TOTAL_CVX_CRV_EARNED)
  totalCvxCrvEarned: number;

  @ApiProperty(SWAGGER_TOTAL_XBE_EARNED)
  totalXbeEarned: number;

  @ApiProperty(SWAGGER_STAKED_ASSETS_YIELD)
  stakedAssetsYield: number;

  @ApiProperty(SWAGGER_DAILY_YIELD)
  dailyYield: number;
}
