import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional } from 'class-validator';
import { CurrencyTypeEnum, NetworkEnum, VenueEnum } from '@app/constants';
import {
  SWAGGER_TOKEN_CURRENCY,
  SWAGGER_TOKEN_NETWORK,
  SWAGGER_TOKEN_SYMBOL,
  SWAGGER_TOKEN_VENUE,
} from '@app/swagger';

export class RequestRatesDto {
  @ApiProperty(SWAGGER_TOKEN_SYMBOL)
  @IsEnum(CurrencyTypeEnum)
  readonly token: CurrencyTypeEnum;

  @ApiProperty(SWAGGER_TOKEN_CURRENCY)
  @IsEnum(CurrencyTypeEnum)
  @IsOptional()
  readonly currency?: CurrencyTypeEnum;

  @ApiProperty(SWAGGER_TOKEN_NETWORK)
  @IsEnum(NetworkEnum)
  @IsOptional()
  readonly network?: NetworkEnum;

  @ApiProperty(SWAGGER_TOKEN_VENUE)
  @IsEnum(VenueEnum)
  @IsOptional()
  readonly venue?: VenueEnum;
}
