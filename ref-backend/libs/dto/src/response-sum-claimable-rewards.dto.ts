import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_COMMON_SUM_CLAIMABLE_REWARDS } from '@app/swagger';

export class ResponseSumClaimableRewardsDto {
  @ApiProperty(SWAGGER_COMMON_SUM_CLAIMABLE_REWARDS)
  readonly sum: number;
}
