import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_USER_ID } from '@app/swagger';
import { IsString } from 'class-validator';

export class AddressDto {
  @ApiProperty(SWAGGER_USER_ID)
  @IsString()
  readonly address: string;
}
