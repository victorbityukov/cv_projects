import { SWAGGER_USER_REFERRAL_CODE, SWAGGER_USER_ID } from '@app/swagger';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class CreateUserDto {
  @ApiProperty(SWAGGER_USER_ID)
  @IsString()
  readonly address: string;

  @ApiProperty(SWAGGER_USER_REFERRAL_CODE)
  @IsOptional()
  @IsString()
  readonly referralCode?: string;
}
