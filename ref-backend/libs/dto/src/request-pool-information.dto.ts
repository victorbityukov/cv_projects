import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { SWAGGER_POOL_INFORMATION_POOL_NAME } from '@app/swagger';
import { PoolNameEnum } from '@app/constants';
import { AddressDto } from '@app/dto';

export class RequestPoolInformationDto extends AddressDto {
  @ApiProperty(SWAGGER_POOL_INFORMATION_POOL_NAME)
  @IsOptional()
  @IsString()
  @IsEnum(PoolNameEnum)
  readonly poolName?: PoolNameEnum;
}
