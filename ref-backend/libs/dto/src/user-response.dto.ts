import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_USER_ID,
  SWAGGER_USER_PARENT_ID,
  SWAGGER_USER_REFERRAL_CODE,
} from '@app/swagger';

export class UserResponseDto {
  @ApiProperty(SWAGGER_USER_ID)
  readonly address: string;

  @ApiProperty(SWAGGER_USER_REFERRAL_CODE)
  readonly referralCode: string;

  @ApiProperty(SWAGGER_USER_PARENT_ID)
  readonly parentId?: string;
}
