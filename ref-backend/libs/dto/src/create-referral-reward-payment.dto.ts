import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_COMMON_REFERRAL_CURRENCY,
  SWAGGER_COMMON_REFERRAL_CUSTOM,
  SWAGGER_COMMON_REFERRAL_FROM,
  SWAGGER_COMMON_REFERRAL_TITHE,
  SWAGGER_COMMON_REFERRAL_TO,
  SWAGGER_COMMON_REFERRAL_VALUE,
} from '@app/swagger';

export class CreatePaymentDto {
  @ApiProperty(SWAGGER_COMMON_REFERRAL_FROM)
  readonly from: string;

  @ApiProperty(SWAGGER_COMMON_REFERRAL_TO)
  readonly to: string;

  @ApiProperty(SWAGGER_COMMON_REFERRAL_TITHE)
  readonly tithe: number;

  @ApiProperty(SWAGGER_COMMON_REFERRAL_CURRENCY)
  readonly currency: string;

  @ApiProperty(SWAGGER_COMMON_REFERRAL_VALUE)
  readonly value: string;

  @ApiProperty(SWAGGER_COMMON_REFERRAL_CUSTOM)
  readonly custom: string;
}
