export interface UserMockInterface {
  readonly _id: string;
  readonly referralCode: string;
  readonly parentId?: string;
}
