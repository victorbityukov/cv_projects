import { LeanDocument } from 'mongoose';
import { Token } from '@app/dbo';
import { NetworkEnum, VenueEnum } from '@app/constants';

export const tokenMocks: LeanDocument<Token>[] = [
  {
    _id: '0x0000000000000000000000000000000000000000',
    name: 'Ethereum',
    decimals: 18,
    symbol: 'ETH',
    network: NetworkEnum.ETH_M,
    is_stablecoin: false,
    provider: VenueEnum.MOCK,
    sushi_pairs: [],
  },
  {
    _id: '0x8ce5f9558e3e0cd7de8be15a93dffabec83e314e',
    name: 'Mocked XBE by sfxDx',
    decimals: 18,
    symbol: 'mockXBE',
    network: NetworkEnum.ETH_R,
    is_stablecoin: false,
    provider: VenueEnum.MOCK,
    sushi_pairs: [],
  },
];
