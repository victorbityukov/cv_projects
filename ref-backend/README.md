# Readme

## Project
Please, define here what is your project about. 

## Starter
Custom fast starter for `Nest.js` + `TypeORM`(`Postgres`) + `Redis`. This project aims to create a versatile starter kit, that can be ready for continuos integration as early as possible in development process. It also might help you as a fast introduction to `Nest.js`. For each app and lib there is a readme file included to explain why and how to use it.  The `example` app is also here to show some code style best practices with `Nest.js` stack.
Also powered by:
- `docker`, `docker-compose` as a deployment standard
- `config` library for managing env params
- `airbnb-typescript/base` for best linting we can found
- custom `error-handler` for removing most of `catch-error>(optionally)log-error>throw-exception` boilerplate code.

## Setup
### Part one: specify project vars and libs for your needs
0. Inspect this readme file, please. I've spent some time to write it for you.
1. If needed, redefine linter rules in `.eslintrc.js`
2. Create your own `.env` and `docker-compose-local.yml` and define external ports.
3. Remove unused dependencies if you see it or add yours.
4. Manually update libs versions in `package.json`, remove/add libraries you (don't) need.
5. Define your project name in `package.json` `name`.
6. Define the image name `package.json` in `scripts/docker:build` and `image` tag in `ci_cd/docker_compose.yml`. In most cases, it matches the project name.
7. `yarn install`

### Part two: start your development process with defining configs and data models
8. Examine the `config` folder and `docker-compose` files to redefine values for database, redis, ports. For local deployment, you can also create `local.yml` in `config` and redefine any values you need. 
9. For newly added config variables you need to define objects in `libs/configuration` to use it in your projects
10. Define your database model in `libs/dbo`

## Environment
### `.env` file and `config/`
> According to my development experience, using Node.js `config` lib is the more efficient and transparent way to work with process parameters, rather than `ConfigService` from `Nest.js`. 
1. Most of environment variables to the project can be passed via `config/*.yml` files. But we also need to select mode, that redefines `default.yml` settings. The `NODE_ENV` param should be `development`, `staging`, `production`. 
2. If `local.yml`  exists, it automatically redefines params for your local build. That's why `config/local.yml` is added to `gitignore` and `dockerignore`. My `local.yml` example for working with `docker-compose-local.yml` looks like this:
```
db:
  host: 'localhost'
  port: 15432

redis:
  host: 'localhost'
  port: 16379
```
3. In `.env` you should also define external ports for your `ci_cd/docker-compose.yml`


### `docker-compose-local.yml`
This file helps with fast-deployment of db, redis and other services (like nginx) locally. It can be the great help when each new developer joins team. When this set of containers is running, you can manually start your apps with `yarn start:dev %app-name%` and comfortably monitor the output in console. You can also fast turn it on via `yarn docker:env:up` or down via `yarn docker:env:down` commands.

### `Dockerfile`
Do not forget to add your new apps in dockerfile

### `ci_cd/docker-compose.yml`
This is the main compose file, containing all services that should be deployed in development/stage/production servers. We are using single file for testing consistency of whole project to exclude such human-ocurred errors like wrong settings for containers.

### `tsconfig.json`
We are mostly develop backend services that working with blockchain, so some TS specifics below are might not be so useful for your project:
```
    "esModuleInterop": true,
    "strict": true,
    "strictPropertyInitialization": false,
```

### Dependencies in `package.json`
Most useful deps are already included
- db: `pg`, `@nestjs/typeorm`, `typeorm`,
- redis: `@nestjs-modules/ioredis`, `ioredis`, `@nestjs-modules/ioredis`
- auth: `passport`, `passport-jwt`, `passport-local`, `@nestjs/jwt`, `@nestjs/passport`, `@types/passport-jwt`, `@types/passport-local`, `bcrypt`, `@types/bcrypt`
- websockets: `ws`, `@types/ws`, `nestjs/platform-socket.io`, `@nestjs/websockets`, 
- email: `nodemailer`, `@types/nodemailer`,
- short codes lib: `nanoid`,
- common usage: `bignumber.js`, `lodash`, `@types/lodash`,
- lint & pre-commit: `eslint`, `eslint-config-airbnb-typescript`, `eslint-plugin-import`, `husky`, `lint-staged`, `prettier`,

Also, you can see this warnings during `yarn install`
```
warning "eslint-config-airbnb-typescript > eslint-config-airbnb@18.2.1" has unmet peer dependency "eslint-plugin-jsx-a11y@^6.4.1".
warning "eslint-config-airbnb-typescript > eslint-config-airbnb@18.2.1" has unmet peer dependency "eslint-plugin-react@^7.21.5".
warning "eslint-config-airbnb-typescript > eslint-config-airbnb@18.2.1" has unmet peer dependency "eslint-plugin-react-hooks@^4 || ^3 || ^2.3.0 || ^1.7.0".
```
Unfortunately, this behavior okay, `eslint-config-airbnb-typescript` lib wants you to install useless for backend React dependencies. There is several custom forks that solves this problem, but in the longrun they can become outdated.

## Apps
Please, include here your apps while your development process is in progress.
- [bench](apps/bench/src/readme.md) a place for your experiments
- [initial-loader](apps/initial-loader/src/readme.md) service module with db `sync=true` that rewrites your database and add predefined values. Using this app in your infrastructure makes your development process faster in early stages, but later on it is better to switch to database migrations model for avoiding any potential data losses.


## Libs
Please, include here your custom project libraries (not node_modules libraries) while your development process is in progress.
- [configuration](libs/configuration/src/readme.md) wrapper for `config` lib to represent your process variables during runtime
- [dao](libs/dao/src/readme.md) Data Access Objects. Place for your ORM queries implementations.
- [dbo](libs/dbo/src/readme.md) Database Objects (entities)
- [error-handler](libs/error-handler/src/readme.md) Boilerplate replacer for `catch` block, it logs unexpected errors and throws exceptions.

## Conclusions
