FROM node:14.17.4-alpine3.14 as builder
WORKDIR /app

RUN npm install -g @nestjs/cli

COPY package.json yarn.lock ./
RUN yarn install

COPY . .

ARG services

RUN for service in $services; do nest build $service; done

FROM node:14.17.4-alpine3.14 as executor
WORKDIR /app
COPY --from=builder /app .