import { Module, Provider } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RedisModule } from '@nestjs-modules/ioredis';
import { BullModule } from '@anchan828/nest-bullmq';
import {
  MONGO_CONFIG,
  MONGO_OPTIONS,
  REDIS_CONFIG,
  veXbeDepositQueueConfig,
  referralProgramQueueConfig,
  registerUserQueueConfig,
  veXbeWithdrawQueueConfig,
} from '@app/configuration';
import { Web3ProviderModule } from '@app/web3-provider';
import { WssModule } from '@app/wss';
import {
  User,
  UserSchema,
  Token,
  TokenSchema,
  ReferralRewardNode,
  ReferralRewardNodeSchema,
  ReferralRewardPayment,
  ReferralRewardPaymentSchema,
  XBELockup,
  XBELockupSchema,
  ContractCollectionSchema,
  ContractCollection,
} from '@app/dbo';
import { ContractEnum } from '@app/constants';
import { ReferralProgramContract, VeXbeContract } from '@app/contracts';
import { ReferralProgramListener } from './listeners/referral-program.listener';
import {
  ReferralEvents,
  ReferralWorker,
  VeXbeDepositWorker,
  RegisterUserEvents,
  RegisterUserWorker,
  VeXbeWithdrawWorker, 
} from './worker';
import { VeXbeListener } from './listeners/ve-xbe.listener';

type ContractListenerProviders =
  | ReferralProgramListener
  | RegisterUserEvents
  | RegisterUserWorker
  | ReferralEvents
  | ReferralWorker
  | VeXbeDepositWorker
  | VeXbeWithdrawWorker;

let providers: Provider<ContractListenerProviders>[] = [];

const referralProgram = [
  ReferralProgramListener,
  ReferralProgramContract,
  RegisterUserEvents,
  RegisterUserWorker,
  ReferralEvents,
  ReferralWorker,
];

const veXbe = [VeXbeListener, VeXbeDepositWorker, VeXbeContract, VeXbeWithdrawWorker];

if (process.env.CONTRACT === ContractEnum.REFERRAL_PROGRAM) {
  providers = [...providers, ...referralProgram];
}

if (process.env.CONTRACT === ContractEnum.VE_XBE) {
  providers = [...providers, ...veXbe];
}

@Module({
  imports: [
    MongooseModule.forRoot(MONGO_CONFIG.connection_string, MONGO_OPTIONS),
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Token.name, schema: TokenSchema },
      { name: XBELockup.name, schema: XBELockupSchema },
      { name: ReferralRewardNode.name, schema: ReferralRewardNodeSchema },
      { name: ContractCollection.name, schema: ContractCollectionSchema },
      { name: ReferralRewardPayment.name, schema: ReferralRewardPaymentSchema },
    ]),
    RedisModule.forRoot({
      config: REDIS_CONFIG,
    }),
    Web3ProviderModule,
    BullModule.forRoot({
      options: {
        connection: {
          host: REDIS_CONFIG.host,
          port: REDIS_CONFIG.port,
        },
      },
    }),
    BullModule.registerQueue({
      queueName: referralProgramQueueConfig.name,
      options: referralProgramQueueConfig.options,
    }),
    BullModule.registerQueue({
      queueName: registerUserQueueConfig.name,
      options: registerUserQueueConfig.options,
    }),
    BullModule.registerQueue({
      queueName: veXbeDepositQueueConfig.name,
      options: veXbeDepositQueueConfig.options,
    }),
    BullModule.registerQueue({
      queueName: veXbeWithdrawQueueConfig.name,
      options: veXbeWithdrawQueueConfig.options,
    }),
    WssModule,
  ],
  providers,
})
export class ContractListenerModule {}
