export class RewardsClaimedResponseInterface {
  readonly user: string;

  readonly tokens: string[];

  readonly amounts: string[];

  readonly contract?: string;

  readonly operation?: string;
}
