import { DepositTypeEnum } from '@app/constants';

export interface DepositResponseValues {
  // '0': string, // '0xe2ef933B971e9e4Bfbcec64Df858e1c65faCf4Cf',
  // '1': string, // '1000000000000000000',
  // '2': string, // '1628726400',
  // '3': string, // '2',
  // '4': string, // '1627260445',
  provider: string; // '0xe2ef933B971e9e4Bfbcec64Df858e1c65faCf4Cf',
  value: string; // '1000000000000000000',
  locktime: string; // '1628726400',
  _type: DepositTypeEnum; // '2',
  ts: string; // '1627260445'
}

export interface WithdrawResponseValues {
  provider: string; // '0xe2ef933B971e9e4Bfbcec64Df858e1c65faCf4Cf',
  value: string; // '1000000000000000000',
  ts: string; // '1627260445'
}

export enum VeXbeEventsTypes {
  ApplyOwnership = 'ApplyOwnership',
  CommitOwn = 'CommitOwn',
  Deposit = 'Deposit',
  Supply = 'Supply',
  Withdraw = 'Withdraw',
  allEvents = 'allEvents',
}

export interface ContractEventResponseInterface {
  removed: boolean; // false,
  logIndex: number; // 15,
  transactionIndex: number; // 16,
  transactionHash: string; // '0x29e8fa96fbf0ed324c54af2f8d91dde0157ed8f6f00da15680b56ae9151a85f2',
  blockHash: string; // '0x7d72c632d65040297b99c26073a68f8416f9a87f463dea4f1af4f5a73e99d812',
  blockNumber: number; // 9001449,
  address: string; // '0x1483C5306BfeE5746D67E2d04C5e31F44bB242Ee',
  id: string; // 'log_a91b705b',
  returnValues: DepositResponseValues | WithdrawResponseValues;
  event: VeXbeEventsTypes;
  signature: string; // '0x4566dfc29f6f11d13a418c26a02bef7c28bae749d4de47e4e6a7cddea6730d59',
  raw: {
    // eslint-disable-next-line max-len
    // '0x0000000000000000000000000000000000000000000000000de0b6b3a764000000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000060fe061d',
    data: string;
    // ['0x4566dfc29f6f11d13a418c26a02bef7c28bae749d4de47e4e6a7cddea6730d59']
    topics: string[];
  };
}
