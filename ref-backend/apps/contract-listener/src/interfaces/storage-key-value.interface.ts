export interface StorageKeyValueInterface {
  [symbol: string]: string;
}
