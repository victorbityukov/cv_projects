export { RegisterUserResponseInterface } from './register-user-response.interface';
export { RewardsReceivedResponseInterface } from './rewards-received-response.interface';
export { RewardsClaimedResponseInterface } from './rewards-claimed-response.interface';
export { StorageKeyValueInterface } from './storage-key-value.interface';
export {
  ContractEventResponseInterface,
  DepositResponseValues,
  VeXbeEventsTypes,
  WithdrawResponseValues,
} from './ve-xbe-events.interfaces';
