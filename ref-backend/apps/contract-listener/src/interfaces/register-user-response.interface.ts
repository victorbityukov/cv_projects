export class RegisterUserResponseInterface {
  readonly user: string;

  readonly referrer?: string;

  readonly contract?: string;

  readonly operation?: string;
}
