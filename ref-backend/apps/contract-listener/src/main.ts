import { NestFactory } from '@nestjs/core';
import { ContractListenerModule } from './contract-listener.module';

async function bootstrap() {
  const app = await NestFactory.create(ContractListenerModule);

  const PORT = process.env.PORT || 5000;
  await app.listen(PORT);
}

bootstrap();
