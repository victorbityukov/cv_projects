import { Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { WebSocketServer } from '@nestjs/websockets';
import {
  BullQueueInject,
  BullWorker,
  BullWorkerProcess,
} from '@anchan828/nest-bullmq';
import { Job, Queue } from 'bullmq';
import { Server } from 'ws';
import { Model } from 'mongoose';
import { Redis } from 'ioredis';
import { InjectRedis } from '@nestjs-modules/ioredis';
import nanoid from 'nanoid';

import { SHORT_CODE_LENGTH, TithesEnum, WsEventEnum } from '@app/constants';
import { WssAdapter } from '@app/wss';
import { ReferralRewardNode, Token, User } from '@app/dbo';
import { registerUserQueueConfig, WEBSOCKET_CONFIG } from '@app/configuration';
import { RegisterUserResponseInterface } from '../interfaces';

@BullWorker({ queueName: registerUserQueueConfig.name })
export class RegisterUserWorker {
  private readonly logger = new Logger(RegisterUserWorker.name, { timestamp: true });

  @WebSocketServer()
  private server: Server;

  constructor(
    @BullQueueInject(registerUserQueueConfig.name)
    private readonly registerUserQueue: Queue<
    RegisterUserResponseInterface,
    void
    >,
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
    @InjectModel(ReferralRewardNode.name)
    private readonly referralRewardNodeModel: Model<ReferralRewardNode>,
    @InjectModel(Token.name)
    private readonly tokenModel: Model<Token>,
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly wssAdapter: WssAdapter,
  ) {
    this.server = this.wssAdapter.create(WEBSOCKET_CONFIG.registration_port);

    this.server.on(WsEventEnum.registration, (message: string) => {
      const json = JSON.stringify({
        type: WsEventEnum.message_registration,
        message,
      });
      this.server.clients.forEach((client) => client.send(json));
    });
  }

  @BullWorkerProcess(registerUserQueueConfig.workerOptions)
  public async process(job: Job<RegisterUserResponseInterface, void>) {
    try {
      const { data } = job;
      const { user, referrer } = data;
      const parent: User | null = await this.userModel.findOne({
        _id: referrer,
      });

      if (!parent) {
        throw new Error(
          `${RegisterUserWorker.name}:not parent:_id=${referrer}`,
        );
      }

      const exists = await this.userModel.findById({ _id: user });
      if (exists) {
        throw new Error(
          `${RegisterUserWorker.name}:user already exists:_id= ${user}`,
        );
      }

      const child = await this.userModel.create({
        _id: user,
        parentId: parent._id,
        referralCode: `XBE-${nanoid.nanoid(SHORT_CODE_LENGTH).toUpperCase()}`,
      });

      await child.save();

      const nodes = [];
      let titheRootFather = TithesEnum.FATHER;
      const grandfather = await this.userModel.findOne({
        _id: parent.parentId,
      });
      if (!grandfather) {
        titheRootFather
          += TithesEnum.GRANDFATHER + TithesEnum.GREAT_GRANDFATHER;
      }
      const fatherNode = await this.referralRewardNodeModel.create({
        from: child._id,
        to: parent._id,
        tithe: titheRootFather,
      });
      nodes.push(fatherNode);
      if (grandfather) {
        let titheRootGrandfather = TithesEnum.GRANDFATHER;
        const greatGrandFather = await this.userModel.findOne({
          _id: grandfather.parentId,
        });
        if (!greatGrandFather) {
          titheRootGrandfather += TithesEnum.GREAT_GRANDFATHER;
        }
        const grandFatherNode = await this.referralRewardNodeModel.create({
          from: child._id,
          to: grandfather._id,
          tithe: titheRootGrandfather,
        });
        nodes.push(grandFatherNode);
        if (greatGrandFather) {
          const greatGrandFatherNode = await this.referralRewardNodeModel.create(
            {
              from: child._id,
              to: greatGrandFather._id,
              tithe: TithesEnum.GREAT_GRANDFATHER,
            },
          );
          nodes.push(greatGrandFatherNode);
        }
      }
      const { _id: address, referralCode, parentId } = child;
      this.server.emit(WsEventEnum.registration, {
        address,
        referralCode,
        parentId,
      });
    } catch (error) {
      await job.log(`error: ${error.message}`);
      this.logger.error(error);
    }
  }
}
