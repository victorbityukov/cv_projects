import { BullQueueEvents } from '@anchan828/nest-bullmq';
import { referralProgramQueueConfig } from '@app/configuration';

@BullQueueEvents({ queueName: referralProgramQueueConfig.name })
export class ReferralEvents {
  /**
   * Any event from queue
   * @BullQueueEventProcess('completed')
   * public async completed(job: Job): Promise<void> {
   * this.logger.log(job);
   * }
   */
}
