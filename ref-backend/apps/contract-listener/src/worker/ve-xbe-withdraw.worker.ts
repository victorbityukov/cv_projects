import { Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { InjectRedis } from '@nestjs-modules/ioredis';
import {
  BullQueueInject,
  BullWorker,
  BullWorkerProcess,
} from '@anchan828/nest-bullmq';
import { Job, Queue } from 'bullmq';
import { Model } from 'mongoose';
import { Redis } from 'ioredis';

import { veXbeWithdrawQueueConfig } from '@app/configuration';
import { XBELockup } from '@app/dbo';
import { WithdrawResponseValues } from '../interfaces';

@BullWorker({ queueName: veXbeWithdrawQueueConfig.name })
export class VeXbeWithdrawWorker {
  private readonly logger = new Logger(VeXbeWithdrawWorker.name, { timestamp: true });

  constructor(
    @BullQueueInject(veXbeWithdrawQueueConfig.name)
    private readonly veXbeWithdrawQueue: Queue<WithdrawResponseValues, void>,
    @InjectModel(XBELockup.name)
    private readonly xbeLockupModel: Model<XBELockup>,
    @InjectRedis()
    private readonly redisService: Redis,
  ) {}

  @BullWorkerProcess(veXbeWithdrawQueueConfig.workerOptions)
  async process({
    data,
    log,
  }: Job<WithdrawResponseValues, void>): Promise<void> {
    try {
      const { provider } = data;

      const existXbeLockupRow = await this.xbeLockupModel.findOne({
        userAddress: provider,
      });

      if (existXbeLockupRow) {
        existXbeLockupRow.withdrawn = true;
        await existXbeLockupRow.save();
      }
    } catch (errorOrException) {
      await log(`error: ${errorOrException}`);
      this.logger.error(`error: ${errorOrException}`);
    }
  }
}
