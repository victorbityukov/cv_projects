import { Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Model } from 'mongoose';
import { Redis } from 'ioredis';
import { Job, Queue } from 'bullmq';
import {
  BullQueueInject,
  BullWorker,
  BullWorkerProcess,
} from '@anchan828/nest-bullmq';
import { referralProgramQueueConfig } from '@app/configuration';
import { ReferralRewardNode, ReferralRewardPayment, Token } from '@app/dbo';
import {
  RewardsClaimedResponseInterface,
  RewardsReceivedResponseInterface,
  StorageKeyValueInterface,
} from '../interfaces';

@BullWorker({ queueName: referralProgramQueueConfig.name })
export class ReferralWorker {
  private readonly logger = new Logger(ReferralWorker.name, { timestamp: true });

  constructor(
    @BullQueueInject(referralProgramQueueConfig.name)
    private readonly referralProgramQueue: Queue<
    RewardsReceivedResponseInterface | RewardsClaimedResponseInterface,
    void
    >,
    @InjectModel(ReferralRewardNode.name)
    private readonly referralRewardNodeModel: Model<ReferralRewardNode>,
    @InjectModel(ReferralRewardPayment.name)
    private readonly referralRewardPaymentModel: Model<ReferralRewardPayment>,
    @InjectModel(Token.name)
    private readonly tokenModel: Model<Token>,
    @InjectRedis()
    private readonly redisService: Redis,
  ) {}

  @BullWorkerProcess(referralProgramQueueConfig.workerOptions)
  public async process(
    job: Job<
    RewardsReceivedResponseInterface | RewardsClaimedResponseInterface,
    void
    >,
  ) {
    try {
      const { data } = job;
      const {
        user, tokens, amounts, operation,
      } = data;

      const amountsStorage: StorageKeyValueInterface = {};
      const tokensLowerCase = tokens.map((token: string) => token.toLowerCase());
      const tokensNames: string[] = [];

      await this.tokenModel
        .find({ address: { $in: tokensLowerCase } })
        .cursor()
        .eachAsync(async (token: Token) => {
          const symbol = token.get('symbol');
          const address = token.get('address');
          const index = tokensLowerCase.indexOf(address);
          if (index !== -1) {
            tokensNames.push(symbol);
            amountsStorage[symbol] = amounts[index];
          }
        });

      const nodes = await this.referralRewardNodeModel
        .find()
        .where({ from: user, currency: { $in: tokensNames } });

      if (nodes && nodes.length > 0) {
        const payments = nodes
          .filter((node) => tokensNames.indexOf(node.currency) !== -1)
          .map((node) => {
            const { to, tithe, _id: relationId } = node;
            const { currency } = node;
            return {
              from: user,
              to,
              tithe,
              currency,
              relationId,
              value: amountsStorage[currency],
              custom: operation,
            };
          });
        await this.referralRewardPaymentModel.insertMany(payments);
      }
    } catch (errorOrException) {
      await job.log(`error: ${errorOrException}`);
      this.logger.error(`error: ${errorOrException}`);
    }
  }
}
