import { Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { BullQueueInject, BullWorker, BullWorkerProcess } from '@anchan828/nest-bullmq';
import { Job, Queue } from 'bullmq';
import { Model } from 'mongoose';
import { Redis } from 'ioredis';
import BigNumber from 'bignumber.js';
import utils from 'web3-utils';

import { DepositTypeEnum } from '@app/constants';
import { TOTAL_LOCKED_XBE_AVG_LOCK_TIME, XBELockup } from '@app/dbo';
import { veXbeDepositQueueConfig } from '@app/configuration';
import { DepositResponseValues } from '../interfaces';

@BullWorker({ queueName: veXbeDepositQueueConfig.name })
export class VeXbeDepositWorker {
  private readonly logger = new Logger(VeXbeDepositWorker.name, { timestamp: true });

  constructor(
    @BullQueueInject(veXbeDepositQueueConfig.name)
    private readonly veXbeDepositQueue: Queue<DepositResponseValues, void>,
    @InjectModel(XBELockup.name)
    private readonly xbeLockupModel: Model<XBELockup>,
    @InjectRedis()
    private readonly redisService: Redis,
  ) {}

  @BullWorkerProcess(veXbeDepositQueueConfig.workerOptions)
  async process({ data, log }: Job<DepositResponseValues, void>): Promise<void> {
    try {
      const {
        value,
        locktime,
        provider,
        ts,
        _type: depositType,
      } = data;
      const type: DepositTypeEnum = Number(depositType);

      const existXbeLockupRow = await this.xbeLockupModel.findOne({
        userAddress: provider,
      });

      switch (type) {
        case DepositTypeEnum.DEPOSIT_FOR_TYPE:
        case DepositTypeEnum.CREATE_LOCK_TYPE:
          await this.xbeLockupModel.create({
            lockAmountBigNumber: new BigNumber(utils.fromWei(value)).toFixed(),
            // This value is probably not required, the value above can always be relevant
            lockAmountDecimal: Number(utils.fromWei(value)),
            userAddress: provider,
            depositType: type,
            lockStart: Number(ts),
            lockEnd: Number(locktime),
          });
          break;

        case DepositTypeEnum.INCREASE_LOCK_AMOUNT:
        case DepositTypeEnum.INCREASE_UNLOCK_TIME:

          if (existXbeLockupRow) {
            if (type === DepositTypeEnum.INCREASE_LOCK_AMOUNT) {
              const currentLockAmountBigint = new BigNumber(
                existXbeLockupRow.lockAmountBigNumber,
              );

              const valueBN = new BigNumber(utils.fromWei(value));
              const amount = currentLockAmountBigint.plus(valueBN);

              existXbeLockupRow.lockAmountBigNumber = amount.toFixed();
              existXbeLockupRow.lockAmountDecimal = Number(existXbeLockupRow.lockAmountBigNumber);
            }

            if (type === DepositTypeEnum.INCREASE_UNLOCK_TIME) {
              existXbeLockupRow.lockEnd = Number(locktime);
            }

            existXbeLockupRow.depositType = type;
            await existXbeLockupRow.save();
          } else {
            // TODO send in queue
          }
          break;
        default:
          return;
      }

      const avgLockupAmount = await this.xbeLockupModel.aggregate<{ avgAmount: number; }>([
        {
          $group: {
            _id: '$item',
            avgAmount: { $avg: { $subtract: ['$lockEnd', '$lockStart'] } },
          },
        },
      ]);

      this.logger.log(`TOTAL_LOCKED_XBE_AVG_LOCK_TIME: ${avgLockupAmount[0].avgAmount.toFixed()}`);

      await this.redisService.set(
        TOTAL_LOCKED_XBE_AVG_LOCK_TIME,
        avgLockupAmount[0].avgAmount,
      );
    } catch (errorOrException) {
      await log(`error: ${errorOrException}`);
      this.logger.error(`error: ${errorOrException}`);
    }
  }
}
