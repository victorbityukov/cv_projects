import { BullQueueEvents } from '@anchan828/nest-bullmq';
import { registerUserQueueConfig } from '@app/configuration';

@BullQueueEvents({ queueName: registerUserQueueConfig.name })
export class RegisterUserEvents {
  /**
   * Any event from queue
   * @BullQueueEventProcess('completed')
   * public async completed(job: Job): Promise<void> {
   * this.logger.log(job);
   * }
   */
}
