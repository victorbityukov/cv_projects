import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { BullQueueInject } from '@anchan828/nest-bullmq';
import { Queue } from 'bullmq';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { referralProgramQueueConfig, registerUserQueueConfig } from '@app/configuration';
import { ContractCollection } from '@app/dbo';
import { ContractListenerEventEnum } from '@app/constants';
import { ReferralProgramContract } from '@app/contracts';
import {
  RegisterUserResponseInterface,
  RewardsClaimedResponseInterface,
  RewardsReceivedResponseInterface,
} from '../interfaces';

@Injectable()
export class ReferralProgramListener implements OnApplicationBootstrap {
  private readonly logger = new Logger(ReferralProgramListener.name);

  constructor(
    @InjectModel(ContractCollection.name)
    private readonly contractCollectionModel: Model<ContractCollection>,
    @BullQueueInject(referralProgramQueueConfig.name)
    private readonly referralProgramQueue: Queue< | RewardsReceivedResponseInterface
    | RewardsClaimedResponseInterface,
    void>,
    @BullQueueInject(registerUserQueueConfig.name)
    private readonly registerUserQueue: Queue<RegisterUserResponseInterface,
    void>,
    private readonly referralProgramContract: ReferralProgramContract,
  ) {}

  async onApplicationBootstrap(): Promise<void> {
    this.registerUser();
    this.rewardReceived();
    this.rewardsClaimed();
  }

  registerUser(): void {
    this.referralProgramContract.registerUser()
      .on('connected', (subscriptionId: string) => {
        this.logger.log(`| UserRegistered | events | ${subscriptionId}`);
      })
      .on('data', async (event: {
        removed: boolean;
        returnValues: RegisterUserResponseInterface;
      }) => {
        try {
          if (event.removed) {
            return;
          }
          const { user, referrer } = event.returnValues;

          await this.registerUserQueue.add(
            ContractListenerEventEnum.REGISTER_USER,
            {
              user,
              referrer,
              contract: this.referralProgramContract.contract.options.address,
              operation: ContractListenerEventEnum.REGISTER_USER,
            },
          );
        } catch (e) {
          this.logger.log(`| ONCE | ${e}`);
        }
      })
      .on('error', (error: NodeJS.ErrnoException) => {
        this.logger.log('error');
        this.logger.log(error);
      });
  }

  rewardReceived(): void {
    this.referralProgramContract.rewardReceived()
      .on('connected', (subscriptionId: string) => {
        this.logger.log(`| RewardsReceived | events | ${subscriptionId}`);
      })
      .on(
        'data',
        async (event: {
          removed: boolean;
          returnValues: RewardsReceivedResponseInterface;
        }) => {
          try {
            if (event.removed) {
              return;
            }
            const { user, tokens, amounts } = event.returnValues;
            await this.referralProgramQueue.add(
              ContractListenerEventEnum.REWARDS_RECEIVED,
              {
                user,
                tokens,
                amounts,
                contract: this.referralProgramContract.contract.options.address,
                operation: ContractListenerEventEnum.REWARDS_RECEIVED,
              },
            );
          } catch (e) {
            this.logger.log(`| ONCE | ${e}`);
          }
        },
      )
      .on('error', (error: NodeJS.ErrnoException) => {
        this.logger.log('error');
        this.logger.log(error);
      });
  }

  rewardsClaimed(): void {
    this.referralProgramContract.rewardsClaimed()
      .on('connected', (subscriptionId: string) => {
        this.logger.log(`| RewardsClaimed | events | ${subscriptionId}`);
      })
      .on(
        'data',
        async (event: {
          removed: boolean;
          returnValues: RewardsClaimedResponseInterface;
        }) => {
          try {
            if (event.removed) {
              return;
            }
            const { user, tokens, amounts } = event.returnValues;

            await this.referralProgramQueue.add(
              ContractListenerEventEnum.REWARDS_CLAIMED,
              {
                user,
                tokens,
                amounts,
                contract: this.referralProgramContract.contract.options.address,
                operation: ContractListenerEventEnum.REWARDS_CLAIMED,
              },
            );
          } catch (e) {
            this.logger.log(`| ONCE | ${e}`);
          }
        },
      )
      .on('error', (error: NodeJS.ErrnoException) => {
        this.logger.log('error');
        this.logger.log(error);
      });
  }
}
