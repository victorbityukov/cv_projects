import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { BullQueueInject } from '@anchan828/nest-bullmq';
import { Queue } from 'bullmq';

import {
  veXbeDepositQueueConfig,
  veXbeWithdrawQueueConfig,
} from '@app/configuration';
import { ContractListenerEventEnum } from '@app/constants';

import { VeXbeContract } from '@app/contracts';
import {
  ContractEventResponseInterface,
  DepositResponseValues,
  WithdrawResponseValues,
} from '../interfaces';

@Injectable()
export class VeXbeListener implements OnApplicationBootstrap {
  private readonly logger = new Logger(VeXbeListener.name);

  constructor(
    @BullQueueInject(veXbeDepositQueueConfig.name)
    private readonly veXbeDepositQueue: Queue<DepositResponseValues, void>,
    @BullQueueInject(veXbeWithdrawQueueConfig.name)
    private readonly veXbeWithdrawQueue: Queue<WithdrawResponseValues, void>,
    private readonly veXbeContract: VeXbeContract,
  ) {}

  async onApplicationBootstrap(): Promise<void> {
    this.deposit();
    this.withdraw();
  }

  deposit(): void {
    this.veXbeContract.deposit()
      .on('connected', (subscriptionId: string) => {
        this.logger.log(`| VE_XBE | Deposit | ${subscriptionId}`);
      })
      .on('data', async (event: ContractEventResponseInterface) => {
        if (event.removed) {
          return;
        }

        const {
          provider,
          locktime,
          value,
          ts,
          _type: typeDeposit,
        } = <DepositResponseValues>event.returnValues;

        await this.veXbeDepositQueue.add(
          ContractListenerEventEnum.VE_XBE_DEPOSIT,
          {
            provider,
            locktime,
            value,
            ts,
            _type: typeDeposit,
          },
        );
      })
      .on('error', (error: NodeJS.ErrnoException) => {
        this.logger.error(`| VE_XBE | ${error}`);
      });
  }

  withdraw(): void {
    this.veXbeContract.withdraw()
      .on('connected', (subscriptionId: string) => {
        this.logger.log(`| VE_XBE | Withdraw | ${subscriptionId}`);
      })
      .on('data', async (event: ContractEventResponseInterface) => {
        if (event.removed) {
          return;
        }
        const { provider, value, ts } = <WithdrawResponseValues>(
          event.returnValues
        );
        await this.veXbeWithdrawQueue.add(
          ContractListenerEventEnum.VE_XBE_WITHDRAW,
          {
            provider,
            value,
            ts,
          },
        );
      })
      .on('error', (error: NodeJS.ErrnoException) => {
        this.logger.error(`| VE_XBE | ${error}`);
      });
  }
}
