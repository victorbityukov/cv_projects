import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { BullQueueInject } from '@anchan828/nest-bullmq';
import { InjectModel } from '@nestjs/mongoose';
import { tokenMocks, userMock, userMocks } from 'files/mock';
import { Queue } from 'bullmq';
import { Model } from 'mongoose';
import { Redis } from 'ioredis';

import { handleServerErrors } from '@app/error-handler';
import { contractsConfig, REDIS_CONFIG, referralProgramQueueConfig } from '@app/configuration';
import {
  ContractCollection,
  ReferralRewardNode,
  ReferralRewardPayment,
  Token,
  TokenRates,
  User,
} from '@app/dbo';
import { Web3ProviderService } from '@app/web3-provider';
import { from, lastValueFrom } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { NetworkEnum, VenueEnum } from '@app/constants';
import path from 'path';
import { appConfig } from '@app/configuration/app.config';
import { existsSync } from 'fs';
import { readFile } from 'fs/promises';
import { contractMock } from '../../../files/mock/contract.mock';
import { AbiService } from './abi.service';

@Injectable()
export class InitialLoaderService implements OnApplicationBootstrap {
  private readonly logger = new Logger(InitialLoaderService.name, { timestamp: true });

  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
    @InjectModel(Token.name)
    private readonly tokenModel: Model<Token>,
    @InjectModel(TokenRates.name)
    private readonly tokenRatesModel: Model<TokenRates>,
    private readonly abiService: AbiService,
    @InjectModel(ReferralRewardNode.name)
    private readonly referralRewardNodeModel: Model<ReferralRewardNode>,
    @InjectModel(ReferralRewardPayment.name)
    private readonly referralRewardPaymentModel: Model<ReferralRewardPayment>,
    @InjectModel(ContractCollection.name)
    private readonly contractCollectionModel: Model<ContractCollection>,
    @InjectRedis()
    private readonly redisService: Redis,
    @BullQueueInject(referralProgramQueueConfig.name)
    private readonly referralQueue: Queue,
    private readonly web3Provider: Web3ProviderService,
  ) {}

  async onApplicationBootstrap(): Promise<void> {
    this.logger.debug('loadInitialState started');

    await this.loadCommonContracts();
    await this.loadInitialContracts();

    await this.loadInitialSettings();
    await this.loadInitialUsers();
    await this.loadInitialTokens();
    this.logger.debug('loadInitialState ended');
  }

  private async loadInitialSettings() {
    if (REDIS_CONFIG.flushAll) {
      this.logger.debug('====================');
      this.logger.debug('Clear Redis Storage');
      await this.redisService.flushall();
    }
    this.logger.debug('====================');
    this.logger.debug(`Clear Queue ${referralProgramQueueConfig.name}`);
    await this.referralQueue.drain();
    this.logger.debug('====================');
  }

  private async loadInitialUsers(): Promise<void> {
    try {
      const userMockIds = userMocks.map((userMockItem) => userMockItem._id);
      const { address } = contractsConfig.list.referralProgram;
      const referralProgramContract = await this.web3Provider.getContractInstanceByAddress(
        address,
      );

      await this.userModel.deleteMany({ _id: { $in: userMockIds } });
      await this.userModel.insertMany(userMocks);

      await this.userModel
        .deleteMany({ referralCode: { $in: ['XBE-DEFAULT', 'INITIAL'] } });

      const rootAddress = await referralProgramContract.methods.rootAddress().call();
      this.logger.log(`rootAddress ${rootAddress}`);

      const createdUser = await this.userModel.create({
        _id: rootAddress,
        referralCode: userMock.referralCode,
      });

      const exists = await this.userModel.findById(createdUser._id);
      if (!exists) await createdUser.save();
    } catch (errorOrException) {
      this.logger.log(errorOrException, 'ERROR loadInitialUsers');
    }
  }

  private async loadInitialContracts(): Promise<void> {
    try {
      // @ts-ignore
      const contracts = Object.entries(contractsConfig.list);

      for (let i = 0; i < contracts.length; i += 1) {
        const [name, conf] = contracts[i];
        const { address, tags, abiFile } = conf;

        const pathFile = path.join(
          appConfig.rootDir,
          'config',
          'abi',
          abiFile,
        );

        let fileData = '';
        if (existsSync(pathFile)) {
          fileData = await readFile(pathFile, 'utf-8');
          const fileDataString = JSON.parse(fileData);

          if (address) {
            const existContract = await this.contractCollectionModel.findOne({ _id: address });

            if (!existContract) {
              await this.contractCollectionModel.create({
                _id: address,
                abi: fileDataString,
                name,
                tags,
              });
              this.logger.debug(`New contract ADDED ${address}`);
            } else {
              if (JSON.stringify(existContract.abi) !== JSON.stringify(fileDataString)) {
                await this.contractCollectionModel.updateOne({ _id: address }, {
                  abi: fileDataString,
                });
                this.logger.debug(`Contract UPDATED ${address}`);
              } else {
                this.logger.debug(`Contract EXISTS ${address}`);
              }
            }
          }
        }
      }
    } catch (errorOrException) {
      this.logger.error(errorOrException.message);
      throw handleServerErrors({
        errorOrException,
        logger: this.logger,
        methodName: 'loadInitialContracts',
      });
    }
  }

  private async loadInitialTokens(): Promise<void> {
    try {
      await lastValueFrom(from(tokenMocks)
        .pipe(
          mergeMap(async (token) => {
            const tokenExist = await this.tokenModel.findById(token._id);
            if (!tokenExist) {
              await this.tokenModel.create(token);
              return;
            }

            if (token.symbol.includes('XBE')) {
              const xbeRate = await this.tokenRatesModel.findOne({
                symbol: 'XBE',
                currency: 'USD',
              });

              if (!xbeRate) {
                this.logger.warn('Rates for XBE not found. Skipping...');
                return;
              }

              if (xbeRate) {
                await this.tokenRatesModel.findOneAndUpdate(
                  { symbol: 'mockXBE' },
                  {
                    token: 'mockXbe',
                    token_id: token._id,
                    network: NetworkEnum.ETH_R,
                    venue: VenueEnum.MOCK,
                    quote: xbeRate.quote,
                    quantity: xbeRate.quantity,
                    currency: xbeRate.currency,
                  }, {
                    upsert: true,
                    new: true,
                  },
                );
              }
            }
          }, 1),
        ), { defaultValue: [] });
    } catch (errorOrException) {
      this.logger.log(errorOrException, 'ERROR loadInitialTokens');
    }
  }

  private async loadCommonContracts(): Promise<void> {
    try {
      await lastValueFrom(from(contractMock)
        .pipe(
          mergeMap(async (token) => {
            const contractExist = await this.contractCollectionModel.findById(token._id);
            if (!contractExist) {
              await this.contractCollectionModel.create(token);
            }
          }, 1),
        ), { defaultValue: [] });
    } catch (errorOrException) {
      throw handleServerErrors({
        errorOrException,
        logger: this.logger,
        methodName: 'loadInitialTokens',
      });
    }
  }
}
