import { NestFactory } from '@nestjs/core';
import { InitialLoaderModule } from './initial-loader.module';

async function bootstrap() {
  const app = await NestFactory.create(InitialLoaderModule);
  await app.init();
  await app.listen(4001);
  // await process.exit();
}
bootstrap();
