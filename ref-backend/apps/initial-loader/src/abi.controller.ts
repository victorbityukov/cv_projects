import { Controller, Get, Param } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ContractCollection } from '@app/dbo';
import { Model } from 'mongoose';
import { AbiService } from './abi.service';

@Controller()
export class AbiController {
  constructor(
    private readonly abiService: AbiService,
    @InjectModel(ContractCollection.name)
    private readonly contractCollectionModel: Model<ContractCollection>,
  ) {}

  @Get('/getabi/:address')
  async getAbi(@Param('address') address: string) {
    let check = true;
    let data = { result: '' };

    do {
      const res = await this.abiService.getAbi(address);
      if (res.data.status === '1') {
        check = false;
        data = res.data;
      } else {
        await new Promise((resolve) => {
          setTimeout(() => resolve(null), 5000);
        });
      }
    } while (check);

    return data.result;
  }

  @Get('/get-contract')
  async getListAbiFiles() {
    return this.contractCollectionModel.find();
  }
}
