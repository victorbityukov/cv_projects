import { Injectable } from '@nestjs/common';
import * as path from 'path';
import { writeFile, readdir } from 'fs/promises';
import { networkConfig } from '@app/configuration/network.config';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class AbiService {
  private readonly abiDirPath: string;

  constructor(private httpService: HttpService) {
    this.abiDirPath = path.join(__dirname, '../../../config/abi');
  }

  async getAbiFilesList() {
    return readdir(this.abiDirPath);
  }

  async getAbi(address: string) {
    const params = {
      module: 'contract',
      action: 'getabi',
      address,
    };

    return lastValueFrom(this.httpService
      .get(`${networkConfig.etherscanUrl}/api`, {
        params,
      }));
  }

  async saveAbiInFile(filename: string, abiInfo: string): Promise<void> {
    await writeFile(filename, abiInfo);
  }
}
