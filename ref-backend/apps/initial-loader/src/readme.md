### Initial loader

This service sync database, and pre-load all values (db constants, mocks, etc) on startup, then it shuts down.
You need it to run once on new deployment or every time you need to sync your database schema.
It is also a good practice to run it on Gitlab as a separate task.