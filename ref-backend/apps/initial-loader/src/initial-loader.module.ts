import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BullModule } from '@anchan828/nest-bullmq';
import { RedisModule } from '@nestjs-modules/ioredis';
import { HttpModule } from '@nestjs/axios';

import {
  referralProgramQueueConfig,
  MONGO_CONFIG,
  MONGO_OPTIONS,
  REDIS_CONFIG,
} from '@app/configuration';

import {
  ContractCollection,
  ContractCollectionSchema,
  ReferralRewardNode,
  ReferralRewardNodeSchema,
  ReferralRewardPayment,
  ReferralRewardPaymentSchema,
  Token,
  TokenRates,
  TokenRatesSchema,
  TokenSchema,
  User,
  UserSchema,
  XBELockup,
  XBELockupSchema,
} from '@app/dbo';
import { Web3ProviderModule } from '@app/web3-provider';
import { InitialLoaderService } from './initial-loader.service';
import { AbiService } from './abi.service';
import { AbiController } from './abi.controller';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forRoot(MONGO_CONFIG.connection_string, MONGO_OPTIONS),
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: ContractCollection.name, schema: ContractCollectionSchema },
      { name: ReferralRewardNode.name, schema: ReferralRewardNodeSchema },
      { name: ReferralRewardPayment.name, schema: ReferralRewardPaymentSchema },
      { name: TokenRates.name, schema: TokenRatesSchema },
      { name: Token.name, schema: TokenSchema },
      { name: XBELockup.name, schema: XBELockupSchema },
    ]),
    RedisModule.forRoot({
      config: REDIS_CONFIG,
    }),
    BullModule.forRoot({
      options: {
        connection: {
          host: REDIS_CONFIG.host,
          port: REDIS_CONFIG.port,
        },
      },
    }),
    BullModule.registerQueue({
      queueName: referralProgramQueueConfig.name,
      options: referralProgramQueueConfig.options,
    }),
    Web3ProviderModule,
  ],
  providers: [InitialLoaderService, AbiService],
  controllers: [AbiController],
})
export class InitialLoaderModule {}
