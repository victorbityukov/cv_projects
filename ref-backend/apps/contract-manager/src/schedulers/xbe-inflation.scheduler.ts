import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';

import { XBE_INFLATION_TOTAL_MINTED } from '@app/dbo';
import { XbeInflationContract } from '@app/contracts';

@Injectable()
export class XbeInflationScheduler {
  private readonly logger = new Logger(XbeInflationScheduler.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly xbeInflationContract: XbeInflationContract,
  ) {}

  @Cron(CronExpression.EVERY_MINUTE)
  async totalMinted(): Promise<void> {
    try {
      const totalMinted = await this.xbeInflationContract.totalMinted();

      await this.redisService.set(XBE_INFLATION_TOTAL_MINTED, totalMinted.toFixed());
    } catch (e) {
      this.logger.error(e);
    }
  }
}
