import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import { CvxCrvCurveContract } from '@app/contracts/curve-pools/cvx-crv-curve.contract';
import { Cron, CronExpression } from '@nestjs/schedule';
import { sleep } from '@app/utils';

@Injectable()
export class CvxCrvCurveScheduler implements OnApplicationBootstrap {
  private readonly logger = new Logger(CvxCrvCurveScheduler.name, { timestamp: true });

  private countRunningBootstrap = 0;

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly cvxCrvCurveContract: CvxCrvCurveContract,
  ) {}

  async onApplicationBootstrap(): Promise<void> {
    try {
      await sleep(1000);
      await this.getVirtualPrice();
    } catch (e) {
      this.logger.log(e.message);
      if (this.countRunningBootstrap < 5) {
        this.countRunningBootstrap += 1;
        await this.onApplicationBootstrap();
      }
    }
  }

  @Cron(CronExpression.EVERY_MINUTE)
  async getVirtualPrice(): Promise<void> {
    try {
      const virtualPrice = await this.cvxCrvCurveContract.contract.methods.get_virtual_price()
        .call();
      const symbol = await this.cvxCrvCurveContract.contract.methods.symbol().call();
      const { address } = this.cvxCrvCurveContract.contract.options;

      await this.redisService.set(`${symbol}:price:usd`, virtualPrice);
      await this.redisService.set(`${address}:price:usd`, virtualPrice);
    } catch (e) {
      this.logger.error(e);
    }
  }
}
