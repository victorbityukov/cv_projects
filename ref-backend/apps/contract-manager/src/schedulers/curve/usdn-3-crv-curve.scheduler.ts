import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import { Cron, CronExpression } from '@nestjs/schedule';
import { sleep } from '@app/utils';
import { Usdn3CrvCurveContract } from '@app/contracts/curve-pools/usdn-3-crv-curve.contract';

@Injectable()
export class Usdn3CrvCurveScheduler implements OnApplicationBootstrap {
  private lpTokenSymbol = 'usdn3CRV';

  private lpTokenAddress = '0x4f3e8f405cf5afc05d68142f3783bdfe13811522';

  private countRunningBootstrap = 0;

  private readonly logger = new Logger(Usdn3CrvCurveScheduler.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly usdn3CrvCurveContract: Usdn3CrvCurveContract,
  ) {}

  async onApplicationBootstrap(): Promise<void> {
    try {
      await sleep(1000);
      await this.getVirtualPrice();
    } catch (e) {
      this.logger.log(e.message);
      if (this.countRunningBootstrap < 5) {
        this.countRunningBootstrap += 1;
        await this.onApplicationBootstrap();
      }
    }
  }

  @Cron(CronExpression.EVERY_MINUTE)
  async getVirtualPrice(): Promise<void> {
    try {
      const virtualPrice = await this.usdn3CrvCurveContract.contract.methods.get_virtual_price()
        .call();

      await this.redisService.set(`${this.lpTokenSymbol}:price:usd`, virtualPrice);
      await this.redisService.set(`${this.lpTokenAddress}:price:usd`, virtualPrice);
    } catch (e) {
      this.logger.error(e);
    }
  }
}
