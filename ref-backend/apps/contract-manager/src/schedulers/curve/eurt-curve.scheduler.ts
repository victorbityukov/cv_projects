import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import { Cron, CronExpression } from '@nestjs/schedule';
import { BaseTokenContractFactory } from '@app/contracts';
import { sleep } from '@app/utils';
import { EurtCurveContract } from '@app/contracts/curve-pools/eurt-curve.contract';

@Injectable()
export class EurtCurveScheduler implements OnApplicationBootstrap {
  private lpTokenSymbol: string;

  private lpTokenAddress: string;

  private countRunningBootstrap = 0;

  private readonly logger = new Logger(EurtCurveScheduler.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly eurtCurveContract: EurtCurveContract,
    private readonly baseTokenContractFactory: BaseTokenContractFactory,
  ) {}

  async onApplicationBootstrap(): Promise<void> {
    try {
      await sleep(1000);
      this.lpTokenSymbol = await this.eurtCurveContract.contract.methods.symbol().call();
      this.lpTokenAddress = this.eurtCurveContract.contract.options.address;

      await this.getVirtualPrice();
    } catch (e) {
      this.logger.log(e.message);
      if (this.countRunningBootstrap < 5) {
        this.countRunningBootstrap += 1;
        await this.onApplicationBootstrap();
      }
    }
  }

  @Cron(CronExpression.EVERY_MINUTE)
  async getVirtualPrice(): Promise<void> {
    try {
      const virtualPrice = await this.eurtCurveContract.contract.methods.get_virtual_price()
        .call();

      await this.redisService.set(`${this.lpTokenSymbol}:price:usd`, virtualPrice);
      await this.redisService.set(`${this.lpTokenAddress}:price:usd`, virtualPrice);
    } catch (e) {
      this.logger.error(e);
    }
  }
}
