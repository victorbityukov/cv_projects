import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Web3Contract } from '@app/web3-provider';
import { sleep } from '@app/utils';
import { BaseTokenContractFactory, TokenMethods } from '@app/contracts/base-token-contract.factory';
import { StEthCurveContract } from '@app/contracts/curve-pools/st-eth-curve.contract';

@Injectable()
export class StEthCurveScheduler implements OnApplicationBootstrap {
  private lpTokenContract: Web3Contract<TokenMethods>;

  private lpTokenSymbol: string;

  private lpTokenAddress: string;

  private countRunningBootstrap = 0;

  private readonly logger = new Logger(StEthCurveScheduler.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly stEthCurveContract: StEthCurveContract,
    private readonly baseTokenContractFactory: BaseTokenContractFactory,
  ) {}

  async onApplicationBootstrap(): Promise<void> {
    try {
      await sleep(1000);
      const tokenAddress = await this.stEthCurveContract.contract.methods.lp_token().call();
      this.lpTokenContract = await this.baseTokenContractFactory.init(
        tokenAddress,
      );
      this.lpTokenSymbol = await this.lpTokenContract.methods.symbol().call();
      this.lpTokenAddress = tokenAddress;

      await this.getVirtualPrice();
    } catch (e) {
      this.logger.log(e.message);
      if (this.countRunningBootstrap < 5) {
        this.countRunningBootstrap += 1;
        await this.onApplicationBootstrap();
      }
    }
  }

  @Cron(CronExpression.EVERY_MINUTE)
  async getVirtualPrice(): Promise<void> {
    try {
      const virtualPrice = await this.stEthCurveContract.contract.methods.get_virtual_price()
        .call();

      await this.redisService.set(`${this.lpTokenSymbol}:price:usd`, virtualPrice);
      await this.redisService.set(`${this.lpTokenAddress}:price:usd`, virtualPrice);
    } catch (e) {
      this.logger.error(e);
    }
  }
}
