import { Injectable, Logger } from '@nestjs/common';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Redis } from 'ioredis';

import {
  BONUS_CAMPAIGN_BONUS_EMISSION,
  BONUS_CAMPAIGN_PERIOD_FINISH,
  BONUS_CAMPAIGN_REWARDS_DURATION,
  BONUS_CAMPAIGN_START_MINT_TIME,
  BONUS_CAMPAIGN_STOP_REGISTER_TIME,
  BONUS_CAMPAIGN_TOTAL_SUPPLY,
} from '@app/dbo';
import { BonusCampaignContract } from '@app/contracts';

@Injectable()
export class BonusCampaignScheduler {
  private readonly logger = new Logger(BonusCampaignScheduler.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly bonusCampaignContract: BonusCampaignContract,
  ) {
    (async () => {
      try {
        await new Promise((resolve) => { setTimeout(resolve, 1000); });
        await this.startMintTime();
        await this.periodFinish();
        await this.stopRegisterTime();
        await this.rewardsDuration();
      } catch (e) {
        this.logger.error(e);
      }
    })();
  }

  @Cron(CronExpression.EVERY_10_SECONDS)
  async totalSupply(): Promise<void> {
    try {
      const totalSupply = await this.bonusCampaignContract.totalSupply();

      await this.redisService.set(
        BONUS_CAMPAIGN_TOTAL_SUPPLY,
        totalSupply.toFixed(),
      );
    } catch (e) {
      this.logger.error(e);
    }
  }

  @Cron(CronExpression.EVERY_10_SECONDS)
  async bonusEmission(): Promise<void> {
    try {
      const bonusEmission = await this.bonusCampaignContract.bonusEmission();

      await this.redisService.set(
        BONUS_CAMPAIGN_BONUS_EMISSION,
        bonusEmission.toFixed(),
      );
    } catch (e) {
      this.logger.error(e);
    }
  }

  @Cron(CronExpression.EVERY_10_SECONDS)
  async rewardsDuration(): Promise<void> {
    try {
      const rewardsDuration = await this.bonusCampaignContract.rewardsDuration();

      await this.redisService.set(
        BONUS_CAMPAIGN_REWARDS_DURATION,
        rewardsDuration,
      );
    } catch (e) {
      this.logger.error(e);
    }
  }

  @Cron(CronExpression.EVERY_2_HOURS)
  async startMintTime(): Promise<void> {
    try {
      const startMintTime = await this.bonusCampaignContract.startMintTime();

      await this.redisService.set(
        BONUS_CAMPAIGN_START_MINT_TIME,
        startMintTime,
      );
    } catch (e) {
      this.logger.error(e);
    }
  }

  @Cron(CronExpression.EVERY_2_HOURS)
  async periodFinish(): Promise<void> {
    try {
      const periodFinish = await this.bonusCampaignContract.periodFinish();

      await this.redisService.set(
        BONUS_CAMPAIGN_PERIOD_FINISH,
        periodFinish,
      );
    } catch (e) {
      this.logger.error(e);
    }
  }

  @Cron(CronExpression.EVERY_2_HOURS)
  async stopRegisterTime(): Promise<void> {
    try {
      const stopRegisterTime = await this.bonusCampaignContract.stopRegisterTime();

      await this.redisService.set(
        BONUS_CAMPAIGN_STOP_REGISTER_TIME,
        stopRegisterTime,
      );
    } catch (e) {
      this.logger.error(e);
    }
  }
}
