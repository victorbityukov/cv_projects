import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import {
  VE_XBE_LOCKED_SUPPLY,
  VE_XBE_SUPPLY,
  VE_XBE_TOTAL_SUPPLY,
} from '@app/dbo';
import { VeXbeContract } from '@app/contracts';

@Injectable()
export class VeXbeScheduler {
  private readonly logger = new Logger(VeXbeScheduler.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly veXbeContract: VeXbeContract,
  ) {}

  @Cron(CronExpression.EVERY_10_SECONDS)
  async supply(): Promise<void> {
    try {
      const totalLocked = await this.veXbeContract.supply();
      await this.redisService.set(VE_XBE_SUPPLY, totalLocked.toFixed());
    } catch (e) {
      this.logger.error(e);
    }
  }

  @Cron(CronExpression.EVERY_10_SECONDS)
  async lockedSupply(): Promise<void> {
    try {
      const totalLocked = await this.veXbeContract.lockedSupply();
      await this.redisService.set(VE_XBE_LOCKED_SUPPLY, totalLocked.toFixed());
    } catch (e) {
      this.logger.error(e);
    }
  }

  @Cron(CronExpression.EVERY_10_SECONDS)
  async totalSupply(): Promise<void> {
    try {
      const totalSupply = await this.veXbeContract.totalSupply();
      await this.redisService.set(VE_XBE_TOTAL_SUPPLY, totalSupply.toFixed());
    } catch (e) {
      this.logger.error(e);
    }
  }
}
