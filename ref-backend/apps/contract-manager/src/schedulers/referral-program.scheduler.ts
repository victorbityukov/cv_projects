import { Injectable, Logger } from '@nestjs/common';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import { CronExpression } from '@nestjs/schedule';
import { ReferralProgramContract } from '@app/contracts';
import { CONTRACT_MANAGER } from '@app/configuration';

@Injectable()
export class ReferralProgramScheduler {
  private readonly logger = new Logger(ReferralProgramScheduler.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly referralProgramContract: ReferralProgramContract,
  ) {}

  // @Cron('5 17 * * 3')
  async claimRewardsForRoot(init: boolean = CONTRACT_MANAGER.claimRewardsForRoot): Promise<void> {
    this.logger.log('Contract: referralProgram');
    this.logger.log(`Claim Rewards For Root Cron: ${CronExpression.EVERY_WEEK}`);
    if (!init) {
      this.logger.log(`claimRewardsForRoot: init ${init}`);
      return;
    }
    await this.referralProgramContract.claimRewardsForRoot();
  }
}
