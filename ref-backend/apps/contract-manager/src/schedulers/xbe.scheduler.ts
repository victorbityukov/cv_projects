import { Injectable, Logger } from '@nestjs/common';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import { contractsConfig } from '@app/configuration';
import { Cron, CronExpression } from '@nestjs/schedule';
import {
  XBE_BALANCE_OF_CLIENT,
  XBE_BALANCE_OF_TREASURY,
  XBE_BALANCE_OF_XBE,
  XBE_TOTAL_SUPPLY,
} from '@app/dbo';
import { XbeContract } from '@app/contracts';

@Injectable()
export class XbeScheduler {
  private readonly logger = new Logger(XbeScheduler.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly xbeContract: XbeContract,
  ) {}

  @Cron(CronExpression.EVERY_10_SECONDS)
  async totalSupply(): Promise<void> {
    try {
      const totalSupply = await this.xbeContract.totalSupply();

      await this.redisService.set(XBE_TOTAL_SUPPLY, totalSupply.toFixed());
    } catch (e) {
      this.logger.error(e);
    }
  }

  @Cron(CronExpression.EVERY_10_SECONDS)
  async xbeBalanceOfTreasury(): Promise<void> {
    try {
      const treasuryBalance = await this.xbeContract.balanceOf(
        contractsConfig.list.treasury.address,
      );

      await this.redisService.set(XBE_BALANCE_OF_TREASURY, treasuryBalance.toFixed());
    } catch (e) {
      this.logger.error(e);
    }
  }

  @Cron(CronExpression.EVERY_10_SECONDS)
  async xbeBalanceOfXbe(): Promise<void> {
    try {
      const xbeAddress = contractsConfig.list.mockXbe.address || contractsConfig.list.xbe.address;
      const xbeBalance = await this.xbeContract.balanceOf(
        xbeAddress,
      );

      await this.redisService.set(XBE_BALANCE_OF_XBE, xbeBalance.toFixed());
    } catch (e) {
      this.logger.error(e);
    }
  }

  @Cron(CronExpression.EVERY_10_SECONDS)
  async xbeBalanceOfClient(): Promise<void> {
    try {
      const clientBalance = await this.xbeContract.balanceOf(
        contractsConfig.clientAddress,
      );

      await this.redisService.set(XBE_BALANCE_OF_CLIENT, clientBalance.toFixed());
    } catch (e) {
      this.logger.error(e);
    }
  }
}
