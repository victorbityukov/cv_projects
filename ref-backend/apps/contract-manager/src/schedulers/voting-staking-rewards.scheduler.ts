import { Injectable, Logger } from '@nestjs/common';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Redis } from 'ioredis';

import {
  VOTING_STAKING_REWARDS_DAILY_APY,
  VOTING_STAKING_REWARDS_REWARD_RATE,
  VOTING_STAKING_REWARDS_TOTAL_SUPPLY, VOTING_STAKING_REWARDS_YEARLY_APY,
} from '@app/dbo';
import { VotingStakingRewardsContract } from '@app/contracts';

@Injectable()
export class VotingStakingRewardsScheduler {
  private readonly logger = new Logger(VotingStakingRewardsScheduler.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly votingStakingRewardsContract: VotingStakingRewardsContract,
  ) {
    (async () => {
      try {
        await new Promise((resolve) => { setTimeout(resolve, 1000); });
        await this.calculateAPY();
      } catch (e) {
        this.logger.error(e);
      }
    })();
  }

  @Cron(CronExpression.EVERY_5_MINUTES)
  async calculateAPY(): Promise<void> {
    try {
      const rewardRate = await this.votingStakingRewardsContract.rewardRate();
      const totalSupply = await this.votingStakingRewardsContract.totalSupply();
      const dailyAPY = rewardRate.multipliedBy(86400).dividedBy(totalSupply).multipliedBy(100);
      const yearlyAPY = dailyAPY.multipliedBy(365);

      await this.redisService.set(
        VOTING_STAKING_REWARDS_REWARD_RATE,
        rewardRate.toFixed(),
      );
      await this.redisService.set(
        VOTING_STAKING_REWARDS_TOTAL_SUPPLY,
        totalSupply.toFixed(),
      );
      await this.redisService.set(
        VOTING_STAKING_REWARDS_DAILY_APY,
        dailyAPY.toFixed(),
      );
      await this.redisService.set(
        VOTING_STAKING_REWARDS_YEARLY_APY,
        yearlyAPY.toFixed(),
      );
    } catch (e) {
      this.logger.error(e);
    }
  }
}
