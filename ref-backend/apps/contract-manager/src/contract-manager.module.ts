import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { HttpModule } from '@nestjs/axios';
import { RedisModule } from '@nestjs-modules/ioredis';
import { Web3ProviderModule } from '@app/web3-provider';
import { MONGO_CONFIG, MONGO_OPTIONS, REDIS_CONFIG } from '@app/configuration';
import {
  ContractCollection,
  ContractCollectionSchema,
  Token,
  TokenRates,
  TokenRatesSchema,
  TokenSchema,
} from '@app/dbo';
import { CvxCrvCurveContract } from '@app/contracts/curve-pools/cvx-crv-curve.contract';
import {
  BaseTokenContractFactory,
  BonusCampaignContract,
  ControllerContract,
  CvxCrvVaultContract,
  CvxVaultContract,
  EurtHiveVaultContract,
  IronbankVaultContract,
  ReferralProgramContract,
  RegistryContract,
  StEthHiveVaultContract,
  SushiVaultContract,
  TreasureContract,
  UsdnHiveVaultContract,
  VeXbeContract,
  VotingStakingRewardsContract,
  XbeContract,
  XbeInflationContract,
} from '@app/contracts';
import { Ib3CrvCurveContract } from '@app/contracts/curve-pools/ib-3-crv-curve.contract';
import { Usdn3CrvCurveContract } from '@app/contracts/curve-pools/usdn-3-crv-curve.contract';
import { StEthCurveContract } from '@app/contracts/curve-pools/st-eth-curve.contract';
import { EurtCurveContract } from '@app/contracts/curve-pools/eurt-curve.contract';
import {
  BonusCampaignScheduler,
  ReferralProgramScheduler,
  VeXbeScheduler,
  XbeInflationScheduler,
  XbeScheduler,
} from './schedulers';
import { ContractManagerService } from './contract-manager.service';
import { VotingStakingRewardsScheduler } from './schedulers/voting-staking-rewards.scheduler';
import { CvxCrvCurveScheduler } from './schedulers/curve/cvx-crv-curve.scheduler';
import { Ib3CrvCurveScheduler } from './schedulers/curve/ib-3-crv-curve.scheduler';
import { Usdn3CrvCurveScheduler } from './schedulers/curve/usdn-3-crv-curve.scheduler';
import { StEthCurveScheduler } from './schedulers/curve/st-eth-curve.scheduler';
import { EurtCurveScheduler } from './schedulers/curve/eurt-curve.scheduler';

@Module({
  imports: [
    HttpModule,
    Web3ProviderModule,
    ScheduleModule.forRoot(),
    RedisModule.forRoot({
      config: REDIS_CONFIG,
    }),
    MongooseModule.forRoot(MONGO_CONFIG.connection_string, MONGO_OPTIONS),
    MongooseModule.forFeature([
      { name: ContractCollection.name, schema: ContractCollectionSchema },
      { name: TokenRates.name, schema: TokenRatesSchema },
      { name: Token.name, schema: TokenSchema },
    ]),
  ],
  providers: [
    ContractManagerService,
    EurtCurveContract,
    EurtCurveScheduler,
    StEthCurveContract,
    StEthCurveScheduler,
    Ib3CrvCurveScheduler,
    Ib3CrvCurveContract,
    Usdn3CrvCurveScheduler,
    Usdn3CrvCurveContract,
    BaseTokenContractFactory,
    CvxCrvCurveContract,
    CvxCrvCurveScheduler,
    VeXbeScheduler,
    ReferralProgramScheduler,
    XbeScheduler,
    XbeInflationScheduler,
    BonusCampaignScheduler,
    VotingStakingRewardsContract,
    VotingStakingRewardsScheduler,
    VeXbeContract,
    XbeContract,
    XbeInflationContract,
    BonusCampaignContract,
    SushiVaultContract,
    CvxCrvVaultContract,
    CvxVaultContract,
    IronbankVaultContract,
    RegistryContract,
    TreasureContract,
    ControllerContract,
    ReferralProgramContract,
    EurtHiveVaultContract,
    StEthHiveVaultContract,
    UsdnHiveVaultContract,
  ],
})
export class ContractManagerModule {
}
