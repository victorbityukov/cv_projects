import { NestFactory } from '@nestjs/core';
import { ContractManagerModule } from './contract-manager.module';

async function bootstrap() {
  const app = await NestFactory.create(ContractManagerModule);
  await app.listen(3002);
}
bootstrap();
