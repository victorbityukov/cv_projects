import { Web3ProviderService } from '@app/web3-provider';
import {
  Injectable,
  Logger,
  OnApplicationBootstrap,
  ServiceUnavailableException,
} from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { HttpService } from '@nestjs/axios';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, lastValueFrom } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import {
  CMC_ENDPOINT,
  CurrencyTypeEnum,
  NetworkEnum,
  SUSHI_ENDPOINT,
  VenueEnum,
} from '@app/constants';
import { CmcQuotesDto, SushiApiAction, SushiPair, SushiTokenDto } from '@app/dto';
import { ContractCollection, sushiXCCYBoard, Token, TokenRates } from '@app/dbo';
import { handleServerErrors } from '@app/error-handler';
import { isStableCoin } from '@app/utils';
import { CMC_CONFIG, CONTRACT_MANAGER } from '@app/configuration';
import {
  BonusCampaignContract,
  ControllerContract,
  CvxCrvVaultContract,
  CvxVaultContract,
  EurtHiveVaultContract,
  IronbankVaultContract,
  RegistryContract,
  StEthHiveVaultContract,
  SushiVaultContract,
  TreasureContract,
  UsdnHiveVaultContract,
  XbeInflationContract,
} from '@app/contracts';

@Injectable()
export class ContractManagerService implements OnApplicationBootstrap {
  private readonly logger = new Logger(ContractManagerService.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private httpService: HttpService,
    private readonly web3Provider: Web3ProviderService,
    @InjectModel(Token.name)
    private readonly tokenModel: Model<Token>,
    @InjectModel(TokenRates.name)
    private readonly tokenRatesModel: Model<TokenRates>,
    @InjectModel(ContractCollection.name)
    private readonly contractCollectionModel: Model<ContractCollection>,
    private readonly sushiVaultContract: SushiVaultContract,
    private readonly cvxCrvVaultContract: CvxCrvVaultContract,
    private readonly cvxVaultContract: CvxVaultContract,
    private readonly ironbankVaultContract: IronbankVaultContract,
    private readonly bonusCampaignContract: BonusCampaignContract,
    private readonly xbeInflationContract: XbeInflationContract,
    private readonly registryContract: RegistryContract,
    private readonly treasureContract: TreasureContract,
    private readonly controllerContract: ControllerContract,
    private readonly eurtHiveVaultContract: EurtHiveVaultContract,
    private readonly stHiveVaultContract: StEthHiveVaultContract,
    private readonly usdnHiveVaultContract: UsdnHiveVaultContract,
  ) {}

  async onApplicationBootstrap(): Promise<void> {
    // await this.startMint(contractsConfig.initial);
    await this.sushiBuildXCCY();
    await this.getCmCQuotes();
    await this.getSushiQuotes();
  }

  @Cron(CronExpression.EVERY_30_MINUTES)
  async sushiBuildXCCY(): Promise<void> {
    try {
      this.logger.log('sushiBuildXCCY: starting');
      const response = await lastValueFrom(this.httpService
        .get<[SushiApiAction, SushiPair[]]>(SUSHI_ENDPOINT, {
        params: {
          action: 'all_pairs',
          chainID: 1,
        },
      }));
      if (!response.data) {
        throw new ServiceUnavailableException(
          `Can't fetch data from ${SUSHI_ENDPOINT}`,
        );
      }
      const [, currencyPairs] = response.data;

      const currencyBoard: SushiTokenDto[] = [];

      const redisCurrencyBoard: string[] = [];

      currencyPairs.forEach((currency) => {
        redisCurrencyBoard.push(currency.Pair_ID);

        const tokens = [
          {
            _id: currency.Token_1_contract,
            symbol: currency.Token_1_symbol,
            name: currency.Token_1_name,
            network: NetworkEnum.ETH_M,
            is_stablecoin: isStableCoin(
              currency.Token_1_name,
              currency.Token_1_symbol,
            ),
            decimals: currency.Token_1_decimals,
            provider: VenueEnum.SUSHI,
            sushi_pairs: {
              _id: currency.Pair_ID,
              tokenA: currency.Token_1_symbol,
              tokenB: currency.Token_2_symbol,
            },
          },
          {
            _id: currency.Token_2_contract,
            symbol: currency.Token_2_symbol,
            name: currency.Token_2_name,
            network: NetworkEnum.ETH_M,
            is_stablecoin: isStableCoin(
              currency.Token_2_name,
              currency.Token_2_symbol,
            ),
            decimals: currency.Token_2_decimals,
            provider: VenueEnum.SUSHI,
            sushi_pairs: {
              _id: currency.Pair_ID.toLowerCase(),
              tokenA: currency.Token_1_symbol,
              tokenB: currency.Token_2_symbol,
            },
          },
        ];

        currencyBoard.push(...tokens);
      });

      await this.redisService.set(
        sushiXCCYBoard,
        JSON.stringify(redisCurrencyBoard),
      );
      this.logger.log(`sushiBuildXCCY: board ${sushiXCCYBoard} added!`);

      await lastValueFrom(from(currencyBoard)
        .pipe(
          mergeMap(async (tokenSushi) => {
            const token = await this.tokenModel.findById(tokenSushi._id);
            if (!token) {
              await this.tokenModel.create(tokenSushi);
            }
            /**
             token.symbol = tokenSushi.symbol;
             token.name = tokenSushi.name;
             token.network = tokenSushi.network;
             token.decimals = tokenSushi.decimals;
             token.provider = tokenSushi.provider;
             token.sushi_pairs.addToSet(tokenSushi.sushi_pairs);
             await token.save();
             */
          }, 2),
        ), { defaultValue: [] });
    } catch (errorOrException) {
      throw handleServerErrors({
        errorOrException,
        logger: this.logger,
        methodName: 'sushiBuildXCCY',
      });
    }
  }

  @Cron(CronExpression.EVERY_HOUR)
  async getCmCQuotes(): Promise<void> {
    try {
      this.logger.log('getCmCQuotes: starting');
      await this.tokenModel
        .find({
          $or:
            [
              { symbol: CurrencyTypeEnum.CVX },
              { symbol: CurrencyTypeEnum.ETH },
              { symbol: CurrencyTypeEnum.XBE },
              { symbol: CurrencyTypeEnum.USDT },
              { symbol: CurrencyTypeEnum.CRV },
            ],
        })
        .cursor()
        .eachAsync(async ({ symbol }) => {
          try {
            const response = await lastValueFrom(this.httpService
              .get<CmcQuotesDto>(`${CMC_ENDPOINT}/cryptocurrency/quotes/latest`, {
              params: {
                symbol,
              },
              timeout: 10000,
              maxRedirects: 5,
              headers: {
                [CMC_CONFIG.header]: CMC_CONFIG.key,
                'Content-Type': 'application/json',
              },
            }));

            const r = response.data;
            const data = r.data[symbol];
            const quote = r.data[symbol].quote.USD;

            await this.tokenRatesModel.create({
              token: data.symbol,
              network: NetworkEnum.ETH_M,
              venue: VenueEnum.CMC,
              quote: quote.price,
              quantity: data.circulating_supply,
              currency: 'USD',
            });
          } catch (errorRequest) {
            this.logger.error(errorRequest);
          }
        });
      this.logger.log('getCmCQuotes: all quotes updated successfully');
    } catch (errorOrException) {
      throw handleServerErrors({
        errorOrException,
        logger: this.logger,
        methodName: 'getCmCQuotes',
      });
    }
  }

  @Cron(CronExpression.EVERY_HOUR)
  async getSushiQuotes(): Promise<void> {
    try {
      this.logger.log('getSushiQuotes: starting');
      const unwindStage = { $unwind: '$sushi_pairs' };
      const groupStage = { $group: { _id: '$sushi_pairs._id' } };

      const sushiCurrencyBoard = await this.tokenModel.aggregate([
        unwindStage,
        groupStage,
      ]);

      if (!sushiCurrencyBoard || sushiCurrencyBoard.length === 0) {
        this.logger.error('Sushi currency board not found or returned empty');
        return;
      }

      await lastValueFrom(from(sushiCurrencyBoard.map((pair) => pair._id))
        .pipe(
          mergeMap(async (pair) => {
            try {
              const response = await lastValueFrom(this.httpService
                .get<Partial<SushiPair>[]>(SUSHI_ENDPOINT, {
                params: {
                  action: 'get_pair',
                  chainID: 1,
                  pair,
                },
              }));
              if (!response.data) {
                throw new ServiceUnavailableException(
                  `Can't fetch data from ${SUSHI_ENDPOINT}`,
                );
              }
              const [marketData] = response.data;

              /**
               * Primary to Secondary Quote
               * and vice versa
               */
              await this.tokenRatesModel.create({
                token: marketData.Token_1_symbol,
                token_id: marketData.Token_1_contract,
                network: NetworkEnum.ETH_M,
                venue: VenueEnum.SUSHI,
                quote: marketData.Token_2_price,
                quantity: marketData.Token_2_reserve,
                currency: marketData.Token_2_symbol,
                currency_id: marketData.Token_2_contract,
              });

              await this.tokenRatesModel.create({
                token: marketData.Token_2_symbol,
                token_id: marketData.Token_2_contract,
                network: NetworkEnum.ETH_M,
                venue: VenueEnum.SUSHI,
                quote: marketData.Token_1_price,
                quantity: marketData.Token_1_reserve,
                currency: marketData.Token_1_symbol,
                currency_id: marketData.Token_1_contract,
              });
            } catch (e) {
              this.logger.error(e);
            }
          }, 2),
        ), { defaultValue: [] });
      this.logger.log('getSushiQuotes: updated successfully');
    } catch (errorOrException) {
      throw handleServerErrors({
        errorOrException,
        logger: this.logger,
        methodName: 'getSushiQuotes',
      });
    }
  }

  // @Cron('5 17 * * 3')
  async getRewardStrategy(init: boolean = CONTRACT_MANAGER.getRewardStrategy): Promise<void> {
    this.logger.log('Contract: controller');
    this.logger.log('Get Reward Strategy Cron: 5 17 * * 3');
    if (!init) {
      this.logger.log(`getRewardStrategy: init ${init}`);
      return;
    }
    const vaultsInfo = await this.registryContract.getVaultsInfo();
    const { strategyArray } = vaultsInfo;

    for (let i = 0; i < strategyArray.length; i += 1) {
      await this.controllerContract.getRewardStrategy(strategyArray[i]);
    }
  }

  // @Cron('5 17 * * 3')
  async toVoters(init: boolean = CONTRACT_MANAGER.toVoters): Promise<void> {
    this.logger.log('Contract: treasure !!!');
    this.logger.log('To Voters Cron: 5 17 * * 3');
    if (!init) {
      this.logger.log(`toVoters: init ${init}`);
      return;
    }
    await this.treasureContract.toVoters();
  }

  // @Cron('1 17 * * 3')
  async mintForContracts(init: boolean = CONTRACT_MANAGER.mintForContracts): Promise<void> {
    this.logger.log('Contract: xbeInflation');
    this.logger.log('Mint For Contracts Cron: 1 17 * * 3');
    if (!init) {
      this.logger.log(`mintForContracts: init ${init}`);
      return;
    }
    await this.xbeInflationContract.mintForContracts();
    await this.earnPools(CONTRACT_MANAGER.earnPools);
  }

  async earnPools(init: boolean): Promise<void> {
    this.logger.log('Contracts: cvxCrvVault, cvxVault, hiveVault, sushiVault');
    this.logger.log('Earn Pools');
    if (!init) {
      this.logger.log(`earnPools: init ${init}`);
      return;
    }
    await this.cvxCrvVaultContract.earn();
    await this.cvxVaultContract.earn();
    await this.ironbankVaultContract.earn();
    await this.eurtHiveVaultContract.earn();
    await this.stHiveVaultContract.earn();
    await this.usdnHiveVaultContract.earn();
    await this.sushiVaultContract.earn();
    // await this.cvxCrvVaultContract.earn();
    // await this.cvxVaultContract.earn();
    // await this.hiveVaultContract.earn();
  }

  async startMint(init: boolean = CONTRACT_MANAGER.startMint): Promise<void> {
    if (!init) {
      this.logger.log(`startMint: init ${init}`);
      return;
    }
    await this.bonusCampaignContract.startMint();
    this.logger.log('Start Bonus Campaign');
  }
}
