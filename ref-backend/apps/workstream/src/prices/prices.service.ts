import { Injectable } from '@nestjs/common';
import { Redis } from 'ioredis';
import { InjectRedis } from '@nestjs-modules/ioredis';

@Injectable()
export class PricesService {
  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
  ) {}

  async getPriceBySymbol(symbol: string, currency: string = 'usd'): Promise<string | null> {
    // redis -> get -> symbol:price:currency
    return this.redisService.get(`${symbol}:price:${currency}`);
  }

  async getPriceByAddress(address: string, currency: string = 'usd'): Promise<string | null> {
    // redis -> get -> address:price:currency
    return this.redisService.get(`${address}:price:${currency}`);
  }
}
