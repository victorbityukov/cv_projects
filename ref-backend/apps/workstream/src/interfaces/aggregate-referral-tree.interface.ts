export interface AggregateReferralTreeInterface {
  readonly _id: string;
  readonly parentId?: string;
  readonly referralCode: string;
  readonly children: Array<ChildrenInterface>;
}

export interface ChildrenInterface {
  readonly _id: string;
  readonly createdAt: string;
  readonly referralCode: string;
  readonly depth: number;
  readonly parentId?: string;
}
