import { Injectable } from '@nestjs/common';
import {
  BaseVaultContract,
  CvxCrvVaultContract,
  CvxVaultContract,
  EurtHiveVaultContract,
  // HiveVaultContract,
  IronbankVaultContract,
  StEthHiveVaultContract,
  SushiVaultContract,
  UsdnHiveVaultContract,
} from '@app/contracts';
import { AddressDto, RequestGetStakeDto, ResponsePoolInformationDto } from '@app/dto';
import { CurrencyTypeEnum, PoolNameEnum } from '@app/constants';
import { RatesService } from './rates.service';
import { VaultService } from './vault.service';
import { VotingStakingRewardsService } from './voting-staking-rewards.service';

// deprecated
@Injectable()
export class ClaimService {
  constructor(
    // base pool handlers
    private readonly vaultService: VaultService,
    private readonly votingStakingRewardsService: VotingStakingRewardsService,
    // this is necessary for the logic of claims, this is a temporary solution
    private readonly tokenRatesService: RatesService,
    private readonly cvxVaultContract: CvxVaultContract,
    private readonly cvxCrvVaultContract: CvxCrvVaultContract,
    private readonly sushiVaultContract: SushiVaultContract,
    private readonly ironbankVaultContract: IronbankVaultContract,
    // private readonly hiveVaultContract: HiveVaultContract,
    private readonly eurtHiveVaultContract: EurtHiveVaultContract,
    private readonly stEthHiveVaultContract: StEthHiveVaultContract,
    private readonly usdnHiveVaultContract: UsdnHiveVaultContract,
  ) {}

  // deprecated
  async getAllClaimInfo({ address }: AddressDto): Promise<ResponsePoolInformationDto[]> {
    const xbeClaimInfo = await this.getClaimInfoFofXbe({ address });

    const cvxCrvClaimInfo = await this.getClaimInfo({
      address,
      contract: this.cvxCrvVaultContract,
    });

    const cvxClaimInfo = await this.getClaimInfo({
      address,
      contract: this.cvxVaultContract,
    });

    // const crvClaimInfo = await this.getClaimInfo({
    //   address,
    //   contract: this.hiveVaultContract,
    // });
    // crvClaimInfo.poolName = PoolNameEnum.CRV_VAULT;

    const ironbankClaimInfo = await this.getClaimInfo({
      address,
      contract: this.ironbankVaultContract,
    });
    ironbankClaimInfo.poolName = PoolNameEnum.IRONBANK_VAULT;

    const eurtHiveClaimInfo = await this.getClaimInfo({
      address,
      contract: this.eurtHiveVaultContract,
    });
    eurtHiveClaimInfo.poolName = PoolNameEnum.EURT_VAULT;

    const stEthHiveClaimInfo = await this.getClaimInfo({
      address,
      contract: this.stEthHiveVaultContract,
    });
    stEthHiveClaimInfo.poolName = PoolNameEnum.ST_ETH_VAULT;

    const usdnHiveClaimInfo = await this.getClaimInfo({
      address,
      contract: this.usdnHiveVaultContract,
    });
    usdnHiveClaimInfo.poolName = PoolNameEnum.USDN_VAULT;

    const sushiClaimInfo = await this.getClaimInfo({
      address,
      contract: this.sushiVaultContract,
    });

    return [
      xbeClaimInfo,
      cvxCrvClaimInfo,
      cvxClaimInfo,
      // crvClaimInfo,
      sushiClaimInfo,
      ironbankClaimInfo,
      eurtHiveClaimInfo,
      stEthHiveClaimInfo,
      usdnHiveClaimInfo,
    ];
  }

  // deprecated
  async getClaimInfoByToken({ address, token }: RequestGetStakeDto) {
    switch (token) {
      case CurrencyTypeEnum.XBE:
        return this.getClaimInfoFofXbe({ address });

      case CurrencyTypeEnum.CVX:
        return this.getClaimInfo({
          address,
          contract: this.cvxVaultContract,
        });

      case CurrencyTypeEnum.CVX_CRV:
        return this.getClaimInfo({
          address,
          contract: this.cvxCrvVaultContract,
        });

        // case CurrencyTypeEnum.CRV:
        //   return this.getClaimInfo({
        //     address,
        //     contract: this.hiveVaultContract,
        //   });

      case CurrencyTypeEnum.LP:
        return this.getClaimInfo({
          address,
          contract: this.sushiVaultContract,
        });

      default:
        throw new Error('There is no information about the token.');
    }
  }

  // deprecated
  async getClaimInfoFofXbe({ address }: AddressDto): Promise<ResponsePoolInformationDto> {
    return this.votingStakingRewardsService.getPoolInformationByAddress({ address });
  }

  // deprecated
  async getClaimInfo({
    address,
    contract,
  }: AddressDto & { contract: BaseVaultContract }): Promise<ResponsePoolInformationDto> {
    return this.vaultService.getPoolInformationByAddress(address, contract);
  }
}
