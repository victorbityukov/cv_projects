import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { CurrencyTypeEnum, NetworkEnum, VenueEnum } from '@app/constants';
import { TokenRates } from '@app/dbo';
import { RequestRatesDto, ResponseRatesDto } from '@app/dto';
import BigNumber from 'bignumber.js';

interface GetQuoteProps {
  tokenAddress?: string,
  tokenSymbol?: string,
}

@Injectable()
export class RatesService {
  constructor(
    @InjectModel(TokenRates.name)
    private readonly tokenRatesModel: Model<TokenRates>,
  ) {}

  /**
   * @param input
   */
  async getTokenRates(input: RequestRatesDto): Promise<ResponseRatesDto> {
    const tokenRate = await this.tokenRatesModel
      .findOne({
        token: input.token,
        network: input.network ? input.network : NetworkEnum.ETH_M,
        currency: input.currency ? input.currency : CurrencyTypeEnum.USDT,
        venue: input.venue ? input.venue : { $in: [VenueEnum.SUSHI, VenueEnum.CMC] },
      })
      .sort({ createdAt: -1 });

    if (!tokenRate) {
      throw new NotFoundException(`Rates for ${input.token} in selected currency hasn't been updated yet`);
    }

    return {
      token: tokenRate.token,
      network: tokenRate.network,
      venue: tokenRate.venue,
      quote: new BigNumber(tokenRate.quote).toFixed(),
      quantity: new BigNumber(tokenRate.quantity).toFixed(),
      currency: tokenRate.currency,
      timestamp: tokenRate.updatedAt,
    };
  }

  /**
   * Get a token value in USD
   * @param props
   */
  async getQuoteInUsd(props: GetQuoteProps) {
    const { tokenAddress, tokenSymbol } = props;

    const params: { token?: string, token_id?: string } = {};

    if (tokenSymbol) {
      params.token = tokenSymbol;
    }

    if (tokenAddress) {
      params.token_id = tokenAddress;
    }

    let tokenRate = await this.tokenRatesModel
      .findOne({
        currency: CurrencyTypeEnum.USD,
        ...params,
      })
      .sort({ createdAt: -1 });

    if (!tokenRate) {
      tokenRate = await this.tokenRatesModel
        .findOne({
          currency: CurrencyTypeEnum.USDT,
          ...params,
        })
        .sort({ createdAt: -1 });
    }

    return Number(tokenRate && tokenRate.quote);
  }

  /**
   * @param props
   */
  async getCostInUsdOfToken(props: GetQuoteProps): Promise<BigNumber> {
    return new BigNumber(await this.getQuoteInUsd(props));
  }
}
