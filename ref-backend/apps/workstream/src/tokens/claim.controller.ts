import {
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
} from '@nestjs/swagger';
import {
  Controller, Get, HttpCode, HttpStatus, Query,
} from '@nestjs/common';
import {
  RequestGetClaimDto,
  ResponsePoolInformationDto,
} from '@app/dto';
import { ClaimService } from './claim.service';

// deprecated
@ApiTags('tokens')
@Controller('tokens')
export class ClaimController {
  constructor(private readonly claimService: ClaimService) {}

  @ApiOperation({ description: 'Claim info' })
  @ApiOkResponse({ description: 'Returns claim info', type: () => ResponsePoolInformationDto })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @HttpCode(HttpStatus.OK)
  @Get('get-claim')
  async getTokenStakeInfo(@Query() input: RequestGetClaimDto) {
    const { address, token } = input;

    if (token) {
      return this.claimService.getClaimInfoByToken({ address, token });
    }

    return this.claimService.getAllClaimInfo({ address });
  }
}
