import {
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
} from '@nestjs/swagger';
import {
  Controller, Get, HttpCode, HttpStatus, Query,
} from '@nestjs/common';
import {
  RequestGetStakeDto,
  ResponsePoolInformationDto,
} from '@app/dto';
import { ClaimService } from './claim.service';

// deprecated
@ApiTags('stake')
@Controller('stake')
export class StakeController {
  constructor(
    private readonly claimService: ClaimService,
  ) {}

  @ApiOperation({ description: 'Get token stake info' })
  @ApiOkResponse({ description: 'Returns token stake info', type: () => ResponsePoolInformationDto })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @HttpCode(HttpStatus.OK)
  @Get('get-stake')
  async getStakeInfo(@Query() input: RequestGetStakeDto) {
    const { address, token } = input;

    if (token) {
      return this.claimService.getClaimInfoByToken({ address, token });
    }

    return this.claimService.getAllClaimInfo({ address });
  }
}
