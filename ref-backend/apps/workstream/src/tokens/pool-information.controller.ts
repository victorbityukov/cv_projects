import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
} from '@nestjs/swagger';
import {
  // BadRequestException,
  Controller, Get, HttpCode, HttpStatus, Query,
} from '@nestjs/common';
import {
  ResponsePoolInformationDto,
  RequestPoolInformationDto,
} from '@app/dto';
import { User } from '@app/dbo';
import { PoolInformationService } from './pool-information.service';

@ApiTags('tokens')
@Controller('tokens')
export class PoolInformationController {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
    private readonly poolInformationService: PoolInformationService,
  ) {}

  @ApiOperation({ description: 'Pool information' })
  @ApiOkResponse({
    description: 'Returns pool info',
    type: () => ResponsePoolInformationDto,
    isArray: true,
  })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @HttpCode(HttpStatus.OK)
  @Get('get-pool-info')
  async getTokenStakeInfo(
    @Query() input: RequestPoolInformationDto,
  ): Promise<ResponsePoolInformationDto[]> {
    const { address, poolName } = input;

    if (poolName) {
      const poolInformation = await this.poolInformationService.getPoolInformationByName(
        address,
        poolName,
      );
      return [poolInformation];
    }

    return this.poolInformationService.getPoolsInformation({ address });
  }
}
