import {
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
} from '@nestjs/swagger';
import {
  Controller, Get, HttpCode, HttpStatus, Query,
} from '@nestjs/common';
import {
  ResponseRatesDto,
  RequestRatesDto,
} from '@app/dto';
import { RatesService } from './rates.service';

@ApiTags('rates')
@Controller('rates')
export class RatesController {
  constructor(private readonly ratesService: RatesService) {}

  @ApiOperation({ description: 'Get requested token rates in USD' })
  @ApiOkResponse({
    description: 'Returns usd rates',
    type: () => ResponseRatesDto,
  })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Information not found or hasn\'t been updated yet on server-side' })
  @HttpCode(HttpStatus.OK)
  @Get('get-rates')
  async getTokenRates(
    @Query() input: RequestRatesDto,
  ): Promise<ResponseRatesDto> {
    return this.ratesService.getTokenRates(input);
  }
}
