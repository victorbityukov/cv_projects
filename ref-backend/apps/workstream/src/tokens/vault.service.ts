import { Injectable, Logger } from '@nestjs/common';
import BigNumber from 'bignumber.js';
import {
  BasePairContractFactory,
  BaseTokenContractFactory,
  BaseVaultContract,
} from '@app/contracts';
import { ResponsePoolInformationDto } from '@app/dto';
import utils from 'web3-utils';
import { fixSymbol } from '@app/utils';
import { RatesService } from './rates.service';

@Injectable()
export class VaultService {
  private readonly logger = new Logger(VaultService.name, { timestamp: true });

  constructor(
    private readonly tokenRatesService: RatesService,
    private readonly basePairContractFactory: BasePairContractFactory,
    private readonly baseTokenContractFactory: BaseTokenContractFactory,
  ) {}

  async getPoolInformationByAddress(
    address: string,
    contract: BaseVaultContract,
  ): Promise<ResponsePoolInformationDto> {
    const stakingToken = await contract.stakingToken();
    const stakingTokenContract = await this.baseTokenContractFactory.init(
      stakingToken,
    );
    const stakingTokenSymbol = fixSymbol(
      await stakingTokenContract.methods.symbol().call(),
    );

    let costInUsd = await this.tokenRatesService.getCostInUsdOfToken({
      tokenSymbol: stakingTokenSymbol,
    });
    try {
      if (costInUsd.isZero()) {
        this.logger.warn(`the cost for the token was not found ${stakingTokenSymbol} address: ${stakingToken}`);

        const pairContract = await this.basePairContractFactory.init(stakingToken);
        const totalSupplyForPairContract = await pairContract.methods.totalSupply().call();
        const totalSupplyForPairContractBn = new BigNumber(
          utils.fromWei(totalSupplyForPairContract),
        );

        const token0Address = await pairContract.methods.token0().call();
        const token0Contract = await this.baseTokenContractFactory.init(token0Address);

        const balanceOfToken0 = await token0Contract.methods.balanceOf(stakingToken).call();
        const balanceOfToken0Bn = new BigNumber(utils.fromWei(balanceOfToken0));

        const costLPInToken0Bn = balanceOfToken0Bn.multipliedBy(2).dividedBy(
          totalSupplyForPairContractBn,
        );

        const symbolToken0 = fixSymbol(await token0Contract.methods.symbol().call());
        /* his cost in USD */
        const costToken0InUsdBn = await this.tokenRatesService.getCostInUsdOfToken({
          tokenSymbol: symbolToken0,
        });

        costInUsd = costLPInToken0Bn.multipliedBy(costToken0InUsdBn);
        if (costInUsd.isZero()) {
          this.logger.warn(`the cost for the token0 was not found ${stakingTokenSymbol} address: ${stakingToken}`);
        }
      }
    } catch (e) {
      this.logger.log(e);
    }

    // TVL
    const balancePool = await contract.balance();
    const tvl = balancePool.multipliedBy(costInUsd);
    // Deposits
    const addressBalance = await contract.balanceOf(address);
    const deposits = addressBalance.multipliedBy(costInUsd);
    // Total supply
    const totalSupply = await contract.totalSupply();
    const totalSupplyInUsd = totalSupply.multipliedBy(costInUsd);

    const {
      amountRewardRates,
      amountEarned,
    } = await this.getEarnedAndRewardRates(address, contract);

    let dailyApy = new BigNumber(0);
    let apr = new BigNumber(0);
    if (!totalSupplyInUsd.isZero()) {
      dailyApy = amountRewardRates.multipliedBy(86400)
        .dividedBy(totalSupplyInUsd)
        .multipliedBy(100);
      apr = dailyApy.multipliedBy(365);
    }

    const potentialEarned = amountRewardRates.multipliedBy(86400);

    return {
      poolName: contract.poolName,
      apr: Number(apr && apr.toFixed()),
      tvl: Number(tvl && tvl.toFixed()),
      earned: Number(amountEarned && amountEarned.toFixed()),
      deposits: Number(deposits && deposits.toFixed()),
      potentialEarned: Number(potentialEarned && potentialEarned.toFixed()),
    };
  }

  async oldLogicForAPR(address: string, contract: BaseVaultContract) {
    let earnedInUsdBn = new BigNumber(0);
    let potentialEarnedInUsdBn = new BigNumber(0);
    const duration = 86400 * 365;

    /* all reward tokens addresses that can be receiving in this contract */
    const rewardTokensAddresses = await contract.getRewardTokensAddresses();
    /** get the values for each token */
    for (let i = 0; i < rewardTokensAddresses.length; i += 1) {
      /** string token address */
      const rewardTokensAddress = rewardTokensAddresses[i];
      /** just a web3 contract for a token to get its symbol */
      const tokenContract = await this.baseTokenContractFactory.init(
        rewardTokensAddress,
      );
      /** token contract method to receive symbol */
      const symbol = fixSymbol(await tokenContract.methods.symbol().call());
      /** search by symbol, because in testnet have not actual token address */
      const costTokenInUsdBn = await this.tokenRatesService.getCostInUsdOfToken({
        tokenSymbol: symbol,
      });
      if (costTokenInUsdBn.isZero()) {
        this.logger.warn(`the cost for the token was not found ${symbol} address: ${rewardTokensAddress}`);
      }
      const amountEarnedBn = await contract.getEarned({
        userAddress: address,
        tokenAddress: rewardTokensAddress,
      });
      const costInUsdAllRewardTokensBn = amountEarnedBn.multipliedBy(costTokenInUsdBn);
      earnedInUsdBn = earnedInUsdBn.plus(costInUsdAllRewardTokensBn);
      /** the amount of rewards accumulated for this token */
      const amountPotentialBn = await contract.potentialRewardReturns({
        address,
        duration,
        rewardToken: rewardTokensAddress,
      });
      const amountPotentialInUsdBn = amountPotentialBn.multipliedBy(costTokenInUsdBn);
      potentialEarnedInUsdBn = potentialEarnedInUsdBn.plus(amountPotentialInUsdBn);
    }
  }

  async getEarnedAndRewardRates(address: string, contract: BaseVaultContract) {
    let amountRewardRates = new BigNumber(0);
    let amountEarned = new BigNumber(0);

    /* all reward tokens addresses that can be receiving in this contract */
    const rewardTokensAddresses = await contract.getRewardTokensAddresses();
    /** get the values for each token */
    for (let i = 0; i < rewardTokensAddresses.length; i += 1) {
      /** string token address */
      const rewardTokensAddress = rewardTokensAddresses[i];
      /** just a web3 contract for a token to get its symbol */
      const tokenContract = await this.baseTokenContractFactory.init(
        rewardTokensAddress,
      );
      /** token contract method to receive symbol */
      const symbol = fixSymbol(await tokenContract.methods.symbol().call());
      /** search by symbol, because in testnet have not actual token address */
      const tokenCostInUsd = await this.tokenRatesService.getCostInUsdOfToken({
        tokenSymbol: symbol,
      });
      if (tokenCostInUsd.isZero()) {
        this.logger.warn(`the cost for the token was not found ${symbol} address: ${rewardTokensAddress}`);
      }

      const rewardRates = await contract.getRewardRates({
        rewardToken: rewardTokensAddress,
      });
      const rewardRatesCostInUsd = rewardRates.multipliedBy(tokenCostInUsd);
      amountRewardRates = amountRewardRates.plus(rewardRatesCostInUsd);

      const earned = await contract.getEarned({
        userAddress: address,
        tokenAddress: rewardTokensAddress,
      });
      const earnedCostInUsd = earned.multipliedBy(tokenCostInUsd);
      amountEarned = amountEarned.plus(earnedCostInUsd);
    }

    return { amountRewardRates, amountEarned };
  }
}
