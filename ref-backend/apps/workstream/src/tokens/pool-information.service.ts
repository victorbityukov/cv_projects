import { Injectable } from '@nestjs/common';
import {
  CvxCrvVaultContract,
  CvxVaultContract,
  EurtHiveVaultContract,
  IronbankVaultContract,
  StEthHiveVaultContract,
  SushiVaultContract,
  UsdnHiveVaultContract,
} from '@app/contracts';
import { AddressDto, ResponsePoolInformationDto } from '@app/dto';
import { PoolNameEnum } from '@app/constants';
import { RatesService } from './rates.service';
import { VaultService } from './vault.service';
import { VotingStakingRewardsService } from './voting-staking-rewards.service';
import { CvxCrvService } from './curve-pools/cvx-crv.service';
import { Ib3CrvService } from './curve-pools/ib-3-crv.service';
import { Usdn3CrvCurveService } from './curve-pools/usdn-3-crv-curve.service';
import { StEthCrvService } from './curve-pools/st-eth-crv.service';
import { EurtCrvService } from './curve-pools/eurt-crv.service';

@Injectable()
export class PoolInformationService {
  constructor(
    private readonly tokenRatesService: RatesService,
    // BASE POOLS
    private readonly vaultService: VaultService,
    private readonly cvxCrvService: CvxCrvService,
    private readonly ib3CrvService: Ib3CrvService,
    private readonly usdn3CrvCurveService: Usdn3CrvCurveService,
    private readonly stEthCrvService: StEthCrvService,
    private readonly eurtCrvService: EurtCrvService,
    private readonly votingStakingRewardsService: VotingStakingRewardsService,
    // VAULT POOLS
    // private readonly hiveVaultContract: HiveVaultContract,
    private readonly ironbankVaultContract: IronbankVaultContract,
    private readonly sushiVaultContract: SushiVaultContract,
    private readonly cvxVaultContract: CvxVaultContract,
    private readonly cvxCrvVaultContract: CvxCrvVaultContract,
    private readonly eurtHiveVaultContract: EurtHiveVaultContract,
    private readonly stEthHiveVaultContract: StEthHiveVaultContract,
    private readonly usdnHiveVaultContract: UsdnHiveVaultContract,
  ) {}

  async getPoolsInformation({ address }: AddressDto): Promise<ResponsePoolInformationDto[]> {
    return Promise.all([
      this.getPoolInformationByName(
        address,
        PoolNameEnum.VOTING_STAKING_REWARDS,
      ),
      this.getPoolInformationByName(
        address,
        PoolNameEnum.CVX_VAULT,
      ),
      this.getPoolInformationByName(
        address,
        PoolNameEnum.SUSHI_VAULT,
      ),
      // this.getPoolInformationByName(
      //   address,
      //   PoolNameEnum.CRV_VAULT,
      // ),
      this.getPoolInformationByName(
        address,
        PoolNameEnum.CVX_CRV_VAULT,
      ),
      this.getPoolInformationByName(
        address,
        PoolNameEnum.EURT_VAULT,
      ),
      this.getPoolInformationByName(
        address,
        PoolNameEnum.ST_ETH_VAULT,
      ),
      this.getPoolInformationByName(
        address,
        PoolNameEnum.USDN_VAULT,
      ),
      this.getPoolInformationByName(
        address,
        PoolNameEnum.IRONBANK_VAULT,
      ),
    ]);
  }

  async getPoolInformationByName(
    address: string,
    poolName: PoolNameEnum,
  ): Promise<ResponsePoolInformationDto> {
    let poolInfo: ResponsePoolInformationDto;

    switch (poolName) {
      case PoolNameEnum.VOTING_STAKING_REWARDS:
        poolInfo = await this.votingStakingRewardsService.getPoolInformationByAddress(
          { address },
        );
        break;
      case PoolNameEnum.CVX_VAULT:
        poolInfo = await this.vaultService.getPoolInformationByAddress(
          address,
          this.cvxVaultContract,
        );
        break;
      case PoolNameEnum.SUSHI_VAULT:
        poolInfo = await this.vaultService.getPoolInformationByAddress(
          address,
          this.sushiVaultContract,
        );
        break;

      // For temporary contracts that use hiveVault
      // case PoolNameEnum.CRV_VAULT:
      //   poolInfo = await this.vaultService.getPoolInformationByAddress(
      //     address,
      //     this.hiveVaultContract,
      //   );
      //   poolInfo.poolName = PoolNameEnum.CRV_VAULT;
      //   break;
      case PoolNameEnum.CVX_CRV_VAULT:
        poolInfo = await this.cvxCrvService.getInformationByAddress(
          address,
          // this.cvxCrvVaultContract,
        );
        break;
      case PoolNameEnum.IRONBANK_VAULT:
        poolInfo = await this.ib3CrvService.getInformationByAddress(
          address,
          // this.ironbankVaultContract,
        );
        poolInfo.poolName = PoolNameEnum.IRONBANK_VAULT;
        break;
      case PoolNameEnum.USDN_VAULT:
        poolInfo = await this.usdn3CrvCurveService.getInformationByAddress(
          address,
          // this.usdnHiveVaultContract,
        );
        poolInfo.poolName = PoolNameEnum.USDN_VAULT;
        break;
      case PoolNameEnum.ST_ETH_VAULT:
        poolInfo = await this.stEthCrvService.getInformationByAddress(
          address,
          // this.stEthHiveVaultContract,
        );
        poolInfo.poolName = PoolNameEnum.ST_ETH_VAULT;
        break;
      // case PoolNameEnum.EUR_XB_VAULT:
      //   poolInfo = await this.vaultService.getPoolInformationByAddress(
      //     address,
      //     this.hiveVaultContract,
      //   );
      //   poolInfo.poolName = PoolNameEnum.EUR_XB_VAULT;
      //   break;
      // case PoolNameEnum.EURS_VAULT:
      //   poolInfo = await this.vaultService.getPoolInformationByAddress(
      //     address,
      //     this.hiveVaultContract,
      //   );
      //   poolInfo.poolName = PoolNameEnum.EURS_VAULT;
      //   break;
      case PoolNameEnum.EURT_VAULT:
        poolInfo = await this.eurtCrvService.getInformationByAddress(
          address,
          // this.eurtHiveVaultContract,
        );
        poolInfo.poolName = PoolNameEnum.EURT_VAULT;
        break;
      default:
        throw new Error(`There is no information about the pool. Your pool name is ${poolName}`);
    }

    return poolInfo;
  }
}
