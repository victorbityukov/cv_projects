import { Injectable, Logger } from '@nestjs/common';
import { InjectRedis } from '@nestjs-modules/ioredis';
import BigNumber from 'bignumber.js';
import { Redis } from 'ioredis';
import { ResponsePoolInformationDto } from '@app/dto';
import { BaseTokenContractFactory, IronbankVaultContract } from '@app/contracts';
import { PoolNameEnum } from '@app/constants';
import { fixSymbol } from '@app/utils';
import { fromWei } from 'web3-utils';
import { RatesService } from '../rates.service';

@Injectable()
export class Ib3CrvService {
  private readonly logger = new Logger(Ib3CrvService.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    readonly ironbankVaultContract: IronbankVaultContract,
    private readonly baseTokenContractFactory: BaseTokenContractFactory,
    private readonly tokenRatesService: RatesService,
  ) {}

  async getInformationByAddress(
    address: string,
  ): Promise<ResponsePoolInformationDto> {
    const stakingToken = await this.ironbankVaultContract.stakingToken();
    const virtualPriceBn = new BigNumber(
      fromWei(
        await this.redisService.get(`${stakingToken}:price:usd`) || '0',
      ),
    );
    // TVL
    const balancePool = await this.ironbankVaultContract.balance();
    const tvl = balancePool.multipliedBy(virtualPriceBn);
    // Deposits
    const addressBalance = await this.ironbankVaultContract.balanceOf(address);
    const deposits = addressBalance.multipliedBy(virtualPriceBn);
    // Total supply
    const totalSupply = await this.ironbankVaultContract.totalSupply();
    const totalSupplyInUsd = totalSupply.multipliedBy(virtualPriceBn);

    const {
      amountRewardRates,
      amountEarned,
    } = await this.getEarnedAndRewardRates(address);

    let dailyApy = new BigNumber(0);
    let apr = new BigNumber(0);
    if (!totalSupplyInUsd.isZero()) {
      dailyApy = amountRewardRates.multipliedBy(86400)
        .dividedBy(totalSupplyInUsd)
        .multipliedBy(100);
      apr = dailyApy.multipliedBy(365);
    }

    const potentialEarned = amountRewardRates.multipliedBy(86400);

    return {
      poolName: PoolNameEnum.IRONBANK_VAULT,
      apr: Number(apr && apr.toFixed()),
      tvl: Number(tvl && tvl.toFixed()),
      earned: Number(amountEarned && amountEarned.toFixed()),
      deposits: Number(deposits && deposits.toFixed()),
      potentialEarned: Number(potentialEarned && potentialEarned.toFixed()),
    };
  }

  async getEarnedAndRewardRates(address: string) {
    let amountRewardRates = new BigNumber(0);
    let amountEarned = new BigNumber(0);

    const rewardTokensAddresses = await this.ironbankVaultContract.getRewardTokensAddresses();

    for (let i = 0; i < rewardTokensAddresses.length; i += 1) {
      const rewardTokensAddress = rewardTokensAddresses[i];

      const tokenContract = await this.baseTokenContractFactory.init(
        rewardTokensAddress,
      );

      const symbol = fixSymbol(await tokenContract.methods.symbol().call());
      // console.log(`symbol: ${symbol}`);

      const tokenCostInUsd = await this.tokenRatesService.getCostInUsdOfToken({
        tokenSymbol: symbol,
      });
      // console.log(`tokenCostInUsd: ${tokenCostInUsd}`);

      if (tokenCostInUsd.isZero()) {
        this.logger.warn(`the cost for the token was not found ${symbol} address: ${rewardTokensAddress}`);
      }

      const rewardRates = await this.ironbankVaultContract.getRewardRates({
        rewardToken: rewardTokensAddress,
      });
      // console.log(`rewardRates: ${rewardRates.toFixed()}`);

      const rewardRatesCostInUsd = rewardRates.multipliedBy(tokenCostInUsd);
      // console.log(`rewardRatesCostInUsd: ${rewardRatesCostInUsd.toFixed()}`);

      amountRewardRates = amountRewardRates.plus(rewardRatesCostInUsd);
      // console.log(`amountRewardRates: ${amountRewardRates.toFixed()}`);

      const earned = await this.ironbankVaultContract.getEarned({
        userAddress: address,
        tokenAddress: rewardTokensAddress,
      });
      // console.log(`earned: ${earned.toFixed()}`);

      const earnedCostInUsd = earned.multipliedBy(tokenCostInUsd);
      // console.log(`earnedCostInUsd: ${earnedCostInUsd.toFixed()}`);

      amountEarned = amountEarned.plus(earnedCostInUsd);
      // console.log(`amountEarned: ${amountEarned.toFixed()}`);
    }

    return { amountRewardRates, amountEarned };
  }
}
