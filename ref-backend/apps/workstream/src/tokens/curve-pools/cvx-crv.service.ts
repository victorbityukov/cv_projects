import { Injectable, Logger } from '@nestjs/common';
import { ResponsePoolInformationDto } from '@app/dto';
import { BaseTokenContractFactory, CvxCrvVaultContract } from '@app/contracts';
import { PoolNameEnum } from '@app/constants';
import BigNumber from 'bignumber.js';
import { fixSymbol } from '@app/utils';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import { RatesService } from '../rates.service';

@Injectable()
export class CvxCrvService {
  private readonly logger = new Logger(CvxCrvService.name, { timestamp: true });

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    readonly cvxCrvVaultContract: CvxCrvVaultContract,
    private readonly baseTokenContractFactory: BaseTokenContractFactory,
    private readonly tokenRatesService: RatesService,
  ) {}

  async getInformationByAddress(
    address: string,
  ): Promise<ResponsePoolInformationDto> {
    const stakingToken = await this.cvxCrvVaultContract.stakingToken();
    const virtualPriceBn = new BigNumber(await this.redisService.get(`${stakingToken}:price:usd`) || 0);

    // TVL
    const balancePool = await this.cvxCrvVaultContract.balance();
    const tvl = balancePool.multipliedBy(virtualPriceBn);
    // Deposits
    const addressBalance = await this.cvxCrvVaultContract.balanceOf(address);
    const deposits = addressBalance.multipliedBy(virtualPriceBn);
    // Total supply
    const totalSupply = await this.cvxCrvVaultContract.totalSupply();
    const totalSupplyInUsd = totalSupply.multipliedBy(virtualPriceBn);

    const {
      amountRewardRates,
      amountEarned,
    } = await this.getEarnedAndRewardRates(address);

    let dailyApy = new BigNumber(0);
    let apr = new BigNumber(0);
    if (!totalSupplyInUsd.isZero()) {
      dailyApy = amountRewardRates.multipliedBy(86400)
        .dividedBy(totalSupplyInUsd)
        .multipliedBy(100);
      apr = dailyApy.multipliedBy(365);
    }

    const potentialEarned = amountRewardRates.multipliedBy(86400);

    return {
      poolName: PoolNameEnum.CVX_CRV_VAULT,
      apr: Number(apr && apr.toFixed()),
      tvl: Number(tvl && tvl.toFixed()),
      earned: Number(amountEarned && amountEarned.toFixed()),
      deposits: Number(deposits && deposits.toFixed()),
      potentialEarned: Number(potentialEarned && potentialEarned.toFixed()),
    };
  }

  async getEarnedAndRewardRates(address: string) {
    let amountRewardRates = new BigNumber(0);
    let amountEarned = new BigNumber(0);

    const rewardTokensAddresses = await this.cvxCrvVaultContract.getRewardTokensAddresses();

    for (let i = 0; i < rewardTokensAddresses.length; i += 1) {
      const rewardTokensAddress = rewardTokensAddresses[i];

      const tokenContract = await this.baseTokenContractFactory.init(
        rewardTokensAddress,
      );

      const symbol = fixSymbol(await tokenContract.methods.symbol().call());
      // console.log(`symbol: ${symbol}`);

      const tokenCostInUsd = await this.tokenRatesService.getCostInUsdOfToken({
        tokenSymbol: symbol,
      });
      // console.log(`tokenCostInUsd: ${tokenCostInUsd}`);

      if (tokenCostInUsd.isZero()) {
        this.logger.warn(`the cost for the token was not found ${symbol} address: ${rewardTokensAddress}`);
      }

      const rewardRates = await this.cvxCrvVaultContract.getRewardRates({
        rewardToken: rewardTokensAddress,
      });
      // console.log(`rewardRates: ${rewardRates.toFixed()}`);

      const rewardRatesCostInUsd = rewardRates.multipliedBy(tokenCostInUsd);
      // console.log(`rewardRatesCostInUsd: ${rewardRatesCostInUsd.toFixed()}`);

      amountRewardRates = amountRewardRates.plus(rewardRatesCostInUsd);
      // console.log(`amountRewardRates: ${amountRewardRates.toFixed()}`);

      const earned = await this.cvxCrvVaultContract.getEarned({
        userAddress: address,
        tokenAddress: rewardTokensAddress,
      });
      // console.log(`earned: ${earned.toFixed()}`);

      const earnedCostInUsd = earned.multipliedBy(tokenCostInUsd);
      // console.log(`earnedCostInUsd: ${earnedCostInUsd.toFixed()}`);

      amountEarned = amountEarned.plus(earnedCostInUsd);
      // console.log(`amountEarned: ${amountEarned.toFixed()}`);
    }

    return { amountRewardRates, amountEarned };
  }
}
