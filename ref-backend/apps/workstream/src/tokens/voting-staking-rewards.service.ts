import { Injectable, Logger } from '@nestjs/common';
import BigNumber from 'bignumber.js';
import { BaseTokenContractFactory, VotingStakingRewardsContract } from '@app/contracts';
import { AddressDto, ResponsePoolInformationDto } from '@app/dto';
import { fixSymbol } from '@app/utils';
import { RatesService } from './rates.service';

@Injectable()
export class VotingStakingRewardsService {
  private readonly logger = new Logger(VotingStakingRewardsService.name, { timestamp: true });

  constructor(
    private readonly tokenRatesService: RatesService,
    private readonly baseTokenContractFactory: BaseTokenContractFactory,
    private readonly votingStakingRewardsContract: VotingStakingRewardsContract,
  ) {}

  async getPoolInformationByAddress(
    { address }: AddressDto,
  ): Promise<ResponsePoolInformationDto> {
    /* rewards tokens for xbe pool */
    const rewardsToken = await this.votingStakingRewardsContract.rewardsToken();
    const costInUsd = await this.calculateCostInUsd(rewardsToken);

    const totalSupply = await this.votingStakingRewardsContract.totalSupply();
    const addressBalance = await this.votingStakingRewardsContract.balanceOf(address);
    const amountEarned = await this.votingStakingRewardsContract.earned(address);
    const potentialXbeReturns = await this.votingStakingRewardsContract.potentialXbeReturns(
      86400 * 365,
      address,
    );

    const dailyApy = await this.calculateDailyAPY();
    const apr = this.calculateAPR(dailyApy);

    const tvl = totalSupply.multipliedBy(costInUsd);
    const earnedInUsd = amountEarned.multipliedBy(costInUsd);
    const deposits = addressBalance.multipliedBy(costInUsd);
    const potentialEarnedInUsd = potentialXbeReturns.multipliedBy(costInUsd);

    return {
      poolName: this.votingStakingRewardsContract.poolName,
      apr: Number(apr.toFixed()),
      tvl: Number(tvl.toFixed()),
      earned: Number(earnedInUsd.toFixed()),
      deposits: Number(deposits.toFixed()),
      potentialEarned: Number(potentialEarnedInUsd.toFixed()),
    };
  }

  async calculateCostInUsd(tokenAddress: string) {
    /* cost in usd */
    let costInUsd = await this.tokenRatesService.getCostInUsdOfToken({
      tokenAddress,
    });

    if (costInUsd.isZero()) {
      const tokenContract = await this.baseTokenContractFactory.init(
        tokenAddress,
      );

      const tokenSymbol = fixSymbol(await tokenContract.methods.symbol().call());

      costInUsd = await this.tokenRatesService.getCostInUsdOfToken({
        tokenSymbol,
      });

      if (costInUsd.isZero()) {
        this.logger.warn(`the cost for the token was not found ${tokenSymbol}, address: ${tokenAddress}`);
      }
    }
    return costInUsd;
  }

  async calculateDailyAPY() {
    const rewardRate = await this.votingStakingRewardsContract.rewardRate();
    const totalSupplyBn = await this.votingStakingRewardsContract.totalSupply();
    return rewardRate.multipliedBy(86400).dividedBy(totalSupplyBn).multipliedBy(100);
  }

  calculateAPR(dailyAPY: BigNumber) {
    return dailyAPY.multipliedBy(365);
  }

  // deprecated
  calculateAPROld(addressBalance: BigNumber, potentialXbeReturns: BigNumber) {
    if (addressBalance.isZero()) {
      return new BigNumber(0);
    }
    return potentialXbeReturns.dividedBy(addressBalance).multipliedBy(100);
  }
}
