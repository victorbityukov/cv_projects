import { PipeTransform, Injectable } from '@nestjs/common';
import { RequestUserByAddressDto } from '@app/dto';

@Injectable()
export class UserByAddressPipe implements PipeTransform {
  transform(value: RequestUserByAddressDto) {
    const { address } = value;
    return {
      ...value,
      address: address.toLowerCase(),
    };
  }
}
