import { MiddlewareConsumer, Module } from '@nestjs/common';
import {
  MONGO_CONFIG,
  MONGO_OPTIONS,
  REDIS_CONFIG,
  referralProgramQueueConfig,
  registerUserQueueConfig,
  veXbeDepositQueueConfig,
  veXbeWithdrawQueueConfig,
} from '@app/configuration';
import { MongooseModule } from '@nestjs/mongoose';
import { WssModule } from '@app/wss';
import {
  ContractCollection,
  ContractCollectionSchema,
  ReferralRewardNode,
  ReferralRewardNodeSchema,
  ReferralRewardPayment,
  ReferralRewardPaymentSchema,
  TokenRates,
  TokenRatesSchema,
  User,
  UserSchema,
} from '@app/dbo';
import { RedisModule } from '@nestjs-modules/ioredis';
import { Express } from 'express';
import { BullModule, BullQueueInject } from '@anchan828/nest-bullmq';
import { Queue } from 'bullmq';
import { HttpModule } from '@nestjs/axios';
import { createBullBoard } from 'bull-board';
import { BullMQAdapter } from 'bull-board/bullMQAdapter';
import {
  BaseTokenContractFactory,
  CvxCrvVaultContract,
  CvxVaultContract,
  EurtHiveVaultContract,
  IronbankVaultContract,
  ReferralProgramContract,
  StEthHiveVaultContract,
  SushiVaultContract,
  UsdnHiveVaultContract,
  VeXbeContract,
  VotingStakingRewardsContract,
} from '@app/contracts';
import { Web3ProviderModule } from '@app/web3-provider';
import { BasePairContractFactory } from '@app/contracts/base-pair-contract.factory';
import { CvxCrvCurveContract } from '@app/contracts/curve-pools/cvx-crv-curve.contract';
import { StakeController } from './tokens/stake.controller';
import { ClaimService } from './tokens/claim.service';
import { RatesService } from './tokens/rates.service';
import { StatisticsController } from './statistics/statistics.controller';
import { StatisticsService } from './statistics/statistics.service';
import { ClaimController } from './tokens/claim.controller';
import { XbeLockupService } from './statistics/xbe-lockup.service';
import { RatesController } from './tokens/rates.controller';
import { UsersController } from './users/users.controller';
import { UsersService } from './users/users.service';
import { VaultService } from './tokens/vault.service';
import { VotingStakingRewardsService } from './tokens/voting-staking-rewards.service';
import { PoolInformationController } from './tokens/pool-information.controller';
import { PoolInformationService } from './tokens/pool-information.service';
import { CvxCrvService } from './tokens/curve-pools/cvx-crv.service';
import { PricesModule } from './prices/prices.module';
import { Ib3CrvService } from './tokens/curve-pools/ib-3-crv.service';
import { Usdn3CrvCurveService } from './tokens/curve-pools/usdn-3-crv-curve.service';
import { StEthCrvService } from './tokens/curve-pools/st-eth-crv.service';
import { EurtCrvService } from './tokens/curve-pools/eurt-crv.service';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forRoot(MONGO_CONFIG.connection_string, MONGO_OPTIONS),
    MongooseModule.forFeature([
      { name: ContractCollection.name, schema: ContractCollectionSchema },
      { name: User.name, schema: UserSchema },
      { name: TokenRates.name, schema: TokenRatesSchema },
      { name: ReferralRewardNode.name, schema: ReferralRewardNodeSchema },
      { name: ReferralRewardPayment.name, schema: ReferralRewardPaymentSchema },
    ]),
    RedisModule.forRoot({
      config: REDIS_CONFIG,
    }),
    BullModule.forRoot({
      options: {
        connection: {
          host: REDIS_CONFIG.host,
          port: REDIS_CONFIG.port,
        },
      },
    }),
    BullModule.registerQueue({ queueName: registerUserQueueConfig.name }),
    BullModule.registerQueue({ queueName: referralProgramQueueConfig.name }),
    BullModule.registerQueue({ queueName: veXbeDepositQueueConfig.name }),
    BullModule.registerQueue({ queueName: veXbeWithdrawQueueConfig.name }),
    Web3ProviderModule,
    WssModule,
    PricesModule,
  ],
  controllers: [
    StakeController,
    RatesController,
    ClaimController,
    UsersController,
    StatisticsController,
    PoolInformationController,
  ],
  providers: [
    ClaimService,
    Ib3CrvService,
    Usdn3CrvCurveService,
    StEthCrvService,
    EurtCrvService,
    StEthHiveVaultContract,
    UsdnHiveVaultContract,
    CvxCrvService,
    CvxCrvCurveContract,
    VeXbeContract,
    XbeLockupService,
    VaultService,
    VotingStakingRewardsService,
    PoolInformationService,
    VotingStakingRewardsContract,
    CvxVaultContract,
    BaseTokenContractFactory,
    BasePairContractFactory,
    CvxCrvVaultContract,
    SushiVaultContract,
    UsersService,
    RatesService,
    StatisticsService,
    IronbankVaultContract,
    ReferralProgramContract,
    EurtHiveVaultContract,
    StEthHiveVaultContract,
    UsdnHiveVaultContract,
    // HiveVaultContract,
  ],
})
export class WorkstreamModule {
  private router: Express;

  constructor(
    @BullQueueInject(registerUserQueueConfig.name)
    private readonly registerUserQueue: Queue,
    @BullQueueInject(referralProgramQueueConfig.name)
    private readonly referralProgramQueue: Queue,
    @BullQueueInject(veXbeDepositQueueConfig.name)
    private readonly veXbeDepositQueue: Queue,
    @BullQueueInject(veXbeWithdrawQueueConfig.name)
    private readonly veXbeWithdrawQueue: Queue,
  ) {
    const { router } = createBullBoard([
      new BullMQAdapter(this.registerUserQueue, { readOnlyMode: true }),
      new BullMQAdapter(this.referralProgramQueue, { readOnlyMode: true }),
      new BullMQAdapter(this.veXbeDepositQueue, { readOnlyMode: true }),
      new BullMQAdapter(this.veXbeWithdrawQueue, { readOnlyMode: true }),
    ]);
    this.router = router;
  }

  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(this.router).forRoutes('/queues');
  }
}
