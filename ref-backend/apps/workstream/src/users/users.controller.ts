import {
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Logger,
  Query,
  UsePipes,
} from '@nestjs/common';
import {
  AddressDto,
  ReferralCodeDto,
  RequestUserByAddressDto,
  ResponseMemberLiquidityDto,
  ResponseReferralStatisticsDto,
  ResponseUserDateRegisteredDto,
} from '@app/dto';
import { handleServerErrors } from '@app/error-handler';
import { UserByAddressPipe } from '../pipes';
import { UsersService } from './users.service';

@ApiTags('users')
@Controller('users')
export class UsersController {
  private readonly logger = new Logger(UsersController.name, { timestamp: true });

  constructor(private readonly usersService: UsersService) {}

  @ApiOperation({ description: 'Get member liquidity by user address' })
  @ApiOkResponse({
    description: 'Return list member liquidity by tier one',
    type: () => ResponseMemberLiquidityDto,
  })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Information not found or hasn\'t been updated yet on server-side' })
  @UsePipes(UserByAddressPipe)
  @HttpCode(HttpStatus.OK)
  @Get('user/member-liquidity')
  async getMemberLiquidity(
    @Query() input: RequestUserByAddressDto,
  ): Promise<ResponseMemberLiquidityDto[]> {
    const { address } = input;
    return this.usersService.getMemberLiquidity(address);
  }

  @ApiOperation({ description: 'Get referrals, network deposits and sum claimable referrals by address' })
  @ApiOkResponse({
    description: 'Return referrals, network deposits group by tier and sum claimable',
    type: () => ResponseReferralStatisticsDto,
  })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Information not found or hasn\'t been updated yet on server-side' })
  @UsePipes(UserByAddressPipe)
  @HttpCode(HttpStatus.OK)
  @Get('user/referral-statistics')
  async getReferralStatistic(
    @Query() input: RequestUserByAddressDto,
  ): Promise<ResponseReferralStatisticsDto> {
    const { address } = input;
    return this.usersService.getReferralStatistic(address);
  }

  @ApiOperation({ description: 'Get date registered by address' })
  @ApiOkResponse({ description: 'Returns string', type: () => ResponseUserDateRegisteredDto })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Information not found or hasn\'t been updated yet on server-side' })
  @UsePipes(UserByAddressPipe)
  @HttpCode(HttpStatus.OK)
  @Get('user/date-registered')
  async getDateRegistered(
    @Query() input: RequestUserByAddressDto,
  ): Promise<ResponseUserDateRegisteredDto> {
    const { address } = input;
    return this.usersService.getDateRegistered(address);
  }

  @ApiOperation({ description: 'Add referral counter to user by referral code' })
  @ApiOkResponse({ description: 'Returns returns void, without checking referral code status' })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Information not found or hasn\'t been updated yet on server-side' })
  @HttpCode(HttpStatus.OK)
  @Get('user/referral-hit')
  async referralHit(@Query() input: ReferralCodeDto): Promise<void> {
    const { referralCode } = input;
    return this.usersService.referralHit(referralCode);
  }

  @ApiOperation({ description: 'Get user referral code by address' })
  @ApiOkResponse({
    description: 'Returns user referral code',
    type: () => ReferralCodeDto,
  })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Information not found or hasn\'t been updated yet on server-side' })
  @UsePipes(UserByAddressPipe)
  @HttpCode(HttpStatus.OK)
  @Get('user/code')
  async getCodeByAddress(@Query() input: RequestUserByAddressDto): Promise<ReferralCodeDto> {
    try {
      return await this.usersService.getCodeByAddress(input.address);
    } catch (errorOrException) {
      throw handleServerErrors({
        errorOrException,
        logger: this.logger,
        methodName: 'getCodeByAddress',
      });
    }
  }

  @ApiOperation({ description: 'Get user address by code' })
  @ApiOkResponse({
    description: 'Returns user address',
    type: () => AddressDto,
  })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Information not found or hasn\'t been updated yet on server-side' })
  @HttpCode(HttpStatus.OK)
  @Get('user/address')
  async getAddressByAdCode(
    @Query() input: ReferralCodeDto,
  ): Promise<AddressDto> {
    try {
      return await this.usersService.getAddressByCode(input.referralCode);
    } catch (errorOrException) {
      throw handleServerErrors({
        errorOrException,
        logger: this.logger,
        methodName: 'getAddressByCode',
      });
    }
  }
}
