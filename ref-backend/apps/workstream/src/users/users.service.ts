import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import { Web3ProviderService } from '@app/web3-provider';
import {
  BaseVaultContract,
  CvxCrvVaultContract,
  CvxVaultContract,
  EurtHiveVaultContract,
  IronbankVaultContract,
  ReferralProgramContract,
  StEthHiveVaultContract,
  SushiVaultContract,
  UsdnHiveVaultContract,
  VotingStakingRewardsContract,
} from '@app/contracts';
import { BadRequestException, Logger, NotFoundException } from '@nestjs/common';
import { handleServerErrors } from '@app/error-handler';
import { ContractCollection, ReferralRewardNode, ReferralRewardPayment, User } from '@app/dbo';
import {
  AddressDto,
  ReferralCodeDto,
  ResponseMemberLiquidityDto,
  ResponseReferralStatisticsDto,
  ResponseUserDateRegisteredDto,
} from '@app/dto';
import { from, lastValueFrom } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { RatesService } from '../tokens/rates.service';
import { ClaimService } from '../tokens/claim.service';
import { userMock } from '../../../../files';
import { AggregateReferralTreeInterface, ChildrenInterface } from '../interfaces';

export class UsersService {
  private readonly logger = new Logger(UsersService.name, { timestamp: true });

  private vaultContracts: BaseVaultContract[];

  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
    @InjectModel(ReferralRewardNode.name)
    private readonly referralRewardNodeModel: Model<ReferralRewardNode>,
    @InjectModel(ReferralRewardPayment.name)
    private readonly referralRewardPaymentModel: Model<ReferralRewardPayment>,
    @InjectModel(ContractCollection.name)
    private readonly contractModel: Model<ContractCollection>,
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly web3Provider: Web3ProviderService,
    private readonly tokenRatesService: RatesService,
    private readonly ironbankVaultContract: IronbankVaultContract,
    private readonly cvxVaultContract: CvxVaultContract,
    private readonly cvxCrvVaultContract: CvxCrvVaultContract,
    private readonly sushiVaultContract: SushiVaultContract,
    private readonly votingStakingRewardsContract: VotingStakingRewardsContract,
    private readonly referralProgramContract: ReferralProgramContract,
    private readonly claimService: ClaimService,
    private readonly eurtHiveVaultContract: EurtHiveVaultContract,
    private readonly stEthHiveVaultContract: StEthHiveVaultContract,
    private readonly usdnHiveVaultContract: UsdnHiveVaultContract,
  ) {
    this.vaultContracts = [
      this.ironbankVaultContract,
      this.eurtHiveVaultContract,
      this.stEthHiveVaultContract,
      this.usdnHiveVaultContract,
      this.cvxVaultContract,
      this.cvxCrvVaultContract,
      this.sushiVaultContract,
    ];
  }

  async createUser(address: string, referralCode?: string): Promise<void> {
    let parent: User | null;

    if (!referralCode) {
      parent = await this.userModel.findOne({
        referralCode: userMock.referralCode,
      });
    } else {
      parent = await this.userModel.findOne({ referralCode });
    }

    if (!parent) {
      throw new NotFoundException(`Parent user with code: ${referralCode} not found`);
    }

    const exists = await this.userModel.findById({ _id: address });
    if (exists) {
      throw new BadRequestException('User already exists');
    }

    try {
      await this.referralProgramContract.createUser(parent._id, address);
    } catch (errorOrException) {
      throw handleServerErrors({
        errorOrException,
        logger: this.logger,
        methodName: 'createUser',
      });
    }
  }

  async referralHit(referralCode: string): Promise<void> {
    try {
      const user = await this.userModel.findOne({ referralCode });
      if (user) {
        user.referralHit += 1;
        await user.save();
      }
    } catch (errorOrException) {
      throw handleServerErrors({
        errorOrException,
        logger: this.logger,
        methodName: 'referralHit',
      });
    }
  }

  async getCodeByAddress(address: string): Promise<ReferralCodeDto> {
    const user = await this.userModel.findOne({ _id: address });
    if (!user) {
      throw new NotFoundException(`User by address '${address}' not found`);
    }
    return {
      referralCode: user.referralCode,
    };
  }

  async getAddressByCode(code: string): Promise<AddressDto> {
    const user = await this.userModel.findOne({ referralCode: code });
    if (!user) {
      throw new NotFoundException(`User by code '${code}' not found`);
    }
    return {
      address: user._id,
    };
  }

  async getReferralStatistic(address: string): Promise<ResponseReferralStatisticsDto> {
    const children: ChildrenInterface[] = await this.getChildrenByAddress(address, 2);
    const distributionList = await this.referralProgramContract.getDistributionList();
    let referralHits = 0;

    const user = await this.userModel.findById(address);

    if (user) referralHits = user.referralHit;

    const childrenByTier = {
      list: [
        children.filter((child) => child.depth === 0).length,
        children.filter((child) => child.depth === 1).length,
        children.filter((child) => child.depth === 2).length,
      ],
      totalAmount: children.length,
    };

    const levelsDeposit = [0, 0, 0];
    const levelsClaim = [0, 0, 0];

    await lastValueFrom(from(children)
      .pipe(
        mergeMap(async (child) => {
          const { depth } = child;

          const deposits: number[] = [];
          const earned: number[] = [];

          await lastValueFrom(from(this.vaultContracts)
            .pipe(
              mergeMap(async (contract) => {
                try {
                  const claim = await this.claimService
                    .getClaimInfo({ address: child._id, contract });
                  const { deposits: deposit, earned: earn } = claim;
                  deposits.push(deposit);
                  earned.push(earn);
                } catch (errorOrException) {
                  handleServerErrors({
                    errorOrException,
                    logger: this.logger,
                    methodName: 'getReferralStatistic',
                  });
                }
              }, this.vaultContracts.length),
            ), { defaultValue: [] });

          levelsDeposit[depth] += deposits
            .reduce((accumulator: number, deposit: number) => accumulator + deposit);
          levelsClaim[depth] += earned
            .reduce((accumulator: number, claim: number) => accumulator + claim);
        }, 1),
      ), { defaultValue: [] });

    const [claimTier1, claimTier2, claimTier3] = levelsClaim;
    const claimReward1 = claimTier1 * (distributionList[0] / 1000);
    const claimReward2 = claimTier2 * (distributionList[1] / 1000);
    const claimReward3 = claimTier3 * (distributionList[2] / 1000);

    const [depositsTier1, depositsTier2, depositsTier3] = levelsDeposit;

    return {
      members: childrenByTier,
      deposits: {
        list: [depositsTier1, depositsTier2, depositsTier3],
        totalSum: levelsDeposit.reduce((sum: number, deposits: number) => sum + deposits),
      },
      claimable: claimReward1 + claimReward2 + claimReward3,
      referralHits,
    };
  }

  async getDateRegistered(
    address: string,
  ): Promise<ResponseUserDateRegisteredDto> {
    try {
      const date:
        | (User & { createdAt?: string })
        | null = await this.userModel.findOne({
        _id: address.toLowerCase(),
      });
      if (!date) {
        throw new NotFoundException(`User by address '${address}' not found`);
      }
      const { createdAt } = date;
      if (!createdAt) {
        throw new NotFoundException('Field createdAt not found');
      }
      return { date: createdAt };
    } catch (errorOrException) {
      throw handleServerErrors({
        errorOrException,
        logger: this.logger,
        methodName: 'getDateRegistered',
      });
    }
  }

  async getMemberLiquidity(address: string): Promise<ResponseMemberLiquidityDto[]> {
    const children: ChildrenInterface[] = await this.getChildrenByAddress(address, 0);

    const memberLiquidity: ResponseMemberLiquidityDto[] = [];
    await lastValueFrom(from(children)
      .pipe(
        mergeMap(async (child) => {
          const { createdAt, referralCode } = child;
          const res: number[] = [];

          await lastValueFrom(from(this.vaultContracts)
            .pipe(
              mergeMap(async (contract) => {
                try {
                  const liquidity = await this.calculateLiquidityForMember(child._id, contract);
                  res.push(liquidity);
                } catch (errorOrException) {
                  handleServerErrors({
                    errorOrException,
                    logger: this.logger,
                    methodName: 'getMemberLiquidity',
                  });
                }
              }, this.vaultContracts.length),
            ), { defaultValue: [] });

          const liquidity = res
            .reduce((accumulator: number, currentValue: number) => accumulator + currentValue);
          memberLiquidity.push({
            referralCode,
            date: createdAt,
            liquidity,
          });
        }, 1),
      ), { defaultValue: [] });

    return memberLiquidity;
  }

  async calculateLiquidityForMember(user: string, contract: BaseVaultContract): Promise<number> {
    const stakingToken = await contract.stakingToken();
    const costInUSD = await this.tokenRatesService.getQuoteInUsd({
      tokenAddress: stakingToken,
    });
    const balanceOfAddressUint256 = await contract.balanceOf(user);
    const balanceOfAddress = Number(balanceOfAddressUint256.toFixed());
    return balanceOfAddress * costInUSD;
  }

  private async getChildrenByAddress(
    address: string,
    depth: number = 2,
  ): Promise<ChildrenInterface[]> {
    const [user] = await this.userModel.aggregate<AggregateReferralTreeInterface>([
      { $match: { _id: address } },
      {
        $graphLookup: {
          from: 'users',
          startWith: address,
          connectFromField: '_id',
          connectToField: 'parentId',
          depthField: 'depth',
          as: 'children',
          maxDepth: depth,
        },
      },
    ]);

    if (!user) {
      throw new NotFoundException(`User by address '${address}' not found`);
    }

    const { children } = user;
    return children;
  }
}
