import {
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Redis } from 'ioredis';
import {
  Controller, Get, HttpCode, HttpStatus, Query, 
} from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { InjectRedis } from '@nestjs-modules/ioredis';
import {
  RequestGetStatisticsDto,
  ResponseGetCurveApysDto,
  ResponseGetStatisticsDto,
  ResponseXbeLockupDto,
  ResponseXbeLookupBoostDto,
} from '@app/dto';
import { lastValueFrom } from 'rxjs';
import { CURVE_APYS_ENDPOINT } from '@app/constants';
import { ResponseGetCurveApysInterface } from '@app/dto/response-get-curve-apys.dto';
import { StatisticsService } from './statistics.service';
import { XbeLockupService } from './xbe-lockup.service';

@ApiTags('statistics')
@Controller('statistics')
export class StatisticsController {
  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
    private httpService: HttpService,
    private readonly statisticsService: StatisticsService,
    private readonly xbeLockupService: XbeLockupService,
  ) {}

  @ApiOperation({ description: 'Statistics info' })
  @ApiOkResponse({ description: 'Returns statistics info', type: () => ResponseGetStatisticsDto })
  @ApiNotFoundResponse({ description: 'User not found' })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @HttpCode(HttpStatus.OK)
  @Get('get-statistics')
  async getStatistics(@Query() input: RequestGetStatisticsDto): Promise<ResponseGetStatisticsDto> {
    const { address } = input;

    return this.statisticsService.getStatisticsInfo({ address });
  }

  @ApiOperation({ description: 'Get xbe lockup' })
  @ApiOkResponse({
    description: 'Returns xbe lockup',
    type: () => ResponseXbeLockupDto,
  })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Information not found or hasn\'t been updated yet on server-side' })
  @HttpCode(HttpStatus.OK)
  @Get('xbe/lockup')
  async getXBELockup(): Promise<ResponseXbeLockupDto> {
    return this.xbeLockupService.getXBELockup();
  }

  @ApiOperation({ description: 'Get boost information about locked token' })
  @ApiOkResponse({
    description: 'Returns xbe lockup boost',
    type: () => ResponseXbeLookupBoostDto,
  })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Information not found or hasn\'t been updated yet on server-side' })
  @HttpCode(HttpStatus.OK)
  @Get('xbe/lockup/boost')
  async getXBELockupBoost(): Promise<ResponseXbeLookupBoostDto> {
    return this.xbeLockupService.getXBELockupBoost();
  }

  @ApiOperation({ description: 'Proxy for GET https://www.convexfinance.com/api/curve-apys' })
  @ApiOkResponse({
    description: 'Returns info for ib, usdn, steth, eurs, cvxcrv',
    type: () => ResponseGetCurveApysDto,
  })
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @HttpCode(HttpStatus.OK)
  @Get('curve-apys')
  async getCurveApys(): Promise<ResponseGetCurveApysDto> {
    let curveApysResponse: ResponseGetCurveApysDto;

    const cacheName = 'CURVE_APYS_ENDPOINT';
    const cache = await this.redisService.get(cacheName);

    if (cache) {
      curveApysResponse = JSON.parse(cache);
    } else {
      const response = await lastValueFrom(
        this.httpService.get<ResponseGetCurveApysInterface>(CURVE_APYS_ENDPOINT),
      );

      const {
        ib, usdn, steth, eurs, cvxcrv, eurt,
      } = response.data.apys;

      curveApysResponse = {
        ib, usdn, steth, eurs, cvxcrv, eurt,
      };

      await this.redisService.set(cacheName, JSON.stringify(curveApysResponse), 'EX', 30);
    }

    return curveApysResponse;
  }
}
