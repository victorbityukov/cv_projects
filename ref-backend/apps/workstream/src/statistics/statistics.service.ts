import { Injectable } from '@nestjs/common';
import { AddressDto, ResponseGetStatisticsDto } from '@app/dto';
import { PoolNameEnum } from '@app/constants';
import { PoolInformationService } from '../tokens/pool-information.service';

@Injectable()
export class StatisticsService {
  constructor(
    private readonly poolInformationService: PoolInformationService,
  ) {}

  /**
   * Getting all statistics data for the current user
   * @param address
   */
  async getStatisticsInfo({ address }: AddressDto): Promise<ResponseGetStatisticsDto> {
    const allClaimInfoForUser = await this.poolInformationService.getPoolsInformation({ address });

    let totalClaimable = 0;
    let totalDeposits = 0;
    let totalCombinedApy = 0;
    let totalCvxEarned = 0;
    let totalCrvEarned = 0;
    let totalXbeEarned = 0;
    let totalCvxCrvEarned = 0;
    let stakedAssetsYield = 0;
    let totalPotentialRewards = 0;

    for (let i = 0; i < allClaimInfoForUser.length; i += 1) {
      const claimInfo = allClaimInfoForUser[i];

      const { deposits, earned, potentialEarned } = claimInfo;

      totalPotentialRewards += potentialEarned || 0;
      stakedAssetsYield += totalPotentialRewards;

      totalDeposits += deposits;
      totalClaimable = earned;

      switch (claimInfo.poolName) {
        case PoolNameEnum.VOTING_STAKING_REWARDS:
          totalXbeEarned = earned;
          break;
        case PoolNameEnum.CVX_VAULT:
          totalCvxEarned = earned;
          break;
        case PoolNameEnum.CVX_CRV_VAULT:
          totalCvxCrvEarned = earned;
          break;
        case PoolNameEnum.IRONBANK_VAULT:
        case PoolNameEnum.EURT_VAULT:
        case PoolNameEnum.USDN_VAULT:
        case PoolNameEnum.ST_ETH_VAULT:
          totalCrvEarned += earned;
          break;
        default:
          break;
      }
    }

    if (stakedAssetsYield && totalDeposits) {
      totalCombinedApy = (stakedAssetsYield / totalDeposits) * 100;
    }

    const dailyYield = stakedAssetsYield && stakedAssetsYield / 365;

    return {
      totalClaimable,
      totalCombinedApy,
      totalDeposits,
      totalCvxEarned,
      totalCrvEarned,
      totalCvxCrvEarned,
      totalXbeEarned,
      stakedAssetsYield,
      dailyYield,
    };
  }
}
