import { Injectable, Logger } from '@nestjs/common';
import { ResponseXbeLockupDto, ResponseXbeLookupBoostDto } from '@app/dto';
import {
  BONUS_CAMPAIGN_BONUS_EMISSION,
  BONUS_CAMPAIGN_STOP_REGISTER_TIME,
  BONUS_CAMPAIGN_TOTAL_SUPPLY,
  TOTAL_LOCKED_XBE_AVG_LOCK_TIME,
  VE_XBE_LOCKED_SUPPLY,
  VE_XBE_SUPPLY,
  VE_XBE_TOTAL_SUPPLY,
  VOTING_STAKING_REWARDS_DAILY_APY,
  VOTING_STAKING_REWARDS_YEARLY_APY,
  XBE_BALANCE_OF_CLIENT,
  XBE_TOTAL_SUPPLY,
} from '@app/dbo';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import BigNumber from 'bignumber.js';

@Injectable()
export class XbeLockupService {
  private readonly logger = new Logger(XbeLockupService.name);

  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
  ) {}

  async getXBELockup(): Promise<ResponseXbeLockupDto> {
    try {
      const xbeTotalSupply = new BigNumber(
        await this.redisService.get(XBE_TOTAL_SUPPLY) || 0,
      );

      const veXbeSupply = new BigNumber(
        await this.redisService.get(VE_XBE_SUPPLY) || 0,
      );
      const veXbeLockedSupply = new BigNumber(
        await this.redisService.get(VE_XBE_LOCKED_SUPPLY) || 0,
      );

      const bonusCampaignBonusEmission = new BigNumber(
        await this.redisService.get(BONUS_CAMPAIGN_BONUS_EMISSION) || 0,
      );
      const bonusCampaignTotalSupply = new BigNumber(
        await this.redisService.get(BONUS_CAMPAIGN_TOTAL_SUPPLY) || 0,
      );
      const bonusCampaignStopRegisterTime = Number(
        await this.redisService.get(BONUS_CAMPAIGN_STOP_REGISTER_TIME) || 0,
      );

      let votingStakingRewardsDailyApy = new BigNumber(
        await this.redisService.get(VOTING_STAKING_REWARDS_DAILY_APY) || 0,
      );
      let votingStakingRewardsYearlyApy = new BigNumber(
        await this.redisService.get(VOTING_STAKING_REWARDS_YEARLY_APY) || 0,
      );

      if (Date.now() < (bonusCampaignStopRegisterTime * 1000)) {
        const addingApy = bonusCampaignBonusEmission
          .multipliedBy(365)
          .dividedBy(700)
          .dividedBy(bonusCampaignTotalSupply)
          .multipliedBy(100);

        if (addingApy.isFinite()) {
          votingStakingRewardsYearlyApy = votingStakingRewardsYearlyApy.plus(addingApy);
        } else {
          votingStakingRewardsYearlyApy = new BigNumber(0);
        }
        votingStakingRewardsDailyApy = votingStakingRewardsYearlyApy.dividedBy(365);
      }

      const circulatingSupply = await this.getCirculatingSupply();
      let totalLockedXbeShare = new BigNumber(0);
      if (!veXbeLockedSupply.isZero() && !circulatingSupply.isZero()) {
        totalLockedXbeShare = veXbeLockedSupply
          .dividedBy(circulatingSupply)
          .multipliedBy(100);
      }

      return {
        apy: Number(votingStakingRewardsYearlyApy.toFixed()),
        daily_apy: Number(votingStakingRewardsDailyApy.toFixed()),
        total_locked_xbe: Number(veXbeSupply.toFixed()),
        circulating_supply: Number(circulatingSupply.toFixed()),
        total_circulating_supply: Number(xbeTotalSupply.toFixed()),
        total_locked_xbe_share: Number(totalLockedXbeShare.toFixed()),
      };
    } catch (e) {
      this.logger.error(e);
      throw Error('Server error');
    }
  }

  async getXBELockupBoost(): Promise<ResponseXbeLookupBoostDto> {
    try {
      const totalLocked = new BigNumber(
        await this.redisService.get(VE_XBE_SUPPLY) || 0,
      );
      const totalVeXbe = new BigNumber(
        await this.redisService.get(VE_XBE_TOTAL_SUPPLY) || 0,
      );
      const lockedSupply = new BigNumber(
        await this.redisService.get(VE_XBE_LOCKED_SUPPLY) || 0,
      );
      const totalLockedXbeAvgLockTime = Number(
        await this.redisService.get(TOTAL_LOCKED_XBE_AVG_LOCK_TIME) || 0,
      );

      const circulatingSupply = await this.getCirculatingSupply();

      let totalLockedXbeShare = new BigNumber(0);
      if (!lockedSupply.isZero() && !circulatingSupply.isZero()) {
        totalLockedXbeShare = lockedSupply
          .dividedBy(circulatingSupply)
          .multipliedBy(100);
      }

      return {
        total_locked_xbe_avg_lock_time: totalLockedXbeAvgLockTime,
        total_votes_locked: Number(totalLocked.toFixed()),
        total_locked_xbe_ve_total: Number(totalVeXbe.toFixed()),
        total_locked_xbe_share: Number(totalLockedXbeShare.toFixed()),
      };
    } catch (e) {
      this.logger.error(e);
      throw Error('Server error');
    }
  }

  async getCirculatingSupply(): Promise<BigNumber> {
    const xbeBalanceOfClient = new BigNumber(
      await this.redisService.get(XBE_BALANCE_OF_CLIENT) || 0,
    );

    const xbeTotalSupply = new BigNumber(
      await this.redisService.get(XBE_TOTAL_SUPPLY) || 0,
    );

    return xbeTotalSupply.minus(xbeBalanceOfClient);
  }
}
