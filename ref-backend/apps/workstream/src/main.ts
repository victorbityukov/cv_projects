import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import cors from 'cors';
import { WorkstreamModule } from './workstream.module';

const PORT = 3004;

async function bootstrap() {
  const app = await NestFactory.create(WorkstreamModule);

  app.use(cors());
  app.setGlobalPrefix('api/v1');
  app.useGlobalPipes(new ValidationPipe());

  app.enableCors({
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    allowedHeaders:
      'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Observe',
    credentials: true,
  });

  const options = new DocumentBuilder()
    .setTitle('Workstream')
    .setDescription('Provides REST API')
    .setVersion('1.0.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/v1/docs', app, document);

  await app.listen(PORT);
  Logger.log(`workstream service running PORT: ${PORT}`);
}

bootstrap();
