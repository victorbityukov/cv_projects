import { firstValueFrom } from 'rxjs';
import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { OpenWeatherConfig } from '@app/config';
import { ResponseCityWeatherDto } from './dto';

@Injectable()
export class AppService {
  constructor(private http: HttpService) {}

  async getWeatherCity(
    city: string,
  ): Promise<ResponseCityWeatherDto | undefined> {
    const { key, url } = OpenWeatherConfig;
    try {
      const res = await firstValueFrom(
        this.http.get(
          `${url}/weather?q=${city}&appid=${key}&units=metric&lang=ru`,
        ),
        { defaultValue: undefined },
      );
      const { data } = res;
      return data;
    } catch (e) {
      return undefined;
    }
  }
}
