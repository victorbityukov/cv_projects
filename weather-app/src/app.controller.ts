import { Controller, Get, Query, Render } from '@nestjs/common';
import { AppService } from './app.service';
import { ResponseCityWeatherDto } from './dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @Render('index')
  searchPage() {
    return;
  }

  @Get('/weather')
  @Render('weather')
  cityWeather(@Query('city') city: string): Promise<ResponseCityWeatherDto> {
    return this.appService.getWeatherCity(city);
  }
}
