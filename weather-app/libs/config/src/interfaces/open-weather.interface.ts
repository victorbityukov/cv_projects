export interface OpenWeatherInterface {
  readonly url: string;
  readonly key: string;
}
