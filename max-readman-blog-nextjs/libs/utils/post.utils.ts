export const calcReadTimeByText = (content: string): number => {
  const wordsPerMinute = 150;
  const countSpaces = content.split(' ').length;
  return Math.ceil(countSpaces / wordsPerMinute);
};
