function getYoutubeVideoId(url: string): string | null {
  const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/
  const match = url.match(regExp)
  return match && match[2].length === 11 ? match[2] : null
}

function getYoutubeIframeMarkup({url}: { url: string }): string {
  const videoId = getYoutubeVideoId(url)
  if (!videoId) {
    return ''
  }
  return `<iframe
              src="https://www.youtube-nocookie.com/embed/${videoId}"
              width="640px"
              height="390px"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
           ></iframe>`
}

export const youtubeOptions = {
  validate: function(params: string) {
    const match = params.trim().match(/\[(.*)/);
    if (match) {
      return match[1];
    }
    return null;
  },
  render: function(tokens: any, idx: any) {
    // check if the type is 'container_youtube_open'
    if (tokens[idx].type === 'container_youtube_open') {
      // match the strings that are in the form `youtube[YOUTUBE_URL]`
      const matches = tokens[idx].info.trim().match(/^youtube\s*\[(.*)]$/)

      // If matches are found, and there is also a match for the first group, fetch it. That match is our youtube URL.
      if (matches && matches[1]) {
        // Now generate iframe markup from that youtube url.
        return (
          '<div class="text-center video-container">' +
          getYoutubeIframeMarkup({url: matches[1].trim()}) +
          '</div>' +
          '<div class="video-description font-weight-light text-capitalize">'
        )
      }
    } else if (tokens[idx].type === 'container_youtube_close') {
      // when we get to the 'container_youtube_close', close the div that we opened before.
      return '</div>'
    }
  }
};

