import MarkdownIt from 'markdown-it';
import markdownItContainer from 'markdown-it-container';
import { insPlugin } from './ins-plugin';
import { youtubeOptions } from './youtube-plugin';

const mdParser = new MarkdownIt();
mdParser
  .use(insPlugin)
  .use(markdownItContainer, 'youtube', youtubeOptions);

export default mdParser;
