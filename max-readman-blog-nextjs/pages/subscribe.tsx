import { ReactElement } from 'react';
import { Layout, SubscribeContent } from '../components';
import { ThemeEnum } from '../libs/constants';

export default function Subscribe() {
  return <SubscribeContent />;
}

Subscribe.getLayout = function getLayout(page: ReactElement) {
  return (
    <Layout theme={ThemeEnum.LIGHT}>
      {page}
    </Layout>
  )
}
