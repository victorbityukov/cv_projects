import { ReactElement } from 'react';
import { Layout } from '../components';
import { ThemeEnum } from '../libs/constants';
import Head from 'next/head';

export default function NotFound404() {
  return (
    <div className='not-found-container'>
      <Head>
        <title>Max Readman | 404</title>
      </Head>
      <h1>404 | Страницы не существует</h1>
    </div>
  );
}

NotFound404.getLayout = function getLayout(page: ReactElement) {
  return (
    <Layout theme={ThemeEnum.LIGHT}>
      {page}
    </Layout>
  )
}
