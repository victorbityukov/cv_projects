import { ReactElement } from 'react';
import axios from 'axios';
import {
  ArticlesSubscriberSection,
  ArticlesSection,
  CategoriesSection,
  GreetingsSection,
  Layout,
} from '../components';
import { ThemeEnum } from '../libs/constants';
import { API_URL } from '../libs/config';
import { IPost } from '../store';

interface IHome {
  posts: IPost[]
}

export default function Home({posts}: IHome) {
  return (
    <>
      <GreetingsSection/>
      <CategoriesSection/>
      <ArticlesSection posts={posts}/>
      <ArticlesSubscriberSection posts={posts}/>
    </>
  );
}

export const getServerSideProps = async () => {
  try {
    const {data} = await axios.get(`${API_URL}/posts`);
    if (!data) {
      return {
        props: {posts: []},
      }
    }
    return {
      props: {posts: data.filter((post: IPost) => (post.status === 'approve'))},
    }
  } catch (e) {
    return {
      props: {posts: []},
    }
  }
}

Home.getLayout = function getLayout(page: ReactElement) {
  return (
    <Layout theme={ThemeEnum.LIGHT}>
      {page}
    </Layout>
  )
}
