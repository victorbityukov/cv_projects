import { ReactElement } from 'react';
import { AdminContainer, Layout } from '../components';
import { ThemeEnum } from '../libs/constants';

const Admin = () => {
  return <AdminContainer />;
}

Admin.getLayout = function getLayout(page: ReactElement) {
  return (
    <Layout theme={ThemeEnum.LIGHT}>
      {page}
    </Layout>
  )
}

export default Admin
