import { ReactElement } from 'react';
import { Layout } from '../components';
import { ThemeEnum } from '../libs/constants';
import { ArchiveContent, ArchiveDescription } from '../components/Archive';

export default function Archive() {
  return (
    <div className={'archive'}>
      <ArchiveDescription />
      <ArchiveContent />
    </div>
  )
}

Archive.getLayout = function getLayout(page: ReactElement) {
  return (
    <Layout theme={ThemeEnum.LIGHT}>
      {page}
    </Layout>
  )
}
