import { ReactElement, useEffect, useState } from 'react';
import moment from 'moment';
import { NextPageContext } from 'next';
import axios from 'axios';
import { inject, observer } from 'mobx-react';
import ReactHtmlParser from 'react-html-parser';
import { checkCookies, getCookie } from 'cookies-next';
import Head from 'next/head';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { ExtendSubscribeBlock, GuestBlock, Layout, RelatedPosts } from '../../components';
import { ThemeEnum } from '../../libs/constants';
import ArrowDownIcon from '../../public/icons/arrow-down.svg';
import mdParser from '../../libs/utils/md-parser';
import { getFromStorage, IPost, IPostFull, RootStore } from '../../store';
import { API_URL } from '../../libs/config';
import styles from './Post.module.scss';

const Post = ({post: serverPost}: { post: IPostFull | null }) => {
  const router = useRouter();
  const [post, setPost] = useState(serverPost);

  useEffect(() => {
    const load = async () => {
      const isAccessToken = getFromStorage('accessToken');
      if (!isAccessToken) {
        try {
          const {data} = await axios.get(`${API_URL}/posts/${router.query.pid}`);
          if (!data) {
            await router.push('/404')
          }
          setPost(data);
        } catch (e) {
          await router.push('/404')
        }
      } else {
        const accessToken = getFromStorage('accessToken');
        try {
          const {data} = await axios.get(`${API_URL}/posts/private/${router.query.pid}`,
            {
              headers: {Authorization: `Bearer ${accessToken}`}
            });
          if (!data) {
            await router.push('/404')
          }
          setPost(data);
        } catch (e) {
          await router.push('/404')
        }
      }
    }

    if (!serverPost) {
      load();
    }
  }, [serverPost, router])

  if (!post) {
    return <LoadingPost />
  }
  return <PostContent post={post}/>
};

interface IPostContent {
  post: IPostFull,
  rootStore?: RootStore,
}

const PostContent = inject('rootStore')(observer(({rootStore, post}: IPostContent) => {
  const isAdmin = rootStore?.userStore.role === 'admin';
  const {id, title, subtitle, content, readTime, imgHead, subscribe} = post;
  const posts = rootStore?.postStore?.items;
  const isGuest = !rootStore?.userStore?.isAuth && subscribe;
  const subscriptionRange = rootStore?.userStore?.subscriptionRange;

  let isNeedExtends = false;
  if (!isGuest) {
    if (subscriptionRange) {
      isNeedExtends = moment(subscriptionRange).isBefore();
    }
  }


  let relatedPosts: IPost[] = [];

  if (posts) {
    const categoriesId = post.categories.map((category) => category.id);
    relatedPosts = posts.filter((post) => {
      const categories = post.categories.filter((category) => {
        return categoriesId.indexOf(category.id) !== -1;
      });
      return !!categories.length && post.id !== id;
    })
  }

  const contentHtml = mdParser.render(content);
  const headBackStyles = imgHead ? {backgroundImage: `url(${imgHead})`} : {};

  return (
    <div className={styles['post-container']}>
      <Head>
        <title>{title}</title>
        <meta property="og:title" content={title} key="title" />
      </Head>
      <div className={styles['head']}
           style={headBackStyles}>
        <div className={styles['overlay']}/>
        <div className={styles['head-content']}>
          <h1>{title}</h1>
          <h4>{isAdmin && <Link href={`/admin?post=${id}`}><a>[📝]</a></Link>}{subtitle}</h4>
          <p>{readTime} мин — Max Readman</p>
          <Link href={'#article-content'}>
            <a className={styles['btn-down']}>
              <ArrowDownIcon/>
            </a></Link>
        </div>
      </div>
      <div id='article-content' className='article-content custom-html-style'>
        {ReactHtmlParser(contentHtml)}
        {isGuest && <GuestBlock />}
        {isNeedExtends && <ExtendSubscribeBlock />}
        <RelatedPosts posts={relatedPosts}/>
      </div>
    </div>
  )
}));

const LoadingPost = () => (
  <div className={styles['post-container']}>
    <Head>
      <title>Загрузка...</title>
    </Head>
    <div className={`${styles['head']} ${styles['just-black']}`}>
      <div className={styles['overlay']}/>
      <div className={styles['head-content']}>
        <h1>Загрузка</h1>
        <h4>...</h4>
        <p>Max Readman</p>
        <Link href={'#article-content'}>
          <a className={styles['btn-down']}>
            <ArrowDownIcon/>
          </a></Link>
      </div>
    </div>
    <div id='article-content' className='custom-html-style' />
  </div>
)

interface IPostNextPageContext extends NextPageContext {
  params: {
    pid: string,
  }
}

Post.getInitialProps = async ({req, res, query}: IPostNextPageContext) => {
  if (!req) {
    return {
      post: null,
    }
  }
  const optionsCookie = {req, res};
  const isAccessToken = checkCookies('accessToken', optionsCookie);
  if (!isAccessToken) {
    try {
      const {data} = await axios.get(`${API_URL}/posts/${query.pid}`);
      if (!data) {
        return {
          notFound: true,
        }
      }
      return {
        post: data,
      }
    } catch (e) {
      return {
        notFound: true,
      }
    }
  } else {
    const accessToken = getCookie('accessToken', optionsCookie);
    try {
      const {data} = await axios.get(`${API_URL}/posts/private/${query.pid}`,
        {
          headers: {Authorization: `Bearer ${accessToken}`}
        });
      if (!data) {
        return {
          notFound: true,
        }
      }
      return {
        post: data,
      }
    } catch (e) {
      return {
        notFound: true,
      }
    }
  }
}

Post.getLayout = function getLayout(page: ReactElement) {
  return (
    <Layout theme={ThemeEnum.DARK}>
      {page}
    </Layout>
  )
}

export default Post
