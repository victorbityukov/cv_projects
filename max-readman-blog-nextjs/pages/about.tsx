import { ReactElement } from 'react';
import { Layout } from '../components';
import { ThemeEnum } from '../libs/constants';
import { AboutContent } from '../components/About';

export default function About() {
  return <AboutContent />;
}

About.getLayout = function getLayout(page: ReactElement) {
  return (
    <Layout theme={ThemeEnum.LIGHT}>
      {page}
    </Layout>
  )
}
