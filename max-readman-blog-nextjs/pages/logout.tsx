import { ReactElement } from 'react';
import { Layout, LogoutContent } from '../components';
import { ThemeEnum } from '../libs/constants';

export default function Logout() {
  return <LogoutContent />;
}

Logout.getLayout = function getLayout(page: ReactElement) {
  return (
    <Layout theme={ThemeEnum.LIGHT}>
      {page}
    </Layout>
  )
}
