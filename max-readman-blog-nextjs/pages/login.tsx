import { ReactElement } from 'react';
import { Layout, LoginContent } from '../components';
import { ThemeEnum } from '../libs/constants';

export default function Login() {
  return <LoginContent />;
}

Login.getLayout = function getLayout(page: ReactElement) {
  return (
    <Layout theme={ThemeEnum.LIGHT}>
      {page}
      </Layout>
  )
}
