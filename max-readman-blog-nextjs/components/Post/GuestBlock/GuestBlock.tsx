import Link from 'next/link';
import styles from './GuestBlock.module.scss';

export const GuestBlock = () => {
  return (
    <div className={styles['guest-block']}>
      <div className={styles['gradient']}/>
      <h2>Прости, что так грубо прерываю</h2>
      <p>Это статья для подписчиков сайта Readman. Если ты ещё не в банде — кликни вот <Link href={'/login'}><a>ТУТ</a></Link> и-и-и ты сможешь стать одним из нас.</p>
    </div>
  )
};
