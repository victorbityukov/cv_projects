import Link from 'next/link';
import { IPost } from '../../../store';
import styles from './RelatedPosts.module.scss';

interface IRelatedPosts {
  posts: IPost[],
}

export const RelatedPosts = ({ posts }: IRelatedPosts) => {
  return (
    <div className={styles['related-posts']}>
      <hr/>
      <h2>Похожие статьи</h2>
      {!posts.length && <p>Похожих постов не найдено</p>}
      <ul>
        {posts.map((post) => (
          <li key={post.id}>
            <Link href={`/posts/${post.id}`}><a>{post.title}</a></Link>
            <span className={styles['subscribe']}>
                {post.subscribe ? ' (Только для подписчиков)' : ''}
            </span>
          </li>
        ))}
      </ul>
    </div>
  )
}
