import { BlackBtn } from '../../ui';
import styles from './ExtendSubscribeBlock.module.scss';

export const ExtendSubscribeBlock = () => {
  return (
    <div className={styles['extend-block']}>
      <div className={styles['gradient']}/>
      <p>
        Компадрэ! Не знаю вкурсе ли ты, но… Твоя подписка недавно закончилась. Если захочешь продлить — кликай по кнопке ниже.
      </p>
      <div className={styles['btn-container']}>
        <BlackBtn href={'/subscribe'}>Продлить</BlackBtn>
      </div>
    </div>
  )
};
