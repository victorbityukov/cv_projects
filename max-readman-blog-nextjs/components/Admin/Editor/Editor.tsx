// @ts-nocheck
import { useEffect, useState } from 'react';
import axios from 'axios';
import Select from 'react-select';
import moment from 'moment';
import { inject, observer } from 'mobx-react';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Image from 'next/image'
import mdParser from '../../../libs/utils/md-parser';
import { calcReadTimeByText } from '../../../libs/utils';
import { API_URL } from '../../../libs/config';
import { addNotice, NOTICE_TYPE } from '../../ui';
import { RootStore } from '../../../store';
import styles from './Editor.module.scss';

interface IMyImage {
  img: string,
  width: number,
  height: number,
}

// @ts-ignore
const MdEditor = dynamic(() => {
  return new Promise((resolve) => {
    Promise.all([
      import('react-markdown-editor-lite'),
      import('./plugins/youtube.plugin')
    ]).then((res) => {
      res[0].default.use(res[1].default);
      // @ts-ignore
      resolve(res[0].default);
    });
  });
}, {
  ssr: false,
});

const myLoader = ({src}: any) => {
  return src;
}

const MyImage = ({img, width, height}: IMyImage) => {
  return (
    <Image
      unoptimized
      loader={myLoader}
      src={img}
      alt="Picture of the author"
      width={width}
      height={height}
    />
  )
}

const configEditor = {
  view: {
    menu: true,
    md: true,
    html: true,
    fullScreen: true,
    hideMenu: true,
  },
  table: {
    maxRow: 5,
    maxCol: 6,
  },
  imageUrl: 'https://octodex.github.com/images/minion.png',
  syncScrollMode: ['leftFollowRight', 'rightFollowLeft'],
};

const stylesEditor = {minHeight: '500px', width: '100%'};

type SelectType = {
  value: string,
  label: string,
};

type PostType = {
  id: number | null,
  title: string,
  subtitle: string,
  teaser: string,
  categories: SelectType[] | null,
  createdAt: string,
  subscribe: boolean,
  statusPublication: SelectType | null,
  imgLogo: string,
  imgLogoRect: string,
  imgHead: string,
  readTime: number,
  content: string,
};

const formatDate = 'yy-MM-DD';
const currentDate = moment().format(formatDate);

const generateListPosts = (items) => {
  return [
    {value: null, label: 'Новый пост'},
    ...items.map(post => {
      const {id, title, status} = post;
      const titleWithStatus = (status === 'draft') ? `[📝] ${title}` : title;
      return {value: id.toString(), label: titleWithStatus};
    }),
  ];
}

const generateListCategories = (items) => {
  return items.map(category => {
    const {id, title} = category;
    return {value: id.toString(), label: title};
  });
};

interface IEditor {
  rootStore?: RootStore,
}

interface ISelect {
  value: string | null,
  label: string,
}

export const Editor = inject(['rootStore'])(observer(({rootStore}: IEditor) => {
  const router = useRouter();
  const {post: idPost} = router.query;
  const accessToken = rootStore.userStore.accessToken;
  const statuses = [
    {value: 'approve', label: 'Чистовик'},
    {value: 'draft', label: 'Черновик'},
  ];

  let listPosts = generateListPosts(rootStore.postStore.itemsAdmin);
  let categoriesDefault = generateListCategories(rootStore.categoryStore.items)

  //Post States
  const [post, setPost] = useState<ISelect | null>(null);
  const [id, setId] = useState<number | null>(null);
  const [title, setTitle] = useState('');
  const [subtitle, setSubtitle] = useState('');
  const [teaser, setTeaser] = useState('');
  const [createdAt, setCreatedAt] = useState(currentDate);
  const [categories, setCategories] = useState<ISelect[] | null>(null);
  const [subscribe, setSubscribe] = useState(false);
  const [statusPublication, setStatusPublication] = useState<ISelect | null>(statuses[1]);
  const [imgLogo, setImgLogo] = useState<null | string>(null);
  const [imgLogoRect, setImgLogoRect] = useState<null | string>(null);
  const [imgHead, setImgHead] = useState<null | string>(null);
  const [readTime, setReadTime] = useState(0);
  const [content, setContent] = useState('');
  const [actionStatus, setActionStatus] = useState(true);

  //Category States
  const [newCategoryTitle, setNewCategoryTitle] = useState('');
  const [removeCategory, setRemoveCategory] = useState<ISelect | null>(null);

  useEffect(() => {
    if (idPost) {
      const idPostStr = idPost.toString();
      if (idPostStr !== id) {
        const [tempPost] = listPosts.filter((post) => post.value === idPostStr);
        setPost(tempPost);
      }
    }
  }, [idPost])

  useEffect(() => {
    if (!post) {
      setDefaultValuesOfPost();
      return;
    }
    if (!post.value) {
      setDefaultValuesOfPost();
      return;
    }
    getPostData();
  }, [post]);

  const getPostData = async () => {
    const response = await axios
      .get(`${API_URL}/posts/private/${post.value}`,
        {
          headers: {Authorization: `Bearer ${accessToken}`}
        })

    const {
      id,
      title,
      subtitle,
      teaser,
      categories,
      content,
      imgHead,
      imgLogo,
      imgLogoRect,
      readTime,
      status,
      subscribe,
      createdAt,
    } = response.data;

    let statusPost = {value: 'draft', label: 'Черновик'};
    statuses.forEach((statusItem) => {
      if (statusItem.value === status) {
        statusPost = statusItem;
      }
    })

    const categoriesPost = categories.map(({id, title}: { id: number, title: string }) => {
      return {value: id, label: title}
    })

    const currentPostData = {
      id,
      title,
      subtitle,
      teaser,
      content,
      subscribe,
      createdAt,
      imgLogo,
      imgLogoRect,
      imgHead,
      readTime,
      categories: categoriesPost,
      statusPublication: statusPost,
    };
    setValuesOfPost(currentPostData);
  }

  const setDefaultValuesOfPost = async () => {
    setId(null);
    setTitle('');
    setSubtitle('');
    setTeaser('');
    setCategories(null);
    setCreatedAt(currentDate);
    setSubscribe(false);
    setStatusPublication(statuses[1]);
    setImgLogo('https://via.placeholder.com/200');
    setImgLogoRect('https://via.placeholder.com/320x200');
    setImgHead('https://via.placeholder.com/200');
    setReadTime(0);
    setContent('');
    await router.push(`/admin`, undefined, {shallow: true});
  };

  const setValuesOfPost = async (data: PostType) => {
    setId(data.id);
    setTitle(data.title);
    setSubtitle(data.subtitle);
    setTeaser(data.teaser);
    setCategories(data.categories);
    setCreatedAt(moment(data.createdAt).format(formatDate));
    setSubscribe(data.subscribe);
    setStatusPublication(data.statusPublication);
    setImgLogo(data.imgLogo);
    setImgLogoRect(data.imgLogoRect);
    setImgHead(data.imgHead);
    setReadTime(data.readTime);
    setContent(data.content);
  };

  const handleImageUpload = async (file: File) => {
    return new Promise(async (resolve, reject) => {
      let formData = new FormData()
      formData.append('file', file)
      try {
        const response = await axios
          .post(`${API_URL}/posts/upload-file`,
            formData, {
              headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-Type': 'multipart/form-data'
              }
            })
        //TODO: Вставить ссылку из response
        resolve('https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/3045-saunders-gentoo_RJ.jpg/266px-3045-saunders-gentoo_RJ.jpg#center');
      } catch (e) {
        //TODO: Ссылка на картинку по умолчанию
        reject('https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/3045-saunders-gentoo_RJ.jpg/266px-3045-saunders-gentoo_RJ.jpg#center');
      }
    });
  };

  const handleSelectImgLogo = (file: File | null) => {
    return uploadImage(file, setImgLogo);
  }

  const handleSelectImgLogoRect = (file: File | null) => {
    return uploadImage(file, setImgLogoRect);
  }

  const handleSelectImgHead = (file: File | null) => {
    return uploadImage(file, setImgHead);
  }

  const uploadImage = (file: File | null, setState) => {
    if (!file) return;
    return new Promise(async (resolve, reject) => {
      let formData = new FormData()
      formData.append('file', file)
      try {
        const response = await axios
          .post(`${API_URL}/posts/upload-file`,
            formData, {
              headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-Type': 'multipart/form-data',
              }
            })
        //TODO: Вставить ссылку из response
        setState('https://via.placeholder.com/300')
        resolve('https://via.placeholder.com/300')
      } catch (e) {
        //TODO: Ссылка на картинку по умолчанию
        reject('https://via.placeholder.com/300')
      }
    });
  }

  const handleEditorChange = (value: string): void => {
    setContent(value);
    setReadTime(calcReadTimeByText(content));
  };

  const handleClickSave = async () => {
    setActionStatus(false);
    let categoriesReq: string[] = [];
    if (categories) {
      categoriesReq = categories.map((category) => category.value)
    }
    let statusReq = 'draft';
    if (statusPublication) statusReq = statusPublication.value;
    const post = {
      title,
      subtitle,
      teaser,
      subscribe,
      readTime,
      content,
      createdAt,
      imgLogo,
      imgLogoRect,
      imgHead,
      status: statusReq,
      categories: categoriesReq,
    };
    try {
      const resp = await axios.post(`${API_URL}/posts`, post,
        {
          headers: {Authorization: `Bearer ${accessToken}`}
        });
      setId(resp.data.id);
      await rootStore.postStore.getPosts();
      setActionStatus(true);
      listPosts = generateListPosts(rootStore.postStore.itemsAdmin);
      setPost(listPosts.filter(post => post.value == resp.data.id)[0])
      addNotice(NOTICE_TYPE.ACTION_SUCCESS, 'Пост добавлен');
    } catch (e) {
      setActionStatus(true);
      addNotice(NOTICE_TYPE.ERROR, e.message);
    }
  }

  const handleClickUpdate = async () => {
    setActionStatus(false);
    let categoriesReq: string[] = [];
    if (categories) {
      categoriesReq = categories.map((category) => category.value)
    }
    let statusReq = 'draft';
    if (statusPublication) statusReq = statusPublication.value;
    const post = {
      id,
      title,
      subtitle,
      teaser,
      subscribe,
      readTime,
      content,
      createdAt,
      imgLogo,
      imgLogoRect,
      imgHead,
      status: statusReq,
      categories: categoriesReq,
    };

    try {
      await axios.put(`${API_URL}/posts`, post, {
        headers: {Authorization: `Bearer ${accessToken}`}
      });
      await rootStore.postStore.getPosts();
      setActionStatus(true);
      listPosts = generateListPosts(rootStore.postStore.itemsAdmin);
      setPost(listPosts.filter(post => post.value == id)[0])
      addNotice(NOTICE_TYPE.ACTION_SUCCESS, 'Пост изменен');
    } catch (e) {
      setActionStatus(true);
      addNotice(NOTICE_TYPE.ERROR, e.message);
    }
  }

  const handleClickRemove = async () => {
    try {
      await axios.delete(`${API_URL}/posts/${id}`, {
        headers: {Authorization: `Bearer ${accessToken}`}
      })
      setActionStatus(true);
      await rootStore.postStore.getPosts();
      listPosts = generateListPosts(rootStore.postStore.itemsAdmin);
      setPost(listPosts[0]);
      addNotice(NOTICE_TYPE.ACTION_SUCCESS, 'Пост удален');
    } catch (e) {
      setActionStatus(true);
      addNotice(NOTICE_TYPE.ERROR, e.message);
    }
  }

  const handleClickCreateCategory = async () => {
    try {
      await axios.post(`${API_URL}/categories`,
        {title: newCategoryTitle},
        {
        headers: {Authorization: `Bearer ${accessToken}`}
      })
      setActionStatus(true);
      await rootStore?.categoryStore.getCategories();
      categoriesDefault = generateListCategories(rootStore.categoryStore.items)
      addNotice(NOTICE_TYPE.ACTION_SUCCESS, 'Категория добавлена');
    } catch (e) {
      setActionStatus(true);
      addNotice(NOTICE_TYPE.ERROR, e.message);
    }
  }

  const handleClickRemoveCategory = async () => {
    try {
      if (removeCategory?.value) {
        await axios.delete(`${API_URL}/categories/${removeCategory.value}`, {
          headers: {Authorization: `Bearer ${accessToken}`}
        })
        setActionStatus(true);
        await rootStore.categoryStore.getCategories();
        categoriesDefault = generateListCategories(rootStore.categoryStore.items);
        setRemoveCategory(categoriesDefault[0]);
        addNotice(NOTICE_TYPE.ACTION_SUCCESS, 'Категория удалена');
      }
    } catch (e) {
      setActionStatus(true);
      addNotice(NOTICE_TYPE.ERROR, e.message);
    }
  }

  return (
    <div className={styles['editor']}>
      <div className={styles['head']}>
        <h3>Редактор постов</h3>
      </div>
      <div className={styles['body']}>
        <div className={styles['props']}>
          <span className={styles['id-post']}>ID: {id ?? 'будет присовен после создания'}</span>
          <div className={styles['props-container']}>
            <div className={styles['props-left']}>
              <div className={styles['input-container']}>
                <label htmlFor='post'>Выбрать пост</label>
                <Select
                  instanceId='select-post'
                  onChange={(post) => setPost(post)}
                  options={listPosts}
                  value={post}
                  placeholder='Сохраненные посты'
                />
              </div>
              <div className={styles['input-container']}>
                <label htmlFor='title'>Заголовок</label>
                <input id='title' value={title} onChange={(e) => setTitle(e.target.value)}/>
              </div>
              <div className={styles['input-container']}>
                <label htmlFor='subtitle'>Подзаголовок</label>
                <input id='subtitle' value={subtitle} onChange={(e) => setSubtitle(e.target.value)}/>
              </div>
              <div className={styles['input-container']}>
                <label htmlFor='teaser'>Тизер</label>
                <input id='teaser' value={teaser} onChange={(e) => setTeaser(e.target.value)}/>
              </div>
              <div className={styles['images']}>
                <div className={styles['image-container']}>
                  <div className={styles['image-input']}>
                    <label htmlFor='img-logo'>Лого поста (квадрат)</label>
                    <input type='file'
                           onChange={(e) => handleSelectImgLogo(e.target.files ? e.target.files[0] : null)}/>
                  </div>
                  {
                    imgLogo ?
                      <MyImage img={imgLogo} width={200} height={200}/>
                      :
                      <MyImage img={'/images/logo-default.png'} width={200} height={200}/>
                  }
                </div>
                <div className={styles['image-container']}>
                  <div className={styles['image-input']}>
                    <label htmlFor='img-logo-rect'>Лого поста (прямоугольник)</label>
                    <input type='file'
                           onChange={(e) => handleSelectImgLogoRect(e.target.files ? e.target.files[0] : null)}/>
                  </div>
                  {
                    imgLogoRect ?
                      <MyImage img={imgLogoRect} width={350} height={200}/>
                      :
                      <MyImage img={'/images/logo-default-rect.png'} width={350} height={200}/>
                  }
                </div>
                <div className={`${styles['image-container']} ${styles['image-head']}`}>
                  <div className={styles['image-input']}>
                    <label htmlFor='img-head'>Фон шапки</label>
                    <input type='file'
                           onChange={(e) => handleSelectImgHead(e.target.files ? e.target.files[0] : null)}/>
                  </div>
                  {
                    imgHead ?
                      <MyImage img={imgHead} width={400} height={250}/>
                      :
                      <MyImage img={'/images/back-default.jpg'} width={400} height={250}/>
                  }
                </div>
              </div>
            </div>
            <div className={styles['props-right']}>
              <div>
                <h3>Параметры поста</h3>
              </div>
              <div className={styles['input-container']}>
                <label htmlFor='category'>Категории</label>
                <Select
                  isMulti
                  instanceId='select-categories'
                  onChange={(categories) => setCategories([...categories])}
                  options={categoriesDefault}
                  value={categories}
                  placeholder='Выбор категории'
                />
              </div>
              <div className={styles['input-container']}>
                <label htmlFor='only-subscribe'>Для подписчиков</label>
                <input type='checkbox' checked={subscribe} onChange={(e) => setSubscribe(e.target.checked)}/>
              </div>
              <div className={styles['input-container']}>
                <Select
                  instanceId='status'
                  onChange={(status) => setStatusPublication(status)}
                  options={statuses}
                  value={statusPublication}
                />
              </div>
              <div className={styles['input-container']}>
                <label htmlFor='read-time'>Время чтения (мин)</label>
                <input id='read-time' type='number' value={readTime}
                       onChange={(e) => setReadTime(Number(e.target.value))}/>
              </div>
              <div className={styles['input-container']}>
                <label htmlFor='created-at'>Дата создания ;)</label>
                <input id='created-at' type='date' placeholder='dd-mm-yyyy' value={createdAt}
                       onChange={(e) => setCreatedAt(e.target.value)}/>
              </div>
              <div className={styles['input-container']}>
                <button className={`${styles['save']} ${actionStatus ? '' : styles['disabled']}`}
                        onClick={id ? handleClickUpdate : handleClickSave}>Сохранить
                </button>
              </div>
              <div className={styles['input-container']}>
                {id && <Link href={`posts/${id}`}><a className={styles['preview']}>Предпросмотр</a></Link>}
              </div>
              <div className={`${styles['input-container']} ${styles['remove-container']}`}>
                <button className={styles['remove']} onClick={handleClickRemove}>Удалить</button>
              </div>
              <div className={`${styles['input-container']} ${styles['category-editor']}`}>
                <h3>Редактор категорий</h3>
                <div>
                  <label htmlFor='new-category'>Создать категорию</label>
                  <input
                    id='new-category'
                    type='text'
                    placeholder={'Название категории'}
                    value={newCategoryTitle}
                    onChange={(e) => setNewCategoryTitle(e.target.value)}
                  />
                  <button className={styles['save']} onClick={handleClickCreateCategory}>Создать</button>
                </div>
                <div>
                  <label htmlFor='remove-category'>Удалить категорию</label>
                  <Select
                    instanceId='remove-category'
                    onChange={(category) => setRemoveCategory(category)}
                    options={categoriesDefault}
                    value={removeCategory}
                    placeholder='Выбрать'
                  />
                  <button
                    className={styles['remove-category-btn']}
                    onClick={handleClickRemoveCategory}
                  >Удалить</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <MdEditor
          value={content}
          style={stylesEditor}
          renderHTML={value => mdParser.render(value)}
          config={configEditor}
          onChange={({text}: { text: string }) => handleEditorChange(text)}
          onImageUpload={handleImageUpload}
        />
      </div>
    </div>
  )
}));
