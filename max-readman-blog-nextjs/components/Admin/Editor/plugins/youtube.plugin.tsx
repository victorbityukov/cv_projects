// @ts-nocheck
import * as React from 'react';
import { PluginComponent } from 'react-markdown-editor-lite';
import YoutubeIcon from '../../../../public/icons/youtube-icon.svg';

export default class Youtube extends PluginComponent {
  static pluginName = "youtube-video";
  static align = "left";

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.editor.insertText(':::youtube[url]\n' +
      'Подзаголовок(если_нужен)\n' +
      ':::');
  }

  render() {
    return (
      <span
        className="button button-type-counter"
        title="Counter"
        onClick={this.handleClick}
      >
        <YoutubeIcon/>
      </span>
    );
  }
}
