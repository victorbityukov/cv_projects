import { ReactNode } from 'react';
import styles from './Button.module.scss';

type Props = {
  children: ReactNode;
};

export const Button = ({ children }: Props) => {
  return (
    <div className={`${styles['button']} unselectable`}>
      {children}
    </div>
  )
}
