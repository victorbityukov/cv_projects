import { inject, observer } from 'mobx-react';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { Editor } from './Editor';
import { UserStore } from '../../store';

interface IAdminContainer {
  userStore?: UserStore;
}

export const AdminContainer = inject('userStore')(observer(({ userStore }: IAdminContainer) => {
  const router = useRouter();
  const isAdmin = userStore?.isAuth && userStore?.role === 'admin';

  useEffect(() => {
    if (!isAdmin) {
      router.push('/404');
    }
  }, [router, isAdmin])

  if (isAdmin) {
    return (
      <>
        <Head>
          <title>Админка</title>
        </Head>
        <Editor />
       </>
    )
  }
  return <></>
}));
