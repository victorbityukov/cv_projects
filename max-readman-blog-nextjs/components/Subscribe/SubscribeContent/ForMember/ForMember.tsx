import styles from './ForMember.module.scss';

interface IForMember {
  email: string,
}

export const ForMember = ({email}: IForMember) => {
  return (
    <div className={styles['for-member']}>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A at dicta ipsa ipsam labore possimus, quia quidem
        recusandae reprehenderit, sapiente sint ullam, vero? Alias blanditiis, consectetur deleniti dicta et explicabo,
        ipsam itaque libero molestias pariatur sed veniam voluptatibus. Architecto culpa cum dicta eos ex molestias
        quisquam, saepe tempore ut voluptatem.
      </p>
      <p className={styles['email-container']}>
        Email: {email}
      </p>
    </div>
  )
}
