import Select from 'react-select';
import { inject, observer } from 'mobx-react';
import { useEffect, useState } from 'react';
import Head from 'next/head';
import { ForMember } from './ForMember';
import { ForGuest } from './ForGuest';
import { UserStore } from '../../../store';
import { BlackBtn } from '../../ui';
import styles from './SubscribeContent.module.scss';


interface ISubscribeContent {
  userStore?: UserStore,
}

const typePayments = [
  {value: '30days', label: '30 дней - [цена]р'},
  {value: '90days', label: '90 дней - [цена]р'},
  {value: '180days', label: '180 дней - [цена]р'},
  {value: '365days', label: '365 дней - [цена]р'},
];

interface ISelectPayment {
  value: string,
  label: string,
}

export const SubscribeContent = inject('userStore')(observer(({userStore}: ISubscribeContent) => {
  const [email, setEmail] = useState('');
  const [payment, selectPayment] = useState<ISelectPayment | null>(null);

  useEffect(() => {
    if (userStore) {
      setEmail(userStore?.email ?? '');
    }
  }, [userStore])

  const isAuth = userStore?.isAuth;
  let btnText = isAuth ? 'Продлить' : 'Подписаться';

  return (
    <div className={styles['subscribe-content']}>
      <Head>
        <title>Подписка</title>
      </Head>
      <h1>Подписка</h1>
      {
        isAuth ?
          <ForMember email={email}/>
          :
          <ForGuest email={email} setEmail={setEmail}/>
      }
      <Select
        className={styles['select-payment']}
        instanceId='select-payment'
        onChange={(payment) => selectPayment(payment)}
        options={typePayments}
        value={payment}
        placeholder='Выбрать время подписки'
      />
      <BlackBtn>{btnText}</BlackBtn>
    </div>
  )
}));
