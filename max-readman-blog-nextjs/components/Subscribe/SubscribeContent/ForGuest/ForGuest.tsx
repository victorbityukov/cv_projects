import { ChangeEvent } from 'react';
import styles from './ForGuest.module.scss';

interface IForGuest {
  email: string,
  setEmail: (email: string) => void,
}

export const ForGuest = ({email, setEmail}: IForGuest) => {
  const onHandleChangeEmail = (e: ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  }

  return (
    <div className={styles['for-guest']}>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid aut earum in necessitatibus quidem sit? Ab
        alias at, beatae delectus dignissimos doloremque est explicabo facilis fugiat magnam nam neque nihil nostrum
        porro quas rerum sed? Aliquam fugiat laboriosam laborum odit? Consequatur culpa dolores eaque magni sed soluta
        ut vel voluptates.
      </p>
      <input type='email' value={email} onChange={onHandleChangeEmail} placeholder={'Email'}/>
      <p className={styles['description']}>
        Обязательно укажите корректную почту, на неё придет пароль после подтверждения оплаты
      </p>
    </div>)
}
