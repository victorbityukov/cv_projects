import Head from 'next/head';
import styles from './AboutContent.module.scss';

export const AboutContent = () => {
  return (
    <div className={styles['about-content']}>
      <Head>
        <title>Об авторе</title>
      </Head>
      <h1>Об авторе</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid architecto atque culpa cum ea facere fuga illo
        illum ipsa, iste iusto minima nam natus nihil officia officiis pariatur possimus praesentium quam quisquam sunt
        tempora tempore velit voluptas voluptates! Accusantium at cum cupiditate dolorum fugiat molestias natus officia
        quidem quo voluptatem!
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur assumenda aut blanditiis consectetur
        deleniti eligendi harum hic nihil non numquam pariatur quidem ratione, similique temporibus ut veniam veritatis
        voluptatem voluptatibus. Dolor doloribus excepturi numquam quas repellat rerum tenetur? Ab aliquid animi
        blanditiis consectetur cumque error esse, ipsam laudantium libero molestias mollitia, nemo nostrum nulla quae
        quam quibusdam quis quod velit, vero voluptas voluptate voluptatem voluptatum? Blanditiis, commodi consequatur,
        distinctio facilis nemo neque nostrum, numquam quos saepe tempora ullam vel veniam voluptatibus. Commodi cum
        distinctio dolor nobis quos ut velit? Accusamus ad consequuntur explicabo id inventore magni maxime, nesciunt
        perferendis ullam?
      </p>
    </div>
  )
}
