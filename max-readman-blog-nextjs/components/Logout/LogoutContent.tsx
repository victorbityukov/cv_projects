import { inject, observer } from 'mobx-react';
import { useRouter } from 'next/router';
import { UserStore } from '../../store';
import { BlackBtn } from '../ui';
import styles from './LogoutContent.module.scss';
import Head from 'next/head';

interface ILogoutContent {
  userStore?: UserStore,
}

export const LogoutContent = inject('userStore')(observer(({ userStore }: ILogoutContent) => {
  const router = useRouter();

  const logout = async () => {
    await userStore?.logout();
    await router.push('/');
  }

  return (
    <div className={styles['logout-content']}>
      <Head>
        <title>Уже уходишь?</title>
      </Head>
      <h1>Ты действительно хочешь выйти?</h1>
      <BlackBtn href={'/'}>Передумал, хочу на главную</BlackBtn>
      <BlackBtn action={logout}>Да</BlackBtn>
    </div>
  );
}))
