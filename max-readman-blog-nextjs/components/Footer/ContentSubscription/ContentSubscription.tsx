import { WarningBtn } from '../../ui';

interface ContentSubscription {
  timeLeft: number,
  email: string,
}

export const ContentSubscription = ({timeLeft, email}: ContentSubscription) => {
  let subscriptionDaysLeft = `Осталось дней: ${timeLeft}`
  if (timeLeft <= 0) {
    subscriptionDaysLeft = 'Время подписки закончилось'
  }
  return (
    <>
      <h1>ПОДПИСКА УВЕЛИЧИВАЕТ ГРУДЬ ТВОЕЙ ДЕВУШКИ НА ОДИН РАЗМЕР</h1>
      <div className='btn-container'>
        <p>
          Это не точно. Но ты уже подписался. Стоит проверить.
        </p>
        <p>
          {email}
        </p>
        <p>
          {subscriptionDaysLeft}
        </p>
        <WarningBtn href='/subscribe'>Продлить</WarningBtn>
      </div>
    </>
  )
}
