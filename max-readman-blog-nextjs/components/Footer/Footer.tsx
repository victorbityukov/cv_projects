import { inject, observer } from 'mobx-react';
import moment from 'moment';
import { UserStore } from '../../store';
import { ContentDefault } from './ContentDefault';
import { ContentSubscription } from './ContentSubscription';
import { Copyright } from './Copyright';

interface IFooter {
  userStore?: UserStore,
}

export const Footer = inject('userStore')(observer(({userStore}: IFooter) => {
  const isAuth = userStore?.isAuth;
  const timeLeft = userStore?.subscriptionRange ? moment(userStore?.subscriptionRange).diff(moment(), 'days') : 0;
  const email = userStore?.email ?? '';

  return (
    <footer>
      <div className='footer-container'>
        <div className='footer-container__content'>
          {
            isAuth ?
              <ContentSubscription timeLeft={timeLeft} email={email}/>
              :
              <ContentDefault/>
          }
        </div>
        <Copyright isAuth={isAuth} />
      </div>
    </footer>
  )
}));
