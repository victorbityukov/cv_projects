import moment from 'moment';
import Link from 'next/link';
import Telegram from '../../../public/icons/telegram-icon.svg';

interface ICopyright {
  isAuth?: boolean,
}

export const Copyright = ({isAuth}: ICopyright) => {
  const privacy = <span><Link href='/'><a>Политика конфиденциальности</a></Link> / </span>;
  const subscribe = <span><Link href={'/subscribe'}><a>Подписка</a></Link> / </span>;
  let loginExit;
  if (isAuth) {
    loginExit = <span><Link href={'/logout'}><a>Выход</a></Link> / </span>;
  } else {
    loginExit = <span><Link href={'/login'}><a>Логин</a></Link> / </span>;
  }
  const telegram = <span><Link href='/'><a><Telegram /></a></Link></span>;
  const year = moment().year();

  return (
    <nav className='copyright'>
      <div className='copyright__title'>
        © {year} Max Readman
      </div>
      <div className='copyright__menu'>
        {privacy} {subscribe} {loginExit} {telegram}
      </div>
    </nav>
  )
}
