import { ArticleElement } from './ArticleElement';
import { BlackBtn } from '../../ui';
import { IPost } from '../../../store';
import styles from './ArticlesSubscriberSection.module.scss';

interface IArticleSection {
  posts: IPost[],
}

export const ArticlesSubscriberSection = ({posts}: IArticleSection) => {
  return (
    <section className={styles['articles-subscriber-section']}>
      <div className={styles['divider']} />
      <div className={styles['container']}>
        <h2>Контент для подписчиков</h2>
        <div>
          {!posts.length && <h3>Список постов пуст</h3>}
          {posts
            .filter((post) => (post.subscribe))
            .slice(0, 5)
            .map((post) => (<ArticleElement key={post.id} {...post}/>))}
        </div>
        <p className={styles['description']}>
          Это лишь небольшая часть всего содержимого, доступного для подписчиков. В закрытом архиве сейчас 2 статей, ещё
          один телеграм канал и прямая связь с автором. Да-да-да, мы общаемся. И ты тоже можешь мне написать. Чтобы узнать
          все подробности нажми на кнопочку ниже.
        </p>
        <div className={styles['btn-container']}>
          <BlackBtn href={'/archive'}>Кликни для просмотра всех статей</BlackBtn>
        </div>
      </div>
    </section>
  )
};
