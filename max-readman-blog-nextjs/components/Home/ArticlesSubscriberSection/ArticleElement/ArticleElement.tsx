import Link from 'next/link';
import Image from 'next/image';
import { IPost } from '../../../../store';
import styles from './ArticlesElement.module.scss'

const myLoader = ({ src }: any) => {
  return src;
}

const MyImage = ({ url }: { url: string }) => {
  return (
    <Image
      unoptimized
      loader={myLoader}
      src={url}
      alt="img post"
      width={350}
      height={200}
    />
  )
}

export const ArticleElement = ({id, title, teaser, imgLogoRect}: IPost) => {
  return (
    <div className={styles['article-element']}>
      <Link href={`/posts/${id}`}>
        <a>
          {imgLogoRect ? <MyImage url={imgLogoRect}/> : <MyImage url={'/images/logo-default-rect.png'}/>}
        </a>
      </Link>
      <div className={styles['article-content']}>
        <Link href={`/posts/${id}`}>
          <a>
            <h3>{title}</h3>
          </a>
        </Link>
        <p>{teaser}</p>
        <div className={styles['read-more-container']}>
          <Link href={`/posts/${id}`}>
            <a className={styles['read-more']}>
              Читать далее...
            </a>
          </Link>
        </div>
      </div>
    </div>
  )
}
