import { ArticleElement } from './ArticleElement';
import { BlackBtn } from '../../ui';
import { IPost } from '../../../store';
import styles from './ArticlesSection.module.scss';

interface IArticleSection {
  posts: IPost[],
}

export const ArticlesSection = ({posts}: IArticleSection) => {
  return (
    <section className={styles['article-section']}>
      <h2>Последние статьи</h2>
      <div>
        {!posts.length && <h3>Список постов пуст</h3>}
        {posts.slice(0, 4).map((post) => (<ArticleElement key={post.id} {...post}/>))}
      </div>
      <div className={styles['btn-container']}>
        <BlackBtn href='/archive'>
          Кликни для просмотра всех статей
        </BlackBtn>
      </div>
    </section>
  )
};
