import Moment from 'react-moment';
import 'moment/locale/ru';
import Link from 'next/link';
import Image from 'next/image'
import { IPost } from '../../../../store';
import styles from './ArticlesElement.module.scss';

const myLoader = ({ src }: any) => {
  return src;
}

const MyImage = ({url}: {url: string}) => {
  return (
    <Image
      unoptimized
      loader={myLoader}
      src={url}
      alt="img post"
      width={200}
      height={200}
    />
  )
}

export const ArticleElement = ({
                                 id,
                                 title,
                                 teaser,
                                 imgLogo,
                                 createdAt,
                                 categories,
                                 readTime,
                                 subscribe,
                               }: IPost) => {
  return (
    <div className={styles['article-element']}>
      <div className={styles['article-left']}>
        <Link href={{
          pathname: '/posts/[id]',
          query: { id },
        }}>
          <a className={styles['img-container']}>
            {imgLogo ? <MyImage url={imgLogo}/> : <MyImage url={'/images/logo-default.png'}/>}
          </a>
        </Link>
        <div className={styles['meta-container']}>
          <div className={styles['meta-element']}>
            <div className={styles['label']}>Дата:</div>
            <div className={styles['content']}><Moment format="MMMM - DD, YYYY">{createdAt}</Moment></div>
          </div>
          <div className={styles['meta-element']}>
            <div className={styles['label']}>Категории:</div>
            <div className={styles['content']}>
              {categories.map((category) => (
                <span className={styles['category']} key={category.id}>
                  <Link href={`/archive?category=${category.id}`}>
                    <a>{category.title}</a>
                  </Link>
                </span>
              ))}
            </div>
          </div>
          <div className={styles['meta-element']}>
            <div className={styles['label']}>Время чтения:</div>
            <div className={styles['content']}>{readTime} мин</div>
          </div>
        </div>
      </div>
      <Link href={`/posts/${id}`}>
        <a className={styles['article-container']}>
          <h3>{title}</h3>
          {subscribe && <p className={styles['for-subscribers']}>(Только для подписчиков)</p>}
          <p>{teaser}</p>
        </a>
      </Link>
    </div>
  )
}
