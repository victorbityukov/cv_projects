import { inject, observer } from 'mobx-react';
import Link from 'next/link';
import ManIcon from '../../../public/icons/man-icon.svg';
import PageIcon from '../../../public/icons/page-icon.svg';
import { BlackBtn } from '../../ui';
import { UserStore } from '../../../store';
import styles from './GreetingsSection.module.scss';

interface IGreetingsSection {
  userStore?: UserStore,
}
export const GreetingsSection = inject('userStore')(observer(({ userStore }: IGreetingsSection) => {
  const isAuth = userStore?.isAuth;
  return (
    <section className={styles['greetings-section']}>
      <div className={styles['back-max']}>
        <div className={styles['back-copybook']}>
          <div className={styles['content-container']}>
            <div className={styles['content']}>
              <h1>ПРИВЕТ, Я МАКС!</h1>
              <p>
                Тот самый автор, которого не стыдно посоветовать лучшему другу. Проходи, залезай на диван. Чай? Кофе? Коньячок?
                И к нему есть шашлычок. Ладно, давай я тебе расскажу чем мы тут занимаемся: болтаем о жизни, о людях, об
                отношениях и о том, что тебе со всем этим делать. Здесь бывает жутко интересно, но не для всех. Прочти пару
                статей и сделай выводы сам.
              </p>
              <h4>НАЧАТЬ ЗНАКОМСТВО СО МНОЙ МОЖНО СО СТАТЬИ — <Link href={'/about'}><a>ОБО МНЕ</a></Link></h4>
              <div className={styles['btn-container']}>
                <BlackBtn href='/archive'><PageIcon/>Читать статьи</BlackBtn>
                {!isAuth && <BlackBtn href='/subscribe'><ManIcon/>Подписаться</BlackBtn>}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}));
