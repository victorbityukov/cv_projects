import { CategoryElement } from './CategoryElement';
import styles from './CategoriesSection.module.scss';

export const CategoriesSection = () => {
  return (
    <section className={styles['categories-section']}>
      <div className={styles['background']}>
        <div className={styles['content']}>
          <h2>Всё самое интересное попадает в одну из этих категорий</h2>
          <div className={styles['categories-items']}>
            <CategoryElement
              link={'/'}
              category={'Жизнь'}
              img={1}
              description={'Инструкция к реальности, человеческим языком и с некоторой долей грязных ругательств.'}
            />
            <CategoryElement
              link={'/'}
              category={'Человек'}
              img={2}
              description={'Гайд: Что такое человек и с чем его едят. Как плавать среди акул и не быть съеденным.'}
            />
            <CategoryElement
              link={'/'}
              category={'Развитие'}
              img={3}
              description={'Как не заглохнуть на старте, не прозябать в сорокет и быть актуальным после полтоса.'}
            />
            <CategoryElement
              link={'/'}
              category={'Отношения'}
              img={4}
              description={'Как найти "ту самую", уменьшить вероятность измены и не развалить отношения.'}
            />
          </div>
        </div>
      </div>
    </section>
  )
}
