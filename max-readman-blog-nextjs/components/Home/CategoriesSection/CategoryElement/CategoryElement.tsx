import Link from 'next/link';
import styles from './CategoryElement.module.scss';

type Props = {
  img: number,
  category: string,
  description: string,
  link: string,
}

export const CategoryElement = ({img, category, description, link}: Props) => {
  const imgCurrent = `img_${img}`;
  return (
    <Link href={link}>
      <a className={styles['category-element']}>
        <div className={`${styles['img']} ${styles[imgCurrent]}`}/>
        <div className={styles['category-info']}>
          <h4>{category}</h4>
          <p>{description}</p>
          <p className={styles['more']}>Подробнее</p>
        </div>
      </a>
    </Link>
  )
}
