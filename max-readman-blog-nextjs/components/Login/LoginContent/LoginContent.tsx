import React, { useState } from 'react';
import { inject, observer } from 'mobx-react';
import { useRouter } from 'next/router';
import { addNotice, BlackBtn, NOTICE_TYPE } from '../../ui';
import { UserStore, UserTypeStateEnum } from '../../../store';
import styles from './LoginContent.module.scss';

interface ILoginContent {
  userStore?: UserStore,
}

export const LoginContent = inject('userStore')(observer(({ userStore }: ILoginContent) => {
  const router = useRouter();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const login = async () => {
    if (!userStore) return;
    await userStore.login(email, password);
    if (userStore?.state === UserTypeStateEnum.DONE) {
      addNotice(NOTICE_TYPE.AUTH_SUCCESS);
      await router.push('/');
    }
    if (userStore?.state === UserTypeStateEnum.ERROR) {
      addNotice(NOTICE_TYPE.AUTH_ERROR);
    }
  }

  const handleChangeEmail = (e: React.FormEvent<HTMLInputElement>) => {
    setEmail(e.currentTarget.value)
  }

  const handleChangePassword = (e: React.FormEvent<HTMLInputElement>) => {
    setPassword(e.currentTarget.value)
  }

  return (
    <div className={styles['login-content']}>
      <h1>СВОЙ? ЗАХОДИ</h1>
      <div>
        <input placeholder='Email' value={email} onChange={handleChangeEmail}/>
        <input type={'password'} placeholder='Пароль' value={password} onChange={handleChangePassword}/>
      </div>
      <BlackBtn action={login}>Уже вхожу</BlackBtn>
      <BlackBtn href={'/subscribe'}>Вступить в банду</BlackBtn>
    </div>
  )
}));
