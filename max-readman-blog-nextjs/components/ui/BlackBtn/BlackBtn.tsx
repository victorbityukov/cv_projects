import React from 'react';
import Link from 'next/link';
import styles from './BlackBtn.module.scss';

interface IBlackBtn {
  children: any,
  href?: string,
  action?: () => void,
}

export const BlackBtn = ({href, action, children}: IBlackBtn) => {
  const handleClick = async (event: React.MouseEvent) => {
    event.preventDefault();
    if (action) {
      action();
    }
  }
  if (href) {
    return <Link href={href}><a className={styles['btn']}>{children}</a></Link>
  } else {
    return <a className={styles['btn']} onClick={handleClick}>{children}</a>;
  }
}
