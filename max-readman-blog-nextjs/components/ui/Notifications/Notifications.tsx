import { Store } from 'react-notifications-component';

export enum NOTICE_TYPE {
  AUTH_SUCCESS = 'AUTH_SUCCESS',
  AUTH_ERROR = 'AUTH_ERROR',
  ACTION_SUCCESS = 'ACTION_SUCCESS',
  ERROR = 'ERROR',
}

export const addNotice = (type: NOTICE_TYPE, message?: string) => {
  const options = {
    insert: 'top',
    container: 'bottom-right',
    dismiss: {
      duration: 3000
    }
  };

  switch(type) {
    case NOTICE_TYPE.ACTION_SUCCESS:
      // @ts-ignore
      return Store.addNotification({
        ...options,
        message,
        title: 'Админ панель',
        type: 'success',
      })
    case NOTICE_TYPE.AUTH_ERROR:
      // @ts-ignore
      return Store.addNotification({
        ...options,
        title: 'Авторизация',
        message: 'Что-то пошло не так',
        type: 'danger',
      })
    case NOTICE_TYPE.ERROR:
      // @ts-ignore
      return Store.addNotification({
        ...options,
        message,
        title: 'Что-то пошло не так',
        type: 'danger',
      })
  }
}
