import Link from 'next/link';
import styles from './WarningBtn.module.scss';

type Props = {
  children: string,
  href: string,
};

export const WarningBtn = ({href, children}: Props) => {
  return <Link href={href}><a className={styles.btn}>{children}</a></Link>
}
