import { inject, observer } from 'mobx-react';
import { ReactNotifications } from 'react-notifications-component';
import { Title } from './Title';
import { Menu, MenuMobile } from './Menu';
import { ThemeEnum } from '../../libs/constants';
import { UserStore } from '../../store';

interface IHeader {
  theme: ThemeEnum,
  userStore?: UserStore,
}

export const Header = inject('userStore')(observer(({ userStore, theme }: IHeader) => {
  const isAuth = userStore?.isAuth;
  const role = userStore?.role;

  return (
    <header className={'unselectable'}>
      <Title theme={theme} />
      <Menu theme={theme} isAuth={isAuth} role={role}/>
      <MenuMobile theme={theme} isAuth={isAuth} role={role}/>
      <ReactNotifications />
    </header>
  )
}));
