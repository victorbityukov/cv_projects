import Link from 'next/link';
import Telegram from '../../../public/icons/telegram-icon.svg';
import { ThemeEnum } from '../../../libs/constants';

interface IMenu {
  theme: ThemeEnum,
  isAuth?: boolean,
  role?: string,
}

export const Menu = ({ theme, isAuth, role }: IMenu) => {
  const textSubscribe = isAuth ? 'Подписка' : 'Подписаться';

  return (
    <nav className={`menu ${theme}`}>
      <ul className='menu__main'>
        {role === 'admin' &&
        <li>
          <Link href={'/admin'}><a>Админка</a></Link>
        </li>
        }
        <li>
          <Link href={'/archive'}><a>Статьи</a></Link>
        </li>
        <li>
          <Link href={'/about'}><a>Об авторе</a></Link>
        </li>
        <li>
          <Link href={'/subscribe'}><a>{textSubscribe}</a></Link>
        </li>
        <li>
          <Link href='/'>
            <a className='tg-icon-container' target='_blank'>
              <Telegram />
            </a>
          </Link>
        </li>
      </ul>
    </nav>
  )
}
