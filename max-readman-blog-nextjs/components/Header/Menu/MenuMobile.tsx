import Link from 'next/link';
import { useState } from 'react';
import { ThemeEnum } from '../../../libs/constants';
import Telegram from '../../../public/icons/telegram-icon.svg';

interface IMenuMobile {
  theme: ThemeEnum,
  isAuth?: boolean,
  role?: string,
}

export const MenuMobile = ({ theme, isAuth, role }: IMenuMobile) => {
  const textSubscribe = isAuth ? 'Подписка' : 'Подписаться';
  const [menu, toggleMenu] = useState(false);
  const setToggleMenu = () => {
    toggleMenu((prevState) => {
      return !prevState;
    });
  }

  return (
    <nav className={`menu-mobile ${theme} ${menu ? 'active': ''}`}>
      <div className='menu-mobile__btn-menu' onClick={setToggleMenu}>&#9776;</div>
      {menu && <ul className='menu-mobile__main'>
        {role === 'admin' &&
        <li>
          <Link href={'/admin'}><a>Админка</a></Link>
        </li>
        }
        <li>
          <Link href={'/archive'}><a>Статьи</a></Link>
        </li>
        <li>
          <Link href={'/about'}><a>Об авторе</a></Link>
        </li>
        <li>
          <Link href={'/subscribe'}><a>{textSubscribe}</a></Link>
        </li>
        <li>
          <Link href='/'>
            <a className='tg-icon-container' target='_blank'>
              <Telegram />
            </a>
          </Link>
        </li>
      </ul>}
    </nav>
  )
}
