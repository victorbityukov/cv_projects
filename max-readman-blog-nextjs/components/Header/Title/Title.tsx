import Link from 'next/link';
import { ThemeEnum } from '../../../libs/constants';

type Props = {
  theme: ThemeEnum,
}

export const Title = ({ theme }: Props) => {
  return (
    <nav className={`title ${theme}`}>
      <Link href='/'><a><h1>Max Readman</h1></a></Link>
      <h3>Вся правда о жизни без херни</h3>
    </nav>
  )
}
