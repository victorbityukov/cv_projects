import { inject, observer } from 'mobx-react';
import moment from 'moment';
import 'moment/locale/ru'
import Link from 'next/link';
import { useRouter } from 'next/router'
import Head from 'next/head';
import { IPost, RootStore } from '../../../store';
import styles from './ArchiveContent.module.scss';

moment.locale('ru')

type ArticlesObjByYearsType = {
  [year: string]: IPost[],
}

interface IArchiveContent {
  rootStore?: RootStore,
}

export const ArchiveContent = inject('rootStore')(observer(({ rootStore }: IArchiveContent) => {
  const router = useRouter()
  let { category } = router.query;
  let categoryStore = rootStore?.categoryStore;
  let postStore = rootStore?.postStore;
  const categories = [['all', 'Все категории']];

  if (categoryStore) {
    categories.push(...(categoryStore?.items.map((category) => ([String(category.id), category.title]))));
  }

  let articles = postStore?.items ?? [];

  if (category && category !== 'all') {
    articles = articles.filter(
      (article) => !!(article.categories.filter((categoryItem) => String(categoryItem.id) === category).length)
    )
  }

  const articlesObjByYears: ArticlesObjByYearsType = {};

  articles.forEach((article) => {
    const year = moment(article.createdAt).year();
    if (!articlesObjByYears[year]) {
      articlesObjByYears[year] = []
    }
    articlesObjByYears[year].push(article);
  })

  const articlesArr = Object.entries(articlesObjByYears);
  return (
    <div className={styles['archive-content']}>
      <Head>
        <title>Архив</title>
      </Head>
      <h3>Фильтр по теме</h3>
      <ul className={styles['topic-list']}>
        {categories.map(([key, value]) => {
          return (
            <li key={key}>
              {category === key ?
                <span>{value}</span> :
                <Link href={{
                  pathname: '/archive',
                  query: {category: key}
                }}
                      scroll={false}>
                  <a>{value}</a>
                </Link>
              }
            </li>
          )
        })}
      </ul>
      <div className={styles['articles-by-date']}>
        <h2>Статьи по дате</h2>
        <ul className={styles['years-items']}>
          {articlesArr.map(([year, articles]) =>
            (
              <li className={styles['year-container']} key={year}>
                <h3>{year}</h3>
                <ul className={styles['articles-items']}>
                  {articles.map(
                    ({id, title, subscribe, createdAt}) =>
                      (
                        <li className={styles['article-container']} key={id}>
                          <span className={styles['date']}>{moment(createdAt).format('MMMM DD')}</span>
                          <span
                            className={styles['date-mob']}>{`${moment(createdAt).format('MMMM').slice(0, 3)} ${moment(createdAt).format('DD')}`}</span>
                          <div className={styles['article-title']}>
                            <Link href={`/posts/${id}`}>
                              <a className={styles['title']}>{title}</a>
                            </Link>
                            <span className={styles['subscribe']}>
                              {subscribe ? ' (Только для подписчиков)' : ''}
                            </span>
                          </div>
                        </li>
                      ))}
                </ul>
              </li>
            ))}
        </ul>
      </div>
    </div>
  )
}));
