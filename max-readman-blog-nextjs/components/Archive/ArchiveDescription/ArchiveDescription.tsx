import styles from './ArchiveDescription.module.scss';

export const ArchiveDescription = () => {
  return (
    <div className={styles['archive-description']}>
      <h1>Архив</h1>
      <p>Добро пожаловать в библиотеку джедаев. Здесь ты найдешь тысячелетнюю мудрость человечества распределенную по
        понятным категориям. Или можешь познакомиться со статьями в хронологическом порядке. Наслаждайся.</p>
    </div>
  )
}
