import { ReactNode, useEffect } from 'react';
import { Provider } from 'mobx-react';
import Head from 'next/head';
import { Header } from './Header';
import { Footer } from './Footer';
import { ThemeEnum } from '../libs/constants';
import { getFromStorage, rootStore } from '../store';

interface ILayout {
  children: ReactNode,
  theme: ThemeEnum,
}

export const Layout = ({ children, theme }: ILayout) => {
  useEffect(() => {
    const accessToken = getFromStorage('accessToken');
    if (accessToken) {
      rootStore.userStore.getUser(accessToken);
    }
    rootStore.categoryStore.getCategories();
    rootStore.postStore.getPosts();
  }, []);

  return (
    <Provider
      rootStore={rootStore}
      userStore={rootStore.userStore}
      postStore={rootStore.postStore}
      categoryStore={rootStore.categoryStore}
    >
      <Head>
        <title>Max Readman</title>
        <meta charSet="utf-8" />
        <meta name="keywords" content="blog" />
        <meta name="description" content="Free Web tutorials" />
        <meta name="author" content="Max Readman" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <Header theme={theme} />
      <main>{children}</main>
      <Footer/>
    </Provider>
  )
}
