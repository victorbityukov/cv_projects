import { UserStore } from './userStore';
import { PostStore } from './postStore';
import { CategoryStore } from './categoryStore';

export interface IRootStore {
  userStore: UserStore,
  postStore: PostStore,
  categoryStore: CategoryStore;
}

export class RootStore {
  userStore: UserStore;
  postStore: PostStore;
  categoryStore: CategoryStore;

  constructor() {
    this.userStore = new UserStore(this);
    this.postStore = new PostStore(this);
    this.categoryStore = new CategoryStore(this);
  }
}

export const rootStore = new RootStore();
