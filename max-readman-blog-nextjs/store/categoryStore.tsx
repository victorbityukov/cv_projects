import { action, makeObservable, observable } from 'mobx';
import axios from 'axios';
import { IRootStore } from './rootStore';
import { API_URL } from '../libs/config';

export interface ICategory {
  id: number,
  title: string,
}

export interface ICategoryStore {
  rootStore: IRootStore,
  items: ICategory[],
  isReady: boolean,
}

export class CategoryStore implements ICategoryStore{
  rootStore: IRootStore;
  items: ICategory[] = [];
  isReady: boolean = false;

  constructor(rootStore: IRootStore) {
    makeObservable(this, {
      items: observable.ref,
      isReady: observable,
      setCategories: action,
      getCategories: action.bound,
      rootStore: false
    })
    this.rootStore = rootStore;
  }

  getCategories() {
    axios
      .get(`${API_URL}/categories`)
      .then(response => {
        this.setCategories(response.data);
      })
  }

  setCategories(categories: ICategory[]) {
    this.items = categories;
    this.isReady = true;
  }
}
