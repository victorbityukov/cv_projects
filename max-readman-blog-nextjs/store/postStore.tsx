import { action, makeObservable, observable, runInAction } from 'mobx';
import { IRootStore } from './rootStore';
import { ICategory } from './categoryStore';
import axios from 'axios';
import { API_URL } from '../libs/config';

export enum PostTypeStateEnum {
  DONE = 'done',
  PENDING = 'pending',
  ERROR = 'error',
  WAIT = 'wait',
}

export interface IPost {
  id: number,
  title: string,
  teaser: string,
  readTime: number,
  subscribe: boolean,
  status: string,
  imgLogo: string,
  imgLogoRect: string,
  imgHead: string,
  createdAt: Date,
  categories: ICategory[],
}

export interface IPostFull {
  id: number,
  title: string,
  subtitle: string,
  content: string,
  teaser: string,
  readTime: number,
  subscribe: boolean,
  status: string,
  imgLogo: string,
  imgLogoRect: string,
  imgHead: string,
  createdAt: Date,
  categories: ICategory[],
}

export interface IPostStore {
  rootStore: IRootStore,
  state: PostTypeStateEnum,
  items: IPost[],
  itemsAdmin: IPost[],
  create: () => void,
  update: () => void,
  remove: () => void,
}

export class PostStore implements IPostStore{
  rootStore: IRootStore;
  items: IPost[] = [];
  itemsAdmin: IPost[] = [];
  state = PostTypeStateEnum.PENDING;

  constructor(rootStore: IRootStore) {
    makeObservable(this, {
      items: observable.ref,
      create: action,
      update: action,
      remove: action,
      getPosts: action.bound,
      rootStore: false
    })
    this.rootStore = rootStore;
  }

  async getPosts() {
    if (this.state !== PostTypeStateEnum.WAIT) {
      this.state = PostTypeStateEnum.WAIT;
      try {
        const posts = await PostStore.getPostsAPI();
        runInAction(() => {
          this.itemsAdmin = posts;
          this.items = posts.filter((post: IPost) => post.status !== 'draft');
          this.state = PostTypeStateEnum.DONE;
        })
      } catch(e) {
        runInAction(() => {
          this.state = PostTypeStateEnum.ERROR;
        })
      }
    }
  }

  private static async getPostsAPI() {
    const res = await axios.get(`${API_URL}/posts`);
    return res.data;
  }

  create() {
    console.log('createPost');
  }

  update() {
    console.log('updatePost');
  }

  remove() {
    console.log('updatePost');
  }
}
