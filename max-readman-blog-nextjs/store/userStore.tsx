import { action, makeObservable, observable, runInAction } from 'mobx';
import axios from 'axios';
import { removeCookies, setCookies } from 'cookies-next';
import { RootStore } from './rootStore';
import { getFromStorage, removeFromStorage, setToStorage } from './localStorage';
import { API_URL } from '../libs/config';

export enum UserTypeStateEnum {
  DONE = 'done',
  PENDING = 'pending',
  ERROR = 'error',
  WAIT = 'wait',
}

export interface IUserStore {
  rootStore: RootStore,
  state: UserTypeStateEnum;
  isAuth: boolean,
  email: string,
  subscriptionRange: string,
  accessToken: string,
  refreshToken: string,
  role: string,
  login: (email: string, password: string) => void,
  refresh: (email: string, token: string) => void,
}

export class UserStore implements IUserStore{
  rootStore: RootStore;
  isAuth = false;
  state = UserTypeStateEnum.PENDING;
  email = '';
  subscriptionRange = '';
  accessToken = '';
  refreshToken = '';
  role = 'guest';

  constructor(rootStore: RootStore) {
    makeObservable(this, {
      email: observable,
      isAuth: observable,
      subscriptionRange: observable,
      accessToken: observable,
      refreshToken: observable,
      role: observable,
      state: observable,
      refresh: action,
      setUserData: action,
      getUser: action.bound,
      login: action.bound,
      logout: action.bound,
      rootStore: false
    })
    this.rootStore = rootStore;
  }

  setUserData(data: {email: string, subscriptionRange: string, accessToken: string, refreshToken: string, role: string}) {
    const {email, subscriptionRange, accessToken, refreshToken, role} = data;
    this.email = email;
    this.subscriptionRange = subscriptionRange;
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
    this.role = role;
    this.isAuth = true;
    setCookies('accessToken', accessToken);
    setToStorage('accessToken', accessToken);
    setToStorage('refreshToken', refreshToken);
    setToStorage('email', email);
  }

  async login(email: string, password: string): Promise<void> {
    if (this.state !== UserTypeStateEnum.WAIT) {
      this.state = UserTypeStateEnum.WAIT;
      try {
        const user = await UserStore.loginAPI(email, password)
        runInAction(() => {
          this.setUserData({...user});
          this.state = UserTypeStateEnum.DONE;
        })
      } catch (e) {
        runInAction(() => {
          this.state = UserTypeStateEnum.ERROR;
        })
      }
    }
  }

  async getUser(accessToken: string): Promise<void> {
    if (this.state !== UserTypeStateEnum.WAIT) {
      this.state = UserTypeStateEnum.WAIT;
      try {
        const user = await UserStore.getUserAPI(accessToken);
        runInAction(() => {
          this.setUserData({accessToken, ...user});
          this.state = UserTypeStateEnum.DONE;
        })
      } catch (e) {
        const refreshToken = getFromStorage('refreshToken');
        const email = getFromStorage('email');
        if (refreshToken && email) {
          runInAction(() => {
            this.state = UserTypeStateEnum.DONE;
            this.refresh(email, refreshToken);
          })
        }
      }
    }
  }

  async refresh(email: string, refreshToken: string) {
    if (this.state !== UserTypeStateEnum.WAIT) {
      this.state = UserTypeStateEnum.WAIT;
      try {
        const user = await UserStore.refreshAPI(email, refreshToken);
        runInAction(() => {
          this.setUserData({...user});
          this.state = UserTypeStateEnum.DONE;
        })
      } catch (e) {
        runInAction(() => {
          this.state = UserTypeStateEnum.ERROR;
        })
      }
    }
  }

  async logout() {
    this.email = '';
    this.subscriptionRange = '';
    this.accessToken = '';
    this.refreshToken = '';
    this.role = 'guest';
    this.isAuth = false;
    removeCookies('accessToken');
    removeFromStorage('accessToken');
    removeFromStorage('refreshToken');
    removeFromStorage('email');
  }

  private static async getUserAPI(accessToken: string) {
    const res = await axios.get(`${API_URL}/user`, {
      headers: {Authorization: `Bearer ${accessToken}`}
    })
    return res.data;
  }

  private static async loginAPI(email: string, password: string) {
    const res = await axios.post(`${API_URL}/auth/login`, {
        email,
        password,
      })
    return res.data;
  }

  private static async refreshAPI(email: string, refreshToken: string) {
    const res = await axios.post(`${API_URL}/auth/refresh`, {
      email,
      refreshToken,
    })
    return res.data;
  }
}
