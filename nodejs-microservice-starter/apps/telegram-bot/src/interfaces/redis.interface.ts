export interface RedisSessionConfig {
  readonly host: string;
  readonly port: number;
}
