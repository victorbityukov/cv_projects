export interface TelegramConfig {
  readonly token: string;
  readonly stageTtl: number;
  readonly redisSessionTtl: number;
}
