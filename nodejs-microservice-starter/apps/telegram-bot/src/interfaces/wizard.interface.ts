import { Context, Scenes } from 'telegraf';
import SceneContextScene from 'telegraf/typings/scenes/context';
import WizardContextWizard from 'telegraf/typings/scenes/wizard/context';

export interface State {
  name?: string;
  anything?: string;
}

export interface ExampleWizardSection extends Scenes.WizardSessionData {
  name?: string;
  anything?: string;
  tries: 0;
  state: State;
}

export interface ConfirmContext extends Context {
  scene: Scenes.SceneContextScene<ConfirmContext, ExampleWizardSection>;
  wizard: Scenes.WizardContextWizard<ConfirmContext>;
}

export type ConfirmWizardCtx = ConfirmContext & {
  scene: SceneContextScene<ConfirmContext, ExampleWizardSection>;
  wizard: WizardContextWizard<
    ConfirmContext & {
      scene: SceneContextScene<ConfirmContext, ExampleWizardSection>;
    }
  >;
};
