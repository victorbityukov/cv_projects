import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { setupDotEnv } from '@app/common';
import { TelegramBotConfig } from '@app/config';
import { TelegramModule } from './telegram/telegram.module';

async function bootstrap() {
  setupDotEnv();
  const PORT = TelegramBotConfig.TELEGRAM_BOT_BASE_PORT;
  const NAME_SERVICE = TelegramBotConfig.TELEGRAM_BOT_CONTAINER_NAME;

  const app = await NestFactory.create(TelegramModule);

  await app.listen(PORT);
  Logger.log(`Api microservice running ${NAME_SERVICE}: ${PORT}`);
}
bootstrap();
