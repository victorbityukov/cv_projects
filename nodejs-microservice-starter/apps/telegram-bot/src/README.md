## Telegram Bot

The first thing to do is to find a bot with the username **@BotFather** in the telegram client

It accepts a certain number of commands, among which there is a command `/newbot`, which allows us to create a bot

The creation procedure is not complicated and will not take much time, and as a result, a message with congratulations will be received, but what about us
more interested, the message will contain the **token** (or in other words **private key**) of the bot

This key allows us to control the bot

You can work with the API provided by the telegram itself, but to save time, ready-made packages are often used,
which provide ready-made methods for interacting with the telegram API. In our case, this library is **telegraf**
