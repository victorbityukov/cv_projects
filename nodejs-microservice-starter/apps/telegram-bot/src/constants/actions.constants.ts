export enum ACTIONS {
  MESSAGE = 'message',
  SAY_HI = 'sayhi',
  WHO = 'who',
  KEYBOARD_SHOW = 'keyboard_show',
  KEYBOARD_HIDDEN = 'keyboard_hidden',
  HELP = 'help',
  BACK = 'back',
  EXIT = 'exit',
}
