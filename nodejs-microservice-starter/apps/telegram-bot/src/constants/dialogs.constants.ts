import { ACTIONS } from './actions.constants';
import { ReplyInterface } from '../interfaces';

export const greetings: ReplyInterface = {
  text: 'Приветствие👻',
  language: 'ru',
};

export const onlyText: ReplyInterface = {
  text: 'Принимаю только текстовые сообщения👀',
  language: 'ru',
};

export const hiText: ReplyInterface = {
  text: 'Я то скажу тебе «Привет», но будешь ли ты счастлив от этого?',
  language: 'ru',
};

export const helpText: ReplyInterface = {
  text:
    'Доступные команды:\n\n' +
    `/${ACTIONS.SAY_HI} - попросить сказать привет\n` +
    `/${ACTIONS.HELP} - попросить помощи\n` +
    `/${ACTIONS.WHO} - включить сценарий\n` +
    `/${ACTIONS.EXIT} - выйти из сценария`,
  language: 'ru',
};
