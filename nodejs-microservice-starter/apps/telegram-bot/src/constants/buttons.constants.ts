import { ACTIONS } from './actions.constants';

export const buttonRowHelp = [
  {
    text: 'Помощь',
    callback_data: ACTIONS.HELP,
  },
];

export const buttonRowWhoAmI = [
  {
    text: 'Сценарий',
    callback_data: ACTIONS.WHO,
  },
];

export const buttonRowKeyboard = [
  {
    text: 'Показать клавиатуру',
    callback_data: ACTIONS.KEYBOARD_SHOW,
  },
  {
    text: 'Скрыть клавиатуру',
    callback_data: ACTIONS.KEYBOARD_HIDDEN,
  },
];
