import { Markup, Scenes, Telegraf } from 'telegraf';
import RedisSession from 'telegraf-session-redis';
import { Message } from 'telegraf/typings/core/types/typegram';
import { ExtraReplyMessage } from 'telegraf/typings/telegram-types';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRedis, Redis } from '@nestjs-modules/ioredis';
import { RedisConfig, telegramConfiguration } from '@app/config';
import { ConfirmContext, TelegramConfig } from '../interfaces';
import {
  ACTIONS,
  buttonRowHelp,
  buttonRowKeyboard,
  buttonRowWhoAmI,
  greetings,
  helpText,
  hiText,
  onlyText,
} from '../constants';

const TELEGRAM_CONFIG: TelegramConfig = telegramConfiguration;

@Injectable()
export class TelegramService {
  private bot: Telegraf<ConfirmContext>;

  private readonly WIZARD_SCENE_EXAMPLE = 'WIZARD_SCENE_EXAMPLE';

  private removeKeyboard = { reply_markup: { remove_keyboard: true } } as ExtraReplyMessage;

  private readonly MENU_KEYBOARD = Markup.inlineKeyboard([
    buttonRowKeyboard,
    buttonRowHelp,
    buttonRowWhoAmI,
  ]);
  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
  ) {
    const { token } = TELEGRAM_CONFIG;
    this.bot = new Telegraf<ConfirmContext>(token);

    const exampleWizardScene = this.createWizardScene();

    const stage = new Scenes.Stage<ConfirmContext>([exampleWizardScene], {
      ttl: TELEGRAM_CONFIG.stageTtl,
    });

    const redisSession = new RedisSession({
      store: {
        host: RedisConfig.REDIS_HOST,
        port: RedisConfig.REDIS_PORT,
      },
      ttl: TELEGRAM_CONFIG.redisSessionTtl,
    });

    this.bot.use(redisSession);
    this.bot.use(stage.middleware());

    // Пример реакции на команду /start
    this.bot.start(async (ctx) => {
      await ctx.reply(greetings.text, this.MENU_KEYBOARD);
    });

    // Реакция на нажатие на кнопку
    this.bot.action(ACTIONS.HELP, async (ctx) => {
      await ctx.reply(helpText.text, this.MENU_KEYBOARD);
      await ctx.answerCbQuery();
    });
    // Тоже самое, но уже реакция на команду
    this.bot.command(ACTIONS.HELP, async (ctx) => {
      await ctx.reply(helpText.text, this.MENU_KEYBOARD);
    });

    // Запуск сценария кнопкой
    this.bot.action(ACTIONS.WHO, async (ctx) => {
      await ctx.scene.enter(this.WIZARD_SCENE_EXAMPLE);
      await ctx.answerCbQuery();
    });
    // Запуск сценария командой
    this.bot.command(ACTIONS.WHO, async (ctx) => {
      await ctx.scene.enter(this.WIZARD_SCENE_EXAMPLE);
    });

    // показать клавиатуру по нажатию на кнопку
    this.bot.action(ACTIONS.KEYBOARD_SHOW, async (ctx) => {
      const exampleKeyboard = Markup.keyboard([['1', '2'], ['3'], ['4', '5']])
        .resize()
        .oneTime(true);
      await ctx.reply('Клавиатура доступна', exampleKeyboard);
      await ctx.answerCbQuery();
    });
    // Скрыть клавиатуру по нажатию на кнопку
    this.bot.action(ACTIONS.KEYBOARD_HIDDEN, async (ctx) => {
      await ctx.reply('Клавиатура удалена', this.removeKeyboard);
      await ctx.answerCbQuery();
    });

    // Пример реакции на команду
    this.bot.command(ACTIONS.SAY_HI, async (ctx) => {
      await ctx.reply(hiText.text);
    });

    // Выход из сценария
    exampleWizardScene.command(ACTIONS.EXIT, async (ctx) => {
      try {
        await ctx.reply('Вы покинули сценарий');
        return await ctx.scene.leave();
      } catch (e) {
        Logger.error(`Error leaving stage: ${e}`);
        return await ctx.scene.leave();
      }
    });

    // Пример подписки на событие (в данном случае слушаем новые сообщения от пользователя)
    this.bot.on(ACTIONS.MESSAGE, async (ctx) => {
      const messageText = (ctx.message as Message.TextMessage).text;
      if (messageText) {
        await ctx.reply(`Ты отправил мне\n\n*${messageText}*\n\nКрасавчик!`, {
          parse_mode: 'Markdown',
        });
      } else {
        await ctx.reply(onlyText.text);
      }
    });

    // Запуск бота
    this.bot.launch().catch((err) => {
      Logger.error(err);
    });
    // process.once('SIGINT', () => this.bot.stop('SIGINT'));
    // process.once('SIGTERM', () => this.bot.stop('SIGTERM'));
  }

  // Первый шаг сценария
  private async startStepEnterName(ctx: ConfirmContext) {
    const tgUser = ctx.from;
    if (tgUser) {
      await ctx.reply('Введите свое имя', { parse_mode: 'Markdown' });
    }
    return ctx.wizard.next();
  }

  // Второй шаг сценария
  private async handleTextName(ctx: ConfirmContext) {
    try {
      const name = (ctx.message as Message.TextMessage).text;
      if (!name) {
        await ctx.reply('Не был введен текст, попробуйте ещё');
        ctx.wizard.selectStep(ctx.wizard.cursor - 1);
        return await ctx.wizard.next();
      }
      ctx.scene.session.name = (ctx.message as Message.TextMessage).text;
      await ctx.reply('Введите что угодно');
      return ctx.wizard.next();
    } catch (e) {
      return ctx.wizard.back();
    }
  }

  // Третий шаг сценария
  private async handleTextAnything(ctx: ConfirmContext) {
    try {
      const anything = (ctx.message as Message.TextMessage).text;
      if (!anything) {
        await ctx.reply('Не был введен текст, попробуйте ещё');
        ctx.wizard.selectStep(ctx.wizard.cursor - 1);
        return await ctx.wizard.next();
      }
      ctx.scene.session.anything = (ctx.message as Message.TextMessage).text;
      const { name } = ctx.scene.session;
      await ctx.reply(`Вас зовут ${name} и вы захотели ввести\n«${anything}»`);
      return ctx.scene.leave();
    } catch (e) {
      return ctx.wizard.back();
    }
  }

  // Создаем сценарий (порядок методов имеет значение, каждый метод это шаг сценария)
  private createWizardScene(): Scenes.WizardScene<ConfirmContext> {
    return new Scenes.WizardScene(
      this.WIZARD_SCENE_EXAMPLE,
      (ctx) => this.startStepEnterName(ctx),
      (ctx) => this.handleTextName(ctx),
      (ctx) => this.handleTextAnything(ctx),
    );
  }
}
