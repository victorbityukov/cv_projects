import { Module } from '@nestjs/common';
import { RedisModule } from '@nestjs-modules/ioredis';
import { RedisConfig } from '@app/config';
import { TelegramService } from './telegram.service';

@Module({
  imports: [
    RedisModule.forRoot({
      config: {
        host: RedisConfig.REDIS_HOST,
        port: RedisConfig.REDIS_PORT,
        url: RedisConfig.URL,
      },
    }),
  ],
  providers: [TelegramService],
})
export class TelegramModule {}
