import request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AppModule } from '../src/test-example/app.module';
import { AppService } from '../src/test-example/app.service';

describe('ExampleTestE2e', () => {
  let app: INestApplication;
  const appService = {
    getSum: () => ({ sum: 8 }),
    getRandomDice: () => ({ random: 3 }),
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(AppService)
      .useValue(appService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET getSum`, () => {
    const addends = { a: 4, b: 4 };
    return request(app.getHttpServer())
      .get('/sum')
      .send(addends)
      .expect(200)
      .expect(appService.getSum());
  });

  it(`/GET getRandomDice`, () => {
    return request(app.getHttpServer())
      .get('/random-dice')
      .expect(200)
      .expect(appService.getRandomDice());
  });

  afterAll(async () => {
    await app.close();
  });
});
