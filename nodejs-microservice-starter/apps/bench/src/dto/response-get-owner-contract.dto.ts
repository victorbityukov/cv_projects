import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { SWAGGER_READ_METHOD_OWNER } from '../swagger';

export class ResponseGetOwnerContractDto {
  @ApiProperty(SWAGGER_READ_METHOD_OWNER)
  @IsString()
  readonly owner: string;
}
