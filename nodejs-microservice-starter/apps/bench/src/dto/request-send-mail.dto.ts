import { IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  SWAGGER_SEND_MAIL_MESSAGE,
  SWAGGER_SEND_MAIL_SUBJECT,
  SWAGGER_SEND_MAIL_TO_ADDRESS,
} from '@app/common';

export class RequestSendMailDto {
  @ApiProperty(SWAGGER_SEND_MAIL_TO_ADDRESS)
  @IsString()
  @IsEmail()
  readonly toAddress: string;

  @ApiProperty(SWAGGER_SEND_MAIL_SUBJECT)
  @IsString()
  readonly subject: string;

  @ApiProperty(SWAGGER_SEND_MAIL_MESSAGE)
  @IsString()
  readonly message: string;
}
