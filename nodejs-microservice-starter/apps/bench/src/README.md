## Bench with examples
Contain examples of calling various methods

 - [Example Get Instance Contract](#example-get-instance-contract)
 - [Testing (e2e, unit)](#example-get-instance-contract)
 - [Example Of Use MyLogger](#example-of-use-mylogger)
 - [RabbitMQ Microservice example](#rabbitmq-microservice-example)
 - [Redis Microservice example](#redis-microservice-example)
 - [Send Mail Example](#send-mail-example)
 - [Role Based Access Control](#role-based-access-control)
 - [File Upload](#file-upload)

### Example Get Instance Contract

When creating an instance of a contract, it must be initialized
```typescript
import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { ReferralProgramContract } from '@app/core';

@Injectable()
export class ContractListenerService implements OnApplicationBootstrap {
  constructor(private readonly referralProgramContract: ReferralProgramContract) {}
  
  async onApplicationBootstrap(): Promise<void> {
    await this.referralProgramContract.init();
  }
}

```

### Testing
Тесты были подготовлены для отдельного сервиса `test-example`
#### unit
За данный теста отвечает скрипт `test-example/app.controller.spec.ts`

Для запуска можно использовать команду `test` или `jest`
#### e2e
За e2e тест отвечает скрипт `../test/app.e2e-spec.ts`

Для запуска использовать команду `test:e2e`
### Example Of Use MyLogger
Для работы импортируем модуль `LoggerModule`

В сервисе в конструкторе создаем экземпляр логгера например вот так `private readonly myLogger: MyLogger`

В качестве контекста указываем название сервиса `this.myLogger.setContext(BenchService.name)`
Если контекст не будет указан, то будет выбрано значение по-умолчанию `NestWinston`

Немного подробнее о работе кастомного логгера
 - [Logger Service](../../logger-service/src/README.md#logger-service)
 - [Logger](../../../libs/common/src/logger/README.md#logger)


### RabbitMQ Microservice example
Пример находится в папке `rabbit-mq`

Пример подключения микросервиса `app.connectMicroservice` нахоится в `main.ts`

### Redis Microservice example
Пример находится в папке `redis`

### Send Mail Example
Эндпоинт `@Post('send-mail')`
Для работы необходимы заполненные данные почты в конфиге

```typescript
mailer:
  host: 'smtp.googlemail.com'
  port: 465
  auth:
    user: ''
    pass: ''
  rejectUnauthorized: true
```

### Role Based Access Control
#### Basic RBAC implementation
Работа RBAC обеспечивается следующими декораторами
```typescript
  @ApiBearerAuth()
  @Roles(RoleEnum.Admin)
  @UseGuards(RolesGuard)
```
Подробнее можно увидеть в контроллере, эндпоинт `access-admin`

#### Integrating CASL
Пример использования работы с CASL находится в `bench.service.ts` в методе `checkAbilitiesCasl` и доступен
по эндпоинту `check-abilities-casl`

### File Upload
Для загрузки файлов используем декоратор `@UseInterceptors`, а в качестве параметра `FileInterceptor` с неободимыми параметрами

Подробнее в bench.controller.ts
 - uploadFile
 - uploadFiles
