import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { TreasureContract } from '@app/core';
import { MailerService } from '@app/common';
import { MyLogger } from '@app/common';
import { ExampleEventDto, RequestSendMailDto, ResponseGetOwnerContractDto } from './dto';
import { CaslAbilityFactory } from './casl';
import { Article, User } from './casl/classes';
import { ActionEnum } from './casl/constants';

@Injectable()
export class BenchService implements OnApplicationBootstrap {
  constructor(
    private readonly treasureContract: TreasureContract,
    private readonly mailerService: MailerService,
    private readonly myLogger: MyLogger,
    private readonly caslAbilityFactory: CaslAbilityFactory,
  ) {
    this.myLogger.setContext(BenchService.name);
  }

  async onApplicationBootstrap(): Promise<void> {
    await this.treasureContract.init();
  }

  async toVoters(): Promise<void> {
    await this.treasureContract.toVoters();
  }

  async getOwner(): Promise<ResponseGetOwnerContractDto> {
    const owner = await this.treasureContract.getOwner();
    return { owner };
  }

  async sendMail(options: RequestSendMailDto): Promise<void> {
    const { toAddress, subject, message } = options;
    await this.mailerService.sendEmail(toAddress, subject, message);
  }

  useLoggerExample() {
    this.myLogger.log('TEST Logger (log)');
    this.myLogger.error('TEST Logger (error)');
    this.myLogger.warn('TEST Logger (warn)');
  }

  checkAbilitiesCasl() {
    const user = new User();
    const admin = new User();
    user.isAdmin = false;
    user.id = 1;

    const article = new Article();
    article.authorId = user.id;

    admin.isAdmin = true;
    const abilityUser = this.caslAbilityFactory.createForUser(user);
    const abilityAdmin = this.caslAbilityFactory.createForUser(admin);

    this.myLogger.log('Abilities user');
    this.myLogger.log(`Read: ${abilityUser.can(ActionEnum.Read, Article)}`);
    this.myLogger.log(`Delete: ${abilityUser.can(ActionEnum.Delete, Article)}`);
    this.myLogger.log(`Create: ${abilityUser.can(ActionEnum.Create, Article)}`);
    this.myLogger.log(
      `user.id == article.author.id: ${abilityUser.can(ActionEnum.Update, article)}`,
    ); //true
    article.authorId = 2;
    this.myLogger.log(
      `user.id != article.author.id: ${abilityUser.can(ActionEnum.Update, article)}`,
    ); //false

    this.myLogger.log('Abilities admin');
    this.myLogger.log(`Read: ${abilityAdmin.can(ActionEnum.Read, Article)}`);
    this.myLogger.log(`Delete: ${abilityAdmin.can(ActionEnum.Delete, Article)}`);
    this.myLogger.log(`Create: ${abilityAdmin.can(ActionEnum.Create, Article)}`);
  }

  eventHandler(event: ExampleEventDto) {
    console.log(event);
  }
}
