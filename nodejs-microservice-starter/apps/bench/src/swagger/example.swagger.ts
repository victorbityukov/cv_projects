import { ApiPropertyOptions } from '@nestjs/swagger';

export const SWAGGER_READ_METHOD_OWNER: ApiPropertyOptions = {
  name: 'owner',
  description: 'Owner of contract ',
  type: String,
  example: '0x1FBFAAE3Fc86936101dBFe1B2b9ADfAE8a8282df',
};
