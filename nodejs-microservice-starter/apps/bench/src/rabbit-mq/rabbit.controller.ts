import { Controller, Post } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { RabbitService } from './rabbit.service';

@ApiTags('Rabbit MQ')
@Controller()
export class RabbitController {
  constructor(private readonly rabbitService: RabbitService) {}

  @ApiOperation({ description: 'Added event in queue' })
  @ApiOkResponse({ description: 'Event added in queue' })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Post('event-in-queue')
  addEventInQueue(): void {
    return this.rabbitService.addEventInQueue();
  }
}
