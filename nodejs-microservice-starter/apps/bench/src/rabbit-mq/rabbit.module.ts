import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { BenchServiceConfig, RabbitmqConfig } from '@app/config';
import { RabbitController } from './rabbit.controller';
import { RabbitService } from './rabbit.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: BenchServiceConfig.BENCH_SERVICE_QUEUE,
        transport: Transport.RMQ,
        options: {
          urls: [RabbitmqConfig.CONNECTION_STRING],
          queue: BenchServiceConfig.BENCH_SERVICE_QUEUE,
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
  ],
  controllers: [RabbitController],
  providers: [RabbitService],
})
export class RabbitModule {}
