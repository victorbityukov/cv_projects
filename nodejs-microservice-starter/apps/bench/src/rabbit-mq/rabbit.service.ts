import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { BenchServiceConfig } from '@app/config';
import { RabbitEventEnum } from '../constants';

@Injectable()
export class RabbitService {
  constructor(
    @Inject(BenchServiceConfig.BENCH_SERVICE_QUEUE)
    private readonly client: ClientProxy,
  ) {}

  addEventInQueue(): void {
    this.client.emit(RabbitEventEnum.EXAMPLE_EVENT, {
      title: RabbitEventEnum.EXAMPLE_EVENT,
      id: Math.floor(90 * Math.random()) + 10,
    });
  }
}
