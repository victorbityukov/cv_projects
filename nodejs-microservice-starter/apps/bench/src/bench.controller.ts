import { diskStorage } from 'multer';
import * as fs from 'fs';
import moment from 'moment';
import { extname } from 'path';
import { nanoid } from 'nanoid';
import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  UploadedFile,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { RoleEnum } from '@app/domains';
import { BenchService } from './bench.service';
import { ExampleEventDto, RequestSendMailDto, ResponseGetOwnerContractDto } from './dto';
import { RabbitEventEnum } from './constants';
import { Roles } from './decorators';
import { RolesGuard } from './guards';

const storage = diskStorage({
  destination: (
    req: Express.Request,
    file: Express.Multer.File,
    cb: (error: Error | null, destination: string) => void,
  ) => {
    const date = moment(new Date()).format('DD-MM-YYYY');
    const path = `./files/upload/${date}`;
    fs.mkdirSync(path, { recursive: true });
    cb(null, path);
  },
  filename: (
    req: Express.Request,
    file: Express.Multer.File,
    cb: (error: Error | null, filename: string) => void,
  ) => {
    cb(null, `${nanoid(16)}${extname(file.originalname)}`);
  },
});

@ApiTags('Examples')
@Controller()
export class BenchController {
  constructor(private readonly benchService: BenchService) {}

  @ApiOperation({ description: 'Call write-method toVoters from contract treasure' })
  @ApiOkResponse({ description: 'Transaction sent' })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Post('write-method')
  sendTransactionToVoters(): Promise<void> {
    return this.benchService.toVoters();
  }

  @ApiOperation({ description: 'Call read-method owner from contract treasure' })
  @ApiOkResponse({
    description: 'Received the address of the owner of the contract',
    type: () => ResponseGetOwnerContractDto,
  })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Get('read-method')
  readMethod(): Promise<ResponseGetOwnerContractDto> {
    return this.benchService.getOwner();
  }

  @ApiOperation({ description: 'Send message by email' })
  @ApiOkResponse({ description: 'Mail sent' })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Post('send-mail')
  sendMail(@Body() options: RequestSendMailDto): Promise<void> {
    return this.benchService.sendMail(options);
  }

  @Post('upload-file')
  @UseInterceptors(
    FileInterceptor('file', {
      limits: {
        fileSize: 6000,
      },
      fileFilter: (
        req: Express.Request,
        file: Express.Multer.File,
        cb: (error: Error | null, acceptFile: boolean) => void,
      ) => {
        if (file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) {
          cb(null, true);
        } else {
          cb(
            new HttpException(
              `Unsupported file type ${extname(file.originalname)}`,
              HttpStatus.BAD_REQUEST,
            ),
            false,
          );
        }
      },
      storage,
    }),
  )
  uploadFile(@UploadedFile() file: Express.Multer.File): Express.Multer.File {
    return file;
  }

  @Post('upload-files')
  @UseInterceptors(
    FilesInterceptor('files', 2, {
      limits: {
        fileSize: 6000,
      },
      fileFilter: (
        req: Express.Request,
        file: Express.Multer.File,
        cb: (error: Error | null, acceptFile: boolean) => void,
      ) => {
        if (file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) {
          cb(null, true);
        } else {
          cb(
            new HttpException(
              `Unsupported file type ${extname(file.originalname)}`,
              HttpStatus.BAD_REQUEST,
            ),
            false,
          );
        }
      },
      storage,
    }),
  )
  uploadFiles(@UploadedFiles() files: Array<Express.Multer.File>): Array<Express.Multer.File> {
    return files;
  }

  @Get('use-logger-example')
  useLoggerExample() {
    return this.benchService.useLoggerExample();
  }

  @ApiBearerAuth()
  @Post('access-admin')
  @Roles(RoleEnum.Admin)
  @UseGuards(RolesGuard)
  accessAdminEndpoint() {
    return { status: 'Access granted' };
  }

  @Post('check-abilities-casl')
  checkAbilitiesCasl() {
    return this.benchService.checkAbilitiesCasl();
  }

  @MessagePattern(RabbitEventEnum.EXAMPLE_EVENT)
  handleEvent(@Payload() event: ExampleEventDto): void {
    return this.benchService.eventHandler(event);
  }
}
