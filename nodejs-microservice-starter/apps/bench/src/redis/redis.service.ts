import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { LoggerServiceConfig } from '@app/config';
import { LogLevelEnum, TypeEventEnum } from '@app/common';

@Injectable()
export class RedisService {
  constructor(
    @Inject(LoggerServiceConfig.LOGGER_SERVICE_QUEUE)
    private readonly client: ClientProxy,
  ) {}

  addEventInLogger(): void {
    this.client.emit(TypeEventEnum.LOG, {
      service: 'RedisService',
      message: 'TextMessageInfo',
      level: LogLevelEnum.INFO,
    });
    this.client.emit(TypeEventEnum.LOG, {
      service: 'RedisService',
      message: 'TextMessageError',
      level: LogLevelEnum.ERROR,
    });
    this.client.emit(TypeEventEnum.LOG, {
      service: 'RedisService',
      message: 'TextMessageWarn',
      level: LogLevelEnum.WARN,
    });
  }
}
