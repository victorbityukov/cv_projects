import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { LoggerServiceConfig, RedisConfig } from '@app/config';
import { RedisController } from './redis.controller';
import { RedisService } from './redis.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: LoggerServiceConfig.LOGGER_SERVICE_QUEUE,
        transport: Transport.REDIS,
        options: {
          url: RedisConfig.URL,
          retryAttempts: 3,
        },
      },
    ]),
  ],
  controllers: [RedisController],
  providers: [RedisService],
})
export class RedisModule {}
