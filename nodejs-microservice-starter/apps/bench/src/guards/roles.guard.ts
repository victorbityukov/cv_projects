import { firstValueFrom, timeout } from 'rxjs';
import { Injectable, CanActivate, ExecutionContext, Inject } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ClientProxy } from '@nestjs/microservices';
import { RoleEnum } from '@app/domains';
import { AuthServiceConfig } from '@app/config';
import { ROLES_KEY } from '../decorators';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    @Inject(AuthServiceConfig.AUTH_SERVICE_CONTAINER_NAME)
    private readonly client: ClientProxy,
    private reflector: Reflector,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const requiredRoles = this.reflector.getAllAndOverride<RoleEnum[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!requiredRoles) {
      return true;
    }
    const req = context.switchToHttp().getRequest();

    const response = await firstValueFrom(
      this.client
        .send({ role: 'auth', cmd: 'check' }, { jwt: req.headers['authorization']?.split(' ')[1] })
        .pipe(timeout(5000)),
    );
    const { user } = response;
    return requiredRoles.some((role) => user.role === role);
  }
}
