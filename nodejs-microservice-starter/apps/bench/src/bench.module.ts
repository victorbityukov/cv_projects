import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { MailerModule, Web3Module, Web3ProviderEnum } from '@app/common';
import { TreasureContract } from '@app/core';
import { LoggerModule } from '@app/common';
import { AuthServiceConfig } from '@app/config';
import { BenchController } from './bench.controller';
import { BenchService } from './bench.service';
import { RabbitModule } from './rabbit-mq';
import { RedisModule } from './redis';
import { CaslModule } from './casl';

@Module({
  imports: [
    MailerModule,
    Web3Module.withOptions({ provider: Web3ProviderEnum.WSS }),
    RabbitModule,
    RedisModule,
    LoggerModule,
    ClientsModule.register([
      {
        name: AuthServiceConfig.AUTH_SERVICE_CONTAINER_NAME,
        transport: Transport.TCP,
        options: {
          host: AuthServiceConfig.AUTH_SERVICE_CONTAINER_NAME,
          port: AuthServiceConfig.AUTH_MICROSERVICE_PORT,
        },
      },
    ]),
    CaslModule,
  ],
  controllers: [BenchController],
  providers: [BenchService, TreasureContract],
})
export class BenchModule {}
