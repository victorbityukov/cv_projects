import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Logger } from '@nestjs/common';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { BenchServiceConfig } from '@app/config';
import { RabbitmqConfig } from '@app/config';
import { setupDotEnv } from '@app/common';
import { MyLogger } from '@app/common';
import { BenchModule } from './bench.module';

async function bootstrap() {
  setupDotEnv();
  const PORT = BenchServiceConfig.BENCH_SERVICE_BASE_PORT;
  const NAME_SERVICE = BenchServiceConfig.BENCH_SERVICE_CONTAINER_NAME;

  const app = await NestFactory.create(BenchModule, {
    bufferLogs: true,
    autoFlushLogs: true,
  });

  app.useLogger(app.get(MyLogger));

  app.setGlobalPrefix('api/v1');

  app.enableCors({
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    allowedHeaders: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Observe',
    credentials: true,
  });

  const options = new DocumentBuilder()
    .setTitle('Bench Service')
    .setDescription('Provides REST API')
    .setVersion('1.0.0')
    .addBearerAuth()
    .build();

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.RMQ,
    options: {
      urls: [RabbitmqConfig.CONNECTION_STRING],
      queue: BenchServiceConfig.BENCH_SERVICE_QUEUE,
      queueOptions: {
        durable: false,
      },
    },
  });

  await app.startAllMicroservices();
  Logger.log(
    `Bench microservice running BENCH_SERVICE_QUEUE: ${BenchServiceConfig.BENCH_SERVICE_QUEUE}`,
  );

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/v1/docs', app, document);

  await app.listen(PORT);
  Logger.log(`Api microservice running ${NAME_SERVICE}: ${PORT}`);
}
bootstrap();
