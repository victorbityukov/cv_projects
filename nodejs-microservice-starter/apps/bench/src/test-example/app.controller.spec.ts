import { Test } from '@nestjs/testing';
import { Logger } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('ExampleTestController', () => {
  let appController: AppController;
  let appService: AppService;

  beforeAll(async () => {
    Logger.log('beforeAll');
    const moduleRef = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appService = moduleRef.get<AppService>(AppService);
    appController = moduleRef.get<AppController>(AppController);
  });

  describe('getSum', () => {
    it('should return sum equal 7', () => {
      const expected = { sum: 7 };
      const addends = {
        a: 3,
        b: 4,
      };
      expect(appService.getSum(addends)).toEqual(expected);
      expect(appController.getSum(addends)).toEqual(expected);
    });

    it('should return sum equal 8', async () => {
      const result = { sum: 8 };
      const addends = {
        a: 2,
        b: 2,
      };
      jest.spyOn(appService, 'getSum').mockImplementation(() => result);
      expect(appController.getSum(addends)).toBe(result);
    });
  });

  describe('getRandomDice', () => {
    it('should return greater 0 and less than 7', () => {
      for (let i = 0; i < 100; i += 1) {
        expect(appService.getRandomDice().random).toBeGreaterThanOrEqual(1);
        expect(appService.getRandomDice().random).toBeLessThanOrEqual(6);
      }
    });
  });

  describe('getFormattedData', () => {
    it('check type formatted data', () => {
      const expected = {
        id: expect.any(Number),
        data: expect.objectContaining({
          type: expect.any(String),
          items: expect.any(Array),
        }),
      };
      const data = appService.getFormattedData();
      expect(data).toEqual(expected);
    });
  });

  afterAll(async () => {
    Logger.log('afterAll');
  });
});
