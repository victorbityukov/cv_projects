import { Injectable } from '@nestjs/common';
import { AddendsDto, FormattedDataDto, ResponseGetRandomDice, ResponseSumDto } from './dto';

@Injectable()
export class AppService {
  getSum(addends: AddendsDto): ResponseSumDto {
    const { a, b } = addends;
    return {
      sum: a + b,
    };
  }

  getRandomDice(): ResponseGetRandomDice {
    return {
      random: Math.round(Math.random() * 5 + 1),
    };
  }

  getFormattedData(): FormattedDataDto {
    const countItems = Math.round(Math.random() * 10);
    const items: number[] = [];
    for (let i = 0; i < countItems; i += 1) {
      items.push(this.randomInt(100));
    }
    return {
      id: this.randomInt(100),
      data: {
        type: String(this.randomInt(1000)),
        items,
      },
    };
  }

  private randomInt(decimal: number): number {
    return Math.round(Math.random() * decimal);
  }
}
