export class FormattedDataDto {
  id: number;
  data: {
    type: string;
    items: number[];
  };
}
