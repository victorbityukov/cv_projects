import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { AddendsDto, ResponseGetRandomDice, ResponseSumDto } from './dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('sum')
  getSum(@Query() addends: AddendsDto): ResponseSumDto {
    return this.appService.getSum(addends);
  }

  @Get('random-dice')
  getRandomDice(): ResponseGetRandomDice {
    return this.appService.getRandomDice();
  }
}
