import helmet from 'helmet';
import RateLimit from 'express-rate-limit';
import compression from 'compression';
import morgan from 'morgan';
import { NestFactory, Reflector } from '@nestjs/core';
import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';
import {
  ClassSerializerInterceptor,
  HttpStatus,
  Logger,
  UnprocessableEntityException,
  ValidationPipe,
} from '@nestjs/common';
import { HttpExceptionFilter, setupDotEnv } from '@app/common';
import { QueryFailedFilter } from '@app/common';
import { AdminModule } from './admin.module';
import { AdminServiceConfig } from './config';

// for inter-module transactions
// import {
//   initializeTransactionalContext,
//   patchTypeORMRepositoryWithBaseRepository,
// } from 'typeorm-transactional-cls-hooked';

async function bootstrap(): Promise<NestExpressApplication> {
  setupDotEnv();
  // initializeTransactionalContext();
  // patchTypeORMRepositoryWithBaseRepository();
  const app = await NestFactory.create<NestExpressApplication>(AdminModule, new ExpressAdapter(), {
    cors: true,
  });

  // only if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
  app.enable('trust proxy');
  app.use(helmet());
  app.use(
    RateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windowMs
    }),
  );
  app.use(compression());
  app.use(morgan('combined'));
  app.setGlobalPrefix('api/v1');

  const reflector = app.get(Reflector);

  app.useGlobalFilters(new HttpExceptionFilter(reflector), new QueryFailedFilter(reflector));

  app.useGlobalInterceptors(new ClassSerializerInterceptor(reflector));

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
      transform: true,
      dismissDefaultMessages: true,
      exceptionFactory: (errors) => new UnprocessableEntityException(errors),
    }),
  );

  await app.listen(AdminServiceConfig.ADMIN_SERVICE_BASE_PORT);
  Logger.log(
    `admin-service running ADMIN_SERVICE_BASE_PORT: ${AdminServiceConfig.ADMIN_SERVICE_BASE_PORT}`,
  );

  return app;
}

void bootstrap();
