import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { PostService } from './post.service';
import { CreatePostDto, UpdatePostDto } from './dto';

@Controller('post')
export class PostController {
  constructor(readonly postService: PostService) {}

  @Get()
  async all() {
    return this.postService.all();
  }

  @Post()
  async create(@Body() post: CreatePostDto) {
    return this.postService.create(post);
  }

  @Get(':id')
  async findById(@Param('id') postId: number) {
    return this.postService.findById(postId);
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() post: UpdatePostDto) {
    return this.postService.update(id, post);
  }

  @Delete(':id')
  async delete(@Param('id') id: number) {
    return this.postService.delete(id);
  }
}
