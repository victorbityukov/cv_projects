import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { RabbitmqConfig } from '@app/config';
import { PostController } from './post.controller';
import { PostEntity } from './post.entity';
import { PostService } from './post.service';
import { PostServiceConfig } from '../../../post-service/src/config';

@Module({
  imports: [
    TypeOrmModule.forFeature([PostEntity]),
    ClientsModule.register([
      {
        name: PostServiceConfig.POST_SERVICE_QUEUE,
        transport: Transport.RMQ,
        options: {
          urls: [RabbitmqConfig.CONNECTION_STRING],
          queue: PostServiceConfig.POST_SERVICE_QUEUE,
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
  ],
  controllers: [PostController],
  providers: [PostService],
})
export class PostModule {}
