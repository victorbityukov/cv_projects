import { Repository } from 'typeorm';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PostEntity } from './post.entity';
import { CreatePostDto, UpdatePostDto } from './dto';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(PostEntity)
    readonly postRepository: Repository<PostEntity>,
  ) {}

  async all(): Promise<PostEntity[]> {
    return this.postRepository.find();
  }

  async create(post: CreatePostDto): Promise<PostEntity> {
    return this.postRepository.save(post);
  }

  async findById(postId: number): Promise<PostEntity> {
    return this.postRepository.findOne(postId);
  }

  async update(id: number, post: UpdatePostDto): Promise<UpdateResult> {
    return this.postRepository.update(id, post);
  }

  async delete(id: number): Promise<DeleteResult> {
    return this.postRepository.delete(id);
  }
}
