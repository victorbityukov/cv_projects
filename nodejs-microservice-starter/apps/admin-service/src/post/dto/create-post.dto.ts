import { IsString } from 'class-validator';
import { PostInterface } from '@app/domains';

export class CreatePostDto implements Pick<PostInterface, 'title' | 'content' | 'img'> {
  @IsString()
  content: string;

  @IsString()
  img: string;

  @IsString()
  title: string;
}
