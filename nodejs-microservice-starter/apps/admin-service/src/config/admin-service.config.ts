import { get } from 'env-var';

export class AdminServiceConfig {
  public static readonly ADMIN_SERVICE_CONTAINER_NAME: string = get('ADMIN_SERVICE_CONTAINER_NAME')
    .required()
    .asString();

  public static readonly ADMIN_SERVICE_BASE_PORT: number = get('ADMIN_SERVICE_BASE_PORT')
    .required()
    .asPortNumber();
}
