import { Body, Controller, Get, Logger, Post, Req, Request, UseGuards } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { AuthLocalGuard } from './guards';

@Controller()
export class AuthController {
  constructor(private readonly authServiceTcpService: AuthService) {}

  @UseGuards(AuthLocalGuard)
  @Post('auth')
  async login(@Request() req) {
    return this.authServiceTcpService.login(req.user);
  }

  @Post('sign-up')
  async signUp(@Body() body) {
    return this.authServiceTcpService.signUp(body);
  }

  @Get('discord')
  @UseGuards(AuthGuard('discord'))
  getUserFromDiscordLogin(@Req() req) {
    const { user } = req;
    return user;
  }

  @MessagePattern({ role: 'auth', cmd: 'check' })
  async loggedIn(data) {
    try {
      return this.authServiceTcpService.validateToken(data.jwt);
    } catch (e) {
      Logger.error(e);
      return false;
    }
  }
}
