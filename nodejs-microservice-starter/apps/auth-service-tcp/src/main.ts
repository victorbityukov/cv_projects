import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AuthServiceConfig } from '@app/config';
import { setupDotEnv } from '@app/common';
import { AuthModule } from './auth.module';

async function bootstrap() {
  setupDotEnv();
  const app = await NestFactory.create(AuthModule);

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.TCP,
    options: {
      host: '::',
      port: AuthServiceConfig.AUTH_MICROSERVICE_PORT,
    },
  });
  await app.startAllMicroservices();
  Logger.log(
    `Auth microservice running AUTH_MICROSERVICE_PORT: ${AuthServiceConfig.AUTH_MICROSERVICE_PORT}`,
  );

  await app.listen(AuthServiceConfig.AUTH_SERVICE_BASE_PORT);
  Logger.log(
    `Auth service running AUTH_SERVICE_BASE_PORT: ${AuthServiceConfig.AUTH_SERVICE_BASE_PORT}`,
  );
}

export default bootstrap();
