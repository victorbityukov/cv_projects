import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { PassportModule } from '@nestjs/passport';
import { HttpModule } from '@nestjs/axios';
import { JwtConfig, UserServiceConfig } from '@app/config';
import { DiscordStrategy, JwtStrategy, LocalStrategy } from './strategies';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

@Module({
  imports: [
    HttpModule,
    ClientsModule.register([
      {
        name: UserServiceConfig.USER_SERVICE_CONTAINER_NAME,
        transport: Transport.TCP,
        options: {
          host: UserServiceConfig.USER_SERVICE_CONTAINER_NAME,
          port: UserServiceConfig.USER_MICROSERVICE_PORT,
        },
      },
    ]),
    JwtModule.register({
      secret: JwtConfig.JWT_SECRET,
      signOptions: { expiresIn: '600s' },
    }),
    PassportModule.register({}),
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy, DiscordStrategy],
})
export class AuthModule {}
