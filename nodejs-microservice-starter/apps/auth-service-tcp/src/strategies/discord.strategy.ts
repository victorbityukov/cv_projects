import { lastValueFrom } from 'rxjs';
import { stringify } from 'querystring';
import { Strategy } from 'passport-oauth2';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { discordConfiguration } from '@app/config';
import { AuthService } from '../auth.service';

const { clientID, clientSecret, callbackURL, tokenURL } = discordConfiguration;

@Injectable()
export class DiscordStrategy extends PassportStrategy(Strategy, 'discord') {
  constructor(private authService: AuthService, private http: HttpService) {
    super({
      authorizationURL: `https://discordapp.com/api/oauth2/authorize?${stringify({
        client_id: clientID,
        redirect_uri: callbackURL,
        response_type: 'code',
        scope: 'identify',
      })}`,
      scope: 'identify',
      tokenURL,
      clientID,
      clientSecret,
      callbackURL,
    });
  }

  async validate(accessToken: string) {
    const { data } = await lastValueFrom(
      this.http.get('https://discordapp.com/api/users/@me', {
        headers: { Authorization: `Bearer ${accessToken}` },
      }),
    );
    return data;
    // return this.authService.findUserFromDiscordId(data.id);
  }
}
