import { compareSync } from 'bcrypt';
import { catchError, firstValueFrom, throwError, timeout, TimeoutError } from 'rxjs';
import { Inject, Injectable, Logger, RequestTimeoutException } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { JwtService } from '@nestjs/jwt';
import { UserServiceConfig } from '@app/config';

@Injectable()
export class AuthService {
  constructor(
    @Inject(UserServiceConfig.USER_SERVICE_CONTAINER_NAME)
    private readonly client: ClientProxy,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<any> {
    try {
      const user = await firstValueFrom(
        this.client.send({ role: 'user', cmd: 'get' }, { username }).pipe(
          timeout(5000),
          catchError((err) => {
            if (err instanceof TimeoutError) {
              return throwError(() => new RequestTimeoutException());
            }
            return throwError(err);
          }),
        ),
      );

      if (compareSync(password, user?.password)) {
        return user;
      }

      return null;
    } catch (e) {
      Logger.error(e);
      throw e;
    }
  }

  async signUp(user): Promise<any> {
    try {
      const userData = await firstValueFrom(
        this.client.send({ role: 'user', cmd: 'sign-up' }, user).pipe(
          timeout(5000),
          catchError((err) => {
            if (err instanceof TimeoutError) {
              return throwError(() => new RequestTimeoutException());
            }
            return throwError(err);
          }),
        ),
        { defaultValue: 'Answer is empty' },
      );
      return userData;
    } catch (e) {
      Logger.error(e);
      throw e;
    }
  }

  validateToken(jwt: string) {
    return this.jwtService.verify(jwt);
  }

  async login(user) {
    delete user.password;
    const payload = { user, sub: user.id };

    return {
      userId: user.id,
      accessToken: this.jwtService.sign(payload),
    };
  }
}
