## Auth Service TCP
 - [Authentication JWT](#authentication-jwt)
 - [Authentication OAuth](#authentication-oauth)
 - [Auth TCP](#auth-tcp)

### Authentication OAuth
Авторизация по протоколу OAuth 2 для примера реализована через Discord

Чтобы она начала работать, обязательно должны быть заполнены следующие поля в `config/<config_name>.yml`
```
discord:
  clientID:
  clientSecret:
  callbackURL: 'http://localhost:3010/discord'
  tokenURL: 'https://discordapp.com/api/oauth2/token'
```

### Authentication JWT
Подключение JWT модуля
```typescript
JwtModule.register({
  secret: JwtConfig.JWT_SECRET,
  signOptions: { expiresIn: '600s' },
}),
```
Стратегия JWT в папке `strategies`

### Auth TCP
Для работы TCP импортируем микросервис ClientsModule и регистрируем
```typescript
ClientsModule.register([
  {
    name: UserServiceConfig.USER_SERVICE_CONTAINER_NAME,
    transport: Transport.TCP,
    options: {
      host: UserServiceConfig.USER_SERVICE_CONTAINER_NAME,
      port: UserServiceConfig.USER_MICROSERVICE_PORT,
    },
  },
])
```
Далее в самом сервисе инъектим клиент:
```typescript
 @Inject(UserServiceConfig.USER_SERVICE_CONTAINER_NAME)
 private readonly client: ClientProxy,
```
Для отправки данных используем следующую конструкцию:
```typescript
 const user = await firstValueFrom(
  this.client.send({ role: 'user', cmd: 'get' }, { username }).pipe(
    timeout(5000),
    catchError((err) => {
      if (err instanceof TimeoutError) {
        return throwError(() => new RequestTimeoutException());
      }
      return throwError(err);
    }),
  ),
);
```
Где первый параметр это паттерн, который будет принимать данные, а второй параметр это сами данные, которые будут отправлены

Для того чтобы получить данные мы аналогичным образом импортируем микросервис ClientsModule и регистрируем его
с тем отличием, что в качестве параметров для TCP передаем данные сервиса, который будем слушать

В user-service-tcp это выглядит вот так
```typescript
ClientsModule.register([
  {
    name: AuthServiceConfig.AUTH_SERVICE_CONTAINER_NAME,
    transport: Transport.TCP,
    options: {
      host: AuthServiceConfig.AUTH_SERVICE_CONTAINER_NAME,
      port: AuthServiceConfig.AUTH_MICROSERVICE_PORT,
    },
  },
]),
```
И затем в контроллере мы ловим данные с помощью декоратора вида:
```typescript
@MessagePattern({ role: 'user', cmd: 'get' })
```
