## Contract Manager

Contract-manager must be started after init-service and before other services. It will move actual data abi in abi files
and will save abi.json in database by contract addresses

Web3 Service uses abi files to get the abi of the contract

[Подробнее про Web3 Service](../../../libs/common/src/web3/README.md#web3-service)
