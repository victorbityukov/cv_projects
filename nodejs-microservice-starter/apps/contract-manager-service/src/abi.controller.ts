import { Repository } from 'typeorm';
import { Controller, Get, Param } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ContractEntity } from '@app/domains';
import { AbiService } from './abi.service';

@Controller()
export class AbiController {
  constructor(
    private readonly abiService: AbiService,
    @InjectRepository(ContractEntity)
    private readonly contractRepository: Repository<ContractEntity>,
  ) {}

  @Get('/getabi/:address')
  async getAbi(@Param('address') address: string) {
    let check = true;
    let data = { result: '' };

    do {
      const res = await this.abiService.getAbi(address);
      if (res.data.status === '1') {
        check = false;
        data = res.data;
      } else {
        await new Promise((resolve) => {
          setTimeout(() => resolve(null), 5000);
        });
      }
    } while (check);

    return data.result;
  }
}
