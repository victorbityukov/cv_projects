import { Repository } from 'typeorm';
import { from, lastValueFrom, mergeMap } from 'rxjs';
import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { web3Config } from '@app/config';
import { ContractEntity } from '@app/domains';
import { sleep } from '@app/common';
import { MyLogger } from '@app/common';
import { AbiService } from './abi.service';

@Injectable()
export class ContractManagerService implements OnApplicationBootstrap {
  constructor(
    @InjectRepository(ContractEntity)
    readonly contractRepository: Repository<ContractEntity>,
    readonly abiService: AbiService,
    readonly myLogger: MyLogger,
  ) {
    this.myLogger.setContext(ContractManagerService.name);
  }

  private readonly MAX_ATTEMPT_GET_ABI = 5;
  private readonly DEFAULT_ABI_NAME = 'default';

  async onApplicationBootstrap(): Promise<void> {
    await this.loadInitialContracts();
  }

  private async loadInitialContracts(): Promise<void> {
    try {
      const contracts = Object.entries(web3Config.contracts);
      const contractsConfigAddresses: string[] = [];

      // Объект ключами которого являются адреса контрактов, а значением наименования контрактов
      const contractsByAddress: {
        [address: string]: string;
      } = {};

      // Формируем массив адресов контрактов, которые указаны в конфиг файле
      // Заполняем объект contractsByAddress
      for (let i = 0; i < contracts.length; i += 1) {
        const [name, address] = contracts[i];
        contractsConfigAddresses.push(address);
        contractsByAddress[address] = name;
      }

      // Получаем контракты по адресам из конфига
      // У которых есть актуальное аби, полученное с etherscan
      const contractsDB = await this.contractRepository
        .createQueryBuilder()
        .where('address IN (:...addresses)', { addresses: contractsConfigAddresses })
        .andWhere('abi IS NOT NULL')
        .getMany();

      // Формируем массив адресов контрактов, которые были получены из базы
      const addressesContractsFromDB = contractsDB.map((contract) => contract.address);

      // Актуализируем файлы аби для контрактов с актуальным значением аби в базе
      await this.abiService.writeReadyContracts(contractsDB);

      // Находим адреса контрактов, указанных в конфиге, но для которых в базе нет аби
      const addressContractDiff = contractsConfigAddresses.filter(
        (address) => addressesContractsFromDB.indexOf(address) === -1,
      );

      // Счетчик количества контрактов, для которых удалось догрузить аби с etherscan
      let amountContractAdded = 0;

      // Проходим по массиву контрактов из конфига, но для которых аби ещё не получено
      await lastValueFrom(
        from(addressContractDiff).pipe(
          mergeMap(async (address) => {
            try {
              // Получаем имя контракта по адресу контракта
              const name = contractsByAddress[address];
              // Проверяем и запоминаем был ли контракт создан в базе
              // А он мог быть уже создан, но без аби
              const contractExist = await this.contractRepository.findOne({ address });
              this.myLogger.log(`Address: ${address} Contract: ${name}`);
              // Цикл попыток получения аби контракта из etherscan
              for (let i = 0; i < this.MAX_ATTEMPT_GET_ABI; i += 1) {
                const res = await this.abiService.getAbi(address);
                if (res.data.status === '1') {
                  // Приводим положительный результат к нужному формату
                  const result = JSON.parse(res.data.result);
                  const abi = JSON.stringify(result, null, 2);
                  const fileData = await this.abiService.readAbiFromFileByName(name);
                  // Записываем актуальный аби в файл
                  if (!(fileData === abi || abi === JSON.stringify([]))) {
                    this.myLogger.log(`WriteFile: ${this.abiService.generatePathToAbiFile(name)}`);
                    await this.abiService.writeAbiInFileByName(name, abi);
                  }
                  // В зависимости от того, была ли создана запись контракта в базе ранее
                  // Либо создаем новую запись, либо добавляем аби к существующей
                  const contractData = { name, address, abi };
                  if (contractExist) {
                    await this.contractRepository.update({ address }, contractData);
                  } else {
                    const contract = this.contractRepository.create(contractData);
                    await this.contractRepository.save(contract);
                  }
                  this.myLogger.log('Success');
                  amountContractAdded += 1;
                  break;
                } else {
                  // После каждой неудачной попытки получить аби ожидание
                  // Связано с ограничением на количество запросов
                  // Ограничение можно ослабить приватным ключем
                  // Но в данной реализации он не предусмотрен
                  await sleep(2500);
                  this.myLogger.log(
                    `Try get ABI from Etherscan ${i + 1}/${this.MAX_ATTEMPT_GET_ABI}`,
                  );
                  if (i >= this.MAX_ATTEMPT_GET_ABI - 1) {
                    // Если количество попыток получить аби из etherscan достигло максимума
                    // То переходим к вариантам с менее акутальными аби
                    const name = contractsByAddress[address];
                    // Пробуем получить аби контракта из базы, но не по адресу, а по имени
                    // Ранее мог использоваться контракт с тем же именем
                    // Допустим, предыдущая версия текущего
                    // Аби могло измениться незначительно
                    const contractByName = await this.contractRepository
                      .createQueryBuilder()
                      .where('name = :name', { name })
                      .andWhere('abi IS NOT NULL')
                      .getOne();
                    if (contractByName) {
                      // Аби предыдущего контракта найдено
                      // Актуализируем файл аби
                      await this.abiService.writeAbiInFileByName(name, contractByName.abi);
                    } else {
                      // В худшем случае записываем совсем дефолтное универсальное аби
                      const defaultContractAbi = await this.abiService.readAbiFromFileByName(
                        this.DEFAULT_ABI_NAME,
                      );
                      await this.abiService.writeAbiInFileByName(name, defaultContractAbi);
                    }
                    // Добавляем по необходимости контракт в базу, но без аби
                    // Если контракт ещё не был добавлен
                    if (!contractExist) {
                      const contract = this.contractRepository.create({ name, address });
                      await this.contractRepository.save(contract);
                    }
                    this.myLogger.warn(`Will be get ${this.DEFAULT_ABI_NAME}.abi.json`);
                  }
                }
              }
              this.myLogger.log(
                `New contracts added: ${amountContractAdded}/${addressContractDiff.length}`,
              );
            } catch (e) {
              this.myLogger.error(e);
            }
          }, 1),
        ),
        { defaultValue: [] },
      );
    } catch (e) {
      this.myLogger.error(e);
    }
  }
}
