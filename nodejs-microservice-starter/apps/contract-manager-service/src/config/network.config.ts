import config from 'config';
import { NetworkInterface } from './interfaces';

export const networkConfig = config.get<NetworkInterface>('network');
