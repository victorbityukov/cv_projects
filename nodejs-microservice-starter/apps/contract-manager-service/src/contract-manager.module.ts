import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';
import { PostgresConfig } from '@app/config';
import { ContractEntity } from '@app/domains';
import { LoggerModule } from '@app/common';
import { AbiService } from './abi.service';
import { ContractManagerService } from './contract-manager.service';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: PostgresConfig.POSTGRES_HOST,
      port: PostgresConfig.POSTGRES_PORT,
      username: PostgresConfig.POSTGRES_USER,
      password: PostgresConfig.POSTGRES_PASSWORD,
      database: PostgresConfig.POSTGRES_DB,
      logging: false,
      synchronize: true,
      keepConnectionAlive: true,
      entities: [ContractEntity],
    }),
    TypeOrmModule.forFeature([ContractEntity]),
    LoggerModule,
  ],
  providers: [ContractManagerService, AbiService],
})
export class ContractManagerModule {}
