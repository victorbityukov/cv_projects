import { lastValueFrom } from 'rxjs';
import * as path from 'path';
import { existsSync } from 'fs';
import { writeFile, readFile } from 'fs/promises';
import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { camelCaseToDash } from '@app/common';
import { ContractEntity } from '@app/domains';
import { networkConfig } from './config';

@Injectable()
export class AbiService {
  private readonly abiDirPath: string;

  constructor(private httpService: HttpService) {
    this.abiDirPath = path.join(__dirname, '../../../files/abi');
  }

  async getAbi(address: string) {
    const params = {
      module: 'contract',
      action: 'getabi',
      address,
    };

    return lastValueFrom(
      this.httpService.get(`${networkConfig.etherscanUrl}/api`, {
        params,
      }),
    );
  }

  async writeAbiInFileByName(nameContract: string, abiInfo: string): Promise<void> {
    const pathFileContract = this.generatePathToAbiFile(nameContract);
    await writeFile(pathFileContract, abiInfo);
  }

  async readAbiFromFileByName(nameContract: string): Promise<string> {
    const pathFileContract = this.generatePathToAbiFile(nameContract);
    if (existsSync(pathFileContract)) {
      return readFile(pathFileContract, 'utf-8');
    }
    return '';
  }

  async writeReadyContracts(contracts: ContractEntity[]): Promise<void> {
    for (let i = 0; i < contracts.length; i++) {
      await this.writeAbiInFileByName(contracts[i].name, contracts[i].abi);
    }
  }

  generatePathToAbiFile(nameContract: string): string {
    return path.join(this.abiDirPath, `${camelCaseToDash(nameContract)}.abi.json`);
  }
}
