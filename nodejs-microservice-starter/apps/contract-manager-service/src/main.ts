import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { setupDotEnv } from '@app/common';
import { ContractManagerServiceConfig } from '@app/config';
import { MyLogger } from '@app/common';
import { ContractManagerModule } from './contract-manager.module';

async function bootstrap() {
  setupDotEnv();
  const PORT = ContractManagerServiceConfig.CONTRACT_MANAGER_SERVICE_BASE_PORT;
  const NAME_SERVICE = ContractManagerServiceConfig.CONTRACT_MANAGER_SERVICE_CONTAINER_NAME;

  const app = await NestFactory.create(ContractManagerModule, {
    bufferLogs: true,
    autoFlushLogs: true,
  });
  app.useLogger(app.get(MyLogger));
  await app.listen(PORT);
  Logger.log(`Api microservice running ${NAME_SERVICE}: ${PORT}`);
}
bootstrap();
