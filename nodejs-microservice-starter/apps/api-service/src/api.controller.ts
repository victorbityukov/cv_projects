import { Controller, Get } from '@nestjs/common';
import {
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { ApiService } from './api.service';

@Controller()
export class ApiController {
  constructor(private readonly apiServiceService: ApiService) {}

  @ApiOperation({ description: 'This is the perfect description' })
  @ApiOkResponse({ description: 'This is response' })
  @ApiBadRequestResponse({ description: 'Invalid request body' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @ApiNotFoundResponse({ description: 'Information not found' })
  @Get()
  getHello(): string {
    return this.apiServiceService.getHello();
  }
}
