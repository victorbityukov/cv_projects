import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RedisModule } from '@nestjs-modules/ioredis';
import { RedisConfig, PostgresConfig } from '@app/config';
import { UserEntity } from '@app/domains';
import { ApiController } from './api.controller';
import { ApiService } from './api.service';

@Module({
  imports: [
    RedisModule.forRoot({
      config: {
        url: RedisConfig.URL,
      },
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: PostgresConfig.POSTGRES_HOST,
      port: PostgresConfig.POSTGRES_PORT,
      username: PostgresConfig.POSTGRES_USER,
      password: PostgresConfig.POSTGRES_PASSWORD,
      database: PostgresConfig.POSTGRES_DB,
      logging: true,
      synchronize: false,
      keepConnectionAlive: true,
      entities: [UserEntity],
    }),
  ],
  controllers: [ApiController],
  providers: [ApiService],
})
export class ApiModule {}
