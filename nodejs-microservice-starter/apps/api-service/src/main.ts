import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { ApiServiceConfig } from '@app/config';
import { ApiModule } from './api.module';
import { setupDotEnv, setupSwagger } from '@app/common';
import { SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  setupDotEnv();
  const PORT = ApiServiceConfig.API_SERVICE_BASE_PORT;
  const app = await NestFactory.create(ApiModule);

  // Swagger Setup
  const options = setupSwagger('api-service', 'REST API', '1.0.0');
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/v1/docs', app, document);

  await app.listen(PORT);
  Logger.log(`Api microservice running API_SERVICE_BASE_PORT: ${PORT}`);
}

export default bootstrap();
