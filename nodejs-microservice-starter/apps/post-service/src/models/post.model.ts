import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { PostInterface } from '@app/domains/post';

export type PostDocument = PostModel & Document;

export const PostCollection = 'post';

@Schema({ timestamps: true })
export class PostModel implements PostInterface {
  @Prop()
  img: string;

  @Prop({
    default: 0,
  })
  likes: number;

  @Prop()
  title: string;

  @Prop()
  content: string;
}

export const PostSchema = SchemaFactory.createForClass(PostModel);
