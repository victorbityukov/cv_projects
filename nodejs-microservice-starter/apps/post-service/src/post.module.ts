import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ElasticsearchServiceConfig, MongoConfig, RabbitmqConfig } from '@app/config';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { PostCollection, PostSchema } from './models';
import { PostServiceConfig } from './config';

@Module({
  imports: [
    MongooseModule.forRoot(MongoConfig.CONNECTION_STRING, {
      dbName: PostServiceConfig.POST_SERVICE_CONTAINER_NAME,
      autoCreate: true,
    }),
    MongooseModule.forFeature([{ name: PostCollection, schema: PostSchema }]),
    ClientsModule.register([
      {
        name: ElasticsearchServiceConfig.ELASTIC_SERVICE_QUEUE,
        transport: Transport.RMQ,
        options: {
          urls: [RabbitmqConfig.CONNECTION_STRING],
          queue: ElasticsearchServiceConfig.ELASTIC_SERVICE_QUEUE,
          queueOptions: {
            durable: false,
            exclusive: false,
          },
        },
      },
    ]),
  ],
  controllers: [PostController],
  providers: [PostService],
})
export class PostModule {}
