import helmet from 'helmet';
import RateLimit from 'express-rate-limit';
import compression from 'compression';
import morgan from 'morgan';
import { NestFactory, Reflector } from '@nestjs/core';
import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';
import {
  ClassSerializerInterceptor,
  HttpStatus,
  Logger,
  UnprocessableEntityException,
  ValidationPipe,
} from '@nestjs/common';
import { HttpExceptionFilter, QueryFailedFilter, setupDotEnv } from '@app/common';
import { PostServiceConfig } from './config';
import { PostModule } from './post.module';

async function bootstrap(): Promise<NestExpressApplication> {
  setupDotEnv();
  const app = await NestFactory.create<NestExpressApplication>(PostModule, new ExpressAdapter(), {
    cors: true,
  });

  app.enable('trust proxy');
  app.use(helmet());
  app.use(
    RateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windowMs
    }),
  );
  app.use(compression());
  app.use(morgan('combined'));
  app.setGlobalPrefix('api/v1');

  const reflector = app.get(Reflector);
  app.useGlobalFilters(new HttpExceptionFilter(reflector), new QueryFailedFilter(reflector));
  app.useGlobalInterceptors(new ClassSerializerInterceptor(reflector));

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
      transform: true,
      dismissDefaultMessages: true,
      exceptionFactory: (errors) => new UnprocessableEntityException(errors),
    }),
  );

  await app.listen(PostServiceConfig.POST_SERVICE_BASE_PORT);
  Logger.log(
    `post-service running POST_SERVICE_BASE_PORT: ${PostServiceConfig.POST_SERVICE_BASE_PORT}`,
  );

  return app;
}

void bootstrap();
