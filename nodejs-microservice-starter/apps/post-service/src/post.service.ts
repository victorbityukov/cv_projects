import { catchError, firstValueFrom, throwError, timeout, TimeoutError } from 'rxjs';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Inject, Injectable, Logger, RequestTimeoutException } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import {
  RequestCreatePostDto,
  RequestDeletePostDto,
  RequestGetPostDto,
  RequestUpdatePostDto,
  ResponseCreatePostDto,
  ResponseGetPostDto,
} from '@app/domains';
import { ElasticsearchServiceConfig } from '@app/config';
import { PostModel, PostDocument, PostCollection } from './models';
import { RabbitEventEnum } from '../../elasticsearch-service/src/constants';

@Injectable()
export class PostService {
  constructor(
    @InjectModel(PostCollection)
    private readonly postRepository: Model<PostDocument>,
    @Inject(ElasticsearchServiceConfig.ELASTIC_SERVICE_QUEUE)
    private readonly client: ClientProxy,
  ) {}

  async all(): Promise<PostModel[]> {
    return this.postRepository.find().exec();
  }

  async createPost(postRequest: RequestCreatePostDto): Promise<ResponseCreatePostDto> {
    try {
      const post = await this.postRepository.create(postRequest);
      const { _id: id, img, title, content, likes } = post;
      console.log('Emit RabbitMQ');

      this.client.emit(RabbitEventEnum.ELASTIC_INDEX_POST, { id, title, content });
      return { img, title, content, likes };
    } catch (e) {
      Logger.error(e);
    }
  }

  async searchPost(text: string) {
    try {
      return firstValueFrom(
        this.client.send(RabbitEventEnum.ELASTIC_SEARCH_POST, { text }).pipe(
          timeout(5000),
          catchError((err) => {
            if (err instanceof TimeoutError) {
              return throwError(() => new RequestTimeoutException());
            }
            return throwError(err);
          }),
        ),
      );
    } catch (e) {
      Logger.error(e);
    }
  }

  async getPost(postRequest: RequestGetPostDto): Promise<ResponseGetPostDto> {
    try {
      const { id: _id } = postRequest;
      const { img, title, content, likes } = await this.postRepository.findById(_id);
      return { img, title, content, likes };
    } catch (e) {
      Logger.error(e);
    }
  }

  async updatePost(postRequest: RequestUpdatePostDto): Promise<void> {
    try {
      const { id: _id } = postRequest;
      await this.postRepository.findOneAndUpdate({ _id }, { ...postRequest });
    } catch (e) {
      Logger.error(e);
    }
  }

  async deletePost(postRequest: RequestDeletePostDto): Promise<void> {
    try {
      const { id: _id } = postRequest;
      await this.postRepository.deleteOne({ _id });
    } catch (e) {
      Logger.error(e);
    }
  }
}
