import { get } from 'env-var';

export class PostServiceConfig {
  public static readonly POST_SERVICE_CONTAINER_NAME: string = get('POST_SERVICE_CONTAINER_NAME')
    .required()
    .asString();

  public static readonly POST_SERVICE_QUEUE: string = get('POST_SERVICE_QUEUE')
    .required()
    .asString();

  public static readonly POST_SERVICE_BASE_PORT: number = get('POST_SERVICE_BASE_PORT')
    .required()
    .asPortNumber();
}
