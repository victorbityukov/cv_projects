import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import {
  RequestCreatePostDto,
  RequestDeletePostDto,
  RequestGetPostDto,
  RequestUpdatePostDto,
  ResponseCreatePostDto,
  ResponseGetPostDto,
} from '@app/domains';
import { PostService } from './post.service';

@Controller('post')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @Get('/')
  async all() {
    return this.postService.all();
  }

  @Post()
  async createPost(@Body() postRequest: RequestCreatePostDto): Promise<ResponseCreatePostDto> {
    const post = await this.postService.createPost(postRequest);
    if (post) {
      return post;
    } else {
      throw new BadRequestException();
    }
  }

  @Post('search')
  async searchPost(@Body('text') text: string) {
    return this.postService.searchPost(text);
  }

  @Get(':id')
  async getPost(@Param() postRequest: RequestGetPostDto): Promise<ResponseGetPostDto> {
    const post = await this.postService.getPost(postRequest);
    if (post) {
      return post;
    } else {
      throw new BadRequestException();
    }
  }

  @HttpCode(HttpStatus.OK)
  @Put()
  async updatePost(@Body() postRequest: RequestUpdatePostDto): Promise<void> {
    await this.postService.updatePost(postRequest);
  }

  @HttpCode(HttpStatus.OK)
  @Delete(':id')
  async deletePostEndpoint(@Param() postRequest: RequestDeletePostDto): Promise<void> {
    await this.postService.deletePost(postRequest);
  }
}
