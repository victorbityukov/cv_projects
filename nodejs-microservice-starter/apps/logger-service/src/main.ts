import { utilities as nestWinstonModuleUtilities, WinstonModule } from 'nest-winston';
import winston from 'winston';
import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { setupDotEnv } from '@app/common';
import { LoggerServiceConfig, RedisConfig } from '@app/config';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  setupDotEnv();
  const PORT = LoggerServiceConfig.LOGGER_SERVICE_BASE_PORT;
  const NAME_SERVICE = LoggerServiceConfig.LOGGER_SERVICE_CONTAINER_NAME;
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger({
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.ms(),
            nestWinstonModuleUtilities.format.nestLike(),
          ),
        }),
      ],
    }),
  });

  app.enableCors({
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    allowedHeaders: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Observe',
    credentials: true,
  });

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.REDIS,
    options: {
      url: RedisConfig.URL,
    },
  });

  await app.startAllMicroservices();
  Logger.log(
    `Bench microservice running LOGGER_SERVICE_QUEUE: ${LoggerServiceConfig.LOGGER_SERVICE_QUEUE}`,
  );

  await app.listen(PORT);
  Logger.log(`Api microservice running ${NAME_SERVICE}: ${PORT}`);
}

bootstrap();
