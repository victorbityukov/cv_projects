import { LogLevelEnum } from '@app/common';

export class EventLoggerDto {
  readonly service: string;
  readonly level: LogLevelEnum;
  readonly message: string;
}
