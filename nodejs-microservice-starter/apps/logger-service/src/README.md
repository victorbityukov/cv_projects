## Logger Service
В основе работы нашего логгера лежит библиотека `winston`, а также `nest-winston` для упрощения взаимодействия с nest

При генерации app в опциях мы указываем новый логгер чтобы при инициализации нашего сервиса логирования, он начинал логироваться через `winston`
```typescript
 const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger({
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.ms(),
            nestWinstonModuleUtilities.format.nestLike(),
          ),
        }),
      ],
    }),
  });
```

Для каждого сервиса ведется лог в папке logs следующей структуры `logs/<NameService>/<date>.log`

Пример использования лога можно найти [здесь](../../bench/src/README.md#example-of-use-mylogger)
