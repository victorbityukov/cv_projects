import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { TypeEventEnum } from '@app/common';
import { AppService } from './app.service';
import { EventLoggerDto } from './dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @MessagePattern(TypeEventEnum.LOG)
  handleEvent(@Payload() event: EventLoggerDto): void {
    return this.appService.eventHandler(event);
  }
}
