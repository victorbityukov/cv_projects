import { NestFactory } from '@nestjs/core';
import { setupDotEnv } from '@app/common';
import { InitModule } from './init.module';

async function bootstrap() {
  setupDotEnv();
  const app = await NestFactory.create(InitModule);
  await app.init();
  await app.close();
  process.exit();
}

export default bootstrap();
