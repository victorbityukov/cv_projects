import { Logger, Module, OnModuleInit } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostgresConfig } from '@app/config';
import { InitService } from './init.service';
import entities from './entities';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: PostgresConfig.POSTGRES_HOST,
      port: PostgresConfig.POSTGRES_PORT,
      username: PostgresConfig.POSTGRES_USER,
      password: PostgresConfig.POSTGRES_PASSWORD,
      database: PostgresConfig.POSTGRES_DB,
      logging: true,
      synchronize: true,
      keepConnectionAlive: true,
      entities,
    }),
  ],
  providers: [InitService],
})
export class InitModule implements OnModuleInit {
  private readonly logger = new Logger(InitModule.name, { timestamp: true });

  constructor(readonly initService: InitService) {}

  async onModuleInit(): Promise<void> {
    const firstStep = this.initService.firstStep();
    this.logger.log(firstStep);
    const twoStep = this.initService.twoStep();
    this.logger.log(twoStep);
  }
}
