import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { UserServiceConfig } from '@app/config';
import { setupDotEnv } from '@app/common';
import { UserModule } from './user.module';

async function bootstrap() {
  setupDotEnv();
  const app = await NestFactory.create(UserModule);

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.TCP,
    options: {
      host: '::',
      port: UserServiceConfig.USER_MICROSERVICE_PORT,
    },
  });

  await app.startAllMicroservices();
  Logger.log(
    `User microservice running USER_MICROSERVICE_PORT: ${UserServiceConfig.USER_MICROSERVICE_PORT}`,
  );

  await app.listen(UserServiceConfig.USER_SERVICE_BASE_PORT);
  Logger.log(
    `User service running USER_SERVICE_BASE_PORT: ${UserServiceConfig.USER_SERVICE_BASE_PORT}`,
  );
}

export default bootstrap();
