import { FindConditions, Repository } from 'typeorm';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  RequestCreateUserDto,
  RequestDeleteUserDto,
  RequestGetUserDto,
  RequestUpdateUserDto,
  ResponseCreateUserDto,
  ResponseGetUserDto,
  ResponseUpdateUserDto,
  UserEntity,
} from '@app/domains';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  findOne(query: FindConditions<UserEntity>): Promise<UserEntity> {
    return this.userRepository.findOne(query);
  }

  async getUser(userRequest: RequestGetUserDto): Promise<ResponseGetUserDto> {
    try {
      const { username } = userRequest;
      return await this.userRepository.findOne({ username });
    } catch (e) {
      Logger.error(e);
    }
  }

  async createUser(userRequest: RequestCreateUserDto): Promise<ResponseCreateUserDto> {
    try {
      const user = this.userRepository.create(userRequest);
      await this.userRepository.save(user);
      return {
        username: user.username,
        email: user.email,
      };
    } catch (e) {
      Logger.error(e);
    }
  }

  async updateUser(userRequest: RequestUpdateUserDto): Promise<ResponseUpdateUserDto> {
    try {
      const { username, email, password } = userRequest;
      const user = await this.userRepository.findOne({ username });
      if (!user) {
        return;
      }
      if (password) {
        user.password = password;
      }
      if (email) {
        user.email = email;
      }
      await this.userRepository.save(user);
      return {
        username,
        email: user.email,
      };
    } catch (e) {
      Logger.error(e);
    }
  }

  async deleteUser(userRequest: RequestDeleteUserDto): Promise<UserEntity> {
    try {
      const { username } = userRequest;
      const user = await this.userRepository.findOne({ username });
      if (user) {
        return this.userRepository.remove(user);
      }
      return;
    } catch (e) {
      Logger.error(e);
    }
  }

  async softDeleteUser(userRequest: RequestDeleteUserDto): Promise<UserEntity> {
    try {
      const { username } = userRequest;
      const user = await this.userRepository.findOne({ username });
      if (user) {
        return this.userRepository.softRemove(user);
      }
      return;
    } catch (e) {
      Logger.error(e);
    }
  }
}
