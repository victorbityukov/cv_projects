import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import {
  RequestCreateUserDto,
  RequestDeleteUserDto,
  RequestGetUserDto,
  ResponseGetUserDto,
  RequestUpdateUserDto,
  UserEntity,
  ResponseCreateUserDto,
  ResponseUpdateUserDto,
  ResponseDeleteUserDto,
} from '@app/domains';
import { UserService } from './user.service';
import { AuthGuard } from './guards';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @MessagePattern({ role: 'user', cmd: 'get' })
  getUser(data: any): Promise<UserEntity> {
    return this.userService.findOne({ username: data.username });
  }

  @UseGuards(AuthGuard)
  @Get('greet')
  async greet(): Promise<any> {
    return { text: 'hello' };
  }

  @Get(':username')
  async getUserEndpoint(@Param() userRequest: RequestGetUserDto): Promise<ResponseGetUserDto> {
    const user = await this.userService.getUser(userRequest);
    if (user) {
      return {
        username: user.username,
        email: user.email,
      };
    } else {
      throw new BadRequestException();
    }
  }

  @Post()
  @MessagePattern({ role: 'user', cmd: 'sign-up' })
  async createUserEndpoint(
    @Body() userRequest: RequestCreateUserDto,
  ): Promise<ResponseCreateUserDto> {
    const user = await this.userService.createUser(userRequest);
    if (user) {
      return {
        username: user.username,
        email: user.email,
      };
    } else {
      throw new BadRequestException();
    }
  }

  @Put()
  async updateUserEndpoint(
    @Body() userRequest: RequestUpdateUserDto,
  ): Promise<ResponseUpdateUserDto> {
    const user = await this.userService.updateUser(userRequest);
    if (user) {
      return {
        username: user.username,
        email: user.email,
      };
    } else {
      throw new BadRequestException();
    }
  }

  @Delete(':username')
  async deleteUserEndpoint(
    @Param() userRequest: RequestDeleteUserDto,
  ): Promise<ResponseDeleteUserDto> {
    const user = await this.userService.deleteUser(userRequest);
    if (user) {
      return {
        username: user.username,
        email: user.email,
      };
    } else {
      throw new BadRequestException();
    }
  }

  @Delete('soft/:username')
  async softDeleteUserEndpoint(
    @Param() userRequest: RequestDeleteUserDto,
  ): Promise<ResponseDeleteUserDto> {
    const user = await this.userService.softDeleteUser(userRequest);
    if (user) {
      return {
        username: user.username,
        email: user.email,
      };
    } else {
      throw new BadRequestException();
    }
  }
}
