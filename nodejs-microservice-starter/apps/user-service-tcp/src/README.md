## User Service TCP
 - [Auth TCP](../../auth-service-tcp/src/README.md#auth-service-tcp)
 - [CRUD for User (Postgres, TypeOrm)](#crud-for-user-postgres-typeorm)

### CRUD for User (Postgres, TypeOrm)
Примеры CRUD для Postgres реализованы посредством следующих эндпоинтов
 - getUserEndpoint
 - createUserEndpoint
 - updateUserEndpoint
 - deleteUserEndpoint
 - - softDeleteUserEndpoint
