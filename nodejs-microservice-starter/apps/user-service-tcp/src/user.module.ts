import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AuthServiceConfig, PostgresConfig } from '@app/config';
import { UserEntity } from '@app/domains';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: AuthServiceConfig.AUTH_SERVICE_CONTAINER_NAME,
        transport: Transport.TCP,
        options: {
          host: AuthServiceConfig.AUTH_SERVICE_CONTAINER_NAME,
          port: AuthServiceConfig.AUTH_MICROSERVICE_PORT,
        },
      },
    ]),
    TypeOrmModule.forFeature([UserEntity]),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: PostgresConfig.POSTGRES_HOST,
      port: PostgresConfig.POSTGRES_PORT,
      username: PostgresConfig.POSTGRES_USER,
      password: PostgresConfig.POSTGRES_PASSWORD,
      database: PostgresConfig.POSTGRES_DB,
      logging: true,
      synchronize: true,
      keepConnectionAlive: true,
      entities: [UserEntity],
    }),
  ],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
