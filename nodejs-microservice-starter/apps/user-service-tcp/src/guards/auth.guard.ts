import { firstValueFrom, timeout } from 'rxjs';
import { CanActivate, ExecutionContext, Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { AuthServiceConfig } from '@app/config';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    @Inject(AuthServiceConfig.AUTH_SERVICE_CONTAINER_NAME)
    private readonly client: ClientProxy,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();

    try {
      const response = await firstValueFrom(
        this.client
          .send(
            { role: 'auth', cmd: 'check' },
            { jwt: req.headers['authorization']?.split(' ')[1] },
          )
          .pipe(timeout(5000)),
      );

      req.user = response.user;
      return Boolean(response);
    } catch (err) {
      Logger.error(err);
      return false;
    }
  }
}
