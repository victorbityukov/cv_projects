import { Module } from '@nestjs/common';
import { Web3Module } from '@app/common/web3';
import { Web3ProviderEnum } from '@app/common';
import { ReferralProgramContract } from '@app/core';
import { LoggerModule } from '@app/common';
import { ContractListenerService } from './contract-listener.service';

@Module({
  imports: [Web3Module.withOptions({ provider: Web3ProviderEnum.WSS }), LoggerModule],
  providers: [ContractListenerService, ReferralProgramContract],
})
export class ContractListenerModule {}
