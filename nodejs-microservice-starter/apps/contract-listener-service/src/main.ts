import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Logger } from '@nestjs/common';
import { ContractListenerServiceConfig } from '@app/config';
import { setupDotEnv } from '@app/common';
import { MyLogger } from '@app/common';
import { ContractListenerModule } from './contract-listener.module';

async function bootstrap() {
  setupDotEnv();
  const PORT = ContractListenerServiceConfig.CONTRACT_LISTENER_SERVICE_BASE_PORT;
  const NAME_SERVICE = ContractListenerServiceConfig.CONTRACT_LISTENER_SERVICE_CONTAINER_NAME;

  const app = await NestFactory.create(ContractListenerModule, {
    bufferLogs: true,
    autoFlushLogs: true,
  });

  app.useLogger(app.get(MyLogger));

  app.setGlobalPrefix('api/v1');

  app.enableCors({
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    allowedHeaders: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Observe',
    credentials: true,
  });

  const options = new DocumentBuilder()
    .setTitle('Contact Listener Service')
    .setDescription('Provides REST API')
    .setVersion('1.0.0')
    .addBearerAuth()
    .build();

  await app.startAllMicroservices();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/v1/docs', app, document);

  await app.listen(PORT);
  Logger.log(`Api microservice running ${NAME_SERVICE}: ${PORT}`);
}
bootstrap();
