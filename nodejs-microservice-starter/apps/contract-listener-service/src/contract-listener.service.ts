import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { ReferralProgramContract } from '@app/core';
import { MyLogger } from '@app/common';
import { ResponseRegisterUserInterface } from './interfaces';

@Injectable()
export class ContractListenerService implements OnApplicationBootstrap {
  constructor(
    private readonly referralProgramContract: ReferralProgramContract,
    private readonly myLogger: MyLogger,
  ) {
    this.myLogger.setContext(ContractListenerService.name);
  }

  async onApplicationBootstrap(): Promise<void> {
    await this.referralProgramContract.init();
    await this.registerUser();
  }

  async registerUser(): Promise<void> {
    this.referralProgramContract
      .registerUser()
      .on('connected', (subscriptionId: string) => {
        this.myLogger.log(`| UserRegistered | events | ${subscriptionId}`);
      })
      .on(
        'data',
        async (event: { removed: boolean; returnValues: ResponseRegisterUserInterface }) => {
          try {
            if (event.removed) {
              return;
            }
            const { user, referrer } = event.returnValues;
            this.myLogger.log(user, referrer);
            // TODO: Обработка полученных данных
          } catch (e) {
            this.myLogger.log(`| ONCE | ${e}`);
          }
        },
      )
      .on('error', (error: NodeJS.ErrnoException) => {
        this.myLogger.error(error);
      });
  }
}
