import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ElasticsearchService } from './elasticsearch.service';
import { RabbitEventEnum } from './constants';
import { PostSearchBodyInterface } from './interfaces';

@Controller()
export class ElasticsearchController {
  constructor(private readonly elasticSearchService: ElasticsearchService) {}
  @MessagePattern(RabbitEventEnum.ELASTIC_INDEX_POST)
  async indexTest(@Payload() post: PostSearchBodyInterface): Promise<void> {
    await this.elasticSearchService.indexPost(post);
  }

  @MessagePattern(RabbitEventEnum.ELASTIC_SEARCH_POST)
  async searchTest(@Payload('text') text: string) {
    console.log('text', text);
    return await this.elasticSearchService.searchPost(text);
  }
}
