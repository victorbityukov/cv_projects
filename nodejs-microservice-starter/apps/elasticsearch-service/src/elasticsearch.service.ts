import { Injectable } from '@nestjs/common';
import { ElasticsearchService as ElasticService } from '@nestjs/elasticsearch';
import { MyLogger } from '@app/common';
import { PostSearchBodyInterface, PostSearchResultInterface } from './interfaces';

@Injectable()
export class ElasticsearchService {
  private readonly index = 'posts';
  constructor(
    private readonly myLogger: MyLogger,
    private readonly elasticsearchService: ElasticService,
  ) {
    this.myLogger.setContext(ElasticsearchService.name);
  }

  async indexPost(post: PostSearchBodyInterface) {
    const resIndex = await this.elasticsearchService.index<
      PostSearchResultInterface,
      PostSearchBodyInterface
    >({
      index: this.index,
      body: {
        id: post.id,
        title: post.title,
        content: post.content,
      },
    });
    console.log('resIndex', resIndex);
  }

  async searchPost(text: string) {
    const { body } = await this.elasticsearchService.search<PostSearchResultInterface>({
      index: this.index,
      body: {
        query: {
          multi_match: {
            query: text,
            fields: ['title', 'content'],
          },
        },
      },
    });
    const hits = body.hits.hits;
    return hits.map((item) => item._source);
  }
}
