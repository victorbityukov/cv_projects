import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { MyLogger, setupDotEnv } from '@app/common';
import { ElasticsearchServiceConfig, RabbitmqConfig } from '@app/config';
import { ElasticsearchServiceModule } from './elasticsearch.module';

async function bootstrap() {
  setupDotEnv();
  const PORT = ElasticsearchServiceConfig.ELASTIC_SERVICE_BASE_PORT;
  const NAME_SERVICE = ElasticsearchServiceConfig.ELASTIC_SERVICE_CONTAINER_NAME;

  const app = await NestFactory.create(ElasticsearchServiceModule, {
    bufferLogs: true,
    autoFlushLogs: true,
  });

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.RMQ,
    options: {
      urls: [RabbitmqConfig.CONNECTION_STRING],
      queue: ElasticsearchServiceConfig.ELASTIC_SERVICE_QUEUE,
      queueOptions: {
        durable: false,
        exclusive: false,
      },
    },
  });

  await app.startAllMicroservices();
  Logger.log(`ELASTIC_SERVICE_QUEUE: ${ElasticsearchServiceConfig.ELASTIC_SERVICE_QUEUE}`);

  app.useLogger(app.get(MyLogger));
  await app.listen(PORT);
  Logger.log(`Api microservice running ${NAME_SERVICE}: ${PORT}`);
}
bootstrap();
