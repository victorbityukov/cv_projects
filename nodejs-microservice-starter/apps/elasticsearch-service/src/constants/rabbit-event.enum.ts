export enum RabbitEventEnum {
  ELASTIC_INDEX_POST = 'elastic_index_post',
  ELASTIC_SEARCH_POST = 'elastic_search_post',
}
