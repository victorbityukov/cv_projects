import { Module } from '@nestjs/common';
import { ElasticsearchModule } from '@nestjs/elasticsearch';
import { LoggerModule } from '@app/common';
import { ElasticsearchConfig } from '@app/config';
import { ElasticsearchController } from './elasticsearch.controller';
import { ElasticsearchService } from './elasticsearch.service';

@Module({
  imports: [
    ElasticsearchModule.register({
      node: ElasticsearchConfig.CONNECTION_STRING,
    }),
    LoggerModule,
  ],
  controllers: [ElasticsearchController],
  providers: [ElasticsearchService],
})
export class ElasticsearchServiceModule {}
