export interface PostSearchBodyInterface {
  id: string;
  title: string;
  content: string;
}
