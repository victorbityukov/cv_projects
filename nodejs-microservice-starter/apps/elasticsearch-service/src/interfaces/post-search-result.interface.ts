import { PostSearchBodyInterface } from './post-search-body.interface';

export interface PostSearchResultInterface {
  hits: {
    total: number;
    hits: Array<{
      _source: PostSearchBodyInterface;
    }>;
  };
}
