import { get } from 'env-var';

export class TelegramBotConfig {
  public static readonly TELEGRAM_BOT_CONTAINER_NAME: string = get('TELEGRAM_BOT_CONTAINER_NAME')
    .required()
    .asString();

  public static readonly TELEGRAM_BOT_BASE_PORT: number = get('TELEGRAM_BOT_BASE_PORT')
    .required()
    .asPortNumber();
}
