import { get } from 'env-var';

export class BenchServiceConfig {
  public static readonly BENCH_SERVICE_CONTAINER_NAME: string = get('BENCH_SERVICE_CONTAINER_NAME')
    .required()
    .asString();

  public static readonly BENCH_SERVICE_QUEUE: string = get('BENCH_SERVICE_QUEUE')
    .required()
    .asString();

  public static readonly BENCH_SERVICE_BASE_PORT: number = get('BENCH_SERVICE_BASE_PORT')
    .required()
    .asPortNumber();
}
