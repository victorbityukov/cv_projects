import { get } from 'env-var';

export class ApiServiceConfig {
  public static readonly API_SERVICE_CONTAINER_NAME: string = get('API_SERVICE_CONTAINER_NAME')
    .required()
    .asString();

  public static readonly API_SERVICE_BASE_PORT: number = get('API_SERVICE_BASE_PORT')
    .required()
    .asPortNumber();
}
