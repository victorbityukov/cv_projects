import { get } from 'env-var';

export class AuthServiceConfig {
  public static readonly AUTH_SERVICE_CONTAINER_NAME: string = get('AUTH_SERVICE_CONTAINER_NAME')
    .required()
    .asString();

  public static readonly AUTH_SERVICE_BASE_PORT: number = get('AUTH_SERVICE_BASE_PORT')
    .required()
    .asPortNumber();

  public static readonly AUTH_MICROSERVICE_PORT: number = get('AUTH_MICROSERVICE_PORT')
    .required()
    .asPortNumber();
}
