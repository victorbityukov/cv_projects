import { get } from 'env-var';

export class ContractManagerServiceConfig {
  public static readonly CONTRACT_MANAGER_SERVICE_CONTAINER_NAME: string = get(
    'CONTRACT_MANAGER_SERVICE_CONTAINER_NAME',
  )
    .required()
    .asString();

  public static readonly CONTRACT_MANAGER_SERVICE_BASE_PORT: number = get(
    'CONTRACT_MANAGER_SERVICE_BASE_PORT',
  )
    .required()
    .asPortNumber();
}
