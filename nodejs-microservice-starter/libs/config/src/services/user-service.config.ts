import { get } from 'env-var';

export class UserServiceConfig {
  public static readonly USER_SERVICE_CONTAINER_NAME: string = get('USER_SERVICE_CONTAINER_NAME')
    .required()
    .asString();

  public static readonly USER_SERVICE_BASE_PORT: number = get('USER_SERVICE_BASE_PORT')
    .required()
    .asPortNumber();

  public static readonly USER_MICROSERVICE_PORT: number = get('USER_MICROSERVICE_PORT')
    .required()
    .asPortNumber();
}
