import { get } from 'env-var';

export class ContractListenerServiceConfig {
  public static readonly CONTRACT_LISTENER_SERVICE_CONTAINER_NAME: string = get(
    'CONTRACT_LISTENER_SERVICE_CONTAINER_NAME',
  )
    .required()
    .asString();

  public static readonly CONTRACT_LISTENER_SERVICE_BASE_PORT: number = get(
    'CONTRACT_LISTENER_SERVICE_BASE_PORT',
  )
    .required()
    .asPortNumber();
}
