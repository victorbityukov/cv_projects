import { get } from 'env-var';

export class ElasticsearchServiceConfig {
  public static readonly ELASTIC_SERVICE_CONTAINER_NAME: string = get(
    'ELASTIC_SERVICE_CONTAINER_NAME',
  )
    .required()
    .asString();

  public static readonly ELASTIC_SERVICE_QUEUE: string = get('ELASTIC_SERVICE_QUEUE')
    .required()
    .asString();

  public static readonly ELASTIC_SERVICE_BASE_PORT: number = get('ELASTIC_SERVICE_BASE_PORT')
    .required()
    .asPortNumber();
}
