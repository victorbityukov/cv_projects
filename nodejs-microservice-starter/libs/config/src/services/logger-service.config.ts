import { get } from 'env-var';

export class LoggerServiceConfig {
  public static readonly LOGGER_SERVICE_CONTAINER_NAME: string = get(
    'LOGGER_SERVICE_CONTAINER_NAME',
  )
    .required()
    .asString();

  public static readonly LOGGER_SERVICE_QUEUE: string = get('LOGGER_SERVICE_QUEUE')
    .required()
    .asString();

  public static readonly LOGGER_SERVICE_BASE_PORT: number = get('LOGGER_SERVICE_BASE_PORT')
    .required()
    .asPortNumber();
}
