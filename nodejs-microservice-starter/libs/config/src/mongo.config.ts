import { get } from 'env-var';

export class MongoConfig {
  public static readonly MONGO_HOST: string = get('MONGO_HOST').required().asString();

  public static readonly MONGO_PORT: number = get('MONGO_PORT').required().asPortNumber();

  public static readonly MONGO_USER: string = get('MONGO_USER').required().asString();

  public static readonly MONGO_PASSWORD: string = get('MONGO_PASSWORD').required().asString();

  // eslint-disable-next-line max-len
  public static readonly CONNECTION_STRING: string = `mongodb://${MongoConfig.MONGO_USER}:${MongoConfig.MONGO_PASSWORD}@${MongoConfig.MONGO_HOST}:${MongoConfig.MONGO_PORT}`;
}
