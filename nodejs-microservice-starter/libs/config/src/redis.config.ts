import { get } from 'env-var';

export class RedisConfig {
  public static readonly REDIS_HOST: string = get('REDIS_HOST').required().asString();

  public static readonly REDIS_PORT: number = get('REDIS_PORT').required().asPortNumber();

  // eslint-disable-next-line max-len
  public static readonly URL: string = `redis://${RedisConfig.REDIS_HOST}:${RedisConfig.REDIS_PORT}`;
}
