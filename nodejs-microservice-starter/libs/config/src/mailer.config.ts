import config from 'config';
import { MailerConfigInterface } from './interfaces';

const MAILER_CONFIG: MailerConfigInterface = config.get<MailerConfigInterface>('mailer');

export const mailerConfiguration: MailerConfigInterface = {
  host: MAILER_CONFIG.host,
  port: MAILER_CONFIG.port,
  auth: {
    user: MAILER_CONFIG.auth.user,
    pass: MAILER_CONFIG.auth.pass,
  },
  rejectUnauthorized: MAILER_CONFIG.rejectUnauthorized,
};
