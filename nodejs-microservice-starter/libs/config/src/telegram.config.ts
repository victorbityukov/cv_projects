import config from 'config';
import { TelegramConfigInterface } from './interfaces';

const TELEGRAM_CONFIG = config.get<TelegramConfigInterface>('telegram');

export const telegramConfiguration = {
  token: TELEGRAM_CONFIG.token,
  stageTtl: TELEGRAM_CONFIG.stageTtl,
  redisSessionTtl: TELEGRAM_CONFIG.redisSessionTtl,
};
