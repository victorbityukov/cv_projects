import { get } from 'env-var';

export class RabbitmqConfig {
  public static readonly RABBIT_MQ_CONTAINER_NAME: string = get('RABBIT_MQ_CONTAINER_NAME')
    .required()
    .asString();

  public static readonly RABBIT_MQ_HOST: string = get('RABBIT_MQ_HOST')
    .required()
    .asString();

  public static readonly RABBIT_MQ_PORT: number = get('RABBIT_MQ_PORT').required().asPortNumber();

  public static readonly RABBIT_MQ_ADMIN_PORT: number = get('RABBIT_MQ_ADMIN_PORT')
    .required()
    .asPortNumber();

  // eslint-disable-next-line max-len
  public static readonly CONNECTION_STRING: string = `amqp://${RabbitmqConfig.RABBIT_MQ_HOST}:${RabbitmqConfig.RABBIT_MQ_PORT}`;
}
