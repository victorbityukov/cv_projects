import config from 'config';
import { Web3ConfigInterface } from './interfaces';

export const web3Config = config.get<Web3ConfigInterface>('web3');
