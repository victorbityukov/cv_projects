export interface MailerConfigInterface {
  host: string;
  port: number;
  auth: {
    user: string;
    pass: string;
  };
  rejectUnauthorized: boolean;
}
