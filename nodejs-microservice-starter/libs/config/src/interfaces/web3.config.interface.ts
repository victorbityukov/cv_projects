export interface Web3ConfigInterface {
  readonly privateKey: string;
  readonly abiDir: string;
  readonly contracts: { [name: string]: string };
  readonly providers: {
    wss?: string;
    ipc?: string;
    http?: string;
  };
}
