export interface TelegramConfigInterface {
  readonly token: string;
  readonly stageTtl: number;
  readonly redisSessionTtl: number;
}
