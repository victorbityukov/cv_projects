export interface DiscordConfigInterface {
  readonly clientID: string;
  readonly clientSecret: string;
  readonly callbackURL: string;
  readonly tokenURL: string;
}
