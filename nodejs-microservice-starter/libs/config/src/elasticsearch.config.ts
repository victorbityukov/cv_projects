import { get } from 'env-var';

export class ElasticsearchConfig {
  public static readonly ELASTIC_HOST: string = get('ELASTIC_HOST').required().asString();

  public static readonly ELASTIC_PORT: number = get('ELASTIC_PORT').required().asPortNumber();

  // eslint-disable-next-line max-len
  public static readonly CONNECTION_STRING: string = `http://${ElasticsearchConfig.ELASTIC_HOST}:${ElasticsearchConfig.ELASTIC_PORT}`;
}
