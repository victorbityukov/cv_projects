import EventEmitter from 'events';
import { Injectable } from '@nestjs/common';
import { Web3Contract, Web3TransactionInterface } from '@app/common/web3';
import { BaseContractInterface, Web3Service } from '@app/common';
import { web3Config } from '@app/config';

interface ReferralProgramMethods {
  registerUser: (addressParent: string) => Web3TransactionInterface;
}

interface ReferralProgramEvents {
  RegisterUser: () => EventEmitter;
}

@Injectable()
export class ReferralProgramContract implements BaseContractInterface<ReferralProgramMethods> {
  contract: Web3Contract<ReferralProgramMethods, ReferralProgramEvents>;

  private readonly nameContract = 'referralProgram';
  private readonly addressContract: string;

  constructor(readonly web3Service: Web3Service) {
    const { contracts } = web3Config;
    this.addressContract = contracts[this.nameContract];
  }

  async init(): Promise<void> {
    const abi = await this.web3Service.getAbiData(this.nameContract);
    this.contract = await this.web3Service.getContractInstance(this.addressContract, abi);
  }

  /**
   * @return EventEmitter
   */
  registerUser(): EventEmitter {
    return this.contract.events.RegisterUser();
  }
}
