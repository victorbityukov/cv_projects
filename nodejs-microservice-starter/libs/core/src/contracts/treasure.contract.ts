import { Injectable } from '@nestjs/common';
import {
  ContractBaseMethod,
  Web3Contract,
  Web3TransactionInterface,
} from '@app/common/web3/interfaces';
import { Web3Service } from '@app/common/web3';
import { web3Config } from '@app/config';
import { BaseContractInterface } from '@app/common';

interface TreasureMethods {
  toVoters: () => Web3TransactionInterface;
  owner: () => ContractBaseMethod<string>;
}

@Injectable()
export class TreasureContract implements BaseContractInterface<TreasureMethods> {
  contract: Web3Contract<TreasureMethods>;

  private readonly nameContract = 'treasury';
  private readonly addressContract: string;

  constructor(readonly web3Service: Web3Service) {
    const { contracts } = web3Config;
    this.addressContract = contracts[this.nameContract];
  }

  async init(): Promise<void> {
    const abi = await this.web3Service.getAbiData(this.nameContract);
    this.contract = await this.web3Service.getContractInstance(this.addressContract, abi);
  }

  async toVoters(): Promise<void> {
    const transaction = this.contract.methods.toVoters();
    await this.web3Service.sendTransactions(transaction, this.addressContract);
  }

  async getOwner(): Promise<string> {
    return this.contract.methods.owner().call();
  }
}
