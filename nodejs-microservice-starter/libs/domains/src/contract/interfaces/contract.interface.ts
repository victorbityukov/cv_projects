export interface ContractInterface {
  id: number;
  address: string;
  name: string;
  abi: string;
  createdAt: Date;
}
