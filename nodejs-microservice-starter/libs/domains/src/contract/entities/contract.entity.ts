import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { ContractInterface } from '@app/domains';

@Entity()
@Unique(['address'])
export class ContractEntity implements ContractInterface {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  address: string;

  @Column()
  name: string;

  @Column({
    default: null,
  })
  abi: string;

  @CreateDateColumn()
  createdAt: Date;
}
