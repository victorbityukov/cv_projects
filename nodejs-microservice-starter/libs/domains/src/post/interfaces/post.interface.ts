export interface PostInterface {
  title: string;
  img: string;
  content: string;
  likes: number;
}
