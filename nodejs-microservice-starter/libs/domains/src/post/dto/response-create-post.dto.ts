import { IsNumber, IsString } from 'class-validator';

export class ResponseCreatePostDto {
  @IsString()
  readonly img: string;

  @IsNumber()
  readonly likes: number;

  @IsString()
  readonly title: string;

  @IsString()
  readonly content: string;
}
