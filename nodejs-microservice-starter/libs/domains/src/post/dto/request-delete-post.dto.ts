import { IsNotEmpty, IsString } from 'class-validator';

export class RequestDeletePostDto {
  @IsNotEmpty()
  @IsString()
  readonly id: string;
}
