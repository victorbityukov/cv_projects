import { IsNotEmpty, IsString } from 'class-validator';

export class RequestGetPostDto {
  @IsNotEmpty()
  @IsString()
  readonly id: string;
}
