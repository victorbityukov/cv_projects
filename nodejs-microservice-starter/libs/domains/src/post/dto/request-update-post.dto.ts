import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class RequestUpdatePostDto {
  @IsNotEmpty()
  @IsString()
  id: string;

  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  content: string;

  @IsOptional()
  @IsString()
  img: string;

  @IsOptional()
  @Transform(({ value }) => {
    const likes = Number(value);
    if (!likes) {
      return 0;
    }
    return likes;
  })
  likes: number;
}
