import { IsOptional, IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class RequestCreatePostDto {
  @IsString()
  readonly img: string;

  @IsOptional()
  @Transform(({ value }) => {
    const likes = Number(value);
    if (!likes) {
      return 0;
    }
    return likes;
  })
  readonly likes: number;

  @IsString()
  readonly title: string;

  @IsString()
  readonly content: string;
}
