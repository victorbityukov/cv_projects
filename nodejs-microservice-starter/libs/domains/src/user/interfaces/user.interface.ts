export interface UserInterface {
  id: number;
  username: string;
  password: string;
  email: string;
  role: string;
  createdAt: Date;
  deletedAt: Date;
}
