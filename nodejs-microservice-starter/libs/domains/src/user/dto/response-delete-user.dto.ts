import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class ResponseDeleteUserDto {
  @IsNotEmpty()
  @IsString()
  readonly username: string;

  @IsNotEmpty()
  @IsEmail()
  readonly email: string;
}
