import { IsNotEmpty, IsString } from 'class-validator';

export class RequestGetUserDto {
  @IsNotEmpty()
  @IsString()
  readonly username: string;
}
