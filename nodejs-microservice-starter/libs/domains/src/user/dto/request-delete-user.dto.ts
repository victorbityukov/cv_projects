import { IsNotEmpty, IsString } from 'class-validator';

export class RequestDeleteUserDto {
  @IsNotEmpty()
  @IsString()
  readonly username: string;
}
