import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { UserInterface } from '@app/domains';

export class RequestCreateUserDto
  implements Pick<UserInterface, 'username' | 'password' | 'email'>
{
  @IsNotEmpty()
  @IsString()
  readonly username: string;

  @IsNotEmpty()
  @IsString()
  readonly password: string;

  @IsNotEmpty()
  @IsEmail()
  readonly email: string;
}
