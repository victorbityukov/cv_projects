import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class ResponseGetUserDto {
  @IsNotEmpty()
  @IsString()
  readonly username: string;

  @IsNotEmpty()
  @IsEmail()
  readonly email: string;
}
