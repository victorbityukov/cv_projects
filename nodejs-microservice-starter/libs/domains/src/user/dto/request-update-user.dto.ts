import { IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { UserInterface } from '@app/domains';

export class RequestUpdateUserDto
  implements Pick<UserInterface, 'username' | 'password' | 'email'>
{
  @IsNotEmpty()
  @IsString()
  username: string;

  @IsOptional()
  @IsString()
  password: string;

  @IsOptional()
  @IsEmail()
  email: string;
}
