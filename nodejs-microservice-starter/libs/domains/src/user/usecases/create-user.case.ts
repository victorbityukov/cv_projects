import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RequestCreateUserDto, UserEntity } from '@app/domains';

@Injectable()
export class CreateUserCase {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async execute(payload: RequestCreateUserDto): Promise<UserEntity> {
    // const doesUserExist = !!(await this.userRepository.count({ email: payload.email }));

    return this.userRepository.create(payload);
  }
}
