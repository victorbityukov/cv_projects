import nodemailer from 'nodemailer';
import { Injectable, Logger } from '@nestjs/common';
import { mailerConfiguration } from '@app/config';

@Injectable()
export class MailerService {
  private readonly transporter: nodemailer.Transporter;

  constructor() {
    this.transporter = nodemailer.createTransport(mailerConfiguration);
  }

  public async sendEmail(toAddress: string, subject: string, message: string): Promise<void> {
    try {
      const { auth } = mailerConfiguration;

      await this.transporter.sendMail({
        from: auth.user,
        to: toAddress,
        subject,
        html: message,
      });
    } catch (e) {
      Logger.error(e);
    }
  }
}
