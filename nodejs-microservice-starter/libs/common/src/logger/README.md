## Logger
В некотором роде это посредник между любым потенциальным микросервисом, где будет использован логгер, и миксервисом для логирования

Сервис экстендит `ConsoleLogger`

Используется очередь REDIS чтобы эмитить события, которые в дальнейшем будет перехватывать микросервис my logger
```typescript
ClientsModule.register([
  {
    name: LoggerServiceConfig.LOGGER_SERVICE_QUEUE,
    transport: Transport.REDIS,
    options: {
      url: RedisConfig.URL,
      retryAttempts: 3,
    },
  },
]),
```

Примеры описания микросервиса MyLogger и пример использования по ссылкам ниже

 - [Logger Service](../../../../apps/logger-service/src/README.md)
 - [Example of Use Logger Service](../../../../apps/bench/src/README.md#example-of-use-mylogger)
