import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { LoggerServiceConfig, RedisConfig } from '@app/config';
import { MyLogger } from './my-logger.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: LoggerServiceConfig.LOGGER_SERVICE_QUEUE,
        transport: Transport.REDIS,
        options: {
          url: RedisConfig.URL,
          retryAttempts: 3,
        },
      },
    ]),
  ],
  providers: [MyLogger],
  exports: [MyLogger],
})
export class LoggerModule {}
