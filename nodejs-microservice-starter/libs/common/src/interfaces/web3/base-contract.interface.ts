import { Web3Contract } from '@app/common/web3/interfaces';
import { Web3Service } from '@app/common/web3';

export interface BaseContractInterface<T> {
  readonly contract: Web3Contract<T>;
  readonly web3Service: Web3Service;
  readonly init: () => void;
}
