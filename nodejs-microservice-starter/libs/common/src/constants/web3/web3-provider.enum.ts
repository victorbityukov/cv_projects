export enum Web3ProviderEnum {
  HTTP = 'http',
  WSS = 'wss',
  IPC = 'ipc',
}
