export function camelCaseToDash(str: string) {
  return str
    .split('')
    .map((letter, i, str) => {
      if (letter === letter.toUpperCase()) {
        // First letter
        if (i === 0) {
          return letter.toLowerCase();
        }
        // Last letter
        if (i === str.length - 1) {
          if (str[i - 1] === str[i - 1].toUpperCase()) {
            return letter.toLowerCase();
          }
          return `-${letter.toLowerCase()}`;
        }

        if (str[i + 1] === str[i + 1].toLowerCase()) {
          return `-${letter.toLowerCase()}`;
        } else {
          if (str[i - 1] === str[i - 1].toUpperCase()) {
            return letter.toLowerCase();
          }
        }
        return `-${letter.toLowerCase()}`;
      } else {
        return letter;
      }
    })
    .join('');
}
