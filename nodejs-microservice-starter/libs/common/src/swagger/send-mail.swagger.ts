import { ApiPropertyOptions } from '@nestjs/swagger';

export const SWAGGER_SEND_MAIL_TO_ADDRESS: ApiPropertyOptions = {
  name: 'toAddress',
  description: 'Address of the recipient ',
  type: String,
  required: true,
  example: 'test@test.com',
};

export const SWAGGER_SEND_MAIL_SUBJECT: ApiPropertyOptions = {
  name: 'subject',
  description: 'Subject of message ',
  type: String,
  required: true,
  example: 'Proposal of collaboration',
};

export const SWAGGER_SEND_MAIL_MESSAGE: ApiPropertyOptions = {
  name: 'message',
  description: 'Content of message ',
  type: String,
  required: true,
  example: 'I want to cooperate with you',
};
