import { HttpProviderBase, IpcProviderBase, WebsocketProviderBase } from 'web3-core-helpers';

export interface Web3OptionsInterface {
  readonly web3Provider: IpcProviderBase | HttpProviderBase | WebsocketProviderBase;
  readonly privateKey?: string;
}
