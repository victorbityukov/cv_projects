import Web3 from 'web3';
import { Contract } from 'web3-eth-contract';
import { AbiItem } from 'web3-utils';
import path from 'path';
import { readFile } from 'fs/promises';
import { Inject, Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { Web3OptionsInterface, Web3TransactionInterface } from '@app/common/web3/interfaces';
import { web3Config } from '@app/config';
import { camelCaseToDash, WEB3_OPTIONS } from '@app/common';

@Injectable()
export class Web3Service {
  private readonly web3: Web3;

  private readonly privateKey: string;

  constructor(@Inject(WEB3_OPTIONS) private options: Web3OptionsInterface) {
    const { privateKey, web3Provider } = options;
    this.privateKey = privateKey;
    this.web3 = new Web3(web3Provider);
  }

  async getContractInstance(address: string, abi: AbiItem[] | AbiItem): Promise<Contract> {
    return new this.web3.eth.Contract(abi, address);
  }

  private getAccount() {
    return this.web3.eth.accounts.privateKeyToAccount(this.privateKey);
  }

  public async sendTransactions(transaction: Web3TransactionInterface, to: string): Promise<void> {
    try {
      if (!this.privateKey) throw new UnauthorizedException('Private Key is empty');
      const account = this.getAccount();
      const options = {
        to,
        data: transaction.encodeABI(),
        gas: await transaction.estimateGas({ from: account.address }),
        gasPrice: await this.web3.eth.getGasPrice(),
      };
      const signed = await this.web3.eth.accounts.signTransaction(options, this.privateKey);

      if (signed?.rawTransaction) {
        Logger.log('sendSignedTransaction');
        await this.web3.eth.sendSignedTransaction(signed.rawTransaction);
      }
    } catch (err) {
      Logger.error(err);
    }
  }

  public async getAbiData(contractName: string): Promise<AbiItem | AbiItem[]> {
    const { abiDir } = web3Config;
    const abiFileName = `${camelCaseToDash(contractName)}.abi.json`;
    const pathFile = path.join(path.resolve('./'), abiDir, abiFileName);
    const fileData = await readFile(pathFile, 'utf-8');
    return JSON.parse(fileData);
  }
}
