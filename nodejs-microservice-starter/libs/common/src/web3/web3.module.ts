import Web3 from 'web3';
import net from 'net';
import { HttpProviderBase, IpcProviderBase, WebsocketProviderBase } from 'web3-core-helpers';
import { DynamicModule, Module } from '@nestjs/common';
import { WEB3_OPTIONS, Web3ProviderEnum } from '@app/common';
import { web3Config } from '@app/config';
import { Web3Service } from './web3.service';

@Module({
  providers: [Web3Service],
  exports: [Web3Service],
})
export class Web3Module {
  static withOptions(options: { provider: Web3ProviderEnum }): DynamicModule {
    const { provider } = options;
    const { privateKey, providers } = web3Config;
    const { wss, http, ipc } = providers;
    let web3Provider: IpcProviderBase | HttpProviderBase | WebsocketProviderBase | string;
    switch (provider) {
      case Web3ProviderEnum.WSS:
        web3Provider = new Web3.providers.WebsocketProvider(wss);
        break;
      case Web3ProviderEnum.HTTP:
        web3Provider = new Web3.providers.HttpProvider(http);
        break;
      case Web3ProviderEnum.IPC:
        web3Provider = new Web3.providers.IpcProvider(ipc, net);
        break;
    }
    return {
      module: Web3Module,
      providers: [
        {
          provide: WEB3_OPTIONS,
          useValue: {
            web3Provider,
            privateKey,
          },
        },
        Web3Service,
      ],
      exports: [Web3Service],
    };
  }
}
