export const testMiddleware = (req, res, next) => {
  res.header('x-test', 'X TEST TEXT');
  next();
};
