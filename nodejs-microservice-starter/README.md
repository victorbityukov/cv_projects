<div align="center" >
<p style="width: 100px; background-color: #343434; padding: 20px">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>
</div>
<div align="center" >
<p style="width: 100px; background-color: #03eba5; padding: 20px">
  <a href="https://sfxdx.com/" target="blank"><img src="https://sfxdx.com/static/media/logo.1853e57f.svg" width="320" alt="SFXDX Logo" /></a>
</p>
</div>
<div align="center">

[NestJS](https://github.com/nestjs/nest) Microservices Boilerplate made with ❤️ by [✨️SFXDX✨️](https://www.sfxdx.com).

</div>

---

### Description

This will be the basis for the next project on NestJs. This place will contain the best practices and code variants with
a description of their application. For a better understanding, read the instructions and start implementing another
successful project!

---

### Features

- [x] ✅ [Environment Configuration](#environment-configuration)
- [x] ✅ [Configuration docker-compose](#docker-compose)
- [x] ✅ [Swagger](#swagger)
- [x] ✅ [CRUD for User (Postgres, TypeOrm)](./apps/user-service-tcp/src/README.md#crud-for-user-postgres-typeorm)
- [x] ✅ [CRUD for Post (MongoDB, mongoose)](./apps/post-service/src/README.md#crud-for-post-mongodb-mongoose)
- [x] ✅ [TCP Microservice example](./apps/auth-service-tcp/src/README.md#auth-tcp)
- [x] ✅ [RabbitMQ Microservice example](./apps/bench/src/README.md#rabbitmq-microservice-example)
- [x] ✅ [Authentication (JWT) using Microservices](./apps/auth-service-tcp/src/README.md#authentication-jwt)
- [x] ✅ [Init Service](./apps/init-service/src/README.md#init-service)
- [x] ✅ [TypeOrm Example (Domain)](./libs/domains/src/user/README.md#typeorm-example-domain)
- [x] ✅ [Authentication (OAuth)](./apps/auth-service-tcp/src/README.md#authentication-oauth)
- [x] ✅ [Redis Microservice example](./apps/bench/src/README.md#redis-microservice-example)
- [x] ✅ [File Upload](./apps/bench/src/README.md#file-upload)
- [x] ✅ [Logger](./libs/common/src/logger/README.md#logger)
- [x] ✅ [Mail](./libs/common/src/mailer/README.md#mailer)
- [x] ✅ [Role Based Access Control](./apps/bench/src/README.md#role-based-access-control)
- [ ] ❌ Error
- [ ] ❌ Database Migrations
- [ ] ❌ Database Seeding
- [x] ✅ [Testing (e2e, unit)](./apps/bench/src/README.md#testing)
- [x] ✅ [Contract Manager](./apps/contract-manager-service/src/README.md#contract-manager)
- [x] ✅ [Contract Listener](./apps/contract-listener-service/src/README.md#contract-listener)
- [x] ✅ [Web3 Service](./libs/common/src/web3/README.md#web3-service)
- [x] ✅ [Example Contract](./libs/core/src/contracts/README.md#example-contract)
- [x] ✅ [Bench with examples](./apps/bench/src/README.md#bench-with-examples)
- [x] ✅ [Telegram Bot](./apps/telegram-bot/src/README.md#telegram-bot)
- [x] ✅ [Elasticsearch Example](./apps/elasticsearch-service/src/README.md#elasticsearch)
---

### Guide

#### Installation

```bash  
$ yarn install
```

#### Environment configuration

There are several options to configure the application. Using `.env` or using the `config` folder.

##### - using `.env`

For this approach, the following packages must be installed:

```bash
$ yarn add dotenv env-var
```

Everything related to the configuration of any system modules should be in the file `.env`. As an example, there is a
file `.env.example`. Just copy it.
> **Note:** If possible, try to give names to the environment variables that accurately determine their purpose.
>
This file `.env` should be used in `docker-compose` where it will pass environment variables to each container.

#### - using the `config` folder

You need to install the following packages:

```bash
$ yarn add config
```

Config reads configuration files in the `./config` directory for the running process, typically the application root.
This can be overridden by setting the $NODE_CONFIG_DIR environment variable to the directory containing your
configuration files. It can also be set from node, before loading Node-config:

If `NODE_ENV` is not set in the environment, a default value of `development` is used.

The `default.yml` [json, ...] file is designed to contain all configuration parameters from which other files may
overwrite. Overwriting is done on a parameter by parameter basis, so subsequent files contain only the parameters unique
for that override.

### Launch

You can run this when you are developing through **docker** or through the **console**.
### Docker Compose
#### - using `docker-compose`

```bash
$ docker-compose up -f docker-compose.example.yml --build --remove-orphans -d
```

- `--build` - build images before starting containers;
- `--remove-orphans` - remove containers for services not defined in the Compose file;
- `-d, --detach` - run containers in the background.

#### - using `console`

```bash
# 'NODE_ENV` must be defined for each mode
# `app-service` - this is the name of your app
# for the `development` mode
$ yarn run start:dev app-service

# for the `development` mode with a debug
$ yarn run start:debug app-service

# for the `production` mode, 2 steps
$ yarn run build app-service
$ node dist/main/app-service/main.js
```

### Swagger

[Swagger](https://swagger.io/) is used to generate API documentation.

Packages required:

```bash
$ yarn add @nestjs/swagger swagger-ui-express
```

Let's agree that we will use the `libs/*/swagger` directory for all **swagger** sets.

There is a `swagger.setup.ts` in the `common` directory with the `setupSwagger()` method, most likely you will use it in
every new one `main.js`.


### Folders

- `/apps` - this is microservices directory
- `/config` - to configure when using the configuration package
- `/docs` - for project documentation
- `/libs/common` - common modules in the project [(Назначения)](./libs/common/README.md)
- `/libs/core` - this is the main module of the server part, which houses most of the code base that combines everything together
- `/libs/config` - when using the configuration via `.env`
- `/libs/domains` - this section will soon be big
