export interface UserMockInterface {
  readonly telegram_id: number;
  readonly username: string;
  readonly type: number;
}
