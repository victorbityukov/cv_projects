export interface MovieCategoryMockInterface {
  readonly movie: number;
  readonly category: number;
}
