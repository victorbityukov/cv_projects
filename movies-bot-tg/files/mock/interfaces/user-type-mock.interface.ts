import { UserTypeEnum } from "@app/constants/enum";

export interface UserTypeMockInterface {
  readonly type: UserTypeEnum;
}
