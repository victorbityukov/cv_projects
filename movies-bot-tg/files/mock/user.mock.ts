import { UserMockInterface } from './interfaces';

export const userMocks: UserMockInterface[] = [
  {
    telegram_id: 1923402009,
    username: 'max_readman',
    type: 1,
  },
  {
    telegram_id: 244047014,
    username: 'victorbityukov',
    type: 1,
  },
];
