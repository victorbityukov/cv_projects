import { UserTypeMockInterface } from './interfaces';
import { UserTypeEnum } from '@app/constants';

export const userTypeMocks: UserTypeMockInterface[] = [
  {
    type: UserTypeEnum.ADMIN,
  },
  {
    type: UserTypeEnum.USER,
  },
];
