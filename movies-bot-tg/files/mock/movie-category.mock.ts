import { MovieCategoryMockInterface } from './interfaces';

export const movieCategoryMocks: MovieCategoryMockInterface[] = [
  {
    movie: 3,
    category: 1,
  },
  {
    movie: 3,
    category: 2,
  },
  {
    movie: 3,
    category: 3,
  },
  {
    movie: 4,
    category: 4,
  },
];
