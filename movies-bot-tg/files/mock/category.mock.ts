import { CategoryMockInterface } from './interfaces'

export const categoryMocks: CategoryMockInterface[] = [
  {
    title: 'Боевик',
  },
  {
    title: 'Комедия',
  },
  {
    title: 'Фантастика',
  },
  {
    title: 'Ужасы',
  },
  {
    title: 'Драма',
  },
  {
    title: 'Детектив',
  }
];
