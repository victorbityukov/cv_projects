module.exports = {
  extends: ['airbnb-typescript/base'],
  parserOptions: {
    project: './tsconfig.json',
    sourceType: 'module',
  },
  ignorePatterns: ['*.js'],
  rules: {
    'no-trailing-spaces': 0,
    'no-underscore-dangle': 0,
    'import/prefer-default-export': 0,
    'max-classes-per-file': 0,
    'class-methods-use-this': 0,
    'import/no-cycle': 0,
    'no-await-in-loop': 0,
    '@typescript-eslint/no-explicit-any': 2,
  },
};
