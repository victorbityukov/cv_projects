import { Markup, Scenes, Telegraf } from 'telegraf';
import RedisSession from 'telegraf-session-redis';
import { Repository } from 'typeorm';
import { Message } from 'telegraf/typings/core/types/typegram';
import { Inject, Injectable } from '@nestjs/common';
import { InjectRedis, Redis } from '@nestjs-modules/ioredis';
import { InjectRepository } from '@nestjs/typeorm';
import {
  buttonAddMovies,
  buttonAddMovie,
  buttonDeleteMovie,
  buttonFindMovie,
  buttonHelp,
  buttonLastAdded,
  buttonRowExit,
  buttonViewMovie,
  helpInfo,
  TelegramHandlerService,
  TEXTS,
  buttonStatistic,
  buttonAdvert,
} from '@app/telegram-handler';
import { RabbitmqConfig, redisConfiguration, telegramConfiguration } from '@app/configuration';
import {
  addMovie,
  addMoviesByTemplate,
  checkAccessByType,
  deleteMovie,
  drawStatistics,
  findMoviesByTitle,
  getLastAddedMovies,
  getMovieById,
  getUserById,
} from '@app/dao';
import {
  CategoryEntity,
  LabelEntity,
  MovieCategoryEntity,
  MovieEntity,
  MovieLabelEntity,
  UserEntity,
} from '@app/dbo';
import { BEAR_SECURITY, UserTypeEnum } from '@app/constants';
import { ACTIONS } from '@app/telegram-handler/constants';
import {
  ConfirmContext,
  RedisSessionConfig,
  TelegramConfig,
} from '@app/configuration/interfaces';
import { fileContentByUrl } from '@app/utils';
import { MyLogger, RabbitEventEnum } from '@app/common';
import { getCurrentStats } from '@app/dao/statistic';
import { ClientProxy } from '@nestjs/microservices';

const TELEGRAM_CONFIG: TelegramConfig = telegramConfiguration;
const REDIS_CONFIG: RedisSessionConfig = redisConfiguration;

@Injectable()
export class TelegramService {
  private bot: Telegraf<ConfirmContext>;

  private WIZARD_SCENE_ADD_MOVIE = 'WIZARD_SCENE_ADD_MOVIE';
  private WIZARD_SCENE_ADD_MOVIES = 'WIZARD_SCENE_ADD_MOVIES';
  private WIZARD_SCENE_FIND_MOVIE = 'WIZARD_SCENE_FIND_MOVIE';
  private WIZARD_SCENE_VIEW_MOVIE = 'WIZARD_SCENE_VIEW_MOVIE';
  private WIZARD_SCENE_DELETE_MOVIE = 'WIZARD_SCENE_DELETE_MOVIE';
  private WIZARD_SCENE_ADVERT = 'WIZARD_SCENE_ADVERT';

  private buttonsMainMenu = [
    [buttonLastAdded, buttonAddMovie],
    [buttonViewMovie, buttonAddMovies],
    [buttonStatistic, buttonFindMovie],
    [buttonAdvert, buttonHelp, buttonDeleteMovie],
  ];
  private buttonsExitScene = [buttonRowExit];

  private menuKeyboard = Markup.inlineKeyboard(this.buttonsMainMenu);
  private menuExit = Markup.inlineKeyboard(this.buttonsExitScene);

  private tokenService: string;

  constructor(
    @Inject(RabbitmqConfig.RABBITMQ_ADVERT_QUEUE)
    private readonly clientRabbit: ClientProxy,
    @InjectRepository(CategoryEntity)
    private readonly categoryRepository: Repository<CategoryEntity>,
    @InjectRepository(LabelEntity)
    private readonly labelRepository: Repository<LabelEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(MovieEntity)
    private readonly movieRepository: Repository<MovieEntity>,
    @InjectRepository(MovieCategoryEntity)
    private readonly movieCategoryRepository: Repository<MovieCategoryEntity>,
    @InjectRepository(MovieLabelEntity)
    private readonly movieLabelRepository: Repository<MovieLabelEntity>,
    @InjectRedis()
    private readonly redisService: Redis,
    private readonly telegram: TelegramHandlerService,
    private readonly myLogger: MyLogger,
  ) {
    this.myLogger.setContext(TelegramService.name);
    const { tokenService } = TELEGRAM_CONFIG;
    this.bot = new Telegraf<ConfirmContext>(tokenService);
    this.tokenService = tokenService;

    const addMoviesWizardScene = this.createSceneAddMovies();
    const addMovieWizardScene = this.createSceneAddMovie();
    const viewMovieWizardScene = this.createSceneViewMovie();
    const findMovieWizardScene = this.createSceneFindMovie();
    const deleteMovieWizardScene = this.createSceneDeleteMovie();
    const advertWizardScene = this.createSceneAdvert();
    const stage = new Scenes.Stage<ConfirmContext>([
      addMoviesWizardScene,
      addMovieWizardScene,
      findMovieWizardScene,
      viewMovieWizardScene,
      deleteMovieWizardScene,
      advertWizardScene,
    ], {
      ttl: TELEGRAM_CONFIG.stageTtl
    });

    const redisSession = new RedisSession({
      store: {
        host: REDIS_CONFIG.host,
        port: REDIS_CONFIG.port
      },
      ttl: TELEGRAM_CONFIG.redisSessionTtl
    });

    this.bot.use(redisSession);
    this.bot.use(stage.middleware());

    this.bot.command(ACTIONS.START, async (ctx: ConfirmContext) => {
      try {
        const tgUser = ctx.from;
        if (!tgUser) return;
        if (!await this.checkAccess(ctx)) return;
        const welcome = this.telegram.replyWithUserName(TEXTS.greetingsAdmin, tgUser.username);
        await ctx.reply(welcome, this.menuKeyboard);
      } catch (e) {
        this.myLogger.error(`Error on command ${ACTIONS.START}: ${e}`);
      }
    });

    /**
     * ACTIONS.ADD_MOVIES
     */
    this.bot.command(ACTIONS.ADD_MOVIES, async (ctx) => {
      if (!await this.checkAccess(ctx)) return;
      await ctx.scene.enter(this.WIZARD_SCENE_ADD_MOVIES);
    });

    this.bot.action(ACTIONS.ADD_MOVIES, async (ctx) => {
      if (!await this.checkAccess(ctx)) return;
      await ctx.scene.enter(this.WIZARD_SCENE_ADD_MOVIES);
      ctx.answerCbQuery();
    });
    /**
     * ====================================
     */

    /**
     * ACTIONS.ADD_MOVIE
     */
    this.bot.command(ACTIONS.ADD_MOVIE, async (ctx) => {
      if (!await this.checkAccess(ctx)) return;
      await ctx.scene.enter(this.WIZARD_SCENE_ADD_MOVIE);
    });

    this.bot.action(ACTIONS.ADD_MOVIE, async (ctx) => {
      if (!await this.checkAccess(ctx)) return;
      await ctx.scene.enter(this.WIZARD_SCENE_ADD_MOVIE);
      ctx.answerCbQuery();
    });
    /**
     * ====================================
     */

    /**
     * ACTIONS.LAST_ADDED
     */
    this.bot.command(ACTIONS.LAST_ADDED, async (ctx) => {
      await this.getLastAdded(ctx);
    });

    this.bot.action(ACTIONS.LAST_ADDED, async (ctx) => {
      await this.getLastAdded(ctx);
      ctx.answerCbQuery();
    });
    /**
     * =======================================
     */

    /**
     * ACTIONS.MENU
     */

    this.bot.command(ACTIONS.MENU, async (ctx) => {
      if (!await this.checkAccess(ctx)) return;
      await ctx.reply(TEXTS.mainMenu.text, this.menuKeyboard);
    });

    /**
     * ========================================
     */

    /**
     * ACTIONS.FIND_MOVIE
     */
    this.bot.action(ACTIONS.FIND_MOVIE, async (ctx: ConfirmContext) => {
      try {
        if (!await this.checkAccess(ctx)) return;
        const tgUser = ctx.from;
        if (!tgUser) return;
        await ctx.scene.enter(this.WIZARD_SCENE_FIND_MOVIE);
        await ctx.answerCbQuery();
      } catch (e) {
        this.myLogger.error(`Error on command ${ACTIONS.FIND_MOVIE}: ${e}`);
      }
    });

    this.bot.command(ACTIONS.FIND_MOVIE, async (ctx: ConfirmContext) => {
      try {
        if (!await this.checkAccess(ctx)) return;
        const tgUser = ctx.from;
        if (!tgUser) return;
        await ctx.scene.enter(this.WIZARD_SCENE_FIND_MOVIE);
      } catch (e) {
        this.myLogger.error(`Error on command ${ACTIONS.FIND_MOVIE}: ${e}`);
      }
    });

    /**
     * ==============================================
     */

    /**
     * ACTIONS.VIEW_MOVIE
     */
    this.bot.action(ACTIONS.VIEW_MOVIE, async (ctx: ConfirmContext) => {
      try {
        if (!await this.checkAccess(ctx)) return;
        const tgUser = ctx.from;
        if (!tgUser) return;
        await ctx.scene.enter(this.WIZARD_SCENE_VIEW_MOVIE);
        await ctx.answerCbQuery();
      } catch (e) {
        this.myLogger.error(`Error on command ${ACTIONS.FIND_MOVIE}: ${e}`);
      }
    });

    this.bot.command(ACTIONS.VIEW_MOVIE, async (ctx: ConfirmContext) => {
      try {
        if (!await this.checkAccess(ctx)) return;
        const tgUser = ctx.from;
        if (!tgUser) return;
        await ctx.scene.enter(this.WIZARD_SCENE_VIEW_MOVIE);
      } catch (e) {
        this.myLogger.error(`Error on command ${ACTIONS.FIND_MOVIE}: ${e}`);
      }
    });
    /**
     * ========================================
     */

    /**
     * ACTIONS.HELP
     */
    this.bot.action(ACTIONS.HELP, async (ctx) => {
      try {
        if (!await this.checkAccess(ctx)) return;
        await ctx.reply(helpInfo.text, this.menuKeyboard);
        ctx.answerCbQuery();
      } catch (e) {
        this.myLogger.error(`Error on action ${ACTIONS.HELP}: ${e}`);
      }
    });

    this.bot.command(ACTIONS.HELP, async (ctx) => {
      try {
        if (!await this.checkAccess(ctx)) return;
        await ctx.reply(helpInfo.text, this.menuKeyboard);
      } catch (e) {
        this.myLogger.error(`Error on action ${ACTIONS.HELP}: ${e}`);
      }
    });

    /**
     * ==================================================
     */

    /**
     * ACTIONS.DELETE_MOVIE
     */
    this.bot.command(ACTIONS.DELETE_MOVIE, async (ctx) => {
      try {
        if (!await this.checkAccess(ctx)) return;
        await ctx.scene.enter(this.WIZARD_SCENE_DELETE_MOVIE);
      } catch (e) {
        this.myLogger.error(`Error on action ${ACTIONS.DELETE_MOVIE}: ${e}`);
      }
    });

    this.bot.action(ACTIONS.DELETE_MOVIE, async (ctx) => {
      try {
        if (!await this.checkAccess(ctx)) return;
        await ctx.scene.enter(this.WIZARD_SCENE_DELETE_MOVIE);
        ctx.answerCbQuery();
      } catch (e) {
        this.myLogger.error(`Error on action ${ACTIONS.DELETE_MOVIE}: ${e}`);
      }
    });
    /**
     * ================================================
     */

    /**
     * ACTIONS.STATS
     */
    this.bot.action(ACTIONS.STATS, async (ctx) => {
      const stats = await getCurrentStats(this.userRepository);
      await ctx.replyWithHTML(drawStatistics(stats));
      await ctx.answerCbQuery();
    });

    this.bot.command(ACTIONS.STATS, async (ctx) => {
      const stats = await getCurrentStats(this.userRepository);
      await ctx.replyWithHTML(drawStatistics(stats));
    });
    /**
     * ================================================
     */

    /**
     * ACTIONS.ADVERT
     */
    this.bot.action(ACTIONS.ADVERT, async (ctx) => {
      await ctx.scene.enter(this.WIZARD_SCENE_ADVERT);
      await ctx.answerCbQuery();
    });

    this.bot.command(ACTIONS.ADVERT, async (ctx) => {
      await ctx.scene.enter(this.WIZARD_SCENE_ADVERT);
    });
    /**
     * ================================================
     */

    advertWizardScene.action(ACTIONS.EXIT, async (ctx) => this.exitScenes(ctx));
    addMoviesWizardScene.action(ACTIONS.EXIT, async (ctx) => this.exitScenes(ctx));
    addMovieWizardScene.action(ACTIONS.EXIT, async (ctx) => this.exitScenes(ctx));
    findMovieWizardScene.action(ACTIONS.EXIT, async (ctx) => this.exitScenes(ctx));
    viewMovieWizardScene.action(ACTIONS.EXIT, async (ctx) => this.exitScenes(ctx));
    deleteMovieWizardScene.action(ACTIONS.EXIT, async (ctx) => this.exitScenes(ctx));

    advertWizardScene.command(ACTIONS.EXIT, async (ctx) => this.exitScenesCommand(ctx));
    addMoviesWizardScene.command(ACTIONS.EXIT, async (ctx) => this.exitScenesCommand(ctx));
    addMovieWizardScene.command(ACTIONS.EXIT, async (ctx) => this.exitScenesCommand(ctx));
    findMovieWizardScene.command(ACTIONS.EXIT, async (ctx) => this.exitScenesCommand(ctx));
    viewMovieWizardScene.command(ACTIONS.EXIT, async (ctx) => this.exitScenesCommand(ctx));
    deleteMovieWizardScene.command(ACTIONS.EXIT, async (ctx) => this.exitScenesCommand(ctx));

    this.bot.launch()
      .catch((err) => {
        this.myLogger.error(err);
      });

    // process.once('SIGINT', () => this.bot.stop('SIGINT'));
    // process.once('SIGTERM', () => this.bot.stop('SIGTERM'));
  }

  private async exitScenes(ctx: ConfirmContext): Promise<void> {
    await ctx.scene.leave();
    await ctx.reply(TEXTS.mainMenu.text, this.menuKeyboard);
    await ctx.answerCbQuery();
  }

  private async exitScenesCommand(ctx: ConfirmContext): Promise<void> {
    await ctx.scene.leave();
    await ctx.reply(TEXTS.mainMenu.text, this.menuKeyboard);
  }

  private async findMoviesByTitle(ctx: ConfirmContext, title: string): Promise<void> {
    const movies = await findMoviesByTitle(this.movieRepository, title);
    if (!movies || movies.length === 0) {
      await ctx.reply(`Фильмы по запросу ${title} не найдены`, this.menuKeyboard);
      return;
    }
    const message = movies.map((movie, index) => `${(index + 1) < 10 && '0'}${index + 1}. [${movie.id}] ${movie.title}`);
    await ctx.reply('Найдены следующие фильмы:\n\n' + message.join('\n\n'), this.menuKeyboard);
  }

  private async checkAccess(ctx: ConfirmContext): Promise<boolean> {
    if (!ctx.from) return false;
    const tgUser = ctx.from;
    const access = await checkAccessByType(this.userRepository, UserTypeEnum.ADMIN, tgUser.id);
    if (!access) {
      await ctx.replyWithSticker(BEAR_SECURITY);
      return false;
    }
    return true;
  }

  private async getLastAdded(ctx: ConfirmContext): Promise<void> {
    if (!await this.checkAccess(ctx)) return;

    const movies = await getLastAddedMovies(this.movieRepository);

    if (!movies || movies.length === 0) {
      await ctx.reply(TEXTS.moviesListEmptyText.text, this.menuKeyboard);
      return;
    }
    const message = movies.map((movie, index) => `${(index + 1) < 10 ? '0' : ''}${index + 1}. ${movie.title}  [${movie.id}]`);
    await ctx.reply(message.join('\n'), this.menuKeyboard);
  }

  /**
   * Поиск фильма
   */
  private async startFindMovie(ctx: ConfirmContext) {
    this.myLogger.warn('startFindMovie');
    const tgUser = ctx.from;
    if (tgUser) {
      await ctx.reply(TEXTS.inputTitleMovieForSearch.text, this.menuExit);
    }
    return await ctx.wizard.next();
  }

  private async handleFindByTitleMovie(ctx: ConfirmContext) {
    this.myLogger.warn('handleFindByTitleMovie');
    const tgUser = ctx.from;
    if (!tgUser) return await ctx.scene.leave();

    if (!ctx.message) {
      return ctx.scene.leave();
    }
    const title = (ctx.message as Message.TextMessage).text;

    await this.findMoviesByTitle(ctx, title);
    return ctx.scene.leave();
  }

  private createSceneFindMovie(): Scenes.WizardScene<ConfirmContext> {
    return new Scenes.WizardScene(
      this.WIZARD_SCENE_FIND_MOVIE,
      (ctx) => this.startFindMovie(ctx),
      (ctx) => this.handleFindByTitleMovie(ctx)
    );
  }

  /**
   * Предпросмотр фильма
   */
  private async startViewMovie(ctx: ConfirmContext) {
    this.myLogger.warn('startViewMovie');
    const tgUser = ctx.from;
    if (tgUser) {
      await ctx.reply(TEXTS.inputIdMovieForView.text, this.menuExit);
    }
    return await ctx.wizard.next();
  }

  private async handleViewMovieById(ctx: ConfirmContext) {
    this.myLogger.warn('handleViewMovieById');
    const tgUser = ctx.from;
    if (!tgUser) return await ctx.scene.leave();

    if (!ctx.message) {
      return ctx.scene.leave();
    }
    const id = Number((ctx.message as Message.TextMessage).text);

    if (!id) {
      await ctx.reply(TEXTS.expectedId.text, this.menuKeyboard);
      return ctx.scene.leave();
    }

    const fullContent = await getMovieById(this.movieRepository, id);
    if (!fullContent) {
      await ctx.reply(`Фильм по ID: ${id} не найден`, this.menuKeyboard);
      return ctx.scene.leave();
    }
    await ctx.reply(fullContent, this.menuKeyboard);
    return ctx.scene.leave();
  }

  private createSceneViewMovie(): Scenes.WizardScene<ConfirmContext> {
    return new Scenes.WizardScene(
      this.WIZARD_SCENE_VIEW_MOVIE,
      (ctx) => this.startViewMovie(ctx),
      (ctx) => this.handleViewMovieById(ctx),
    );
  }

  /**
   * Удаление фильма
   */
  private async startDeleteMovie(ctx: ConfirmContext) {
    this.myLogger.warn('startDeleteMovie');
    const tgUser = ctx.from;
    if (!tgUser) return await ctx.scene.leave();
    await ctx.reply(TEXTS.inputIdMovieForDelete.text, this.menuExit);
    return await ctx.wizard.next();
  }

  private async handleDeleteMovieById(ctx: ConfirmContext) {
    this.myLogger.warn('handleDeleteMovieById');
    const tgUser = ctx.from;
    if (!tgUser) return await ctx.scene.leave();

    if (!ctx.message) {
      return ctx.scene.leave();
    }
    const id = Number((ctx.message as Message.TextMessage).text);

    if (!id) {
      await ctx.reply(TEXTS.expectedId.text, this.menuKeyboard);
      return ctx.scene.leave();
    }

    const status = await deleteMovie(this.movieRepository, this.movieCategoryRepository, this.movieLabelRepository, id);
    if (status) {
      await ctx.reply(`Фильм ${id} удален`, this.menuKeyboard);
    } else {
      await ctx.reply(`Фильм ${id} не найден`, this.menuKeyboard);
    }
    return ctx.scene.leave();
  }

  private createSceneDeleteMovie(): Scenes.WizardScene<ConfirmContext> {
    return new Scenes.WizardScene(
      this.WIZARD_SCENE_DELETE_MOVIE,
      (ctx) => this.startDeleteMovie(ctx),
      (ctx) => this.handleDeleteMovieById(ctx)
    );
  }

  /**
   * Добавление фильмов
   */
  private async startAddMovies(ctx: ConfirmContext) {
    this.myLogger.warn('startAddMovies');
    const tgUser = ctx.from;
    if (!tgUser) return await ctx.scene.leave();
    await ctx.reply('Прикрепите <file>.txt фильмов в требуемом шаблоне', this.menuExit);
    return await ctx.wizard.next();
  }

  private async handleAddMovies(ctx: ConfirmContext) {
    this.myLogger.warn('handleAddMovies');
    const tgUser = ctx.from;
    if (!tgUser) return await ctx.scene.leave();
    // @ts-ignore
    if (!ctx.update.message) return await ctx.scene.leave();
    // @ts-ignore
    if (!ctx.update.message.document) {
      await ctx.reply('Должен быть прикреплен текстовый документ .txt, повтори попытку', this.menuExit);
      ctx.wizard.selectStep(ctx.wizard.cursor - 1);
      return await ctx.wizard.next();
    }

    // @ts-ignore
    const { mime_type, file_id } = ctx.update.message.document;
    if (mime_type !== 'text/plain') {
      await ctx.reply('Должен быть прикреплен текстовый документ .txt, повтори попытку', this.menuExit);
      ctx.wizard.selectStep(ctx.wizard.cursor - 1);
      return await ctx.wizard.next();
    }

    // @ts-ignore
    const fileData = await this.bot.telegram.getFile(file_id);
    const { file_path: filePath } = fileData;
    const fileContent = await fileContentByUrl(`https://api.telegram.org/file/bot${this.tokenService}/${filePath}`);

    const user = await getUserById(this.userRepository, tgUser.id);
    if (!user) {
      await ctx.reply('Вы кто вообще?');
      return await ctx.scene.leave();
    }

    const moviesAlreadyExist = await addMoviesByTemplate(
      this.movieRepository,
      this.labelRepository,
      this.categoryRepository,
      this.movieCategoryRepository,
      this.movieLabelRepository,
      user.id,
      fileContent
    );

    await ctx.reply(moviesAlreadyExist, this.menuKeyboard);
    return ctx.scene.leave();
  }

  private createSceneAddMovies(): Scenes.WizardScene<ConfirmContext> {
    return new Scenes.WizardScene(
      this.WIZARD_SCENE_ADD_MOVIES,
      (ctx) => this.startAddMovies(ctx),
      (ctx) => this.handleAddMovies(ctx)
    );
  }


  /**
   * Добавление фильма
   */
  private async startAddMovie(ctx: ConfirmContext) {
    this.myLogger.warn('startAddMovie');
    const tgUser = ctx.from;
    if (!tgUser) return await ctx.scene.leave();
    await ctx.reply('Отправьте фильм в требуемом формате в сообщении', this.menuExit);
    return await ctx.wizard.next();
  }

  private async handleAddMovie(ctx: ConfirmContext) {
    this.myLogger.warn('handleAddMovie');
    const tgUser = ctx.from;
    if (!tgUser) return await ctx.scene.leave();

    if (!ctx.message) {
      return ctx.scene.leave();
    }
    const movie = (ctx.message as Message.TextMessage).text;

    const user = await getUserById(this.userRepository, tgUser.id);
    if (!user) {
      await ctx.reply('Вы кто вообще?');
      return await ctx.scene.leave();
    }

    const somethingError = await addMovie(
      this.movieRepository,
      this.labelRepository,
      this.categoryRepository,
      this.movieCategoryRepository,
      this.movieLabelRepository,
      user.id,
      movie,
    );
    if (!somethingError) {
      await ctx.reply(`Фильм добавлен`, this.menuKeyboard);
    } else {
      await ctx.reply(somethingError, this.menuKeyboard);
    }
    return ctx.scene.leave();
  }

  private createSceneAddMovie(): Scenes.WizardScene<ConfirmContext> {
    return new Scenes.WizardScene(
      this.WIZARD_SCENE_ADD_MOVIE,
      (ctx) => this.startAddMovie(ctx),
      (ctx) => this.handleAddMovie(ctx)
    );
  }

  /**
   * Рассылка рекламы
   */
  private async startAdvert(ctx: ConfirmContext) {
    this.myLogger.warn('startAdvert');
    const tgUser = ctx.from;
    if (tgUser) {
      await ctx.reply('Отправьте контент для рассылки', this.menuExit);
    }
    return await ctx.wizard.next();
  }

  private async handleAdvert(ctx: ConfirmContext) {
    this.myLogger.warn('handleAdvert');
    const tgUser = ctx.from;
    if (!tgUser) return await ctx.scene.leave();

    if (!ctx.message) {
      return ctx.scene.leave();
    }
    const advertContent = (ctx.message as Message.TextMessage).text;

    this.clientRabbit.emit(RabbitEventEnum.SEND_ADVERT_CONTENT, {
      content: advertContent,
    });
    await ctx.reply('Контент рассылка отправлен в клиентского бота, дело за ним', this.menuKeyboard);
    return ctx.scene.leave();
  }

  private createSceneAdvert(): Scenes.WizardScene<ConfirmContext> {
    return new Scenes.WizardScene(
      this.WIZARD_SCENE_ADVERT,
      (ctx) => this.startAdvert(ctx),
      (ctx) => this.handleAdvert(ctx)
    );
  }
}
