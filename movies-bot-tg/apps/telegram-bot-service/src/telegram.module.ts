import { Module } from '@nestjs/common';
import { RedisModule } from '@nestjs-modules/ioredis';
import { coreDbConfiguration, RabbitmqConfig, redisConfiguration } from '@app/configuration';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { TelegramHandlerModule, TelegramHandlerService } from '@app/telegram-handler';
import { DB_ENTITIES } from '@app/configuration/typeorm.config';
import { LoggerModule } from '@app/common';
import { TelegramService } from './telegram.service';

@Module({
  imports: [
    RedisModule.forRoot({
      config: redisConfiguration,
    }),
    ClientsModule.register([
      {
        name: RabbitmqConfig.RABBITMQ_ADVERT_QUEUE,
        transport: Transport.RMQ,
        options: {
          urls: [RabbitmqConfig.CONNECTION_OBJECT],
          queue: RabbitmqConfig.RABBITMQ_ADVERT_QUEUE,
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
    TypeOrmModule.forRoot(coreDbConfiguration),
    TypeOrmModule.forFeature(DB_ENTITIES),
    TelegramHandlerModule,
    LoggerModule,
  ],
  providers: [TelegramService, TelegramHandlerService],
})
export class TelegramModule {}
