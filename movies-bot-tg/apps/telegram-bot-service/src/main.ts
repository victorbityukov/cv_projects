import { NestFactory } from '@nestjs/core';
import { MyLogger } from '@app/common';
import { TelegramModule } from './telegram.module';

async function bootstrap() {
  const app = await NestFactory.create(TelegramModule, {
    bufferLogs: true,
    autoFlushLogs: true,
  });

  app.useLogger(app.get(MyLogger));

  await app.listen(3010);
}
bootstrap();
