import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { MyLogger } from '@app/common';
import { RabbitmqConfig } from '@app/configuration';
import { TelegramMovieModule } from './telegram-movie.module';

async function bootstrap() {
  const app = await NestFactory.create(TelegramMovieModule, {
    bufferLogs: true,
    autoFlushLogs: true,
  });

  app.useLogger(app.get(MyLogger));

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.RMQ,
    options: {
      urls: [RabbitmqConfig.CONNECTION_OBJECT],
      queue: RabbitmqConfig.RABBITMQ_ADVERT_QUEUE,
      queueOptions: {
        durable: false,
      },
    },
  });

  await app.startAllMicroservices();

  await app.listen(3000);
}
bootstrap();
