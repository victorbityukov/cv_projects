import { Markup, Telegraf } from 'telegraf';
import { Repository } from 'typeorm';
import { from, lastValueFrom, mergeMap } from 'rxjs';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  buttonRowCategories,
  buttonRowClientMenu,
  CATEGORIES,
  TelegramHandlerService,
  TEXTS_CLIENT,
} from '@app/telegram-handler';
import { telegramConfiguration } from '@app/configuration';
import {
  addNewUser,
  drawStatistics,
  getCurrentStats,
  getIdsUsers,
  getRandMovie,
  getRandMovieByCategory,
  setUserStatus,
} from '@app/dao';
import {
  MovieEntity,
  UserEntity,
} from '@app/dbo';
import { ACTIONS } from '@app/telegram-handler/constants';
import {
  ConfirmContext,
  TelegramConfig
} from '@app/configuration/interfaces';
import { MyLogger } from '@app/common';
import { UserStatusEnum } from '@app/constants';

const TELEGRAM_CONFIG: TelegramConfig = telegramConfiguration;

@Injectable()
export class TelegramMovieService {
  private bot: Telegraf<ConfirmContext>;

  private menuMain = Markup.inlineKeyboard(buttonRowClientMenu);
  private menuCategories = Markup.inlineKeyboard(buttonRowCategories);

  constructor(
    @InjectRepository(MovieEntity)
    private readonly movieRepository: Repository<MovieEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private readonly telegram: TelegramHandlerService,
    private readonly myLogger: MyLogger,
  ) {
    this.myLogger.setContext(TelegramMovieService.name);
    const { tokenClient } = TELEGRAM_CONFIG;
    this.bot = new Telegraf<ConfirmContext>(tokenClient);

    this.bot.command(ACTIONS.START, async (ctx: ConfirmContext) => {
      try {
        const tgUser = ctx.from;
        if (!tgUser) return;
        await addNewUser(this.userRepository, tgUser);
        const welcome = this.telegram.replyWithFirstName(TEXTS_CLIENT.greetingsFirst, tgUser.first_name, tgUser.username);
        await ctx.reply(welcome, this.menuMain);
      } catch (e) {
        this.myLogger.error(`Error on command ${ACTIONS.START}: ${e}`);
      }
    });

    this.bot.on('my_chat_member', async (ctx) => {
      const id = ctx.update.my_chat_member.from.id;
      const status = ctx.update.my_chat_member.new_chat_member.status;
      if (status === UserStatusEnum.MEMBER) {
        //@ts-ignore
        await addNewUser(this.userRepository, ctx.update.my_chat_member.from);
      }
      await setUserStatus(this.userRepository, { id, status });
    });

    /**
     * ACTIONS.BACK, ACTIONS.MENU
     */
    this.bot.command(ACTIONS.BACK, async (ctx) => {
      await ctx.reply(TEXTS_CLIENT.greetingsNext.text, this.menuMain);
    });

    this.bot.action(ACTIONS.BACK, async (ctx) => {
      await ctx.reply(TEXTS_CLIENT.greetingsNext.text, this.menuMain);
      ctx.answerCbQuery();
    });

    this.bot.command(ACTIONS.MENU, async (ctx) => {
      await ctx.reply(TEXTS_CLIENT.greetingsNext.text, this.menuMain);
    });
    /**
     * =======================================
     */

    this.bot.action(ACTIONS.CATEGORIES_MOVIE, async (ctx: ConfirmContext) => {
      await ctx.reply(TEXTS_CLIENT.selectCategory.text, this.menuCategories);
      await ctx.answerCbQuery();
    });

    this.bot.action(ACTIONS.RANDOM_MOVIE, async (ctx: ConfirmContext) => {
      await this.getRandomMovieContent(ctx);
      await ctx.answerCbQuery();
    });

    /**
     * CATEGORIES
     */
    this.bot.action(CATEGORIES.DRAMA, async (ctx: ConfirmContext) => {
      await this.getMovieByCategory(ctx, CATEGORIES.DRAMA);
    });
    this.bot.action(CATEGORIES.THRILLER, async (ctx: ConfirmContext) => {
      await this.getMovieByCategory(ctx, CATEGORIES.THRILLER);
    });
    this.bot.action(CATEGORIES.FANTASTIC, async (ctx: ConfirmContext) => {
      await this.getMovieByCategory(ctx, CATEGORIES.FANTASTIC);
    });
    this.bot.action(CATEGORIES.COMEDY, async (ctx: ConfirmContext) => {
      await this.getMovieByCategory(ctx, CATEGORIES.COMEDY);
    });
    this.bot.action(CATEGORIES.HORRORS, async (ctx: ConfirmContext) => {
      await this.getMovieByCategory(ctx, CATEGORIES.HORRORS);
    });
    this.bot.action(CATEGORIES.DETECTIVE, async (ctx: ConfirmContext) => {
      await this.getMovieByCategory(ctx, CATEGORIES.DETECTIVE);
    });
    /**
     * =======================================
     */

    this.bot.command(ACTIONS.STATS, async (ctx) => {
      const stats = await getCurrentStats(this.userRepository);
      await ctx.replyWithHTML(drawStatistics(stats));
    });

    this.bot.launch()
      .catch((err) => {
        this.myLogger.error(err);
      });

    // process.once('SIGINT', () => this.bot.stop('SIGINT'));
    // process.once('SIGTERM', () => this.bot.stop('SIGTERM'));
  }

  private async getRandomMovieContent(ctx: ConfirmContext): Promise<void> {
    this.myLogger.log('getRandomMovieContent');
    const movie = await getRandMovie(this.movieRepository);
    await ctx.reply(movie, this.menuMain);
  }

  private async getMovieByCategory(ctx: ConfirmContext, category: string): Promise<void> {
    this.myLogger.log('getMovieByCategory');
    const movie = await getRandMovieByCategory(this.movieRepository, category);
    const menuMoreLocal = Markup.inlineKeyboard([
      [
        {
          text: 'Ещё',
          callback_data: category
        }
      ],
      [
        {
          text: 'Назад',
          callback_data: ACTIONS.CATEGORIES_MOVIE
        }
      ]
    ]);
    await ctx.reply(movie, menuMoreLocal);
    await ctx.answerCbQuery();
  }

  public async sendAdvert(content: string): Promise<void> {
    this.myLogger.warn('sendAdvert');
    const userIds = await getIdsUsers(this.userRepository);
    await lastValueFrom(from(userIds)
      .pipe(
        mergeMap(async (id) => {
          await this.bot.telegram.sendMessage(id, content);
          this.myLogger.warn(`ADVERT TO ${id}`);
        }, 1),
      ), { defaultValue: [] });
    this.myLogger.warn('sendAdvert SUCCESS');
  }
}
