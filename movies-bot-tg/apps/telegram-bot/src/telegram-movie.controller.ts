import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { RabbitEventEnum } from '@app/common';
import { TelegramMovieService } from './telegram-movie.service';

@Controller()
export class TelegramMovieController {
  constructor(private readonly telegramMovieService: TelegramMovieService) {}

  @MessagePattern(RabbitEventEnum.SEND_ADVERT_CONTENT)
  async handleEvent(@Payload() data: { content: string }): Promise<void> {
    const { content } = data;
    await this.telegramMovieService.sendAdvert(content);
  }
}
