import { Module } from '@nestjs/common';
import { RedisModule } from '@nestjs-modules/ioredis';
import { coreDbConfiguration, redisConfiguration } from '@app/configuration';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TelegramHandlerModule, TelegramHandlerService } from '@app/telegram-handler';
import { DB_ENTITIES } from '@app/configuration/typeorm.config';
import { LoggerModule } from '@app/common';
import { TelegramMovieService } from './telegram-movie.service';
import { TelegramMovieController } from './telegram-movie.controller';

@Module({
  imports: [
    RedisModule.forRoot({
      config: redisConfiguration,
    }),
    TypeOrmModule.forRoot(coreDbConfiguration),
    TypeOrmModule.forFeature(DB_ENTITIES),
    TelegramHandlerModule,
    LoggerModule,
  ],
  controllers: [TelegramMovieController],
  providers: [TelegramMovieService, TelegramHandlerService],
})
export class TelegramMovieModule {}
