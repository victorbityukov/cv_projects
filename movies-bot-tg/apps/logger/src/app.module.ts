import { WinstonModule } from 'nest-winston';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    WinstonModule.forRootAsync({
      useFactory: () => ({}),
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
