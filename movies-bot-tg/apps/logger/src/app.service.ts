import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import winston, { format, Logger, transports } from 'winston';
import 'winston-daily-rotate-file';
import { utilities as nestWinstonModuleUtilities } from 'nest-winston/dist/winston.utilities';
import { Inject, Injectable } from '@nestjs/common';
import { EventLoggerDto } from './dto';

const myFormat = format.printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

@Injectable()
export class AppService {
  private container = new winston.Container();
  private readonly dailyRotateFileTransport = (filename: string) =>
    new transports.DailyRotateFile({
      filename: `logs/${filename}/%DATE%.log`,
      maxSize: '250m',
      datePattern: 'DD-MM-YYYY',
      format: format.combine(format.label({ label: filename }), format.timestamp(), myFormat),
    });

  constructor(@Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger) {
    this.logger = winston.createLogger({});
  }

  eventHandler(event: EventLoggerDto): void {
    const { service, message, level } = event;
    if (!this.container.has(service)) {
      this.container.add(service, {
        transports: [
          new winston.transports.Console({
            format: format.combine(
              winston.format.timestamp(),
              winston.format.ms(),
              nestWinstonModuleUtilities.format.nestLike(service),
            ),
          }),
          this.dailyRotateFileTransport(service),
        ],
      });
    }
    const logger = this.container.get(service);
    logger[level](message);
  }
}
