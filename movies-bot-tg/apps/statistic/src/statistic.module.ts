import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';
import { coreDbConfiguration, DB_ENTITIES } from '@app/configuration';
import { LoggerModule } from '@app/common';
import { StatisticController } from './statistic.controller';
import { StatisticService } from './statistic.service';

@Module({
  imports: [
    LoggerModule,
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot(coreDbConfiguration),
    TypeOrmModule.forFeature(DB_ENTITIES),
  ],
  controllers: [StatisticController],
  providers: [StatisticService],
})
export class StatisticModule {}
