import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MyLogger } from '@app/common';
import { StatisticEntity, UserEntity } from '@app/dbo';
import { getCommitStats } from '@app/dao';

@Injectable()
export class StatisticService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(StatisticEntity)
    private readonly statisticRepository: Repository<StatisticEntity>,
    private readonly myLogger: MyLogger,
  ) {
    this.myLogger.setContext(StatisticService.name);
  }

  async commitStats() {
    await getCommitStats(this.userRepository, this.statisticRepository);
  }
}
