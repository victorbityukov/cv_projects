import { Controller } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { StatisticService } from './statistic.service';

@Controller()
export class StatisticController {
  constructor(private readonly statisticService: StatisticService) {}

  @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT)
  async commitStats() {
    await this.statisticService.commitStats();
  }
}
