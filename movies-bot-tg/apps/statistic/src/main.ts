import { NestFactory } from '@nestjs/core';
import { MyLogger } from '@app/common';
import { StatisticModule } from './statistic.module';

async function bootstrap() {
  const app = await NestFactory.create(StatisticModule, {
    bufferLogs: true,
    autoFlushLogs: true,
  });

  app.useLogger(app.get(MyLogger));

  await app.listen(3020);
}
bootstrap();
