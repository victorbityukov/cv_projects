import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  CategoryEntity,
  MovieCategoryEntity,
  MovieEntity,
  UserEntity,
  UserTypeEntity
} from '@app/dbo';
import {
  categoryMocks,
  userTypeMocks
} from '../../../files/mock';

@Injectable()
export class InitialLoaderService implements OnApplicationBootstrap{
  private readonly logger = new Logger(InitialLoaderService.name, { timestamp: true });
  constructor(
    @InjectRepository(UserTypeEntity)
    private readonly userTypeRepository: Repository<UserTypeEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(CategoryEntity)
    private readonly categoryRepository: Repository<CategoryEntity>,
    @InjectRepository(MovieEntity)
    private readonly movieRepository: Repository<MovieEntity>,
    @InjectRepository(MovieCategoryEntity)
    private readonly movieCategoryRepository: Repository<MovieCategoryEntity>,
  ) { }

  private async loadInitialUserTypes() {
    try {
      await this.userTypeRepository.createQueryBuilder()
        .insert()
        .values(userTypeMocks)
        .orIgnore(`("id") DO NOTHING`)
        .execute();
    } catch (errorOrException) {
      this.logger.error(errorOrException);
    }
  }

  private async loadInitialCategories() {
    try {
      await this.categoryRepository.createQueryBuilder()
        .insert()
        .values(categoryMocks)
        .orIgnore(`("id") DO NOTHING`)
        .execute();
    } catch (errorOrException) {
      this.logger.error(errorOrException);
    }
  }

  async onApplicationBootstrap(){
    this.logger.debug('loadInitialState stated');
    await this.loadInitialUserTypes();
    await this.loadInitialCategories();
    this.logger.debug('loadInitialState ended');
  }
}
