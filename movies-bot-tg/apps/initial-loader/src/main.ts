import { NestFactory } from '@nestjs/core';
import { InitialLoaderModule } from './initial-loader.module';

async function bootstrap() {
  const app = await NestFactory.create(InitialLoaderModule);
  await app.init();
  await process.exit();
}
bootstrap();
