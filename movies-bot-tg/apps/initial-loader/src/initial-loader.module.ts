import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InitialLoaderService } from './initial-loader.service';
import { DB_ENTITIES, initialDbConfiguration } from '@app/configuration';

@Module({
  imports: [
    TypeOrmModule.forRoot(initialDbConfiguration),
    TypeOrmModule.forFeature(DB_ENTITIES),
  ],
  providers: [
    InitialLoaderService
  ],
})
export class InitialLoaderModule {}
