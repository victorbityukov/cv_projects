export interface TelegramConfig {
  readonly tokenClient: string,
  readonly tokenService: string,
  readonly stageTtl: number,
  readonly redisSessionTtl: number,
}
