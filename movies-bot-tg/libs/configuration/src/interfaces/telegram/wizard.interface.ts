import { Context, Scenes } from 'telegraf';
import SceneContextScene from 'telegraf/typings/scenes/context';
import WizardContextWizard from 'telegraf/typings/scenes/wizard/context';
import { CategoryEntity, LabelEntity, UserEntity } from '@app/dbo';

export interface State {
  creator?: UserEntity,
  title?: string,
  categories?: CategoryEntity[],
  labels?: LabelEntity[],
}

export interface SubscribeWizardSession extends Scenes.WizardSessionData {
  creator?: UserEntity,
  title?: string,
  categories?: CategoryEntity[],
  labels?: LabelEntity[],
  tries: 0
  state: State
}

export interface ConfirmContext extends Context {
  scene: Scenes.SceneContextScene<ConfirmContext, SubscribeWizardSession>
  wizard: Scenes.WizardContextWizard<ConfirmContext>
}

export type ConfirmWizardCtx = ConfirmContext & {
  scene: SceneContextScene<ConfirmContext, SubscribeWizardSession>;
  wizard: WizardContextWizard<ConfirmContext & {
    scene: SceneContextScene<ConfirmContext, SubscribeWizardSession>;
  }>
};
