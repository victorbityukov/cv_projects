export interface TelegramConfigInterface {
  readonly tokenService: string,
  readonly tokenClient: string,
  readonly stageTtl: number,
  readonly redisSessionTtl: number,
}
