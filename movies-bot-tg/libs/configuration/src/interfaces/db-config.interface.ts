import { SslDataInterface } from '.';

type ConnectionOptionsType = 'mysql' | 'mariadb' | 'postgres' | 'cockroachdb' | 'sqlite' | 'mssql' | 'sap' | 'oracle' | 'cordova' | 'nativescript' | 'react-native' | 'sqljs' | 'mongodb' | 'aurora-data-api' | 'aurora-data-api-pg' | 'expo' | 'better-sqlite3' | undefined;

export interface DbConfigInterface {
  readonly type: ConnectionOptionsType;
  readonly host: string;
  readonly port: number;
  readonly database: string;
  readonly username: string;
  readonly password: string;
  synchronize: boolean;
  readonly sslOn?: boolean;
  readonly ssl?: SslDataInterface;
}
