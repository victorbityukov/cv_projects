export { DbConfigInterface } from './db-config.interface';
export { RedisConfigInterface } from './redis-config.interface';
export { SslDataInterface } from './ssl-data.interface';
export { UserConfigInterface } from './user-config.interface';
export { TelegramConfigInterface } from './telegram-config.interface';
export * from './telegram';
