export interface SslDataInterface {
  readonly ca: string;
  readonly key: string;
  readonly cert: string;
}
