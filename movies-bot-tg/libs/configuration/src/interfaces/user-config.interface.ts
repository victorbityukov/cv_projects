export interface UserConfigInterface {
  readonly SALT_ROUNDS: number;
}
