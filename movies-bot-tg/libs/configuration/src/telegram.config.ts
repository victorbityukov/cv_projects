import config from 'config';
import { TelegramConfigInterface } from './interfaces';

const TELEGRAM_CONFIG = config.get<TelegramConfigInterface>('telegram');

export const telegramConfiguration = {
  tokenClient: TELEGRAM_CONFIG.tokenClient,
  tokenService: TELEGRAM_CONFIG.tokenService,
  stageTtl: TELEGRAM_CONFIG.stageTtl,
  redisSessionTtl: TELEGRAM_CONFIG.redisSessionTtl,
};
