import { get } from 'env-var';
import { RmqUrl } from '@nestjs/microservices/external/rmq-url.interface';

export class RabbitmqConfig {
  public static readonly RABBIT_MQ_CONTAINER_NAME: string = get('RABBIT_MQ_CONTAINER_NAME')
    .required()
    .asString();

  public static readonly RABBIT_MQ_HOST: string = get('RABBIT_MQ_HOST')
    .required()
    .asString();

  public static readonly RABBIT_MQ_PORT: number = get('RABBIT_MQ_PORT').required().asPortNumber();

  public static readonly RABBIT_MQ_ADMIN_PORT: number = get('RABBIT_MQ_ADMIN_PORT')
    .required()
    .asPortNumber();

  public static readonly RABBITMQ_DEFAULT_USER: string = get('RABBITMQ_DEFAULT_USER')
    .required()
    .asString();

  public static readonly RABBITMQ_DEFAULT_PASS: string = get('RABBITMQ_DEFAULT_PASS')
    .required()
    .asString();

  public static readonly RABBITMQ_ADVERT_QUEUE: string = get('RABBITMQ_ADVERT_QUEUE')
    .required()
    .asString();

  // eslint-disable-next-line max-len
  public static readonly CONNECTION_STRING: string = `amqp://${RabbitmqConfig.RABBIT_MQ_HOST}:${RabbitmqConfig.RABBIT_MQ_PORT}`;
  public static readonly CONNECTION_OBJECT: RmqUrl = {
    protocol: 'amqp',
    hostname: RabbitmqConfig.RABBIT_MQ_HOST,
    port: RabbitmqConfig.RABBIT_MQ_PORT,
    username: RabbitmqConfig.RABBITMQ_DEFAULT_USER,
    password: RabbitmqConfig.RABBITMQ_DEFAULT_PASS,
  };
}
