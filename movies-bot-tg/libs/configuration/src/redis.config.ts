import config from 'config';
import { RedisConfigInterface } from '@app/configuration/interfaces';

const REDIS_CONFIG = config.get<RedisConfigInterface>('redis');

export const redisConfiguration = {
  host: REDIS_CONFIG.host,
  port: REDIS_CONFIG.port,
  flushAll: REDIS_CONFIG.flushAll,
  url: `redis://${REDIS_CONFIG.host}:${REDIS_CONFIG.port}`,
};
