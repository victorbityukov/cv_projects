export enum TableNameEnum {
  USERS = 'users',
  USER_TYPES = 'users_types',
  MOVIES = 'movies',
  MOVIES_CATEGORIES = 'movies_categories',
  CATEGORIES = 'categories',
  MOVIES_LABELS = 'movies_labels',
  LABELS = 'labels',
  STATISTIC = 'statistic',
}
