import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { UserTypeEnum } from '@app/constants';
import { TableNameEnum } from '@app/dbo/enum';
import { UserEntity } from '@app/dbo/user.entity';

@Entity({ name: TableNameEnum.USER_TYPES })
export class UserTypeEntity {
  @OneToMany(() => UserEntity, (user: UserEntity) => user.type)
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'type',
    type: 'enum',
    enum: UserTypeEnum,
    nullable: false,
    unique: true,
  })
  type: UserTypeEnum;
}
