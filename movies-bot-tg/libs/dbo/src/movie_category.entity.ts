import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TableNameEnum } from '@app/dbo/enum';
import { CategoryEntity } from '@app/dbo/category.entity';
import { MovieEntity } from '@app/dbo/movie.entity';

@Entity({ name: TableNameEnum.MOVIES_CATEGORIES })
export class MovieCategoryEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => MovieEntity, (movie: MovieEntity) => movie.movies_categories)
  @JoinColumn({ name: 'movie' })
  @Column({
    name: 'movie',
  })
  movie: MovieEntity;

  @ManyToOne(() => CategoryEntity, (category: CategoryEntity) => category.id)
  @JoinColumn({ name: 'category' })
  @Column({
    name: 'category',
  })
  category: CategoryEntity;
}
