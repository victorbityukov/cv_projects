import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { TableNameEnum } from '@app/dbo/enum';
import { BasicEntity } from '@app/dbo/basic.entity';

@Entity({ name: TableNameEnum.STATISTIC })
export class StatisticEntity extends BasicEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'member',
    type: 'bigint',
    nullable: false,
    unique: false,
  })
  member: number;

  @Column({
    name: 'kicked',
    type: 'bigint',
    nullable: false,
    unique: false,
  })
  kicked: number;
}
