import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TableNameEnum } from '@app/dbo/enum';
import { BasicEntity } from '@app/dbo/basic.entity';
import { UserTypeEntity } from '@app/dbo/user_type.entity';
import { MovieEntity } from '@app/dbo/movie.entity';

@Entity({ name: TableNameEnum.USERS })
export class UserEntity extends BasicEntity {
  @OneToMany(() => MovieEntity, (movie: MovieEntity) => movie.creator)
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'first_name',
    type: 'varchar',
    default: '',
    unique: false,
  })
  first_name: string;

  @Column({
    name: 'last_name',
    type: 'varchar',
    default: '',
    unique: false,
  })
  last_name: string;

  @Column({
    name: 'language_code',
    type: 'varchar',
    default: 'en',
    unique: false,
  })
  language_code: string;

  @Column({
    name: 'username',
    type: 'varchar',
    nullable: false,
  })
  username: string;

  @Index()
  @Column({
    name: 'telegram_id',
    type: 'bigint',
    nullable: false,
    unique: true,
  })
  telegram_id: number;

  @ManyToOne(() => UserTypeEntity, (userType: UserTypeEntity) => userType.id)
  @JoinColumn({ name: 'type' })
  @Column({
    name: 'type',
    type: 'int',
    nullable: false,
    unique: false,
    default: 2,
  })
  type: UserTypeEntity;

  @Column({
    name: 'status',
    type: 'varchar',
    nullable: false,
    unique: false,
    default: 'member',
  })
  status: string;
}
