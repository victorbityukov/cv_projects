import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TableNameEnum } from '@app/dbo/enum';
import { MovieLabelEntity } from '@app/dbo/movie_label.entity';

@Entity({ name: TableNameEnum.LABELS })
export class LabelEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'title',
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  title: string;

  @OneToMany(() => MovieLabelEntity, (movieLabel: MovieLabelEntity) => movieLabel.label)
  movies_labels: MovieLabelEntity[];
}
