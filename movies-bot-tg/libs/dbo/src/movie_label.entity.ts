import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TableNameEnum } from '@app/dbo/enum';
import { MovieEntity } from '@app/dbo/movie.entity';
import { LabelEntity } from '@app/dbo/label.entity';

@Entity({ name: TableNameEnum.MOVIES_LABELS })
export class MovieLabelEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => MovieEntity, (movie: MovieEntity) => movie.movies_labels)
  @JoinColumn({ name: 'movie' })
  @Column({
    name: 'movie',
  })
  movie: MovieEntity;

  @ManyToOne(() => LabelEntity, (label: LabelEntity) => label.id)
  @JoinColumn({ name: 'label' })
  @Column({
    name: 'label',
  })
  label: LabelEntity;
}
