import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TableNameEnum } from '@app/dbo/enum';
import { MovieCategoryEntity } from '@app/dbo/movie_category.entity';

@Entity({ name: TableNameEnum.CATEGORIES })
export class CategoryEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'title',
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  title: string;

  @OneToMany(() => MovieCategoryEntity, (movieCategory: MovieCategoryEntity) => movieCategory.category)
  movies_categories: MovieCategoryEntity[];
}
