import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import { TableNameEnum } from '@app/dbo/enum';
import {
  BasicEntity,
  MovieCategoryEntity,
  MovieLabelEntity,
  UserEntity,
} from '@app/dbo';

@Entity({ name: TableNameEnum.MOVIES })
export class MovieEntity extends BasicEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'type',
    type: 'varchar',
    nullable: false,
    unique: false,
  })
  type: string;

  @Column({
    name: 'title',
    type: 'varchar',
    nullable: false,
    unique: false,
  })
  title: string;

  @Column({
    name: 'content',
    type: 'text',
    nullable: false,
    unique: false,
  })
  content: string;

  @Column({
    name: 'content_full',
    type: 'text',
    nullable: false,
    unique: false,
  })
  content_full: string;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.id)
  @JoinColumn({ name: 'creator' })
  @Column({
    name: 'creator',
    type: 'int',
    nullable: false,
    unique: false,
  })
  creator: number;

  @OneToMany(() => MovieCategoryEntity, (movieCategory: MovieCategoryEntity) => movieCategory.movie)
  movies_categories: MovieCategoryEntity[];

  @OneToMany(() => MovieLabelEntity, (movieLabel: MovieLabelEntity) => movieLabel.movie)
  movies_labels: MovieLabelEntity[];
}
