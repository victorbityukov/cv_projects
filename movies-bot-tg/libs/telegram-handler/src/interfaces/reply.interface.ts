export interface ReplyInterface {
  readonly text: string,
  readonly language: string
}
