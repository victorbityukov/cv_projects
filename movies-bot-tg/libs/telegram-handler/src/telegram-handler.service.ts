import { Injectable } from '@nestjs/common';
import { ReplyInterface } from '@app/telegram-handler/interfaces';

@Injectable()
export class TelegramHandlerService {
  replyWithUserName(greeting: ReplyInterface, user?: string): string {
    return greeting.text.replace('$user', user || 'user');
  }
  replyWithFirstName(greeting: ReplyInterface, first_name: string, username: string = 'Viewer'): string {
    return greeting.text.replace('$firstname', first_name || username);
  }
}
