import { Module } from '@nestjs/common';
import { TelegramHandlerService } from './telegram-handler.service';

@Module({
  providers: [TelegramHandlerService],
  exports: [TelegramHandlerService],
})
export class TelegramHandlerModule {}
