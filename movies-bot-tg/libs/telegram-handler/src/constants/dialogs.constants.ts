import { ReplyInterface } from '@app/telegram-handler';
import { ACTIONS } from '@app/telegram-handler/constants/actions.constants';
import { MAX_LABELS_ON_MOVIE } from '@app/constants';

export const TEXTS_CLIENT = {
  greetingsFirst: {
    language: 'ru',
    text: '$firstname, привет!\n' +
      '\n' +
      '— — —\n' +
      '\n' +
      '17 профессиональных кинокритиков собрали свыше 500 шедевров кино, чтобы тебе было что посмотреть.\n' +
      '\n' +
      '— — —\n' +
      '\n' +
      '— 1 клик до качественного фильма\n' +
      '— обновления каждую пятницу\n' +
      '— вопросы и предложения @vm_man',
  },
  greetingsNext: {
    language: 'ru',
    text: '17 профессиональных кинокритиков собрали свыше 500 шедевров кино, чтобы тебе было что посмотреть.\n' +
      '\n' +
      '— — —\n' +
      '\n' +
      '— 1 клик до качественного фильма\n' +
      '— обновления каждую пятницу\n' +
      '— вопросы и предложения @vm_man'
  },
  selectCategory: {
    language: 'ru',
    text: 'Что хочешь посмотреть? 🤔'
  }
}

export const TEXTS = {
  expectedId: {
    language: 'ru',
    text: 'Ожидался ID'
  },
  greetingsAdmin: {
    language: 'ru',
    text: 'Добро пожаловать, $user. Ваш тип доступа Админ😎'
  },
  mainMenu: {
    language: 'ru',
    text: '🎬'
  },
  inputTitleMovie: {
    language: 'ru',
    text: 'Введите название фильма'
  },
  writeLabelsMovie:  {
    language: 'ru',
    text: `Введите метку одну за раз (Например: «🍺 Под пивко»)\nМаксимальное количество меток ${MAX_LABELS_ON_MOVIE}`
  },
  selectCategoryMovie: {
    language: 'ru',
    text: 'Выбор категории'
  },
  contentMovie: {
    language: 'ru',
    text: 'Напишите контент для поста (кроме меток и тегов, они будут добавлены автоматически)'
  },
  exitFromScene: {
    language: 'ru',
    text: 'Вы вышли из сценария'
  },
  editContentText: {
    language: 'ru',
    text: 'Редактировать контент👆'
  },
  editedMovieText: {
    language: 'ru',
    text: 'Фильм изменен'
  },
  movieNotFoundText: {
    language: 'ru',
    text: 'Фильм не найден'
  },
  moviesListEmptyText: {
    language: 'ru',
    text: 'Список фильмов пустует...'
  },
  categoryAddedText: {
    language: 'ru',
    text: 'Категории добавлены'
  },
  allCategoriesInMovieText: {
    language: 'ru',
    text: 'Вы добавили все категории в данный фильм'
  },
  addedMoreOrReadyText: {
    language: 'ru',
    text: 'Добавьте ещё или нажмите/введите «готово»'
  },
  labelsIsAddedText: {
    language: 'ru',
    text: 'Метки добавлены'
  },
  inputTitleMovieForSearch: {
    language: 'ru',
    text: 'Введите название фильма для поиска'
  },
  inputIdMovieForView: {
    language: 'ru',
    text: 'Введите ID фильма для просмотра'
  },
  inputIdMovieForDelete: {
    language: 'ru',
    text: 'Введите ID фильма для удаления'
  },
};


export const greetings: ReplyInterface[] = [
  {
    text: 'Теперь у вас больше не будет проблем с поиском отличного кино! \n' +
      '\n' +
      '❗️ Свыше 300 качественных рекомендаций от пяти кинокритиков\n' +
      '\n' +
      '❗️ Каждую пятницу пополнение коллекции фильмов!\n' +
      '\n' +
      'Все предложения и отзывы по боту можно написать вот сюда @maxreadman',
    language: 'ru'
  }
];

export const helpInfo: ReplyInterface = {
  language: 'ru',
  text: `/${ACTIONS.LAST_ADDED} - возвращает названия и ID до 50-ти последних добавленных фильмов (аналогично кнопке «Последние добавленные»)\n` +
    '\n' +
    `/${ACTIONS.VIEW_MOVIE} - показывает превью фильма, которое будет видеть и пользователь\n` +
    '\n' +
    `/${ACTIONS.FIND_MOVIE} - возвращает названия добавленных фильмов с их id\n` +
    '\n' +
    `/${ACTIONS.ADD_MOVIE} - добавить новый фильм (аналогично кнопке «Добавить один фильм»)\n` +
    '\n' +
    `/${ACTIONS.ADD_MOVIES} - добавить фильмы партией по шаблону (аналогично кнопке «Добавить фильмы»)\n` +
    '\n' +
    `/${ACTIONS.DELETE_MOVIE} - удалить фильм\n` +
    '\n' +
    `/${ACTIONS.MENU} - показывает главное меню\n` +
    '\n' +
    `/${ACTIONS.HELP} - меню помощи\n` +
    '\n' +
    `/${ACTIONS.STATS} - текущая статистика\n` +
    '\n' +
    `/${ACTIONS.ADVERT} - рассылка (будь осторожен, Фродо)\n` +
    '\n' +
    `/${ACTIONS.EXIT} - прерывает сценарий\n`
};
