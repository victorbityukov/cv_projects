import { ACTIONS, CATEGORIES } from '@app/telegram-handler/constants/actions.constants';

export const buttonAdvert = {
  text: 'Рассылка',
  callback_data: ACTIONS.ADVERT,
};

export const buttonStatistic = {
  text: 'Статистика',
  callback_data: ACTIONS.STATS,
};

export const buttonAddMovie = {
  text: 'Добавить фильм',
  callback_data: ACTIONS.ADD_MOVIE,
};

export const buttonAddMovies = {
  text: 'Добавить фильмы',
  callback_data: ACTIONS.ADD_MOVIES,
};

export const buttonHelp = {
  text: 'Помощь',
  callback_data: ACTIONS.HELP,
};

export const buttonFindMovie = {
  text: 'Найти',
  callback_data: ACTIONS.FIND_MOVIE,
};

export const buttonViewMovie = {
  text: 'Предпросмотр',
  callback_data: ACTIONS.VIEW_MOVIE,
};

export const buttonDeleteMovie = {
  text: 'Удалить',
  callback_data: ACTIONS.DELETE_MOVIE,
};

export const buttonLastAdded = {
  text: 'Последние',
  callback_data: ACTIONS.LAST_ADDED,
};

export const buttonRowExit = [
  {
    text: 'Отменить сценарий',
    callback_data: ACTIONS.EXIT,
  },
];

// Client Menu
const buttonRandomMovie = {
  text: 'Случайный фильм',
  callback_data: ACTIONS.RANDOM_MOVIE,
}

const buttonByCategories = {
  text: 'Выбрать по интересам',
  callback_data: ACTIONS.CATEGORIES_MOVIE,
}

export const buttonRowClientMenu = [
  [buttonRandomMovie],
  [buttonByCategories],
]

// Categories
const buttonDETECTIVE = {
  text: 'Детектив',
  callback_data: CATEGORIES.DETECTIVE,
};

const buttonCOMEDY = {
  text: 'Комедия',
  callback_data: CATEGORIES.COMEDY,
};

const buttonHORRORS = {
  text: 'Ужасы',
  callback_data: CATEGORIES.HORRORS,
};

const buttonTHRILLER = {
  text: 'Боевик',
  callback_data: CATEGORIES.THRILLER,
};

const buttonFANTASTIC = {
  text: 'Фантастика',
  callback_data: CATEGORIES.FANTASTIC,
};

const buttonDRAMA = {
  text: 'Драма',
  callback_data: CATEGORIES.DRAMA,
};

const buttonBack = {
  text: 'Назад',
  callback_data: ACTIONS.BACK,
};

export const buttonRowCategories = [
  [buttonDETECTIVE],
  [buttonCOMEDY],
  [buttonHORRORS],
  [buttonTHRILLER],
  [buttonFANTASTIC],
  [buttonDRAMA],
  [buttonBack],
]

const buttonMore = {
  text: 'Ещё',
  callback_data: ACTIONS.MORE,
}

export const buttonCategories = {
  text: 'К категориям',
  callback_data: ACTIONS.CATEGORIES_MOVIE
};

export const buttonRowMore = [
  [buttonMore],
  [buttonCategories],
]
