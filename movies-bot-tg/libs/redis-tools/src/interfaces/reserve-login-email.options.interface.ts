export interface ReserveLoginEmailOptionsInterface {
  readonly login: string;
  readonly email: string;
  readonly shortCode: string;
}
