export function generateTelegramIdVerifyRedisKey(userId: number): string {
  return `verify_telegram_${userId}`;
}
