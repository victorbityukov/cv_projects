export enum LogLevelEnum {
  INFO = 'info',
  ERROR = 'error',
  WARN = 'warn',
}
