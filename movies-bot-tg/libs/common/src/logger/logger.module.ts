import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { LoggerServiceConfig, RabbitmqConfig } from '@app/configuration';
import { MyLogger } from './logger.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: LoggerServiceConfig.LOGGER_SERVICE_QUEUE,
        transport: Transport.RMQ,
        options: {
          urls: [RabbitmqConfig.CONNECTION_OBJECT],
          queue: LoggerServiceConfig.LOGGER_SERVICE_QUEUE,
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
  ],
  providers: [MyLogger],
  exports: [MyLogger],
})
export class LoggerModule {}
