import { Inject, Injectable } from '@nestjs/common';
import { ConsoleLogger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { LoggerServiceConfig } from '@app/configuration';
import { LogLevelEnum, TypeEventEnum } from '@app/common';

@Injectable()
export class MyLogger extends ConsoleLogger {
  constructor(
    @Inject(LoggerServiceConfig.LOGGER_SERVICE_QUEUE)
    private readonly client: ClientProxy,
  ) {
    super();
    if (!this.context) {
      this.setContext('NestWinston');
    }
  }

  log(message: any, stack?: string, context?: string) {
    // @ts-ignore
    super.log.apply(this, arguments);
    this.client.emit(TypeEventEnum.LOG, {
      service: context || this.context,
      message: message,
      level: LogLevelEnum.INFO,
    });
  }

  error(message: any, stack?: string, context?: string) {
    // @ts-ignore
    super.error.apply(this, arguments);
    this.client.emit(TypeEventEnum.LOG, {
      service: context || this.context,
      message: message,
      level: LogLevelEnum.ERROR,
    });
  }

  warn(message: any, stack?: string, context?: string) {
    // @ts-ignore
    super.warn.apply(this, arguments);
    this.client.emit(TypeEventEnum.LOG, {
      service: context || this.context,
      message: message,
      level: LogLevelEnum.WARN,
    });
  }
}
