import { Repository } from 'typeorm';
import { from, lastValueFrom, mergeMap } from 'rxjs';
import { LabelEntity } from '@app/dbo';

export async function getLabelByTitle(
  repository: Repository<LabelEntity>,
  title: string
): Promise<LabelEntity> {
  const label = await repository.findOne({ title });
  if (!label) {
    const newLabel = repository.create({ title });
    return repository.save(newLabel);
  }
  return label;
}

export async function getLabels(
  repository: Repository<LabelEntity>,
  labels: string[]
): Promise<LabelEntity[]> {
  const labelEntities: LabelEntity[] = [];
  await lastValueFrom(from(labels)
    .pipe(
      mergeMap(async (label) => {
        const labelEntity = await getLabelByTitle(repository, label);
        labelEntities.push(labelEntity);
      }, 1),
    ), { defaultValue: [] });
  return labelEntities;
}
