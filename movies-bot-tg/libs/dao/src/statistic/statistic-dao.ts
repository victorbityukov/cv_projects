import { Repository } from 'typeorm';
import moment from 'moment';
import { StatisticEntity, UserEntity } from '@app/dbo';
import { UserStatusEnum } from '@app/constants';
import { GetCurrentStatisticDto } from '@app/dao/statistic/dto';

export async function getCurrentStats(
  repository: Repository<UserEntity>,
): Promise<GetCurrentStatisticDto> {
  const all = await repository.count();
  const member = await repository.count(
    {
      where: {
        status: UserStatusEnum.MEMBER
      }
    }
  );
  return { all, member };
}

export async function getCommitStats(
  repositoryUsers: Repository<UserEntity>,
  repositoryStatistic: Repository<StatisticEntity>,
): Promise<void> {
  const { all, member } = await getCurrentStats(repositoryUsers);
  const commit = {
    member,
    kicked: all - member,
  }
  await repositoryStatistic.insert(commit);
}

export function drawStatistics(stats: GetCurrentStatisticDto): string {
  const { all, member } = stats;
  return `<b>Пользователи</b>\n` +
    ` - активны: <b>${member}</b>\n` +
    ` - остановлены: <b>${all - member}</b>\n` +
    ` - всего: <b>${all}\n\n</b>` +
    `<em>${moment().format('DD.MM.YYYY HH:mm:ss')}</em>`;
}
