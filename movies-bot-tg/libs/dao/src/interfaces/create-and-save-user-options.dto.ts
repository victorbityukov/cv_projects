import { UserTypeEntity } from '@app/dbo';

export interface CreateAndSaveUserOptionsDto {
  readonly telegram_id: number;
  readonly username?: string;
  readonly type: UserTypeEntity;
}
