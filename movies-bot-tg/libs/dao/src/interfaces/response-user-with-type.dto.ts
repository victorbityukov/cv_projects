export interface ResponseUserWithTypeDto {
  readonly username: string;
  readonly type: string;
}
