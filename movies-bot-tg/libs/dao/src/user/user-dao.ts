import { Repository } from 'typeorm';
import { UserEntity } from '@app/dbo';
import { SetUserStatusDto, UserDto } from '@app/dao/user/dto';
import { UserStatusEnum } from '@app/constants';

export function getUserById(
  repository: Repository<UserEntity>,
  id: number,
): Promise<UserEntity | undefined> {
  return repository.findOne({ telegram_id: id });
}

export function getUserTypeById(
  repository: Repository<UserEntity>,
  id: number,
): Promise<UserEntity | undefined> {
  return repository.findOne({
    where: { telegram_id: id },
    relations: ['type'],
  });
}

export async function checkAccessByType(
  repository: Repository<UserEntity>,
  type: string,
  id: number,
): Promise<boolean> {
  const user = await getUserTypeById(repository, id);

  return (!((!user) || (type !== user.type.type)));
}

export function getUserByTelegramId(
  repository: Repository<UserEntity>,
  id: number,
): Promise<UserEntity | undefined> {
  return repository.findOne({
    where: { id },
  });
}

export async function addNewUser(
  repository: Repository<UserEntity>,
  user: UserDto,
) {
  const {is_bot, id, ...args} = user;
  if (!is_bot) {
    const newUser = {
      first_name: '',
      last_name: '',
      username: '',
      language_code: '',
      telegram_id: id,
      ...args
    };

    await repository.createQueryBuilder()
      .insert()
      .values([newUser])
      .orIgnore(`("telegram_id") DO NOTHING`)
      .execute();
  }
}

export async function setUserStatus(
  repository: Repository<UserEntity>,
  user: SetUserStatusDto,
): Promise<void> {
  const { id, status } = user;
  await repository.update({ telegram_id: id }, { status })
}

export async function getIdsUsers(
  repository: Repository<UserEntity>,
): Promise<number[]> {
  const users = await repository.find({
    select: ['telegram_id'],
    where: {
      status: UserStatusEnum.MEMBER,
    }
  });
  return users.map(user => user.telegram_id);
}
