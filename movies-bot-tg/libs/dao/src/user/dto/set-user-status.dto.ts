export class SetUserStatusDto {
  readonly id: number;
  readonly status: string;
}
