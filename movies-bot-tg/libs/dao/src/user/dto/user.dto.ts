export class UserDto {
  readonly id: number;
  readonly is_bot: boolean;
  readonly username?: string;
  readonly first_name?: string;
  readonly last_name?: string;
  readonly language_code?: string;
}
