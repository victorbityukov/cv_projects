import { Repository } from 'typeorm';
import { from, lastValueFrom, mergeMap } from 'rxjs';
import {
  CategoryEntity,
  LabelEntity,
  MovieCategoryEntity,
  MovieEntity,
  MovieLabelEntity,
} from '@app/dbo';
import { getCategories } from '@app/dao/category';
import { getLabels } from '@app/dao/label';

export async function getRandMovie(
  repository: Repository<MovieEntity>,
): Promise<string> {
  const movie = await repository.createQueryBuilder('movies')
    .orderBy("RANDOM()")
    .getOne();

  if (movie) {
    return movie.content_full;
  }
  return 'В базе фильмов нет';
}

export async function getRandMovieByCategory(
  repository: Repository<MovieEntity>,
  category: string,
): Promise<string> {
  const movie = await repository.createQueryBuilder('movies')
    .leftJoinAndSelect('movies.movies_categories', 'movies_categories')
    .leftJoinAndSelect('movies_categories.category', 'category')
    .orderBy("RANDOM()")
    .where('category.title=:category', { category })
    .getOne();

  if (movie) {
    return movie.content_full;
  }
  return 'В базе фильмов нет';
}

export function findMoviesByTitle(
  repository: Repository<MovieEntity>,
  title: string,
): Promise<MovieEntity[] | undefined> {
  return repository.createQueryBuilder()
    .where("LOWER(title) like LOWER(:title)", { title: `%${title}%` })
    .getMany();
}

export async function addMoviesByTemplate(
  repositoryMovie: Repository<MovieEntity>,
  repositoryLabel: Repository<LabelEntity>,
  repositoryCategory: Repository<CategoryEntity>,
  repositoryMovieCategory: Repository<MovieCategoryEntity>,
  repositoryMovieLabel: Repository<MovieLabelEntity>,
  creator: number,
  movies: string,
): Promise<string> {
  let delimiterSections = '\r\n-----\r\n';
  const delimiterMoviesLinux = '\n&&&&&&&&&&&&&&\n';
  const delimiterMoviesWin = '\r\n&&&&&&&&&&&&&&\r\n';
  let moviesArr = movies.split(delimiterMoviesWin);
  if (moviesArr.length <= 1) {
    moviesArr = movies.split(delimiterMoviesLinux);
    if (moviesArr.length <= 1) {
      return `Фильмы не были добавлены, проблемы с разделителем`;
    }
    delimiterSections = '\n-----\n';
  }
  let moviesAlreadyExists: string = '';
  await lastValueFrom(from(moviesArr)
    .pipe(
      mergeMap(async (movie) => {
        const movieAlreadyExistTemp = await addMovie(
          repositoryMovie,
          repositoryLabel,
          repositoryCategory,
          repositoryMovieCategory,
          repositoryMovieLabel,
          creator,
          movie,
          delimiterSections,
        );
        if (movieAlreadyExistTemp) moviesAlreadyExists += movieAlreadyExistTemp;
      }, 1),
    ), { defaultValue: [] });

  if (moviesAlreadyExists === '') return 'Все фильмы из документа были добавлены';
  return `Фильмы добавлены кроме:\n${moviesAlreadyExists}`;
}

export async function addMovie(
  repositoryMovie: Repository<MovieEntity>,
  repositoryLabel: Repository<LabelEntity>,
  repositoryCategory: Repository<CategoryEntity>,
  repositoryMovieCategory: Repository<MovieCategoryEntity>,
  repositoryMovieLabel: Repository<MovieLabelEntity>,
  creator: number,
  movie: string,
  delimiterSections: string = '\n-----\n',
) {
  const [head, content, labelsStr, hashtagsStr] = movie.split(delimiterSections);
  if (!head || !content || !labelsStr || !hashtagsStr) {
    return `- проблема с форматом фильма ${head} (head, content, labelsStr, hashtagsStr)`;
  }
  const [type, title] = head.split('\n');
  if (!type || !title) {
    return `- проблема с форматом фильма ${head} (type, title)`;
  }
  const labelsArr = labelsStr.split('\n');
  const hashtags = hashtagsStr.split(' ');
  const categoriesWithoutTag = hashtags.map(tag => tag
    .replace('#', '')
    .replace('\n', '')
  );

  const movieExist = await repositoryMovie.findOne({ title });
  if (movieExist) {
    return ` - ${title}\n`;
  }
  const labelsEntities = await getLabels(repositoryLabel, labelsArr);
  const categoriesEntities = await getCategories(repositoryCategory, categoriesWithoutTag);

  if ((!categoriesEntities) || (!labelsEntities)) {
    return ` - ${title}\n`;
  }
  const categoriesArrStr = categoriesEntities.map(category => `#${category.title.toLowerCase()}`)
  const content_full = generateFullContent(type, title, content, labelsArr, categoriesArrStr);

  const movieResult = repositoryMovie.create({
    creator,
    type,
    title,
    content,
    content_full,
  });

  await repositoryMovie.save(movieResult);

  const movieCategories = categoriesEntities.map((category) => ({ movie: movieResult, category }))
  await repositoryMovieCategory.insert(movieCategories);

  const movieLabels = labelsEntities.map((label) => ({ movie: movieResult, label }))

  await repositoryMovieLabel.insert(movieLabels);
}

export async function deleteMovie(
  repositoryMovie: Repository<MovieEntity>,
  repositoryMovieCategory: Repository<MovieCategoryEntity>,
  repositoryMovieLabel: Repository<MovieLabelEntity>,
  id: number,
): Promise<boolean> {
  const movie = await repositoryMovie.findOne({ id });
  if (movie) {
    await repositoryMovieCategory.delete({ movie });
    await repositoryMovieLabel.delete({ movie });
    await repositoryMovie.delete({ id });
    return true;
  }
  return false;
}

export async function getLastAddedMovies(
  repositoryMovie: Repository<MovieEntity>,
): Promise<MovieEntity[] | undefined> {
  return repositoryMovie.find({ take: 50, order: { createdAt: 'DESC' } })
}

export async function getMovieById(
  repositoryMovie: Repository<MovieEntity>,
  id: number
): Promise<string | undefined> {

  const movie = await repositoryMovie.findOne({ id })
  if (movie) {
    return movie.content_full;
  }
}

function generateFullContent(type: string, title: string, content: string, labelsArr: string[], categoriesArr: string[]) {
  return `${type}\n${title}\n\n${content}\n\n${labelsArr.join('\n')}\n\n${categoriesArr.join(' ')}`;
}
