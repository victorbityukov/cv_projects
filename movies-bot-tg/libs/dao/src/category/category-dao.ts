import { Repository } from 'typeorm';
import { CategoryEntity } from '@app/dbo';

export function getCategories(
  repository: Repository<CategoryEntity>,
  categories: string[],
): Promise<CategoryEntity[] | undefined> {
  return repository.createQueryBuilder( 'categories')
    .where('LOWER(categories.title) IN (:...categories)', { categories })
    .getMany();
}
