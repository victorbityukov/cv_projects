import * as https from 'https';
import { IncomingMessage } from 'http';

export const onlyUnique = (list: number[]) => {
  return list.filter((x, index, arr) => arr.indexOf(x) === index);
}

export const fileContentByUrl = (url: string): Promise<string> => {
  return new Promise((resolve, reject) => {
    let client = https;

    client.get(url, (resp: IncomingMessage) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        resolve(data);
      });

    }).on("error", (err) => {
      reject(err);
    });
  });
};